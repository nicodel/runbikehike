# ROADMAP

(#130)

## 0.1 - User, Admin, Sessions, Timeline

A new user can create an account in the application. An existing user can be authenticated on the application.

An Admin can be authenticated on the platform.

An authenticated user can add a session by importing a GPS file or by entering the session data manually.

An authenticated user sees in the application home, the timeline of his sessions.

**Uses cases:**

- [x] User Sign In (#103)
- [x] User Sign Out (#101)
- [x] User Register (#139, #138, #137)
- [x] User Edit Profile (#171)
- [x] Admin Account (#168, #169, #170)
- [x] Admin Disabled User (#171)
- [x] Admin Enabled User (#171)
- [x] Session Add (#104)
- [x] Session Import GPS file (#104)
- [x] Session Display Details (#107)
- [x] Session Edit (#105)
- [x] Session Remove (#155)
- [x] Session Add Comment (#113)
- ~~[ ] Session Edit Comment (#156)~~
- [x] Session Remove Comment (#157)
- ~~[ ] Session Add Image (#154)~~ postponed to undefined
- ~~[ ] Session Remove Image (#159)~~ postponed to undefined
- [x] Timeline Display Sessions (#112)

## 0.2 - Social

Users can interact between each other’s.

A user can request to an another user to be his friend. For a friendship to be active both users' needs to approve it. An active friendship can be revoked by any of the concerned users.

A user can see a friend's session in his timeline.

A user can open a friend session's details.

A user can comment a friend's session.

**Uses cases:**

- Social Search Users (#160)
- Social Invite User to be a Friend (#161)
- Social Accept User as Friend (#162)
- Social Refuse User as Friend (#162)
- Timeline Display Friend's sessions (#163)
- Session Display Friend's Session Details (#164)
- Session Comments Friend's Sessions (#165)
- Session Friend Adds Comment
- ~~Session Friend Edits its Comment~~
- Session Friend Removes its Comment

## 0.3 - Records, Objectives, Challenges, Rewards, Reports

A user can see his records per activity.

A user can add an objective for him to target.

A user can create a friendship challenge and propose it to his friends. A challenge can be either accepted or refused by a friend.

A user can create an open challenge, which can be accepted by any user.

A user can see reports based on his sessions.

**Uses cases:**

- Records Display List
- Record Display Details
- Session Beat New Record
- Record Accept New Record
- Reward Created by New Record
- Objective Add
- Objectives Display List
- Objective Display Details
- Session Update Objective
- Objective Completed by User
- Objective Ended
- Timeline Display Objective Start
- Timeline Display Objective Completion
- Reward Created by Objective Completion
- Challenge Add
- Challenge Edit
- Challenge Remove
- Challenge Proposes to Friends
- Challenge Accept
- Challenge Refuse
- Challenge Display Details
- Challenge Add Comment
- Challenge Edit Comment
- Challenge Remove Comment
- Session Update Challenge
- Challenge Completed by User
- Reward Created by Challenge Completion
- Challenge Ended
- Social Search Challenges
- Timeline Display Challenge Start
- Timeline Display Challenge Ended
- Timeline Display Challenge Completion
- Reports Display Standard Reports

## 0.4 - Fediverse

Application instances can interact using Fediverse network (ActivityPub protocols).

Friendship can be established between users of 2 different instances.

Challenges can be searched and accepted through Fediverse network.

**Uses cases:**

- Fediverse:
  - Social Search Users
  - Social Invite User to be a Friend
  - Social Accept User as Friend
  - Social Refuse User as Friend
  - Timeline Display Friend's sessions
  - Session Display Friend's Session Details
  - Session Comments Friend's Sessions
  - Challenge Proposes to Friends
  - Challenge Accept
  - Challenge Refuse
  - Challenge Display Details
  - Session Update Challenge
  - Social Search Challenges
