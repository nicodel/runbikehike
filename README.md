# Run, Bike, Hike...

# Installing

[Démo on Render](https://runbikehike.onrender.com)

(be patient, it is powered on a free account)

# Contributing

This section describes how contribution can be done to this application. Note that many
contributions are valuable and would be glady accepted in all of the following
domains: design, user interface, user experience, translations, documentation,
tutorials. The only limit is the imagination!

## Code contributing

These rules and recommendations can change over time, and should change. If you
have any ideas for improving them, please file an issue or open a merge
request!

- Branch off `main`.
- One feature per commit.
- In case of requests for changes, amend your commit.

## Workflow

Work is done on specific branches created from `main` branch. Each specific branch should be linked to a ticket and have a name reflecting the work that it is done in it (ex: `139-create-user-profile`).

Once work is done, a merge request can be made from this specific branch to the `main` branch.

For each `merge request` submitted to this repository and for whatever branch, it triggers the pipeline stage `verify` tasks (`verify:lint`, `verify:test`, `verify:coverage_server` and `verify:coverage_client`).

For each merge request successfully merged, a entry needs to be made in the `CHANGELOG.md` file.

When a new version is ready to be publish. `CHANGELOG.md` file is updated acordingly. A `git tag` is added, and then pushed. This tag needs to follow [Semantic Versioning 2.0.0](https://semver.org/). It triggers stages `build` task `build:build_prod` and `publish` tasks `publish:container_prod`, `publish:package_prod` and `publish=release_prod`.

```bash
[main] $ git commit -m 'v0.1.22'
[main] $ git tag v0.1.22
[main] $ git push origin v0.1.22
[main] $ git push
```

**The `v` is important because it is used in pipelines to trigger builds.**

### How to hack

#### Development environment

This environnement is based on a Debian Vagrant machine, hosting a NodeJs runtime. All the application components are ran inside this same machine.

- Start the Vagrant machine: `yarn start:vm:dev`
- Install the application's dependencies: `yarn install`

A few NPM scripts are available to help you on your hacking:

- Check code for errors: `yarn lint`.
- Launch full sets of tests: `yarn run test`
- Compile source code: `yarn build:dev`
- Launch application: `yarn dev` (if any client files are changed, it recompiles automatically)
- Application will be accessible at `http://localhost:3000`

#### Qualification environment

This environnement is based on a Debian Vagrant machine, hosting a Docker runtime. Application components are launched in Docker containers.

- Start the Vagrant machine: `yarn start:vm:qual`
- Build the docker image: `docker build -t runbikehike:latest -f Dockerfile-qual .`
- Launch the application: `docker compose -f docker-compose.qual.yml up -d`
- Application will be accessible at `http://localhost:3000`

Some debugging commands:

- Get logs from container: `docker container logs project-app-1`
- Stop all containers: `docker compose -f docker-compose.qual.yml down`

#### Production environment

This environnement needs a Docker runtime. Application components are launched in Docker containers.

- Build the docker image: `docker build -t runbikehike:latest -f Dockerfile-prod .`
- Launch the application: `docker compose -f docker-compose.prod.yml up -d`
- Application will be accessible at `http://localhost:3000`

### About `package.json` file

We use the `package.json` file in a reproducible way, specifying the exact version to use. Please make sure all version numbers are **exact** in `package.json`, thus using no version ranges specifiers like `~`, `>` etc.

### About branches

- `master` contains all changes in the current development version, including some experimental features that could break in production.

### How to contribute

- Please note that not every feature can make it into this, because new features add a lot of complexity and make it harder to maintain the code.
- If you're thinking about a new feature, see if there's already an issue open about it, or open one otherwise. This will ensure that everybody is on track for the feature and willing to have it in this application.
- One commit per feature.
- Branch off the `master` branch. Rebase as often as possible.
- Format your code with `yarn pretty`.
- Lint your code with `yarn lint`.
- Test your code with `yarn test`.
- Ideally, your merge request should be mergeable without any merge commit, that is, it should be a fast-forward merge. For this to happen, your code needs to be always rebased onto `master`. Again, this is something nice to have that we expect from recurring contributors, but not a big deal if you don't do it otherwise.

## Localization contributing

## Versionning

This versionning is fully based on [Semantic Versioning 2.0.0](https://semver.org/).

Given a version number MAJOR.MINOR.PATCH, increment the:

- MAJOR version when you make incompatible API changes,
- MINOR version when you add functionality in a backwards compatible manner, and
- PATCH version when you make backwards compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.

Major version X (X.y.z | X > 0) MUST be incremented if any backwards incompatible changes are introduced to the public API. It MAY also include minor and patch level changes. Patch and minor version MUST be reset to 0 when major version is incremented.

Minor version Y (x.Y.z | x > 0) MUST be incremented if new, backwards compatible functionality is introduced to the public API. It MUST be incremented if any public API functionality is marked as deprecated. It MAY be incremented if substantial new functionality or improvements are introduced within the private code. It MAY include patch level changes. Patch version MUST be reset to 0 when minor version is incremented.

Patch version Z (x.y.Z | x > 0) MUST be incremented if only backwards compatible bug fixes are introduced. A bug fix is defined as an internal change that fixes incorrect behavior.

A pre-release version MAY be denoted by appending a hyphen and a series of dot separated identifiers immediately following the patch version. Identifiers MUST comprise only ASCII alphanumerics and hyphen [0-9A-Za-z-]. Identifiers MUST NOT be empty. Numeric identifiers MUST NOT include leading zeroes. Pre-release versions have a lower precedence than the associated normal version. A pre-release version indicates that the version is unstable and might not satisfy the intended compatibility requirements as denoted by its associated normal version.

Build metadata MAY be denoted by appending a plus sign and a series of dot separated identifiers immediately following the patch or pre-release version. Identifiers MUST comprise only ASCII alphanumerics and hyphen [0-9A-Za-z-]. Identifiers MUST NOT be empty. Build metadata MUST be ignored when determining version precedence. Thus two versions that differ only in the build metadata, have the same precedence.

### Working on a patch / minor or major version update (replace `patch`, by the corresponding)

While working on a patch, we update version inside `package.json`, without changing the git tags.

First to create new patch

```
$ yarn version --no-git-tag-version --prepatch --preid=beta
```

Then to work on new patch

```
$ yarn version --no-git-tag-version --prerelease --preid=beta
```

Once the pacth is done and ready to be merged, we update both package.json`, and git tags.

```
$ yarn version --patch
```
