# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] • te be defined

### Added

- Adding a basic About page, to display current instance version (#73).
- Adding a UserSearch use case, and display results dedicated view (#160).

### Changed

- Upgrade to Node.JS 20.x
- Secure the Redis database and connectivity (#123).

## [0.1.25] • 2024-03-08

### Added

- Admin has a dedicated view to see users status, and enable/disable them (#171).

## [0.1.24] • 2024-02-26

### Changed

- New cleaner UI (#173).

## [0.1.23] • 2024-01-25

### Added

- User can add comment to a session (#113).
- User can remove its own comment (#157).
- Admin account created at start up, if no admin found in database. Credentials to be provided in .env file. (#168)
- When admin account sign in, the UI switch to redish colors, as a warning. (#169)
- TODO: New features details. TO BE SUMMARIZED
- A User can have a status "disabled" (#171).
- Admin can retreive the list of all users (#172). # SERVER ONLY - TO BE IMPLEMENTED ON CLIENT
- Admin can disable and enable a user (#171). # SERVER ONLY - TO BE IMPLEMENTED ON CLIENT

### Changed

- New use case to retreive a session charts data to display on client side (#113).
- New use case to retreive a session map data to display on client side (#113).
- New use case to retreive session's metadata to display on client side (#113).
- New use case to retreive session's images to display on client side (#113).
- New use case to retreive session's comments to display on client side (#113).
- Use case DeleteSession, to allow Admin user to delete any sesison (#170).

### Fixed

- Every API requests check if user is authenticated, and if he is not, answer with a 401 error code. Client side, when receiving a 401 error code, redirects to /sign_in. (#166)

## [0.1.22] • 2023-11-23

### Added

- When a new user register a default user profile is created. When this user first sign in, she/he is invited to fill the user profile page. Once filled, she/he can upload it, and it is saved onto the server side (#139, #138, #137).
- Adding a Session from client side (through manual values, or imported GPS file) (#104).
- Displaying a Session details (#107).
- Displaying users sessions within a Timeline, in Home (#112).
- User can remove a Session (#155).
- User can edit a Session (#105).

## [0.1.21] • 2023-04-13

- Complete reformat of the code, in order to have only one code source for both server and client (#126).

## [0.1.20] • 2020-02-02

- #75: Error when records are being updated.

[0.1.25]: https://gitlab.com/nicodel/runbikehike/-/releases/0.1.25
[0.1.24]: https://gitlab.com/nicodel/runbikehike/-/releases/0.1.24-fix
[0.1.23]: https://gitlab.com/nicodel/runbikehike/-/releases/0.1.23
[0.1.22]: https://gitlab.com/nicodel/runbikehike/-/releases/v0.1.22
[0.1.21]: https://gitlab.com/nicodel/runbikehike/-/releases/0.1.21
[0.1.20]: https://gitlab.com/nicodel/runbikehike/-/releases/0.1.20

<!--
Release template
## [x.x.x] • YYYY-MM-DD

### Added
### Changed
### Deprecated
### Removed
### Fixed
### Removed
-->
