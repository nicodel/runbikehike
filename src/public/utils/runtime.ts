// Thanks: https://medium.com/better-programming/reactive-vue-routes-with-the-composition-api-18c1abd878d1
import { ComponentPublicInstance, getCurrentInstance } from 'vue';

export const getRuntimeVM = (): ComponentPublicInstance => {
  const vm = getCurrentInstance();
  if (vm) {
    if (vm.proxy) {
      return vm.proxy;
    }
  }
  throw new ReferenceError('[vue-hooks] Not found vue instance.');
};
