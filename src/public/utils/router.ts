// Thanks: https://medium.com/better-programming/reactive-vue-routes-with-the-composition-api-18c1abd878d1
import { reactive, toRefs, watch } from 'vue';
import { getRuntimeVM } from './runtime';

export const useRouter = () => {
  const vm = getRuntimeVM();

  const state = reactive({
    route: vm.$route,
  });

  watch(
    () => vm.$route,
    (r) => {
      state.route = r;
    }
  );

  return { ...toRefs(state), router: vm.$router };
};

export const _ = null;
