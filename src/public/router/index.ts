import { createRouter, createWebHistory } from 'vue-router';

import { useUserProfileStore } from '../stores/user.profile';
import { useAuthUserStore } from '../stores/auth.user';

import { apiService } from '../services';

const routes = [
  {
    path: '/',
    component: () => import('../components/content/timeline/Timeline.vue'),
    meta: { requiresAuth: true },
  },

  {
    path: '/sign_in',
    component: () => import('../components/auth/SignIn.vue'),
    meta: { requiresAuth: false },
  },
  {
    path: '/sign_out',
    component: () => import('../components/auth/SignOut.vue'),
    meta: { requiresAuth: false },
  },
  {
    path: '/register',
    component: () => import('../components/auth/Register.vue'),
    meta: { requiresAuth: false },
  },

  {
    path: '/profile',
    component: () => import('../components/content/profile/UserProfile.vue'),
    meta: { requiresAuth: true },
  },

  {
    path: '/profile/edit',
    component: () => import('../components/content/profile/EditUserProfile.vue'),
    meta: { requiresAuth: true },
  },

  {
    path: '/settings',
    component: () => import('../components/content/Settings.vue'),
    meta: { requiresAuth: true },
  },

  {
    path: '/about',
    component: () => import('../components/content/About.vue'),
    meta: { requiresAuth: true },
  },

  {
    path: '/add_session',
    component: () => import('../components/content/session/AddSession.vue'),
    meta: { requiresAuth: true },
  },
  {
    path: '/session/:id',
    component: () => import('../components/content/session/SessionDetails.vue'),
    props: true,
    meta: { requiresAuth: true },
  },
  {
    path: '/session/edit/:id',
    component: () => import('../components/content/session/EditSession.vue'),
    props: true,
    meta: { requiresAuth: true },
  },
  {
    path: '/session/comments/:id',
    component: () => import('../components/content/session/SessionComments.vue'),
    props: true,
    meta: { requiresAuth: true },
  },

  {
    path: '/records',
    component: () => import('../components/content/Records.vue'),
    meta: { requiresAuth: true },
  },
  {
    path: '/reports',
    component: () => import('../components/content/Reports.vue'),
    meta: { requiresAuth: true },
  },
  {
    path: '/objectives',
    component: () => import('../components/content/objective/Objectives.vue'),
    meta: { requiresAuth: true },
  },
  {
    path: '/add_objective',
    component: () => import('../components/content/objective/AddObjective.vue'),
    meta: { requiresAuth: true },
  },
  {
    path: '/challenges',
    component: () => import('../components/content/challenge/Challenges.vue'),
    meta: { requiresAuth: true },
  },
  {
    path: '/add_challenge',
    component: () => import('../components/content/challenge/AddChallenge.vue'),
    meta: { requiresAuth: true },
  },

  {
    path: '/search_users',
    component: () => import('../components/content/social/SearchUsers.vue'),
    meta: { requiresAuth: true },
  },

  /*  */
  {
    path: '/admin',
    component: () => import('../components/content/Admin.vue'),
    meta: { requiresAuth: true },
  },

  {
    path: '/:pathMatch(.*)*',
    redirect: '/',
    meta: { requiresAuth: true },
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
router.beforeEach(async (to, from, next) => {
  console.debug('[Router/beforeEach] ');
  const userProfileStore = useUserProfileStore();
  const authUserStore = useAuthUserStore();

  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);
  console.debug('[Router/beforeEach] to.fullPath: %s requiresAuth: %s', to.fullPath, requiresAuth);

  if (to.fullPath === '/sign_in' && !authUserStore.isAuthenticated) {
    console.debug('[Router/beforeEach] %s', to.fullPath);
    next();
  } else {
    console.debug(
      '[Router/beforeEach] userProfileStore.isProfileStored %s',
      userProfileStore.isProfileStored
    );
    if (requiresAuth && !userProfileStore.isProfileStored) {
      console.debug('[Router/beforeEach] No profile stored for authenticated user');
      try {
        console.debug('[Router/beforeEach] Loading Profile for authenticated user');
        const profile = await apiService.loadUserProfile();
        console.debug('[Router/beforeEach] Profile loaded for authenticated user: %', profile);
        userProfileStore.$patch({
          id: profile.user_id,
          email: profile.email,
          name: profile.name,
          gender: profile.gender,
          birthyear: profile.birthyear,
          height: profile.height,
          weight: profile.weight,
          location: profile.location,
          biography: profile.biography,
          avatarDTO: {
            mimetype: profile.avatar ? profile.avatar.mimetype : '',
            data: profile.avatar ? profile.avatar.data : '',
          },
          is_admin: profile.is_admin,
        });
        if (profile.avatar) userProfileStore.convertBase64ToURL();
        next();
      } catch (err) {
        console.error('[Router/beforeEach] Error when loading UserProfile: %s', err);
        next('/sign_in');
      }
    } else {
      console.debug('[Router/beforeEach] Going next()');
      next();
    }
  }
});

export default router;
