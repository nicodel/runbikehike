export class ImageMap {
  public static base64ToURL(image: { data: string; mimetype: string }): string {
    const imageBinaryString = atob(image.data);
    // console.debug(
    //   '[ImageMap/base64ToURL] onBeforeMount - imageBinaryString: %s',
    //   imageBinaryString
    // );

    const imageBytes = new Uint8Array(imageBinaryString.length);
    // console.debug('[ImageMap/base64ToURL] onBeforeMount - imageBytes: %s', imageBytes);
    for (let i = 0; i < imageBinaryString.length; i++) {
      imageBytes[i] = imageBinaryString.charCodeAt(i);
    }
    const imageBlob = new Blob([imageBytes], { type: image.mimetype });

    return URL.createObjectURL(imageBlob);
  }
}
