import { DateTime } from 'luxon';

const locale = 'fr';

export class DateTimeMap {
  public static toDate(date_time) {
    return DateTime.fromISO(date_time).setLocale(locale).toLocaleString(DateTime.DATE_FULL);
  }

  public static toISODate(date_time) {
    return DateTime.fromISO(date_time).setLocale(locale).toISODate();
  }

  public static toTime(date_time) {
    console.debug('[Mappers/DateTime] toTime: %s', DateTime.fromISO(date_time).zoneName);
    return DateTime.fromISO(date_time).setLocale(locale).toLocaleString(DateTime.TIME_24_SIMPLE);
  }

  public static toNiceDateTime(date_time) {
    return DateTime.fromISO(date_time).setLocale(locale).toLocaleString(DateTime.DATETIME_MED);
  }
}
