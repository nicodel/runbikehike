import { describe, it, expect } from 'vitest';
import { DateTimeMap } from './dateTimeMap';

const date_time = '2023-10-27T06:15:15';

describe('Mappers / DateTimeMap', () => {
  it('toDate', () => {
    const date = DateTimeMap.toDate(date_time);

    expect(date).toEqual('27 octobre 2023');
  });

  it('toTime', () => {
    const time = DateTimeMap.toTime(date_time);

    expect(time).toEqual('06:15');
  });
});
