import { describe, it, expect } from 'vitest';
import { SpeedMap } from './speedMap';

describe('Mappers / SpeedMap', () => {
  it('toNiceRunnigMetricPace seconds less than 10', () => {
    const nice = SpeedMap.toNiceRunnigMetricPace(2.7777777777778);

    expect(nice).toEqual('6:00 /km');
  });

  it('toNiceRunnigMetricPace seconds over 10', () => {
    const nice = SpeedMap.toNiceRunnigMetricPace(3.030303030303);

    expect(nice).toEqual('5:30 /km');
  });

  it('toNiceSwimmingMetricPace seconds less than 10', () => {
    const nice = SpeedMap.toNiceSwimmingMetricPace(1.1111111111111);

    expect(nice).toEqual('1:30 /100m');
  });

  it('toNiceSwimmingMetricPace seconds over 10', () => {
    const nice = SpeedMap.toNiceSwimmingMetricPace(1.53846153846154);

    expect(nice).toEqual('1:05 /100m');
  });
});
