import { Duration } from 'luxon';

export class SpeedMap {
  public static toNiceRunnigMetricPace(speed: number): string {
    const object = Duration.fromObject({
      minutes: 0,
      seconds: this.m_sec_to_sec_km(speed),
    })
      .normalize()
      .toObject();
    console.debug(
      '[Mappers/Speed] toNiceRunnigMetricPace: %s',
      `${object.minutes}:${
        (object.seconds as number) < 10 ? '0' + object.seconds : object.seconds
      } /km`
    );
    return `${object.minutes}:${
      (object.seconds as number) < 10 ? '0' + object.seconds : object.seconds
    } /km`;
  }

  public static toNiceSwimmingMetricPace(speed: number): string {
    const object = Duration.fromObject({
      minutes: 0,
      seconds: this.m_sec_to_sec_100m(speed),
    })
      .normalize()
      .toObject();
    console.debug(
      '[Mappers/Speed] toNiceSwimmingMetricPace: %s',
      `${object.minutes}:${
        (object.seconds as number) < 10 ? '0' + object.seconds : object.seconds
      } /100m`
    );
    return `${object.minutes}:${
      (object.seconds as number) < 10 ? '0' + object.seconds : object.seconds
    } /100m`;
  }

  public static toNiceMetricSpeed(speed) {
    console.debug('[Mappers/Speed] toNiceMetricSpeed: %s', `${this.m_sec_to_km_hour(speed)} km/h`);
    return `${this.m_sec_to_km_hour(speed)} km/h`;
  }

  private static m_sec_to_sec_km(ms: number): number {
    return parseInt((1000 / ms).toFixed());
  }

  private static m_sec_to_sec_100m(ms: number): number {
    return parseInt((100 / ms).toFixed());
  }

  private static m_sec_to_km_hour(ms: number): number {
    return parseFloat(((ms * 60 * 60) / 1000).toFixed(2));
  }
}
