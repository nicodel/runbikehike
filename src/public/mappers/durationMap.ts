import { Duration } from 'luxon';

export class DurationMap {
  public static toNice(duration: number): string {
    const object = Duration.fromObject({ hours: 0, minutes: 0, seconds: duration })
      .normalize()
      .toObject();
    return `${object.hours === 0 ? '' : object.hours + ':'}${
      object.minutes === 0 ? '' : object.minutes
    }${object.seconds === 0 ? '' : ':' + object.seconds}`;
  }
}
