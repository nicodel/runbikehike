import { describe, it, expect } from 'vitest';
import { DurationMap } from './durationMap';

describe('Mappers / DurationMap', () => {
  it('toNice h min s', () => {
    const nice = DurationMap.toNice(3 * 60 * 60 + 30 * 60 + 15);

    expect(nice).toEqual('3:30:15');
  });

  it('toNice min s', () => {
    const nice = DurationMap.toNice(30 * 60 + 15);

    expect(nice).toEqual('30:15');
  });

  it('toNice min s', () => {
    const nice = DurationMap.toNice(15);

    expect(nice).toEqual(':15');
  });
});
