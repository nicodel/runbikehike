import { describe, it, expect } from 'vitest';
import { DistanceMap } from './distanceMap';

describe('Mappers / Distance', () => {
  it('toNiceMeters less than 3000', () => {
    const nice = DistanceMap.toNiceMetrics(2500);
    expect(nice.value).toEqual(2500);
    expect(nice.unit).toEqual('m');
  });

  it('toNiceMeters over 3000', () => {
    const nice = DistanceMap.toNiceMetrics(3500);
    expect(nice.value).toEqual(3.5);
    expect(nice.unit).toEqual('km');
  });
});
