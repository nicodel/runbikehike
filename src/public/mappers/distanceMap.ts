interface IDistance {
  value: number;
  unit: string;
}

export class DistanceMap {
  public static toNiceMetrics(distance: number): IDistance {
    // display in kilometers over 3 000 m
    return distance < 3000
      ? { value: distance, unit: 'm' }
      : { value: distance / 1000, unit: 'km' };
  }
}
