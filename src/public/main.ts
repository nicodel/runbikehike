import { createApp } from 'vue';
import { registerPlugins } from './plugins';
import App from './App.vue';

import './assets/css/font.css';

import 'leaflet/dist/leaflet.css';

const app = createApp(App);

registerPlugins(app);

app.mount('#app');
