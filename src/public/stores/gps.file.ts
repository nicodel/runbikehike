import { defineStore } from 'pinia';

interface State {
  extension: string;
  data: string;
  file: Blob | null;
}

export const useGPSFileStore = defineStore('GPSFile', {
  state: (): State => {
    return { extension: '', data: '', file: null };
  },

  getters: {
    toFormData: (state) => {
      const formData = new FormData();
      formData.append('extension', state.extension);
      formData.append('data', state.data);

      for (const pair of formData.entries()) {
        console.debug('FILE formData', pair);
      }

      return formData;
    },

    toDTO: (state) => {
      return {
        extension: state.extension,
        data: state.data,
      };
    },
  },

  actions: {
    setExtension(input_extension) {
      this.extension = input_extension;
    },
    setFileFromFileReader(input_file) {
      console.debug('[Store/GPSFile] setFileFromFileReader - input_file', input_file);
    },
  },

  // actions: {
  //   setFileFromInput(input_file) {
  //     parseString(input_file, (err, res) => {
  //       if (err) {
  //         console.error('[Store/GPSFile] setFileFromInput() - parserString error', err);
  //       }
  //       console.debug('parserString result', typeof res, res);
  //       this.data = JSON.stringify(res);
  //     });
  //   },
  // },
});
