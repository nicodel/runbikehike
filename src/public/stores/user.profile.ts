import { defineStore } from 'pinia';

interface avatarDTO {
  mimetype: string;
  data: string;
}

interface profileDTO {
  name: string;
  email: string;
  is_admin: boolean;
  gender: string;
  birthyear: string;
  height: string;
  weight: string;
  location?: string;
  biography?: string;
  avatar?: avatarDTO;
}

interface State {
  name: string;
  gender: string;
  birthyear: number;
  height: number;
  weight: number;
  location: string | null;
  biography: string | null;
  email: string;
  id: string;
  avatarURL: string;
  avatarDTO: avatarDTO;
  avatarBase64: string;
  is_admin: boolean;
}

export const useUserProfileStore = defineStore('userProfile', {
  state: (): State => {
    return {
      name: '',
      gender: '',
      birthyear: 0,
      height: 0,
      weight: 0,
      location: null,
      biography: null,
      email: '',
      id: '',
      avatarURL: '',
      avatarDTO: {
        mimetype: '',
        data: '',
      },
      avatarBase64: '',
      is_admin: false,
    };
  },

  getters: {
    isProfileStored: (state) => {
      if (
        state.name === '' ||
        state.gender === '' ||
        state.birthyear === 0 ||
        state.height === 0 ||
        state.weight === 0 ||
        state.email === '' ||
        state.id === ''
      ) {
        return false;
      } else {
        return true;
      }
    },

    isAdmin: (state) => {
      return state.is_admin;
    },

    getProfile: (state) => {
      return state;
    },

    getProfileDTO: (state): profileDTO => {
      return {
        name: state.name,
        email: state.email,
        gender: state.gender,
        birthyear: state.birthyear.toString(),
        height: state.height.toString(),
        weight: state.weight.toString(),
        location: state.location ? state.location : '',
        biography: state.biography ? state.biography : '',
        avatar: state.avatarDTO,
        is_admin: state.is_admin,
      };
    },

    getUserId: (state): string => {
      return state.id;
    },
  },

  actions: {
    convertBase64ToURL() {
      const avatarBinaryString = atob(this.avatarDTO.data);

      const avatarBytes = new Uint8Array(avatarBinaryString.length);
      for (let i = 0; i < avatarBinaryString.length; i++) {
        avatarBytes[i] = avatarBinaryString.charCodeAt(i);
      }
      const avatarBlob = new Blob([avatarBytes], { type: this.avatarDTO.mimetype });

      this.avatarURL = URL.createObjectURL(avatarBlob);
    },
  },
  // persist: true,
});
