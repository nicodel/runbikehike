import { defineStore } from 'pinia';

import { apiService } from '../services/index';

interface UserState {
  id: string;
  name: string;
  avatar: {
    mimetype: string;
    data: string;
  };
}

interface UsersState {
  users: Map<string, UserState>;
}

export const useUsersStore = defineStore('users', {
  state: (): UsersState => {
    return {
      users: new Map(),
    };
  },

  // getters: {
  //   getUserById: (state) => {
  //     return (id) => state.users.get(id);
  //   },
  // },

  actions: {
    async getUser(id: string): Promise<UserState> {
      const user = this.users.get(id);
      if (!user) {
        console.debug('[Stores/Users] Actions getUser - User %s missing from Store', id);
        const new_user = await apiService.getUserNameAndAvatar(id);
        this.users.set(new_user.id, new_user);
        console.debug(
          `[Stores/Users] Actions getUser - User added to Store: ${JSON.stringify(new_user)}`
        );
      }
      return this.users.get(id);
    },

    addUser(user: UserState) {
      this.users.set(user.id, user);
    },

    deleteUser(id: string) {
      this.users.delete(id);
    },
  },
});
