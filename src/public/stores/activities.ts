import { defineStore } from 'pinia';

import { ACTIVITIES, ActivityNames } from './constants';

interface Activity {
  name: ActivityNames;
  label: string;
  icon: string;
  use_distance: boolean;
}

interface ActivityState {
  activities: Activity[];
}

export const useActivitiesStore = defineStore('activities', {
  state: (): ActivityState => ({
    activities: ACTIVITIES,
  }),

  getters: {
    getActivities: (state) => {
      return state.activities;
    },

    getActivitiesListForDisplay: (state) => {
      const list: { title: string; value: number; props: { prependIcon: string } }[] = [];
      for (let index = 0; index < state.activities.length; index++) {
        const activity = state.activities[index];
        list.push({ title: activity.label, value: index, props: { prependIcon: activity.icon } });
      }
      return list;
    },

    getLabels: (state) => {
      const labels: string[] = [];
      for (let index = 0; index < state.activities.length; index++) {
        const activity = state.activities[index];
        labels.push(activity.label);
      }
      return labels;
    },

    getNameFromLabel: (state) => {
      return (label: string): string => {
        const activity = state.activities.find((act) => act.label === label);
        return activity ? activity.name : '';
      };
    },

    getLabelFromName: (state) => {
      return (name: string): string => {
        const activity = state.activities.find((act) => act.name === name);
        return activity ? activity.label : '';
      };
    },

    getIconFromName: (state) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      return (name: string): any => {
        const activity = state.activities.find((act) => act.name === name.toLowerCase());
        return activity ? activity.icon : '';
      };
    },
  },
});
