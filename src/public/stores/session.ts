/* eslint-disable @typescript-eslint/no-explicit-any */
import { defineStore } from 'pinia';
import { useActivitiesStore } from './activities';
import { useUsersStore } from './users';

import { SessionMetadataDTO, SessionsDTO } from './../../app/dtos/sessionsDTO';
import { DateTimeMap } from '../mappers/dateTimeMap';
import { ImageMap } from '../mappers/imageMap';

interface State {
  activity_name: string;
  average_heart_rate: number | null;
  average_speed: number | null;
  burned_calories: number;
  charts_data: any | null;
  comments: {
    id: string;
    text: string;
    date_time: string;
    user_name: string;
    user_avatar: string;
  }[];
  description: string | null;
  distance: number | null;
  duration: number;
  elevation_gain: number | null;
  gps_charts: any | null;
  gps_track: any | null;
  has_images: boolean;
  has_track: boolean;
  id: string;
  images_ids: string[];
  is_favorite: boolean;
  location: string | null;
  map_data: any | null;
  name: string;
  nb_comments: number;
  nb_likes: number;
  start_date: string;
  start_time: string;
  user_id: string;
  user_name: string;
  user_avatar: string;
}

export const useSessionStore = defineStore('session', {
  state: (): State => {
    return {
      activity_name: '',
      average_heart_rate: null,
      average_speed: null,
      burned_calories: 0,
      charts_data: null,
      comments: [],
      description: null,
      distance: null,
      duration: 0,
      elevation_gain: null,
      gps_charts: null,
      gps_track: null,
      has_images: false,
      has_track: false,
      id: '',
      images_ids: [],
      is_favorite: false,
      location: null,
      map_data: null,
      name: '',
      nb_comments: 0,
      nb_likes: 0,
      start_date: '',
      start_time: '',
      user_id: '',
      user_name: '',
      user_avatar: '',
    };
  },

  getters: {
    getSession: (state) => {
      return state;
    },

    getId: (state) => {
      return state.id;
    },

    getStartDateTime: (state) => {
      if (state.start_date && state.start_time) {
        const start_date_time = new Date(state.start_date);
        start_date_time.setHours(
          parseInt(state.start_time.split(':')[0]),
          parseInt(state.start_time.split(':')[1])
        );
        return start_date_time.toISOString();
      }
    },

    getSessionDTO(state): SessionsDTO {
      const activitiesStore = useActivitiesStore();

      const session = {
        activity_name: activitiesStore.getNameFromLabel(state.activity_name),
        average_heart_rate: state.average_heart_rate,
        average_speed: state.average_speed,
        burned_calories: state.burned_calories,
        comments_ids: [],
        description: state.description,
        distance: state.distance,
        duration: state.duration,
        elevation_gain: state.elevation_gain,
        id: state.id,
        images_ids: [],
        is_favorite: state.is_favorite,
        gps_track: state.gps_track,
        gps_charts: null,
        location: state.location,
        name: state.name,
        nb_comments: state.nb_comments,
        nb_likes: state.nb_likes,
        start_date_time: this.getStartDateTime,
        user_id: state.user_id,
      };

      return session;
    },

    getStartDateForDisplay(state): string {
      return new Date(state.start_date).toLocaleDateString('fr-FR', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
      });
    },

    getStartTimeForDisplay(state): string {
      return `${state.start_time.split(':')[0]}:${state.start_time.split(':')[1]}`;
    },

    getDurationInMinutes(state): number {
      const minutes = state.duration / 60;

      return parseFloat(minutes.toFixed());
    },

    getDurationForDisplay(state): string {
      const duration = Number(state.duration);
      const h = Math.floor(duration / 3600);
      const m = Math.floor((duration % 3600) / 60);
      const s = Math.floor((duration % 3600) % 60);

      const hDisplay = h > 0 ? h + (h == 1 ? 'h ' : 'h ') : '';
      const mDisplay = m > 0 ? m + (m == 1 ? 'min ' : 'min ') : '';
      const sDisplay = s > 0 ? s + (s == 1 ? 'sec' : 'sec') : '';

      if (hDisplay === '') {
        return mDisplay + sDisplay;
      } else {
        return hDisplay + mDisplay;
      }
    },

    getAverageSpeedFixed(state): number {
      const speed = state.average_speed ? state.average_speed.toFixed(2) : '0';
      return parseFloat(speed);
    },

    getDistanceFixed(state): number {
      const distance = state.distance ? state.distance.toFixed() : '0';
      return parseFloat(distance);
    },

    getLocationForDisplay(state): string | null {
      return state.location;
    },

    getCommentsNumberForDisplay(state): number {
      return state.nb_comments;
    },

    getLikesNumberForDisplay(state): number {
      return state.nb_likes;
    },

    getImagesIds(state): string[] {
      return state.images_ids;
    },

    getElevationGainForDisplay(state): number | null {
      return state.elevation_gain;
    },

    getAverageHeartRateForDisplay(state): number | null {
      return state.average_heart_rate;
    },

    getMapData(state) {
      return state.has_track ? state.map_data : null;
    },

    getCoordinates(state) {
      return state.has_track ? state.map_data.coordinates : null;
    },

    getChartsData(state) {
      return state.has_track ? state.charts_data : null;
    },

    getGPSChartsDateTimeForDisplay(state) {
      return state.has_track ? state.charts_data.date_times : null;
    },

    getGPSChartsAltitudeForDisplay(state) {
      return state.has_track ? state.charts_data.altitudes : null;
    },

    getGPSChartsHeartRateForDisplay(state) {
      return state.has_track ? state.charts_data.heart_rates : null;
    },

    getGPSChartsSpeedForDisplay(state) {
      return state.has_track ? state.charts_data.speeds : null;
    },
  },

  actions: {
    async setMetada(session_metadata: SessionMetadataDTO) {
      /* Activity Name */
      const activitiesStore = useActivitiesStore();
      this.activity_name = activitiesStore.getLabelFromName(session_metadata.activity_name);

      /* Average Heart Rate */
      this.averahe_heart_rate = session_metadata.average_heart_rate
        ? session_metadata.average_heart_rate
        : null;

      /* Average Speed */
      this.average_speed = session_metadata.average_speed;

      /* Burned Calories */
      this.burned_calories = session_metadata.burned_calories;

      /* Description */
      this.description = session_metadata.description ? session_metadata.description : null;

      /* Distance */
      this.distance = session_metadata.distance;

      /* Duration */
      this.duration = session_metadata.duration;

      /* Elevation Gain */
      this.elevation_gain = session_metadata.elevation_gain
        ? session_metadata.elevation_gain
        : null;

      /* GPS Track */
      this.has_track = session_metadata.gps_track;

      /* Id */
      this.id = session_metadata.id;

      /* Images */
      this.has_images = session_metadata.images;

      /* Is Favorite */
      this.is_favorite = session_metadata.is_favorite;

      /* Location */
      this.location = session_metadata.location ? session_metadata.location : null;

      /* Name */
      this.name = session_metadata.name;

      /* Nb Comments */
      this.nb_comments = session_metadata.nb_comments;

      /* Nb Likes */
      this.nb_likes = session_metadata.nb_likes;

      /* Start Date */
      this.start_date = DateTimeMap.toISODate(session_metadata.start_date_time);

      /* Start Time */
      this.start_time = DateTimeMap.toTime(session_metadata.start_date_time);

      /* User Id, Name and Avatar */
      this.user_id = session_metadata.user_id;
      const usersStore = useUsersStore();
      const user = await usersStore.getUser(session_metadata.user_id);
      this.user_name = user.name;
      this.user_avatar = ImageMap.base64ToURL({
        mimetype: user.avatar.mimetype,
        data: user.avatar.data,
      });
    },

    async setWholeSession(session: SessionsDTO) {
      /* Activity Name */
      const activitiesStore = useActivitiesStore();
      this.activity_name = activitiesStore.getLabelFromName(session.activity_name);

      /* Average Heart Rate */
      this.averahe_heart_rate = session.average_heart_rate ? session.average_heart_rate : null;

      /* Average Speed */
      this.average_speed = session.average_speed;

      /* Burned Calories */
      this.burned_calories = session.burned_calories;

      /* Description */
      this.description = session.description ? session.description : null;

      /* Distance */
      this.distance = session.distance;

      /* Duration */
      this.duration = session.duration;

      /* Elevation Gain */
      this.elevation_gain = session.elevation_gain ? session.elevation_gain : null;

      /* GPS Track */
      this.has_track = session.gps_track;

      /* Id */
      this.id = session.id;

      /* Images */
      this.images_ids = session.images_ids ? session.images_ids : null;

      /* Is Favorite */
      this.is_favorite = session.is_favorite;

      /* Location */
      this.location = session.location ? session.location : null;

      /* GPS charts */
      console.debug('[Stores/Session] setWholeSession - gps_charts: ', session.gps_charts);
      this.gps_charts = session.gps_charts ? session.gps_charts : null;

      /* GPS Track */
      console.debug('[Stores/Session] setWholeSession - gps_track: ', session.gps_track);
      // this.gps_track = session.gps_track ? JSON.parse(session.gps_track) : null;
      this.gps_track = session.gps_track ? session.gps_track : null;

      /* Name */
      this.name = session.name;

      /* Nb Comments */
      this.nb_comments = session.nb_comments;

      /* Nb Likes */
      this.nb_likes = session.nb_likes;

      /* Start Date */
      this.start_date = DateTimeMap.toISODate(session.start_date_time);

      /* Start Time */
      this.start_time = DateTimeMap.toTime(session.start_date_time);

      /* User Id, Name and Avatar */
      this.user_id = session.user_id;
      const usersStore = useUsersStore();
      const user = await usersStore.getUser(session.user_id);
      this.user_name = user.name;
      this.user_avatar = ImageMap.base64ToURL({
        mimetype: user.avatar.mimetype,
        data: user.avatar.data,
      });
    },

    setAverageSpeed() {
      this.average_speed = this.distance / (this.duration * 60);
    },

    setDurationInMinutes(durationInSeconds): number {
      const minutes = durationInSeconds / 60;

      return parseFloat(minutes.toFixed());
    },

    setComments(
      comments: {
        id: string;
        text: string;
        date_time: string;
        user_name: string;
        user_avatar: string;
      }[]
    ) {
      this.comments = comments;
    },

    setNbComments(nb_comments: number) {
      this.nb_comments = nb_comments;
    },

    decreaseNbComments() {
      this.nb_comments = this.nb_comments - 1;
    },

    resetWholeSession() {
      console.debug('[Stores/Session] resetWholeSession ');
      this.activity_name = '';
      this.average_heart_rate = null;
      this.average_speed = null;
      (this.burned_calories = 0), (this.charts_data = null);
      this.comments = [];
      this.description = null;
      this.distance = null;
      (this.duration = 0), (this.elevation_gain = null);
      this.gps_charts = null;
      this.gps_track = null;
      this.has_images = false;
      this.has_track = false;
      this.id = '';
      this.images_ids = [];
      this.is_favorite = false;
      this.location = null;
      this.map_data = null;
      this.name = '';
      this.nb_comments = 0;
      this.nb_likes = 0;
      this.start_date = '';
      this.start_time = '';
      this.user_id = '';
      this.user_name = '';
      this.user_avatar = '';
    },

    addImageId(image_id: string) {
      const present = this.images_ids.find((id: string) => id === image_id);
      if (!present) {
        this.images_ids.push(image_id);
      }
    },
  },
});
