import { defineStore } from 'pinia';
import { apiService } from '../services';

import { ImageMap } from './../mappers/imageMap';

interface ImageState {
  id: string;
  mimetype: string;
  data: string;
  url: string;
}

interface ImagesState {
  images: Map<string, ImageState>;
}

export const useImagesStore = defineStore('images', {
  state: (): ImagesState => {
    return { images: new Map() };
  },

  actions: {
    async getImage(id: string): Promise<ImageState> {
      const image = this.images.get(id);
      if (!image) {
        console.debug('[Stores/Images] Actions getImage - Image %s missing from Store', id);
        const base64Image = await apiService.getImage(id);
        const new_image = {
          data: base64Image.data,
          id: base64Image.id,
          mimetype: base64Image.mimetype,
          url: ImageMap.base64ToURL({
            data: base64Image.data,
            mimetype: base64Image.mimetype,
          }),
        };
        console.debug('[Stores/Images] Actions getImage - Image added to Store %s', new_image);
        this.images.set(new_image.id, new_image);
      }
      return this.images.get(id);
    },

    getImageURL(id: string): string {
      return this.images.get(id).url;
    },

    addImage(image: ImageState) {
      this.images.set(image.id, image);
    },

    deleteImage(id: string) {
      this.images.delete(id);
    },

    isPresent(id: string): boolean {
      const image = this.images.get(id);
      console.debug('[Stores/Images] %s isPresent: %s', id, image);
      return image ? true : false;
    },
  },
});
