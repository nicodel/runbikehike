import { describe, it, beforeEach, afterEach, expect } from 'vitest';

import { setActivePinia, createPinia } from 'pinia';

import { fakeUsersNameAndAvatar } from '../../fake_data/users';

import { useUsersStore } from './users';

const users = new Map();

describe('Stores / Users', () => {
  beforeEach(() => {
    setActivePinia(createPinia());

    for (const key of Object.keys(fakeUsersNameAndAvatar)) {
      const user = fakeUsersNameAndAvatar[key];
      users.set(user.id, user);
    }
  });

  afterEach(() => {
    users.clear();
  });

  it('Actions / getUser', async () => {
    const usersStore = useUsersStore();
    usersStore.$patch({ users: users });

    const user = await usersStore.getUser(fakeUsersNameAndAvatar.forrest_gump.id);

    expect(user).toStrictEqual(fakeUsersNameAndAvatar.forrest_gump);
  });

  it('Actions / addUser', () => {
    const usersStore = useUsersStore();

    usersStore.addUser({
      id: fakeUsersNameAndAvatar.chloe_mccardell.id,
      name: fakeUsersNameAndAvatar.chloe_mccardell.name,
      avatar: {
        mimetype: fakeUsersNameAndAvatar.chloe_mccardell.avatar.type,
        data: fakeUsersNameAndAvatar.chloe_mccardell.avatar.data,
      },
    });

    expect(usersStore.$state.users.size).toEqual(1);
  });

  it('Actions / deleteUser', () => {
    const usersStore = useUsersStore();
    usersStore.$patch({ users: users });

    usersStore.deleteUser(fakeUsersNameAndAvatar.chloe_mccardell.id);

    expect(usersStore.$state.users.size).toEqual(2);
    expect(usersStore.$state.users.has(fakeUsersNameAndAvatar.chloe_mccardell.id)).toBeFalsy();
  });
});
