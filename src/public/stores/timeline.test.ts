import { describe, it, beforeEach, expect } from 'vitest';

import { setActivePinia, createPinia } from 'pinia';
import { useTimelineStore } from './timeline';

import { fakeTimeline } from '../../fake_data/timeline';

const ids: string[] = [];
const all = new Map();

for (const item of fakeTimeline) {
  ids.push(item.id);
  all.set(item.id, item);
}

describe('Stores / Timeline', () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });

  it('Actions / setFilteredPeriod', () => {
    const timelineStore = useTimelineStore();

    timelineStore.setSelectedPeriod('Today');

    expect(timelineStore.$state.selectedPeriod).to.equal('Today');
  });

  it('Actions / loadItemsToTimeline', () => {
    const timelineStore = useTimelineStore();

    timelineStore.loadItemsToTimeline(fakeTimeline);

    expect(timelineStore.$state.ids.length).to.equal(fakeTimeline.length);
  });

  it('Getters / getFilteredItems for This Week', () => {
    const timelineStore = useTimelineStore();
    timelineStore.$patch({
      ids: ids,
      all: all,
      selectedPeriod: 'This Week',
    });
    const filteredItems = timelineStore.getFilteredItems;

    expect(filteredItems.length).to.equal(fakeTimeline.length);
  });

  it('Getters / getFilteredItems for Today', () => {
    const timelineStore = useTimelineStore();
    timelineStore.$patch({
      ids: ids,
      all: all,
      selectedPeriod: 'Today',
    });
    const filteredItems = timelineStore.getFilteredItems;

    expect(filteredItems.length).to.equal(3);
  });
});
