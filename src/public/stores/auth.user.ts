// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import { defineStore } from 'pinia';

interface State {
  isAuthenticated: boolean;
  error: string | null;
}

export const useAuthUserStore = defineStore('authUser', {
  state: (): State => ({
    isAuthenticated: false,
    error: null,
  }),
  persist: true,
});
