import { defineStore } from 'pinia';

interface State {
  displayLeft: boolean;
  displayRight: boolean;
}

export const useDisplayStore = defineStore('display', {
  state: (): State => ({
    displayLeft: true,
    displayRight: true,
  }),

  getters: {
    displaySides: (state) => {
      if (state.displayLeft && state.displayRight) {
        return true;
      } else {
        return false;
      }
    },
  },
});
