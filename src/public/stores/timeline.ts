import { defineStore } from 'pinia';
import { DateTime } from 'luxon';

import { ItemsTypes, Period } from './constants';

interface SessionItemState {
  id: string;
  type: ItemsTypes;
  name: string;
  start_date_time: string;
  duration: number;
  distance?: number | null;
  description?: string | null;
  average_speed?: number | null;
  gps_track_coordinates?: string | null;
  activity_name: string;
  location?: string | null;
  nb_comments: number;
  nb_likes: number;
  user_id: string;
  user_name: string;
  user_avatar?: string | null;
}

interface ItemsState {
  ids: string[];
  all: Map<string, SessionItemState>;
  selectedPeriod: Period;
}

export const useTimelineStore = defineStore('timeline', {
  state: (): ItemsState => {
    return {
      ids: [],
      all: new Map(),
      selectedPeriod: 'This Week',
    };
  },

  getters: {
    getFilteredItems: (state): SessionItemState[] => {
      return state.ids
        .map((id) => {
          const item = state.all.get(id) as SessionItemState;

          if (!item) {
            console.error(
              '[Store/Timeline] Timeline item with id %s was expected but not found.',
              id
            );
          }
          return { ...item };
        })
        .filter((item) => {
          if (state.selectedPeriod === 'Today') {
            return DateTime.fromISO(item.start_date_time) >= DateTime.now().minus({ day: 1 });
          }

          if (state.selectedPeriod === 'This Week') {
            return DateTime.fromISO(item.start_date_time) >= DateTime.now().minus({ week: 1 });
          }
        });
    },
  },

  actions: {
    setSelectedPeriod(period: Period) {
      this.selectedPeriod = period;
    },

    loadItemsToTimeline(items: SessionItemState[]) {
      const ids: string[] = [];
      const all = new Map<string, SessionItemState>();

      for (const item of items) {
        ids.push(item.id);
        all.set(item.id, item);
      }

      this.ids = ids;
      this.all = all;
    },

    //   deleteAnItem(item_id: string) {
    //     this.all.delete(item_id);

    //     const index = this.ids.indexOf(item_id);
    //     if (index > -1) this.ids.splice(index, 1);
    //   },
  },
});
