import { defineStore } from 'pinia';

interface UserState {
  id: string;
  name: string;
  email: string;
  is_admin: boolean;
  is_disabled: boolean;
  avatar: {
    mimetype: string;
    data: string;
  };
}

interface UsersListState {
  adminUsersList: UserState[];
}

export const useAdminUsersListStore = defineStore('adminUsersList', {
  state: (): UsersListState => {
    return {
      adminUsersList: [],
    };
  },

  getters: {
    getAll: (state): UserState[] => {
      return state.adminUsersList;
    },
  },

  actions: {
    updateUser(user: UserState) {
      const userIndex = this.adminUsersList.findIndex((element) => element.id === user.id);
      // console.debug('[Stores/adminUsersList] actions.updateUser - userIndex: ', userIndex);
      this.adminUsersList.splice(userIndex, 1, user);
    },
  },
});
