const periods = ['Today', 'This Week', 'This Month'] as const;
type Period = (typeof periods)[number];

const items_types = ['session'] as const;
type ItemsTypes = (typeof items_types)[number];

const ACTIVITIES = [
  {
    name: 'football',
    label: 'Football',
    icon: 'mdi-soccer',
    use_distance: false,
  },
  {
    name: 'biking',
    label: 'Biking',
    icon: 'mdi-bike',
    use_distance: true,
  },
  {
    name: 'running',
    label: 'Running',
    icon: 'mdi-run',
    use_distance: true,
  },
  {
    name: 'skiing',
    label: 'Skiing',
    icon: 'mdi-ski',
    use_distance: true,
  },
  {
    name: 'swimming',
    label: 'Swimming',
    icon: 'mdi-swim',
    use_distance: true,
  },
  {
    name: 'walking',
    label: 'Walking',
    icon: 'mdi-walk',
    use_distance: true,
  },
  {
    name: 'hiking',
    label: 'Hiking',
    icon: 'mdi-hiking',
    use_distance: true,
  },
  {
    name: 'training',
    label: 'Training',
    icon: 'mdi-weight-lifter',
    use_distance: false,
  },
];

const activity_names = ACTIVITIES.map((activity) => activity.name); /*  as const */
type ActivityNames = (typeof activity_names)[number];

export { Period, ItemsTypes, ActivityNames, ACTIVITIES };
