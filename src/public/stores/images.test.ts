import { describe, it, beforeEach, afterEach, expect } from 'vitest';
import { setActivePinia, createPinia } from 'pinia';

import { fakeImages } from '../../fake_data/images';
import { useImagesStore } from './images';

const images = new Map();

describe('Stores / Images', () => {
  beforeEach(() => {
    setActivePinia(createPinia());

    for (const key of Object.keys(fakeImages)) {
      const image = fakeImages[key];
      images.set(image.id, image);
    }
  });

  afterEach(() => {
    images.clear();
  });
  it('Actions / getImage', async () => {
    const imagesStore = useImagesStore();
    imagesStore.$patch({ images: images });

    const image = await imagesStore.getImage(fakeImages[0].id);

    expect(image).toStrictEqual(fakeImages[0]);
  });

  it('Actions / addImage', () => {
    const imagesStore = useImagesStore();

    imagesStore.addImage({
      data: fakeImages[0].data,
      id: fakeImages[0].id,
      mimetype: fakeImages[0].mimetype,
      url: fakeImages[0].data,
    });

    expect(imagesStore.$state.images.size).toEqual(1);
  });

  it('Actions / deleteImage', () => {
    const imagesStore = useImagesStore();
    imagesStore.$patch({ images: images });

    imagesStore.deleteImage(fakeImages[0].id);

    expect(imagesStore.$state.images.size).toEqual(1);
    expect(imagesStore.$state.images.has(fakeImages[0].id)).toBeFalsy();
  });
});
