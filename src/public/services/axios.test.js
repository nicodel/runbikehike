import { describe, it, expect } from 'vitest';

import { onRejected } from './axios';

describe('Axios Interceptors', () => {
  it('should handle 401 erros', async () => {
    const errorResponse = {
      response: {
        status: 401,
        data: {
          message: 'Unauthorized',
        },
      },
    };
    try {
      await onRejected(errorResponse);
    } catch (error) {
      console.debug('Axios Interceptor tests error: %s', JSON.stringify(error));
      expect(error).to.equal('401 Unauthorized');
    }
  });
});

// https://stackoverflow.com/questions/67101502/how-to-mock-axios-with-jest
// https://github.com/hjack-landr/vitest-mock-axios
// https://stackoverflow.com/questions/65980172/how-to-test-axios-interceptors-with-jest
