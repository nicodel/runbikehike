class ApiService {
  constructor(axiosInstance) {
    this.axiosInstance = axiosInstance;
  }

  __get({ url, params, headers }) {
    return this.axiosInstance({
      method: 'GET',
      url: `${url}`,
      params: params ? params : null,
      headers: headers ? headers : null,
    });
  }

  __post({ url, data, params, headers }) {
    return this.axiosInstance({
      method: 'POST',
      url: `${url}`,
      data: data ? data : null,
      params: params ? params : null,
      headers: headers ? headers : null,
    });
  }

  __put({ url, data, params, headers }) {
    return this.axiosInstance({
      method: 'PUT',
      url: `${url}`,
      data: data ? data : null,
      params: params ? params : null,
      headers: headers ? headers : null,
    });
  }

  __delete({ url, data, params, headers }) {
    return this.axiosInstance({
      method: 'DELETE',
      url: `${url}`,
      data: data ? data : null,
      params: params ? params : null,
      headers: headers ? headers : null,
    });
  }

  /* AUTHENTICATION */
  async signIn({ email, password }) {
    return new Promise((resolve, reject) => {
      this.__post({
        url: '/auth/signin',
        data: { email, password },
      })
        .then(() => {
          resolve();
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  async signOut() {
    try {
      await this.__post({ url: '/auth/signout' });
      return true;
    } catch (err) {
      console.error('[ApiService] signOut Error: %s', JSON.stringify(err));
      return false;
    }
  }

  async register({ email, password }) {
    return new Promise((resolve, reject) => {
      this.__post({
        url: 'auth/register',
        data: { email, password },
      })
        .then(() => {
          return resolve();
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  /* USER PROFILE */
  async loadUserProfile() {
    try {
      const response = await this.__get({ url: '/users/profile' });
      console.debug('[ApiService] GET /users/profile response: %s', JSON.stringify(response));
      const { profile } = response.data;
      console.debug('[ApiService] GET /users/profile profile: %s', JSON.stringify(profile));
      return profile;
    } catch (err) {
      console.error('[ApiService] GET /users/profile Error: %s', JSON.stringify(err));
      throw new Error(err);
    }
  }

  async updateProfile(profile) {
    return new Promise((resolve, reject) => {
      this.__put({
        url: 'users/profile',
        data: profile,
      })
        .then(() => {
          resolve();
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  /* SESSION */
  async addSession(session) {
    return new Promise((resolve, reject) => {
      this.__post({
        url: 'sessions/session',
        data: session,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async importGPSFile(file) {
    return new Promise((resolve, reject) => {
      this.__post({
        url: 'sessions/gps_file',
        data: file,
        headers: { 'Content-Type': 'multipart/form-data' },
      })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async getSessionMetadata(session_id) {
    return new Promise((resolve, reject) => {
      this.__get({
        url: `sessions/session/${session_id}`,
      })
        .then((response) => {
          resolve(response.data.session_metadata);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async getSessionChartsData(session_id) {
    return new Promise((resolve, reject) => {
      this.__get({
        url: `sessions/session/${session_id}/charts_data`,
      })
        .then((response) => {
          resolve(response.data.charts_data);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async getSessionMapData(session_id) {
    return new Promise((resolve, reject) => {
      this.__get({
        url: `sessions/session/${session_id}/map_data`,
      })
        .then((response) => {
          resolve(response.data.map_data);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async getSessionImages(session_id) {
    return new Promise((resolve, reject) => {
      this.__get({
        url: `sessions/session/${session_id}/images`,
      })
        .then((response) => {
          resolve(response.data.session_images);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async getSessionComments(session_id) {
    return new Promise((resolve, reject) => {
      this.__get({
        url: `sessions/session/${session_id}/comments`,
      })
        .then((response) => {
          resolve(response.data.session_comments);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async getUserTimeline(period) {
    return new Promise((resolve, reject) => {
      this.__get({
        url: `timeline/${period}`,
      })
        .then((response) => {
          resolve(response.data.timeline);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async getUserNameAndAvatar(user_id) {
    return new Promise((resolve, reject) => {
      this.__get({
        url: `users/name-avatar/${user_id}`,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          return reject(err);
        });
    });
    /* for (const key in fakeUsersNameAndAvatar) {
      const user = fakeUsersNameAndAvatar[key];
      if (user.id === id) {
        return user;
      }
    } */
  }

  async getImage(image_id) {
    return new Promise((resolve, reject) => {
      this.__get({
        url: `images/image/${image_id}`,
      })
        .then((response) => resolve(response.data.image))
        .catch((err) => reject(err));
    });
  }

  async deleteSession(session_id) {
    return new Promise((resolve, reject) => {
      this.__delete({
        url: `sessions/session/${session_id}`,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async updateSession(session) {
    return new Promise((resolve, reject) => {
      this.__put({
        url: 'sessions/session',
        data: session,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async addComment(comment) {
    return new Promise((resolve, reject) => {
      this.__post({
        url: 'sessions/comment',
        data: comment,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async getComment(comment_id) {
    return new Promise((resolve, reject) => {
      this.__get({
        url: `sessions/comment/${comment_id}`,
      })
        .then((response) => {
          resolve(response.data.comment);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async deleteComment(comment_id) {
    return new Promise((resolve, reject) => {
      this.__delete({
        url: `sessions/comment/${comment_id}`,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  /* ADMIN */
  async getAdminUsersList() {
    return new Promise((resolve, reject) => {
      this.__get({
        url: 'admin/all_users',
      })
        .then((response) => {
          resolve(response.data.all_users);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async disableUser(user_id) {
    return new Promise((resolve, reject) => {
      this.__put({
        url: `admin/disable_user/${user_id}`,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async enableUser(user_id) {
    return new Promise((resolve, reject) => {
      this.__put({
        url: `admin/enable_user/${user_id}`,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  async searchUsers(search_terms) {
    return new Promise((resolve, reject) => {
      this.__get({
        url: `social/users?search=${search_terms}`,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }
}

export { ApiService };
