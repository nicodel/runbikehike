import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: '/api/v1',
  withCredentials: true, // Send cookies with every request
});

/* axiosInstance.interceptors.response.use(
  function (response) {
    return response;
  },
  async (error) => {
    console.debug('[base.api] getErrorResponseHandler Error: %s', error);
    if (error.response) {
      // server error 4xx, 5xx
      console.error(
        '[base.api] getErrorResponseHandler Server Error: %s',
        JSON.stringify(error.response.data)
      );
      return Promise.reject(error.response);
    } else if (error.request) {
      // no answer from server
      console.error(
        '[base.api] getErrorResponseHandler No answer from server Error: %s',
        JSON.stringify(error.request)
      );
      return Promise.reject(error.request);
    } else {
      // error while setting up the request
      console.error(
        '[base.api] getErrorResponseHandler Error while setting up the request: %s',
        JSON.stringify(error)
      );
      return Promise.reject(error);
    }
  }
); */

const onFullfilled = (response) => {
  console.debug('[Services/Axios] onFullfilled - response %s', response);
  return response;
};

const onRejected = async (error) => {
  console.error('[Services/Axios] onRejected Error: ', error);
  console.error('[Services/Axios] onRejected Error.response: ', error.response);
  console.error('[Services/Axios] onRejected Error.request: ', error.request);
  if (error.response) {
    // server error 4xx, 5xx
    console.error(
      '[Services/Axios] onRejected Server Error: %s',
      JSON.stringify(error.response.data)
    );
    const errorStatus = error.response.status;
    const errorMessage = error.response.data.message;
    let message = 'Unknown error';
    if (errorStatus === 400) {
      console.debug('[Services/Axios] 400 Bad Request');
      if (
        errorMessage ===
        `birthyear is not within range ${new Date().getFullYear() - 100} to ${
          new Date().getFullYear() - 18
        }.`
      ) {
        return Promise.reject(errorMessage);
      }

      message = '400 Bad Request';
    } else if (errorStatus === 401) {
      console.debug('[Services/Axios] 401 Unauthorized');
      if (errorMessage === 'Authentication failed') {
        message = errorMessage;
      } else {
        message = '401 Unauthorized';
      }
    } else if (errorStatus === 403) {
      console.debug('[Services/Axios] 403 Forbidden');
      if (errorMessage === 'User is disabled.') {
        message = errorMessage;
      } else {
        message = '403 Forbidden';
      }
    } else if (errorStatus === 404) {
      console.debug('[Services/Axios] 404 Not Found');
      message = '404 Not Found';
    } else if (errorStatus === 409) {
      console.debug('[Services/Axios] 409 Conflict');
      message = '409 Conflict';
    }
    return Promise.reject(message);
  } else if (error.request) {
    // no answer from server
    console.error(
      '[Services/Axios] onRejected No answer from server Error: %s',
      JSON.stringify(error.request)
    );
    return Promise.reject(error.request);
  } else {
    // error while setting up the request
    console.error(
      '[Services/Axios] onRejected Error while setting up the request: %s',
      JSON.stringify(error)
    );
    return Promise.reject(error);
  }
};

axiosInstance.interceptors.response.use(onFullfilled, onRejected);

export { axiosInstance, onFullfilled, onRejected };
