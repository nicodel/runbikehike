import { ApiService } from './api';
import { axiosInstance } from './axios';

const apiService = new ApiService(axiosInstance);

export { apiService };
