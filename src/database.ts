import { dataSource } from './shared/infra/database';

import { Activities } from './shared/infra/database/models/activities';

import { CONFIG } from './config';
import { supportedActivities } from './assets/supportedActivities';
import { initiateActivities } from './app/useCases/startUp/initiateActivities';

import logger from './shared/utils/logger';
import { fakeInjectData } from './fake_data';
import { usersRepo } from './app/repositories/auth';
import { initiateAdminAccount } from './app/useCases/startUp/inititateAdminAccount';

export async function startDatabase() {
  try {
    await dataSource.initialize();

    logger.info('[database] DB TypeORM initialize success');

    const activitiesRepo = dataSource.getRepository(Activities);
    const activitiesFilled = await activitiesRepo.count();
    if (!activitiesFilled) {
      logger.info('[database] Activities table is empty. Filling it up!');
      // Filled the Activities table
      await initiateActivities.execute(supportedActivities.activities);
    } else {
      logger.debug('[database] Activities table is filled.');
    }

    if (!CONFIG.IS_PRODUCTION) {
      logger.debug('[database] loading avatar pictures from filesystem');
      await fakeInjectData();
      logger.debug('[database] Fake data added to the database.');
    }

    const admins = await usersRepo.countAdminUsers();
    logger.debug('[database] Admins count: %s', admins);
    if (admins === 0) {
      await initiateAdminAccount.execute({
        email: CONFIG.ADMIN_EMAIL,
        password: CONFIG.ADMIN_PASSWORD,
      });
    }
  } catch (err) {
    logger.error('[database] DB TypeORM initialize failed %s', err);
    process.exit(1);
  }
}
