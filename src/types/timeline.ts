const timeline_items_types = ['session'] as const;
type TimelineItemsTypes = (typeof timeline_items_types)[number];

const timeline_periods = ['last 7 days', 'last 14 days'] as const;
type TimelinePeriodsTypes = (typeof timeline_periods)[number];

export { TimelineItemsTypes, TimelinePeriodsTypes };
