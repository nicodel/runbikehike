export interface ImagesDTO {
  data: string;
  id: string;
  mimetype: string;
}
