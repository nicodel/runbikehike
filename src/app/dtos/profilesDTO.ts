export interface ProfilesDTO {
  user_id: string;
  email: string;
  is_admin: boolean;
  name: string;
  gender: string;
  birthyear: number;
  height: number;
  weight: number;
  avatar: {
    mimetype: string;
    data: string;
  };
  location: string;
  biography: string;
  // privacy_profile_page: string;
  // privacy_sessions: string;
  // privacy_records: string;
  // privacy_objectives: string;
  // privacy_challenges: string;
  // privacy_map_visibility: string;
}
