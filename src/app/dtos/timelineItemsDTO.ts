import { TimelineItemsTypes } from '@/types/timeline';

export interface SessionItemDTO {
  id: string;
  type: TimelineItemsTypes;
  name: string;
  start_date_time: string;
  duration: number;
  distance?: number | null;
  description?: string | null;
  average_speed?: number | null;
  gps_track_coordinates?: string | null;
  activity_name: string;
  location?: string | null;
  nb_comments: number;
  nb_likes: number;
  is_favorite: boolean;
  user_id: string;
}
