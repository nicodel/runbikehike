export interface CommentDTO {
  id: string;
  date_time: string;
  text: string;
  session_id: string;
  user_id: string;
}
