export interface SessionsDTO {
  activity_name: string;
  average_heart_rate: number | null;
  average_speed: number | null;
  burned_calories: number;
  // comments_ids: string[] | null;
  description: string | null;
  distance: number | null;
  duration: number;
  elevation_gain: number | null;
  id: string;
  images_ids: string[] | null;
  is_favorite: boolean;
  gps_track: string | null;
  gps_charts: string | null;
  location: string | null;
  name: string;
  nb_comments: number;
  nb_likes: number;
  start_date_time: string;
  user_id: string;
}

export interface SessionChartsDataDTO {
  date_times: string[];
  altitudes: number[];
  speeds: number[];
  heart_rates: number[] | null;
}

export interface SessionMapPointDTO {
  date_time: string;
  latitude: number;
  longitude: number;
}

export interface SessionMapDataDTO {
  date_times: string[];
  coordinates: [number, number][];
}

export interface SessionMetadataDTO {
  activity_name: string;
  average_heart_rate: number | null;
  average_speed: number | null;
  burned_calories: number;
  description: string | null;
  distance: number | null;
  duration: number;
  elevation_gain: number | null;
  id: string;
  images: boolean;
  is_favorite: boolean;
  gps_track: boolean;
  location: string | null;
  name: string;
  nb_comments: number;
  nb_likes: number;
  start_date_time: string;
  user_id: string;
}
