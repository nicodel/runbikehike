import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Image } from '../domains/image';
import { ImageMap } from './imageMap';
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';

import { fakeAvatars, fakeAvatarsData } from '@/fake_data/profilesAvatars';

let domainImage;
let persistenceImage;
let dtoImage;

describe('App / Mappers / Image', () => {
  before(async () => {
    domainImage = Image.create(
      {
        data: fakeAvatarsData.chloe_mccardell.data,
        mimetype: fakeAvatarsData.chloe_mccardell.type,
      },
      new UniqueEntityID(fakeAvatars.chloe_mccardell.id)
    ).getValue();

    persistenceImage = {
      data: fakeAvatarsData.chloe_mccardell.data,
      id: fakeAvatars.chloe_mccardell.id,
      mimetype: fakeAvatarsData.chloe_mccardell.type,
    };

    dtoImage = {
      data: fakeAvatarsData.chloe_mccardell.data,
      id: fakeAvatars.chloe_mccardell.id,
      mimetype: fakeAvatarsData.chloe_mccardell.type,
    };
  });

  describe('toPersistence', () => {
    it('should return an object ready to store', () => {
      const image = ImageMap.toPersistence(domainImage);
      image.should.be.eql(persistenceImage);
    });
  });

  describe('toDTO', () => {
    it('should return an object ready to be send to client side', () => {
      const image = ImageMap.toDTO(domainImage);
      image.should.be.eql(dtoImage);
    });
  });
});
