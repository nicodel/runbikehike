// import logger from '@/shared/utils/logger';
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';
import { Image } from '../domains/image';
import { ImagesDTO } from '../dtos/imagesDTO';

/* interface IBase64Image {
  type: string;
  data: string;
} */

/* interface IBinaryStringImage {
  type: string;
  data: Blob;
} */

/* interface IBufferImage {
  data: Buffer;
} */

interface IPersistence {
  id: string;
  mimetype: string;
  data: string;
}

export class ImageMap extends Image {
  public static toPersistence(image: Image): IPersistence {
    return {
      id: image.id.id.toString(),
      mimetype: image.mimetype,
      data: image.data,
    };
  }

  public static toDTO(image: Image): ImagesDTO {
    return {
      data: image.data,
      id: image.id.id.toString(),
      mimetype: image.mimetype,
    };
  }

  public static toBulkIds(images: Image[]): string[] {
    const images_ids = [];
    for (let index = 0; index < images.length; index++) {
      const image = images[index];
      images_ids.push(image.id.id.toString());
    }
    return images_ids;
  }

  public static toDomain(rawImage: IPersistence): Image {
    const imageResult = Image.create(
      {
        data: rawImage.data,
        mimetype: rawImage.mimetype,
      },
      new UniqueEntityID(rawImage.id)
    );
    return imageResult.isSuccess ? imageResult.getValue() : null;
  }

  /*   public static toDomain(base64Image: IBase64Image): Image {
    // logger.debug('[Mappers/Images] toDomain - base64Image: %s', base64Image);

    const imageResult = Image.createFromBase64(base64Image);
    if (imageResult.isFailure) {
      // logger.debug('[Mappers/Images] toDomain - imageResult: %s', imageResult);
      return null;
    }
    // logger.debug(
    //   '[Mappers/Images] toDomain - imageResult.getValue(): %s',
    //   JSON.stringify(imageResult.getValue())
    // );
    return imageResult.getValue();
  } */

  /*   public static decodeBase64Image(base64Image: IBase64Image): IBinaryStringImage {
    const binaryString = atob(base64Image.data);

    const bytes = new Uint8Array(binaryString.length);
    for (let i = 0; i < binaryString.length; i++) {
      bytes[i] = binaryString.charCodeAt(i);
    }
    const blob = new Blob([bytes], { type: base64Image.type });

    return {
      type: base64Image.type,
      data: blob,
    };
  } */

  /* public static fromBase64ToBuffer(base64Image: IBase64Image) {
    const imageBuffer = Buffer.from(base64Image.data, 'base64');
    return { data: imageBuffer };
  } */
}
