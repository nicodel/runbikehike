/* eslint-disable @typescript-eslint/no-explicit-any */
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';

import { ShortText } from '@/app/domains/shortText';
import { Height } from '@/app/domains/users/height';
import { Weight } from '@/app/domains/users/weight';
import { Birthyear } from '@/app/domains/users/birthyear';
import { Gender } from '@/app/domains/users/gender';
import { Profile } from '@/app/domains/users/profile';
import { ProfilesDTO } from '@/app/dtos/profilesDTO';
import { Result } from '@/shared/core/Result';
import { LongText } from '@/app/domains/longText';
import { User } from '@/app/domains/auth/user';
import { Image } from '@/app/domains/image';

import logger from '@/shared/utils/logger';
import { UsersMap } from '../auth/usersMap';
import { ImageMap } from '../imageMap';

interface IPersistence {
  id: string;
  name: string;
  gender: any;
  height: number;
  weight: number;
  birthyear: number;
  location?: string;
  biography?: string;
  user: any;
  avatar?: any;
}

export class ProfileMap extends Profile {
  public static async toPersistence(profile: Profile): Promise<IPersistence> {
    const rawUser = await UsersMap.toPersistence(profile.user);

    return {
      id: profile.id.id.toString(),
      name: profile.name.value,
      gender: profile.gender.value,
      birthyear: profile.birthyear.value,
      height: profile.height.value,
      weight: profile.weight.value,
      location: profile.location ? profile.location.value : null,
      biography: profile.biography ? profile.biography.value : null,
      user: rawUser,
      avatar: profile.avatar ? ImageMap.toPersistence(profile.avatar) : null,
    };
  }

  public static toDomain(raw: IPersistence): Profile {
    logger.debug('[Mappers/Profile] toDomain - raw: %s', raw);
    logger.debug('[Mappers/Profile] toDomain - raw.user: %s', raw.user);

    const nameResult = ShortText.create({ value: raw.name, name: 'name' });
    const genderResult = Gender.create({ value: raw.gender });
    const birthyearResult = Birthyear.create({ value: raw.birthyear });
    const heightResult = Height.create({ value: raw.height });
    const weightResult = Weight.create({ value: raw.weight });
    const userResult = User.create(
      {
        email: raw.user.email,
        password: raw.user.password,
        isAdmin: raw.user.is_admin,
        isDeleted: raw.user.is_deleted,
        isDisabled: raw.user.is_disabled,
      },
      raw.user.id
    );
    logger.debug(`[Mappers/Profile] toDomain - userResult: ${JSON.stringify(userResult)}`);

    const domainResult = Result.combine([
      nameResult,
      genderResult,
      birthyearResult,
      heightResult,
      weightResult,
      userResult,
    ]);
    logger.debug('[Mappers/Profile] toDomain - domainResult: %s', domainResult);

    if (domainResult.isFailure) {
      return null;
    }

    let locationResult = null;
    if (raw.location) {
      locationResult = ShortText.create({ value: raw.location, name: 'location' });
      if (locationResult.isFailure) {
        return null;
      }
    }

    let biographyResult = null;
    if (raw.biography) {
      biographyResult = LongText.create({ value: raw.biography, name: 'biography' });
      if (biographyResult.isFailure) {
        return null;
      }
    }

    logger.debug('[Mappers/Profile] toDomain - raw.avatar: %s', raw.avatar);
    let avatarResult = null;
    if (raw.avatar) {
      avatarResult = Image.create(raw.avatar);
      if (avatarResult.isFailure) {
        logger.debug('[Mappers/Profile] toDomain - avatarResult.isFailure: %s', avatarResult);
        return null;
      }
    }

    const profileResult = Profile.create(
      {
        name: nameResult.getValue(),
        gender: genderResult.getValue(),
        birthyear: birthyearResult.getValue(),
        height: heightResult.getValue(),
        weight: weightResult.getValue(),
        location: locationResult ? locationResult.getValue() : null,
        biography: biographyResult ? biographyResult.getValue() : null,
        user: userResult.getValue(),
        avatar: avatarResult ? avatarResult.getValue() : null,
      },
      new UniqueEntityID(raw.id)
    );
    logger.debug(
      '[Mappers/Profile] toDomain - profileResult.getValue(): %s',
      JSON.stringify(profileResult.getValue())
    );

    return profileResult.isSuccess ? profileResult.getValue() : null;
  }

  public static toDTO(profile: Profile): ProfilesDTO {
    const email = profile.user.email;
    logger.debug('[Mappers/Profiles] toDTO - email: %s', JSON.stringify(email));
    const isAdmin = profile.user.isAdmin;
    logger.debug('[Mappers/Profiles] toDTO - profile.user: %s', JSON.stringify(profile.user));
    return {
      user_id: profile.user.id.toString(),
      email: email.value ? email.value : email.toString(),
      is_admin: isAdmin,
      name: profile.name.value,
      gender: profile.gender.value,
      birthyear: profile.birthyear.value,
      height: profile.height.value,
      weight: profile.weight.value,
      location: profile.location ? profile.location.value : null,
      biography: profile.biography ? profile.biography.value : null,
      avatar: profile.avatar
        ? { mimetype: profile.avatar.mimetype, data: profile.avatar.data }
        : null,
    };
  }
}
