import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { fakeProfiles } from '@/fake_data/usersProfiles';
import { fakeAvatars, fakeAvatarsData } from '@/fake_data/profilesAvatars';
import { fakeUsers } from '@/fake_data/users';

import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';
import { Profile } from '@/app/domains/users/profile';
import { ShortText } from '@/app/domains/shortText';
import { Gender } from '@/app/domains/users/gender';
import { Birthyear } from '@/app/domains/users/birthyear';
import { Height } from '@/app/domains/users/height';
import { Weight } from '@/app/domains/users/weight';
import { LongText } from '@/app/domains/longText';
import { Image } from '@/app/domains/image';
import { GenderType } from '@/app/domains/users/genderType';
import { User } from '@/app/domains/auth/user';
import { Password } from '@/app/domains/auth/password';
import { Email } from '@/app/domains/email';
import { ProfileMap } from './profileMap';

import logger from '@/shared/utils/logger';

const domainUser = User.create(
  {
    email: Email.create({ value: fakeUsers.chloe_mccardell.email }).getValue(),
    password: Password.create({
      value: fakeUsers.chloe_mccardell.password,
      hashed: true,
    }).getValue(),
    isAdmin: fakeUsers.chloe_mccardell.is_admin,
    isDisabled: fakeUsers.chloe_mccardell.is_disabled,
  },
  new UniqueEntityID(fakeUsers.chloe_mccardell.id)
).getValue();

const domainAvatar = Image.create(
  {
    mimetype: fakeAvatarsData.chloe_mccardell.type,
    data: fakeAvatarsData.chloe_mccardell.data,
  },
  new UniqueEntityID(fakeAvatars.chloe_mccardell.id)
).getValue();

const domainProfile = Profile.create(
  {
    name: ShortText.create({ value: fakeProfiles.chloe_mccardell.name, name: 'name' }).getValue(),
    gender: Gender.create({ value: fakeProfiles.chloe_mccardell.gender as GenderType }).getValue(),
    birthyear: Birthyear.create({ value: fakeProfiles.chloe_mccardell.birthyear }).getValue(),
    height: Height.create({ value: fakeProfiles.chloe_mccardell.height }).getValue(),
    weight: Weight.create({ value: fakeProfiles.chloe_mccardell.weight }).getValue(),
    location: ShortText.create({
      value: fakeProfiles.chloe_mccardell.location,
      name: 'location',
    }).getValue(),
    biography: LongText.create({
      value: fakeProfiles.chloe_mccardell.biography,
      name: 'biography',
    }).getValue(),
    avatar: domainAvatar,
    user: domainUser,
  },
  new UniqueEntityID(fakeProfiles.chloe_mccardell.id)
).getValue();

const domainProfileWithoutAvatar = Profile.create(
  {
    name: ShortText.create({ value: fakeProfiles.chloe_mccardell.name, name: 'name' }).getValue(),
    gender: Gender.create({ value: fakeProfiles.chloe_mccardell.gender as GenderType }).getValue(),
    birthyear: Birthyear.create({ value: fakeProfiles.chloe_mccardell.birthyear }).getValue(),
    height: Height.create({ value: fakeProfiles.chloe_mccardell.height }).getValue(),
    weight: Weight.create({ value: fakeProfiles.chloe_mccardell.weight }).getValue(),
    location: ShortText.create({
      value: fakeProfiles.chloe_mccardell.location,
      name: 'location',
    }).getValue(),
    biography: LongText.create({
      value: fakeProfiles.chloe_mccardell.biography,
      name: 'biography',
    }).getValue(),
    avatar: null,
    user: domainUser,
  },
  new UniqueEntityID(fakeProfiles.chloe_mccardell.id)
).getValue();

const persistenceProfile = {
  id: fakeProfiles.chloe_mccardell.id,
  name: fakeProfiles.chloe_mccardell.name,
  gender: fakeProfiles.chloe_mccardell.gender,
  birthyear: fakeProfiles.chloe_mccardell.birthyear,
  height: fakeProfiles.chloe_mccardell.height,
  weight: fakeProfiles.chloe_mccardell.weight,
  location: fakeProfiles.chloe_mccardell.location,
  biography: fakeProfiles.chloe_mccardell.biography,
  avatar: {
    id: fakeAvatars.chloe_mccardell.id,
    mimetype: fakeAvatars.chloe_mccardell.mimetype,
    data: fakeAvatars.chloe_mccardell.data,
  },
  user: {
    id: fakeUsers.chloe_mccardell.id,
    email: fakeUsers.chloe_mccardell.email,
    password: fakeUsers.chloe_mccardell.password,
    is_admin: fakeUsers.chloe_mccardell.is_admin,
    is_disabled: fakeUsers.chloe_mccardell.is_disabled,
  },
};

const persistenceProfileWithoutAvatar = {
  id: fakeProfiles.chloe_mccardell.id,
  name: fakeProfiles.chloe_mccardell.name,
  gender: fakeProfiles.chloe_mccardell.gender,
  birthyear: fakeProfiles.chloe_mccardell.birthyear,
  height: fakeProfiles.chloe_mccardell.height,
  weight: fakeProfiles.chloe_mccardell.weight,
  location: fakeProfiles.chloe_mccardell.location,
  biography: fakeProfiles.chloe_mccardell.biography,
  avatar: null,
  user: {
    id: fakeUsers.chloe_mccardell.id,
    email: fakeUsers.chloe_mccardell.email,
    password: fakeUsers.chloe_mccardell.password,
    is_admin: fakeUsers.chloe_mccardell.is_admin,
    is_disabled: fakeUsers.chloe_mccardell.is_disabled,
  },
};

const dtoProfile = {
  user_id: fakeUsers.chloe_mccardell.id,
  email: fakeUsers.chloe_mccardell.email,
  is_admin: fakeUsers.chloe_mccardell.is_admin,
  name: fakeProfiles.chloe_mccardell.name,
  gender: fakeProfiles.chloe_mccardell.gender,
  birthyear: fakeProfiles.chloe_mccardell.birthyear,
  height: fakeProfiles.chloe_mccardell.height,
  weight: fakeProfiles.chloe_mccardell.weight,
  location: fakeProfiles.chloe_mccardell.location,
  biography: fakeProfiles.chloe_mccardell.biography,
  avatar: {
    mimetype: fakeAvatars.chloe_mccardell.mimetype,
    data: fakeAvatars.chloe_mccardell.data,
  },
};

describe('App / Mappers / Users / Profile', () => {
  describe('toPersistence', () => {
    it('should return an object ready to store', async () => {
      const profile = await ProfileMap.toPersistence(domainProfile);
      profile.should.eql(persistenceProfile);
    });

    it('should return an object without avatar if none was passed', async () => {
      const profile = await ProfileMap.toPersistence(domainProfileWithoutAvatar);
      profile.should.eql(persistenceProfileWithoutAvatar);
    });
  });

  describe('toDomain', () => {
    it('should return a Profile object', () => {
      const profile = ProfileMap.toDomain(persistenceProfile);
      profile.should.have.keys('_id', 'props');

      profile.avatar.data.should.be.equal(domainProfile.avatar.data);
      profile.biography.should.be.eql(domainProfile.biography);
      profile.birthyear.should.be.eql(domainProfile.birthyear);
      profile.gender.should.be.eql(domainProfile.gender);
      profile.height.should.be.eql(domainProfile.height);
      profile.id.should.be.eql(domainProfile.id);
      profile.location.should.be.eql(domainProfile.location);
      profile.name.should.be.eql(domainProfile.name);
      profile.user.id.toString().should.be.equal(domainProfile.user.id.toString());
      profile.weight.should.be.eql(domainProfile.weight);
    });
  });

  describe('toDTO', () => {
    it('should return an object ready to be sent to the client side', () => {
      logger.debug(
        '[TEST Mappers/Profiles] toDTO - domainProfile: %s',
        JSON.stringify(domainProfile.user)
      );
      const profile = ProfileMap.toDTO(domainProfile);
      profile.should.be.eql(dtoProfile);
    });
  });
});
