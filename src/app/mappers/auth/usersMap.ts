/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';
import { Email } from '@/app/domains/email';
// import logger from '@/shared/utils/logger';

import { User } from '@/app/domains/auth/user';
import { Password } from '@/app/domains/auth/password';
import { Profile } from '@/app/domains/users/profile';
// import logger from '@/shared/utils/logger';

export interface IUsersPersistence {
  id: string;
  email: string;
  password: string;
  is_admin: boolean;
  is_disabled: boolean;
  /* is_email_verified: boolean;
  is_deleted: boolean; */
}

interface IUserAdminListing {
  id: string;
  name: string;
  email: string;
  is_admin: boolean;
  is_disabled: boolean;
  avatar: {
    mimetype: string;
    data: string;
  };
}

export class UsersMap extends User {
  public static async toPersistence(user: User): Promise<IUsersPersistence> {
    let password: string = null;
    if (!!user.password === true) {
      if (user.password.isAlreadyHashed()) {
        password = user.password.value;
      } else {
        password = await user.password.getHashedValue();
      }
    }

    return {
      id: user.userId.id.toString(),
      email: user.email.value,
      password: password,
      is_admin: user.isAdmin,
      is_disabled: user.isDisabled,
      /* is_email_verified: user.isEmailVerified,
      is_deleted: user.isDeleted, */
    };
  }

  public static toDomain(raw: any): User {
    // logger.debug('[auth/mappers] toDomain raw %s', raw);
    const passwordResult = Password.create({
      value: raw.password,
      hashed: true,
    });

    const emailResult = Email.create({ value: raw.email });

    const userResult = User.create(
      {
        password: passwordResult.getValue(),
        email: emailResult.getValue(),
        isAdmin: raw.is_admin,
        isDisabled: raw.is_disabled,
      },
      new UniqueEntityID(raw.id)
    );
    // logger.debug(
    //   '[auth/mappers] toDomain userResult.isSuccess %s - %s',
    //   userResult.isSuccess,
    //   userResult.getValue()
    // );
    return userResult.isSuccess ? userResult.getValue() : null;
  }

  public static toAdminListing(usersAndProfiles: {
    user: User;
    profile: Profile;
  }): IUserAdminListing {
    const { user, profile } = usersAndProfiles;

    return {
      id: user.id.toString(),
      name: profile.name.value,
      email: user.email.value,
      is_admin: user.isAdmin,
      is_disabled: user.isDisabled,
      avatar: profile.avatar
        ? {
            mimetype: profile.avatar.mimetype,
            data: profile.avatar.data,
          }
        : null,
    };
  }
}
