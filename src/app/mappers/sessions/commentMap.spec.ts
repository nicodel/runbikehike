import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';

import { Comment } from '@/app/domains/comment';

import { fakeComments } from '@/fake_data/comments';

import { User } from '@/app/domains/auth/user';
import { Email } from '@/app/domains/email';
import { Password } from '@/app/domains/auth/password';
import { Session } from '@/app/domains/sessions/session';
import { BurnedCalories } from '@/app/domains/sessions/burnedCalories';
import { ShortText } from '@/app/domains/shortText';
import { Distance } from '@/app/domains/sessions/distance';
import { HeartRate } from '@/app/domains/sessions/heartRate';
import { LongText } from '@/app/domains/longText';
import { Speed } from '@/app/domains/sessions/speed';
import { Duration } from '@/app/domains/sessions/duration';
import { Activity } from '@/app/domains/sessions/activity';
import { DateTime } from '@/app/domains/dateTime';

import { CommentMap } from './commentMap';

import logger from '@/shared/utils/logger';

const commentDomain = Comment.create(
  {
    value: fakeComments[3].text,
    date_time: fakeComments[3].date_time,
    user_id: fakeComments[3].user.id,
    session_id: fakeComments[3].session.id,
  },
  new UniqueEntityID(fakeComments[3].id)
).getValue();

const commentPersistence = {
  id: fakeComments[3].id,
  date_time: fakeComments[3].date_time,
  text: fakeComments[3].text,
  user: fakeComments[3].user,
  session: fakeComments[3].session,
};

const userDomain = User.create(
  {
    email: Email.create({ value: fakeComments[3].user.email }).getValue(),
    password: Password.create({ value: fakeComments[3].user.password, hashed: true }).getValue(),
    isAdmin: fakeComments[3].user.is_admin,
    isDisabled: fakeComments[3].user.is_disabled,
  },
  new UniqueEntityID(fakeComments[3].user.id)
).getValue();

const sessionDomain = Session.create(
  {
    activity: Activity.create(
      {
        name: fakeComments[3].session.activity.name,
        icon: fakeComments[3].session.activity.icon,
        use_distance: fakeComments[3].session.activity.use_distance,
      },
      new UniqueEntityID(fakeComments[3].session.activity.id)
    ).getValue(),
    burned_calories: BurnedCalories.create({
      value: fakeComments[3].session.burned_calories,
    }).getValue(),
    duration: Duration.create({ value: fakeComments[3].session.duration, unit: 's' }).getValue(),
    distance: Distance.create({ value: fakeComments[3].session.distance, unit: 'm' }).getValue(),
    average_speed: Speed.create({ value: fakeComments[3].session.average_speed }).getValue(),
    description: LongText.create({
      value: fakeComments[3].session.description,
      name: 'description',
    }).getValue(),
    gps_track: null,
    location: ShortText.create({
      value: fakeComments[3].session.location,
      name: 'location',
    }).getValue(),
    average_heart_rate: HeartRate.create({
      value: fakeComments[3].session.average_heart_rate,
    }).getValue(),
    elevation_gain: Distance.create({
      value: fakeComments[3].session.elevation_gain,
      unit: 'm',
    }).getValue(),
    nb_comments: fakeComments[3].session.nb_comments,
    nb_likes: fakeComments[3].session.nb_likes,
    is_favorite: fakeComments[3].session.is_favorite,
    images: null,
    name: ShortText.create({
      value: fakeComments[3].session.name,
      name: 'name',
    }).getValue(),
    start_date_time: DateTime.create({
      value: fakeComments[3].session.start_date_time,
    }).getValue(),
    user: userDomain,
  },
  new UniqueEntityID(fakeComments[3].session.id)
).getValue();

describe('App / Mappers / Sessions / Comment', () => {
  /*it('toDomain', () => {
    const result = CommentMap.toDomain(commentPersistence);
    result.should.be.eql(commentDomain);
  });

  it('toBulkIds', () => {
    const result = CommentMap.toBulkIds([
      Comment.create(
        {
          value: fakeComments[3].text,
          date_time: fakeComments[3].date_time,
          user_id: fakeComments[3].user.id,
          session_id: fakeComments[3].session.id,
        },
        new UniqueEntityID(fakeComments[3].id)
      ).getValue(),
      Comment.create(
        {
          value: fakeComments[1].text,
          date_time: fakeComments[1].date_time,
          user_id: fakeComments[1].user.id,
          session_id: fakeComments[1].session.id,
        },
        new UniqueEntityID(fakeComments[1].id)
      ).getValue(),
    ]);
    result.should.eql([fakeComments[3].id, fakeComments[1].id]);
  });*/

  it('topersistence', async () => {
    const result = await CommentMap.toPersistence(commentDomain, userDomain, sessionDomain);
    logger.debug(`TEST result - ${JSON.stringify(result)} `);
    result.should.eql(commentPersistence);
  });
});
