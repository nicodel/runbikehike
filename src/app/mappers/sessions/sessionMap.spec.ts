import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { fakeImages } from '@/fake_data/images';
import { fakeSessions } from '@/fake_data/sessions';
import { fakeSessionJSON } from '@/fake_data/gpsSession/json';
import { fakeActivities } from '@/fake_data/activities';
import { fakeProfiles } from '@/fake_data/usersProfiles';
import { fakeSessionMapGraphs } from '@/fake_data/gpsSession/map_graphs';

import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';

import { Image } from '@/app/domains/image';
import { Session, SessionProps } from '@/app/domains/sessions/session';
import { ShortText } from '@/app/domains/shortText';
import { DateTime } from '@/app/domains/dateTime';
import { Duration } from '@/app/domains/sessions/duration';
import { Distance } from '@/app/domains/sessions/distance';
import { Speed } from '@/app/domains/sessions/speed';
import { BurnedCalories } from '@/app/domains/sessions/burnedCalories';
import { User } from '@/app/domains/auth/user';
import { Email } from '@/app/domains/email';
import { Password } from '@/app/domains/auth/password';
import { Activity } from '@/app/domains/sessions/activity';
import { HeartRate } from '@/app/domains/sessions/heartRate';
import { SessionItemDTO } from '@/app/dtos/timelineItemsDTO';
import { LongText } from '@/app/domains/longText';
import { SessionsDTO } from '@/app/dtos/sessionsDTO';
import { Profile } from '@/app/domains/users/profile';
import { Gender } from '@/app/domains/users/gender';
import { GenderType } from '@/app/domains/users/genderType';
import { Birthyear } from '@/app/domains/users/birthyear';
import { Height } from '@/app/domains/users/height';
import { Weight } from '@/app/domains/users/weight';

import { TrackMap } from './trackMap';
import { SessionMap } from './sessionMap';

const activityDomain = Activity.create(
  {
    name: fakeActivities.running.name,
    icon: fakeActivities.running.icon,
    use_distance: fakeActivities.running.use_distance,
  },
  new UniqueEntityID(fakeSessions[2].activity.id)
);

const userDomain = User.create(
  {
    email: Email.create({ value: fakeSessions[2].user.email }).getValue(),
    password: Password.create({ value: fakeSessions[2].user.password, hashed: true }).getValue(),
    isAdmin: fakeSessions[2].user.is_admin,
    isDisabled: fakeSessions[2].user.is_disabled,
  },
  new UniqueEntityID(fakeSessions[2].user.id)
);

const profileDomain = Profile.create(
  {
    name: ShortText.create({ value: fakeProfiles.forrest_gump.name, name: 'name' }).getValue(),
    gender: Gender.create({ value: fakeProfiles.forrest_gump.gender as GenderType }).getValue(),
    birthyear: Birthyear.create({ value: fakeProfiles.forrest_gump.birthyear }).getValue(),
    height: Height.create({ value: fakeProfiles.forrest_gump.height }).getValue(),
    weight: Weight.create({ value: fakeProfiles.forrest_gump.weight }).getValue(),
    location: ShortText.create({
      value: fakeProfiles.forrest_gump.location,
      name: 'location',
    }).getValue(),
    biography: LongText.create({
      value: fakeProfiles.forrest_gump.biography,
      name: 'biography',
    }).getValue(),
    user: userDomain.getValue(),
    avatar: null,
  },
  new UniqueEntityID(fakeProfiles.forrest_gump.id)
);

const domainGPSFile = TrackMap.toDomain(JSON.parse(fakeSessionJSON));

const images = [
  Image.create(
    { mimetype: fakeImages[0].mimetype, data: fakeImages[0].data },
    new UniqueEntityID(fakeImages[0].id)
  ).getValue(),
  Image.create(
    { mimetype: fakeImages[1].mimetype, data: fakeImages[1].data },
    new UniqueEntityID(fakeImages[1].id)
  ).getValue(),
];

const sessionProps: SessionProps = {
  activity: activityDomain.getValue(),
  burned_calories: BurnedCalories.create({
    value: fakeSessions[2].burned_calories,
  }).getValue(),
  duration: Duration.create({ value: fakeSessions[2].duration, unit: 's' }).getValue(),
  distance: Distance.create({ value: fakeSessions[2].distance, unit: 'm' }).getValue(),
  average_speed: Speed.create({ value: fakeSessions[2].average_speed }).getValue(),
  description: LongText.create({
    value: fakeSessions[2].description,
    name: 'description',
  }).getValue(),
  gps_track: domainGPSFile,
  location: ShortText.create({ value: fakeSessions[2].location, name: 'location' }).getValue(),
  average_heart_rate: HeartRate.create({
    value: fakeSessions[2].average_heart_rate,
  }).getValue(),
  elevation_gain: Distance.create({
    value: fakeSessions[2].elevation_gain,
    unit: 'm',
  }).getValue(),
  nb_comments: fakeSessions[2].nb_comments,
  nb_likes: fakeSessions[2].nb_likes,
  is_favorite: fakeSessions[2].is_favorite,
  images: images,
  name: ShortText.create({
    value: fakeSessions[2].name,
    name: 'name',
  }).getValue(),
  start_date_time: DateTime.create({
    value: fakeSessions[2].start_date_time,
  }).getValue(),
  user: userDomain.getValue(),
};

const sessionDomain = Session.create(sessionProps, new UniqueEntityID(fakeSessions[2].id));

const sessionDTO: SessionsDTO = {
  activity_name: fakeSessions[2].activity.name,
  average_heart_rate: fakeSessions[2].average_heart_rate,
  average_speed: fakeSessions[2].average_speed,
  burned_calories: fakeSessions[2].burned_calories,
  description: fakeSessions[2].description,
  distance: fakeSessions[2].distance,
  duration: fakeSessions[2].duration,
  elevation_gain: fakeSessions[2].elevation_gain,
  id: fakeSessions[2].id,
  images_ids: [fakeImages[0].id, fakeImages[1].id],
  is_favorite: fakeSessions[2].is_favorite,
  gps_track: fakeSessions[2].gps_track,
  gps_charts: null,
  location: fakeSessions[2].location,
  name: fakeSessions[2].name,
  nb_comments: fakeSessions[2].nb_comments,
  nb_likes: fakeSessions[2].nb_likes,
  start_date_time: fakeSessions[2].start_date_time,
  user_id: fakeSessions[2].user.id,
};

const sessionPersistence = {
  activity: fakeSessions[2].activity,
  average_heart_rate: fakeSessions[2].average_heart_rate,
  average_speed: fakeSessions[2].average_speed,
  burned_calories: fakeSessions[2].burned_calories,
  description: fakeSessions[2].description,
  distance: fakeSessions[2].distance,
  duration: fakeSessions[2].duration,
  elevation_gain: fakeSessions[2].elevation_gain,
  id: fakeSessions[2].id,
  images: fakeImages,
  is_favorite: fakeSessions[2].is_favorite,
  gps_track: fakeSessions[2].gps_track,
  location: fakeSessions[2].location,
  name: fakeSessions[2].name,
  nb_comments: fakeSessions[2].nb_comments,
  nb_likes: fakeSessions[2].nb_likes,
  start_date_time: fakeSessions[2].start_date_time,
  user: fakeSessions[2].user,
};

const sessionItem: SessionItemDTO = {
  id: fakeSessions[2].id,
  type: 'session',
  name: fakeSessions[2].name,
  start_date_time: fakeSessions[2].start_date_time,
  duration: fakeSessions[2].duration,
  distance: fakeSessions[2].distance,
  description: fakeSessions[2].description,
  average_speed: fakeSessions[2].average_speed,
  gps_track_coordinates: JSON.stringify({
    date_times: JSON.parse(fakeSessionMapGraphs).date_times,
    coordinates: JSON.parse(fakeSessionMapGraphs).coordinates,
  }),
  activity_name: fakeSessions[2].activity.name,
  location: fakeSessions[2].location,
  nb_comments: fakeSessions[2].nb_comments,
  nb_likes: fakeSessions[2].nb_likes,
  is_favorite: fakeSessions[2].is_favorite,
  user_id: fakeSessions[2].user.id,
  // images: fakeImages,
};

describe('[App / Mappers / Sessions / Session]', () => {
  it('toPersistence', async () => {
    const persistence = await SessionMap.toPersistence(sessionDomain.getValue());

    persistence.should.have.keys(Object.keys(sessionPersistence));
    persistence.activity.should.eql(sessionPersistence.activity);
    persistence.average_heart_rate.should.eql(sessionPersistence.average_heart_rate);
    persistence.average_speed.should.eql(sessionPersistence.average_speed);
    persistence.burned_calories.should.eql(sessionPersistence.burned_calories);
    persistence.description.should.eql(sessionPersistence.description);
    persistence.distance.should.eql(sessionPersistence.distance);
    persistence.duration.should.eql(sessionPersistence.duration);
    persistence.elevation_gain.should.eql(sessionPersistence.elevation_gain);
    // persistence.gps_track.should.equal(sessionPersistence.gps_track);
    persistence.id.should.eql(sessionPersistence.id);
    persistence.images[0].id.should.equal(sessionPersistence.images[0].id);
    persistence.images[0].mimetype.should.equal(sessionPersistence.images[0].mimetype);
    persistence.images[0].data.should.equal(sessionPersistence.images[0].data);
    persistence.is_favorite.should.equal(sessionPersistence.is_favorite);
    persistence.location.should.eql(sessionPersistence.location);
    persistence.name.should.eql(sessionPersistence.name);
    persistence.nb_likes.should.equal(sessionPersistence.nb_likes);
    persistence.start_date_time.should.eql(sessionPersistence.start_date_time);
    persistence.user.should.eql(sessionPersistence.user);
  });

  it('toDTO', () => {
    const dto = SessionMap.toDTO(sessionDomain.getValue());

    dto.should.have.keys(Object.keys(sessionDTO));
    dto.should.have.keys(Object.keys(sessionDTO));
    dto.activity_name.should.eql(sessionDTO.activity_name);
    dto.average_heart_rate.should.eql(sessionDTO.average_heart_rate);
    dto.average_speed.should.eql(sessionDTO.average_speed);
    dto.burned_calories.should.eql(sessionDTO.burned_calories);
    dto.description.should.eql(sessionDTO.description);
    dto.distance.should.eql(sessionDTO.distance);
    dto.duration.should.eql(sessionDTO.duration);
    dto.elevation_gain.should.eql(sessionDTO.elevation_gain);
    chai.expect(dto.gps_charts).to.be.null;
    // dto.gps_track.should.eql(JSON.parse(sessionDTO.gps_track));
    dto.id.should.eql(sessionDTO.id);
    dto.images_ids.should.eql(sessionDTO.images_ids);
    dto.is_favorite.should.equal(sessionDTO.is_favorite);
    dto.location.should.eql(sessionDTO.location);
    dto.name.should.eql(sessionDTO.name);
    dto.nb_comments.should.equal(sessionDTO.nb_comments);
    dto.nb_likes.should.equal(sessionDTO.nb_likes);
    dto.start_date_time.should.eql(sessionDTO.start_date_time);
    dto.user_id.should.eql(sessionDTO.user_id);
  });

  it('toChartsDTO', () => {
    const dto = SessionMap.toChartsDTO(sessionDomain.getValue());

    dto.should.have.keys(Object.keys(sessionDTO));
    dto.should.have.keys(Object.keys(sessionDTO));
    dto.activity_name.should.eql(sessionDTO.activity_name);
    dto.average_heart_rate.should.eql(sessionDTO.average_heart_rate);
    dto.average_speed.should.eql(sessionDTO.average_speed);
    dto.burned_calories.should.eql(sessionDTO.burned_calories);
    dto.description.should.eql(sessionDTO.description);
    dto.distance.should.eql(sessionDTO.distance);
    dto.duration.should.eql(sessionDTO.duration);
    dto.elevation_gain.should.eql(sessionDTO.elevation_gain);
    chai.expect(dto.gps_track).to.be.null;
    dto.gps_charts.should.eql(JSON.parse(fakeSessionMapGraphs));
    dto.id.should.eql(sessionDTO.id);
    dto.images_ids.should.eql(sessionDTO.images_ids);
    dto.is_favorite.should.equal(sessionDTO.is_favorite);
    dto.location.should.eql(sessionDTO.location);
    dto.name.should.eql(sessionDTO.name);
    dto.nb_comments.should.equal(sessionDTO.nb_comments);
    dto.nb_likes.should.equal(sessionDTO.nb_likes);
    dto.start_date_time.should.eql(sessionDTO.start_date_time);
    dto.user_id.should.eql(sessionDTO.user_id);
  });

  it('toDomain', () => {
    const domain = SessionMap.toDomain(sessionPersistence);

    domain.should.have.keys(Object.keys(sessionDomain.getValue()));
    domain.activity.should.eql(sessionDomain.getValue().activity);
    domain.average_heart_rate.should.eql(sessionDomain.getValue().average_heart_rate);
    domain.average_speed.should.eql(sessionDomain.getValue().average_speed);
    domain.burned_calories.should.eql(sessionDomain.getValue().burned_calories);
    domain.description.should.eql(sessionDomain.getValue().description);
    domain.distance.should.eql(sessionDomain.getValue().distance);
    domain.duration.should.eql(sessionDomain.getValue().duration);
    domain.elevation_gain.should.eql(sessionDomain.getValue().elevation_gain);
    domain.gps_track.should.eql(sessionDomain.getValue().gps_track);
    domain.id.should.eql(sessionDomain.getValue().id);
    domain.images[0].id.should.eql(sessionDomain.getValue().images[0].id);
    domain.images[0].mimetype.should.eql(sessionDomain.getValue().images[0].mimetype);
    domain.images[0].data.should.eql(sessionDomain.getValue().images[0].data);
    domain.is_favorite.should.equal(sessionDomain.getValue().is_favorite);
    domain.location.should.eql(sessionDomain.getValue().location);
    domain.name.should.eql(sessionDomain.getValue().name);
    domain.nb_comments.should.equal(sessionDomain.getValue().nb_comments);
    domain.nb_likes.should.equal(sessionDomain.getValue().nb_likes);
    domain.start_date_time.should.eql(sessionDomain.getValue().start_date_time);
    domain.user.should.eql(sessionDomain.getValue().user);
  });

  it('toTimelineItem', () => {
    const item = SessionMap.toTimelineItem(sessionDomain.getValue());
    item.should.eql(sessionItem);
  });

  it('toDomainFromDTO - Success', () => {
    const result = SessionMap.toDomainFromDTO(
      sessionDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );

    result.isSuccess.should.be.true;
    const domain = result.getValue;

    domain.should.have.keys(Object.keys(sessionDomain.getValue()));
    domain.activity.should.eql(sessionDomain.getValue().activity);
    domain.average_heart_rate.should.eql(sessionDomain.getValue().average_heart_rate);
    domain.average_speed.should.eql(sessionDomain.getValue().average_speed);
    domain.burned_calories.should.eql(sessionDomain.getValue().burned_calories);
    domain.description.should.eql(sessionDomain.getValue().description);
    domain.distance.should.eql(sessionDomain.getValue().distance);
    domain.duration.should.eql(sessionDomain.getValue().duration);
    domain.elevation_gain.should.eql(sessionDomain.getValue().elevation_gain);
    domain.gps_track.should.eql(sessionDomain.getValue().gps_track);
    domain.id.should.eql(sessionDomain.getValue().id);
    domain.images[0].id.should.eql(sessionDomain.getValue().images[0].id);
    domain.images[0].mimetype.should.eql(sessionDomain.getValue().images[0].mimetype);
    domain.images[0].data.should.eql(sessionDomain.getValue().images[0].data);
    domain.is_favorite.should.equal(sessionDomain.getValue().is_favorite);
    domain.location.should.eql(sessionDomain.getValue().location);
    domain.name.should.eql(sessionDomain.getValue().name);
    domain.nb_comments.should.equal(sessionDomain.getValue().nb_comments);
    domain.nb_likes.should.equal(sessionDomain.getValue().nb_likes);
    domain.start_date_time.should.eql(sessionDomain.getValue().start_date_time);
    domain.user.should.eql(sessionDomain.getValue().user);
  });

  it('toDomainFromDTO - Failure, missing activity_name', () => {
    const wrongDTO = { ...sessionDTO, activity_name: null };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Missing value');
    result.getError.value.should.equal('activity_name is null or undefined.');
  });

  it('toDomainFromDTO - Failure, missing duration', () => {
    const wrongDTO = { ...sessionDTO, duration: null };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Missing value');
    result.getError.value.should.equal('duration is null or undefined.');
  });

  it('toDomainFromDTO - Failure, missing id', () => {
    const wrongDTO = { ...sessionDTO, id: null };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Missing value');
    result.getError.value.should.equal('id is null or undefined.');
  });

  it('toDomainFromDTO - Failure, missing is_favorite', () => {
    const wrongDTO = { ...sessionDTO, is_favorite: null };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Missing value');
    result.getError.value.should.equal('is_favorite is null or undefined.');
  });

  it('toDomainFromDTO - Failure, missing name', () => {
    const wrongDTO = { ...sessionDTO, name: null };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Missing value');
    result.getError.value.should.equal('name is null or undefined.');
  });

  it('toDomainFromDTO - Failure, missing nb_likes', () => {
    const wrongDTO = { ...sessionDTO, nb_likes: null };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Missing value');
    result.getError.value.should.equal('nb_likes is null or undefined.');
  });

  it('toDomainFromDTO - Failure, missing start_date_time', () => {
    const wrongDTO = { ...sessionDTO, start_date_time: null };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Missing value');
    result.getError.value.should.equal('start_date_time is null or undefined.');
  });

  it('toDomainFromDTO - Failure, missing user_id', () => {
    const wrongDTO = { ...sessionDTO, user_id: null };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Missing value');
    result.getError.value.should.equal('user_id is null or undefined.');
  });

  it('toDomainFromDTO - Failure, wrong average_heart_rate', () => {
    const wrongDTO = { ...sessionDTO, average_heart_rate: 'wrong' as unknown as number };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Wrong value');
    result.getError.value.should.equal('average_heart_rate: wrong');
  });

  it('toDomainFromDTO - Failure, wrong average_speed', () => {
    const wrongDTO = { ...sessionDTO, average_speed: 'wrong' as unknown as number };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Wrong value');
    result.getError.value.should.equal('average_speed: wrong');
  });

  it('toDomainFromDTO - Failure, wrong description', () => {
    const wrongDTO = { ...sessionDTO, description: 'X' };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Wrong value');
    result.getError.value.should.equal('description: X');
  });

  it('toDomainFromDTO - Failure, wrong distance', () => {
    const wrongDTO = { ...sessionDTO, distance: 'wrong' as unknown as number };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Wrong value');
    result.getError.value.should.equal('distance: wrong');
  });

  it('toDomainFromDTO - Failure, wrong duration', () => {
    const wrongDTO = { ...sessionDTO, duration: 'wrong' as unknown as number };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Wrong value');
    result.getError.value.should.equal('duration: wrong');
  });

  it('toDomainFromDTO - Failure, wrong elevation_gain', () => {
    const wrongDTO = { ...sessionDTO, elevation_gain: 'wrong' as unknown as number };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Wrong value');
    result.getError.value.should.equal('elevation_gain: wrong');
  });

  it('toDomainFromDTO - Failure, wrong is_favorite', () => {
    const wrongDTO = { ...sessionDTO, is_favorite: 'wrong' as unknown as boolean };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Wrong value');
    result.getError.value.should.equal('is_favorite: wrong');
  });

  it('toDomainFromDTO - Failure, wrong location', () => {
    const wrongDTO = { ...sessionDTO, location: 'X' };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Wrong value');
    result.getError.value.should.equal('location: X');
  });

  it('toDomainFromDTO - Failure, wrong nb_likes', () => {
    const wrongDTO = { ...sessionDTO, nb_likes: 'wrong' as unknown as number };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Wrong value');
    result.getError.value.should.equal('nb_likes: wrong');
  });

  it('toDomainFromDTO - Failure, wrong name', () => {
    const wrongDTO = { ...sessionDTO, name: 'X' };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Wrong value');
    result.getError.value.should.equal('name: X');
  });

  it('toDomainFromDTO - Failure, wrong start_date_time', () => {
    const wrongDTO = { ...sessionDTO, start_date_time: 'wrong' };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Wrong value');
    result.getError.value.should.equal('start_date_time: wrong');
  });

  it('toDomainFromDTO - Failure, wrong burned_calories', () => {
    const wrongDTO = { ...sessionDTO, burned_calories: 'wrong' as unknown as number };
    const result = SessionMap.toDomainFromDTO(
      wrongDTO,
      activityDomain.getValue(),
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Wrong value');
    result.getError.value.should.equal('burned_calories: wrong');
  });

  it('toDomainFromDTO - Failure, wrong activity', () => {
    const wrongActivity = null as unknown as Activity;
    const result = SessionMap.toDomainFromDTO(
      sessionDTO,
      wrongActivity,
      userDomain.getValue(),
      images,
      profileDomain.getValue(),
      domainGPSFile
    );
    result.isFailure.should.be.true;
    result.getError.cause.should.equal('Wrong value');
    result.getError.value.should.equal('activity is null or undefined.');
  });
});
