import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Activity } from '@/app/domains/sessions/activity';
import { ActivityMap } from './activityMap';
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';
import { fakeActivities } from '@/fake_data/activities';

const activityDomain = Activity.create(
  {
    name: fakeActivities.running.name,
    icon: fakeActivities.running.icon,
    use_distance: fakeActivities.running.use_distance,
  },
  new UniqueEntityID(fakeActivities.running.id)
).getValue();

const activityPersistence = {
  id: fakeActivities.running.id,
  name: fakeActivities.running.name,
  icon: fakeActivities.running.icon,
  use_distance: fakeActivities.running.use_distance,
};

describe('App / Mappers / Sessions / Activity', () => {
  it('toPersistence', async () => {
    const activity = await ActivityMap.toPersistence(activityDomain);
    activity.should.be.eql(activityPersistence);
  });

  it('toDomain', async () => {
    const activity = await ActivityMap.toDomain(activityPersistence);
    activity.should.have.keys('_id', 'props');
  });
});
