import { Activity } from '@/app/domains/sessions/activity';
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';

interface IPersistence {
  id: string;
  name: string;
  icon: string;
  use_distance: boolean;
}

export class ActivityMap extends Activity {
  public static async toPersistence(activity: Activity): Promise<IPersistence> {
    return {
      id: activity.id.id.toString(),
      name: activity.name,
      icon: activity.icon,
      use_distance: activity.use_distance,
    };
  }

  public static async toDomain(rawActivity: IPersistence): Promise<Activity> {
    const activityResult = Activity.create(
      {
        name: rawActivity.name,
        icon: rawActivity.icon,
        use_distance: rawActivity.use_distance,
      },
      new UniqueEntityID(rawActivity.id)
    );

    return activityResult.isSuccess ? activityResult.getValue() : null;
  }
}
