import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';

import { Comment } from '@/app/domains/comment';
import { Session } from '@/app/domains/sessions/session';
import { User } from '@/app/domains/auth/user';
import { CommentDTO } from '@/app/dtos/commentDTO';

import { UsersMap, IUsersPersistence } from '../auth/usersMap';
import { ISessionPersistence, SessionMap } from './sessionMap';

import logger from '@/shared/utils/logger';

interface IPersistence {
  id: string;
  date_time: string;
  text: string;
  user: IUsersPersistence;
  session: ISessionPersistence;
}

export class CommentMap extends Comment {
  public static toBulkIds(comments: Comment[]): string[] {
    const comments_ids = comments.map((comment) => comment.id.id.toString());
    return comments_ids;
  }

  public static toDomain(rawComment: IPersistence): Comment {
    logger.debug(`[Mappers/Comments] toDomain - comment ${JSON.stringify(rawComment)}`);
    const commentResult = Comment.create(
      {
        date_time: rawComment.date_time,
        value: rawComment.text,
        user_id: rawComment.user.id,
        session_id: rawComment.session.id,
      },
      new UniqueEntityID(rawComment.id)
    );
    return commentResult.isSuccess ? commentResult.getValue() : null;
  }

  public static async toPersistence(
    domainComment: Comment,
    userDomain: User,
    sessionDomain: Session
  ): Promise<IPersistence> {
    const rawUser = await UsersMap.toPersistence(userDomain);

    const rawSession = await SessionMap.toPersistence(sessionDomain);

    return {
      id: domainComment.id.id.toString(),
      date_time: domainComment.date_time,
      text: domainComment.value,
      user: rawUser,
      session: rawSession,
    };
  }

  public static toDTO(comment: Comment): CommentDTO {
    logger.debug(`[Mappers/Comments] toDTO - comment ${JSON.stringify(comment)}`);
    return {
      id: comment.id.id.toString(),
      date_time: comment.date_time,
      text: comment.value,
      user_id: comment.user_id,
      session_id: comment.session_id,
    };
  }
}
