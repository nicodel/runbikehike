/* eslint-disable @typescript-eslint/no-explicit-any */
import { Guard } from '@/shared/core/Guard';
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';
import { Result } from '@/shared/core/Result';

import { Session, SessionProps } from '@/app/domains/sessions/session';
import { SessionMetadataDTO, SessionsDTO } from '@/app/dtos/sessionsDTO';
import { ShortText } from '@/app/domains/shortText';
import { Activity } from '@/app/domains/sessions/activity';
import { DateTime } from '@/app/domains/dateTime';
import { Duration } from '@/app/domains/sessions/duration';
import { Distance } from '@/app/domains/sessions/distance';
import { Speed } from '@/app/domains/sessions/speed';
import { BurnedCalories } from '@/app/domains/sessions/burnedCalories';
import { LongText } from '@/app/domains/longText';
import { User } from '@/app/domains/auth/user';
import { SessionItemDTO } from '@/app/dtos/timelineItemsDTO';
import { Image } from '@/app/domains/image';
import { HeartRate } from '@/app/domains/sessions/heartRate';
import { Email } from '@/app/domains/email';
import { Password } from '@/app/domains/auth/password';
import { Profile } from '@/app/domains/users/profile';
import { Track } from '@/app/domains/sessions/track';
import { Comment } from '@/app/domains/comment';

import { TrackMap } from './trackMap';
import { ActivityMap } from './activityMap';
import { IUsersPersistence, UsersMap } from '../auth/usersMap';
import { ImageMap } from '../imageMap';

import logger from '@/shared/utils/logger';

export interface ISessionPersistence {
  activity: any;
  average_heart_rate: number;
  average_speed?: number;
  burned_calories: number;
  comments?: any[];
  description?: string;
  distance?: number;
  duration: number;
  elevation_gain: number;
  gps_track?: string; // GPSTrack as json
  id: string;
  images?: any[];
  is_favorite: boolean;
  location?: string;
  name: string;
  nb_comments: number;
  nb_likes: number;
  start_date_time: string;
  user: IUsersPersistence;
}

interface MapperResponse {
  isSuccess: boolean;
  isFailure: boolean;
  getValue: any;
  getError: { cause: string; value: string } | null;
}

export class SessionMap extends Session {
  public static async toPersistence(session: Session): Promise<ISessionPersistence> {
    const rawUser = await UsersMap.toPersistence(session.user);
    const rawActivity = await ActivityMap.toPersistence(session.activity);
    const rawImages = [];
    if (session.images) {
      for (let index = 0; index < session.images.length; index++) {
        const image = session.images[index];
        const rawImage = ImageMap.toPersistence(image);
        rawImages.push(rawImage);
      }
    }

    return {
      activity: rawActivity,
      average_speed: session.average_speed ? session.average_speed.speed : null,
      average_heart_rate: session.average_heart_rate ? session.average_heart_rate.value : null,
      burned_calories: session.burned_calories.value,
      description: session.description ? session.description.value : null,
      distance: session.distance ? session.distance.value : null,
      duration: session.duration.value,
      elevation_gain: session.elevation_gain ? session.elevation_gain.value : null,
      gps_track: session.gps_track ? TrackMap.toPersistence(session.gps_track) : null,
      id: session.id.id.toString(),
      images: session.images ? rawImages : null,
      is_favorite: session.is_favorite,
      location: session.location ? session.location.value : null,
      name: session.name.value,
      nb_comments: session.nb_comments,
      nb_likes: session.nb_likes,
      start_date_time: session.start_date_time.value,
      user: rawUser,
    };
  }

  public static toDTO(session: Session): SessionsDTO {
    return {
      activity_name: session.activity.name,
      average_heart_rate: session.average_heart_rate ? session.average_heart_rate.value : null,
      average_speed: session.average_speed ? session.average_speed.speed : null,
      burned_calories: session.burned_calories.value,
      // comments_ids: session.comments ? CommentMap.toBulkIds(session.comments) : null,
      description: session.description ? session.description.value : null,
      distance: session.distance ? session.distance.value : null,
      duration: session.duration.value,
      elevation_gain: session.elevation_gain ? session.elevation_gain.value : null,
      gps_track: session.gps_track ? TrackMap.toDTO(session.gps_track) : null,
      gps_charts: null,
      id: session.id.id.toString(),
      images_ids: session.images ? ImageMap.toBulkIds(session.images) : null,
      is_favorite: session.is_favorite,
      location: session.location ? session.location.value : null,
      name: session.name.value,
      nb_comments: session.nb_comments,
      nb_likes: session.nb_likes,
      start_date_time: session.start_date_time.date_time,
      user_id: session.user.id.toString(),
    };
  }

  public static toDomain(rawSession: ISessionPersistence): Session {
    // logger.debug(
    //   `[Mappers/Session] toDomain - rawSession.user: ${JSON.stringify(rawSession.user)}`
    // );
    const nameResult = ShortText.create({ value: rawSession.name, name: 'name' });
    const startDateTimeResult = DateTime.create({ value: rawSession.start_date_time });
    const durationResult = Duration.create({ value: rawSession.duration });
    const caloriesBurnedResult = BurnedCalories.create({ value: rawSession.burned_calories });

    const userResult = User.create(
      {
        email: Email.create({ value: rawSession.user.email }).getValue(),
        password: Password.create({ value: rawSession.user.password, hashed: true }).getValue(),
        isAdmin: rawSession.user.is_admin,
        isDisabled: rawSession.user.is_disabled,
      },
      new UniqueEntityID(rawSession.user.id)
    );

    const activityResult = Activity.create(
      {
        name: rawSession.activity.name,
        icon: rawSession.activity.icon,
        use_distance: rawSession.activity.use_distance,
      },
      new UniqueEntityID(rawSession.activity.id)
    );

    const domainResults = Result.combine([
      nameResult,
      startDateTimeResult,
      durationResult,
      caloriesBurnedResult,
      userResult,
      activityResult,
    ]);
    logger.debug(
      `[Mappers/Session] toDomain - domainResults.isSuccess: ${JSON.stringify(
        domainResults.isSuccess
      )}`
    );
    if (domainResults.isFailure) {
      return null;
    }

    let distanceResult = null;
    if (rawSession.distance) {
      distanceResult = Distance.create({ value: rawSession.distance });
      if (distanceResult.isFailure) {
        return null;
      }
    }

    let averageSpeedResult = null;
    if (rawSession.average_speed) {
      averageSpeedResult = Speed.create({ value: rawSession.average_speed });
      if (averageSpeedResult.isFailure) {
        return null;
      }
    }

    let descriptionResult = null;
    if (rawSession.description) {
      descriptionResult = LongText.create({ value: rawSession.description, name: 'description' });
      if (descriptionResult.isFailure) {
        return null;
      }
    }

    let trackResult = null;
    if (rawSession.gps_track) {
      trackResult = TrackMap.toDomain(JSON.parse(rawSession.gps_track));
      if (!trackResult || trackResult.isFailure) {
        return null;
      }
    }

    const images = [];
    if (rawSession.images) {
      let imageResult = null;
      for (let index = 0; index < rawSession.images.length; index++) {
        const rawImage = rawSession.images[index];
        imageResult = Image.create(
          { mimetype: rawImage.mimetype, data: rawImage.data },
          new UniqueEntityID(rawImage.id)
        );
        if (imageResult.isFailure) {
          return null;
        }
        images.push(imageResult.getValue());
      }
    }

    let averageHeartRateResult = null;
    if (rawSession.average_heart_rate) {
      averageHeartRateResult = HeartRate.create({ value: rawSession.average_heart_rate });

      if (averageHeartRateResult.isFailure) {
        return null;
      }
    }

    let elevationGainResult = null;
    if (rawSession.elevation_gain) {
      elevationGainResult = Distance.create({ value: rawSession.elevation_gain });
      if (elevationGainResult.isFailure) {
        return null;
      }
    }

    let locationResult = null;
    if (rawSession.location) {
      locationResult = ShortText.create({ value: rawSession.location, name: 'location' });
      if (locationResult.isFailure) {
        return null;
      }
    }

    const comments = [];
    if (rawSession.comments) {
      for (let index = 0; index < rawSession.comments.length; index++) {
        const rawComment = rawSession.comments[index];
        const commentResult = Comment.create(
          {
            value: rawComment.text,
            date_time: rawComment.date_time,
            user_id: userResult.getValue().id.toString(),
            session_id: rawSession.id,
          },
          new UniqueEntityID(rawComment.id)
        );
        if (commentResult.isFailure) {
          return null;
        }
        logger.debug(
          `[Mappers/Session] toDomain - commentResult.getValue() ${JSON.stringify(
            commentResult.getValue()
          )}`
        );
        comments.push(commentResult.getValue());
      }
    }

    const sessionResult = Session.create(
      {
        activity: activityResult.getValue(),
        average_heart_rate: averageHeartRateResult ? averageHeartRateResult.getValue() : null,
        average_speed: averageSpeedResult ? averageSpeedResult.getValue() : null,
        burned_calories: caloriesBurnedResult.getValue(),
        description: descriptionResult ? descriptionResult.getValue() : null,
        distance: distanceResult ? distanceResult.getValue() : null,
        duration: durationResult.getValue(),
        elevation_gain: elevationGainResult ? elevationGainResult.getValue() : null,
        gps_track: trackResult,
        images: images ? images : null,
        is_favorite: rawSession.is_favorite,
        location: locationResult ? locationResult.getValue() : null,
        name: nameResult.getValue(),
        nb_comments: rawSession.nb_comments,
        nb_likes: rawSession.nb_likes,
        start_date_time: startDateTimeResult.getValue(),
        user: userResult.getValue(),
      },
      new UniqueEntityID(rawSession.id)
    );
    logger.debug(
      `[Mappers/Session] toDomain - sessionResult.isSuccess: ${JSON.stringify(
        sessionResult.isSuccess
      )}`
    );

    return sessionResult.isSuccess ? sessionResult.getValue() : null;
  }

  public static toChartsDTO(session: Session): SessionsDTO {
    return {
      activity_name: session.activity.name,
      average_heart_rate: session.average_heart_rate ? session.average_heart_rate.value : null,
      average_speed: session.average_speed ? session.average_speed.speed : null,
      burned_calories: session.burned_calories.value,
      description: session.description ? session.description.value : null,
      distance: session.distance ? session.distance.value : null,
      duration: session.duration.value,
      elevation_gain: session.elevation_gain ? session.elevation_gain.value : null,
      gps_track: null,
      gps_charts: session.gps_track ? TrackMap.toGrapsAndMap(session.gps_track) : null,
      id: session.id.id.toString(),
      images_ids: session.images ? ImageMap.toBulkIds(session.images) : null,
      is_favorite: session.is_favorite,
      location: session.location ? session.location.value : null,
      name: session.name.value,
      nb_comments: session.nb_comments,
      nb_likes: session.nb_likes,
      start_date_time: session.start_date_time.date_time,
      user_id: session.user.id.toString(),
    };
  }

  public static toTimelineItem(session: Session): SessionItemDTO {
    return {
      id: session.id.id.toString(),
      type: 'session',
      name: session.name.value,
      start_date_time: session.start_date_time.date_time,
      duration: session.duration.value,
      distance: session.distance ? session.distance.value : null,
      description: session.description ? session.description.value : null,
      average_speed: session.average_speed ? session.average_speed.speed : null,
      gps_track_coordinates: session.gps_track
        ? JSON.stringify(TrackMap.toCoordinates(session.gps_track))
        : null,
      activity_name: session.activity.name,
      location: session.location ? session.location.value : null,
      nb_comments: session.nb_comments,
      nb_likes: session.nb_likes,
      is_favorite: session.is_favorite,
      user_id: session.user.id.toString(),
    };
  }

  public static toDomainFromDTO(
    dto: SessionsDTO,
    activity: Activity,
    user: User,
    images: Image[] | null,
    profile: Profile,
    gps_track: Track | null
  ): MapperResponse {
    const againstNullOrUndefinedResult = Guard.againstNullOrUndefinedBulk([
      { argument: dto.activity_name, argumentName: 'activity_name' },
      { argument: dto.duration, argumentName: 'duration' },
      { argument: dto.id, argumentName: 'id' },
      { argument: dto.is_favorite, argumentName: 'is_favorite' },
      { argument: dto.name, argumentName: 'name' },
      { argument: dto.nb_likes, argumentName: 'nb_likes' },
      { argument: dto.start_date_time, argumentName: 'start_date_time' },
      { argument: dto.user_id, argumentName: 'user_id' },
    ]);
    if (!againstNullOrUndefinedResult.succeeded) {
      logger.error(
        '[Mappers/Sessions] toDomainFromDTO - Error missing mandatory value for session: %s',
        againstNullOrUndefinedResult.message
      );
      return {
        isSuccess: false,
        isFailure: true,
        getValue: null,
        getError: { cause: 'Missing value', value: againstNullOrUndefinedResult.message },
      };
    }

    const activityDetails = activity;

    let averageHeartRateResult = null;
    if (dto.average_heart_rate) {
      averageHeartRateResult = HeartRate.create({ value: dto.average_heart_rate });
      if (averageHeartRateResult.isFailure) {
        logger.error(
          '[Mappers/Sessions] toDomainFromDTO - Error wrong value for session average_heart_rate: %s',
          dto.average_heart_rate
        );
        return {
          isSuccess: false,
          isFailure: true,
          getValue: null,
          getError: {
            cause: 'Wrong value',
            value: `average_heart_rate: ${dto.average_heart_rate.toString()}`,
          },
        };
      }
    }

    let averageSpeedResult = null;
    if (dto.average_speed) {
      averageSpeedResult = Speed.create({ value: dto.average_speed });
      if (averageSpeedResult.isFailure) {
        logger.error(
          '[Mappers/Sessions] toDomainFromDTO - Error wrong value for session average_speed: %s',
          dto.average_speed
        );
        return {
          isSuccess: false,
          isFailure: true,
          getValue: null,
          getError: {
            cause: 'Wrong value',
            value: `average_speed: ${dto.average_speed.toString()}`,
          },
        };
      }
    }

    //
    let descriptionResult = null;
    if (dto.description) {
      descriptionResult = LongText.create({
        value: dto.description,
        name: 'description',
      });
      if (descriptionResult.isFailure) {
        logger.error(
          '[Mappers/Session] toDomainFromDTO - Error wrong value for session description: %s',
          dto.description
        );
        return {
          isSuccess: false,
          isFailure: true,
          getValue: null,
          getError: {
            cause: 'Wrong value',
            value: `description: ${dto.description}`,
          },
        };
      }
    }

    let distanceResult = null;
    if (dto.distance) {
      distanceResult = Distance.create({ value: dto.distance, unit: 'm' });
      if (distanceResult.isFailure) {
        logger.error(
          '[Mappers/Session] toDomainFromDTO - Error wrong value for session distance: %s',
          dto.distance
        );
        return {
          isSuccess: false,
          isFailure: true,
          getValue: null,
          getError: {
            cause: 'Wrong value',
            value: `distance: ${dto.distance.toString()}`,
          },
        };
      }
    }

    const durationResult = Duration.create({ value: dto.duration, unit: 's' });
    if (durationResult.isFailure) {
      logger.error(
        '[Mappers/Session] toDomainFromDTO - Error wrong value for session duration: %s',
        dto.duration
      );
      return {
        isSuccess: false,
        isFailure: true,
        getValue: null,
        getError: {
          cause: 'Wrong value',
          value: `duration: ${dto.duration.toString()}`,
        },
      };
    }

    let elevationGainResult = null;
    if (dto.elevation_gain) {
      elevationGainResult = Distance.create({ value: dto.elevation_gain, unit: 's' });
      if (elevationGainResult.isFailure) {
        logger.error(
          '[Mappers/Session] toDomainFromDTO - Error wrong value for session elevation_gain: %s',
          dto.elevation_gain
        );
        return {
          isSuccess: false,
          isFailure: true,
          getValue: null,
          getError: {
            cause: 'Wrong value',
            value: `elevation_gain: ${dto.elevation_gain.toString()}`,
          },
        };
      }
    }

    const imagesDetails = images;

    const idResult = new UniqueEntityID(dto.id);

    if (typeof dto.is_favorite !== 'boolean') {
      logger.error(
        '[Mappers/Session] toDomainFromDTO - Error wrong value for session is_favorite: %s',
        dto.is_favorite
      );
      return {
        isSuccess: false,
        isFailure: true,
        getValue: null,
        getError: {
          cause: 'Wrong value',
          value: `is_favorite: ${dto.is_favorite}`,
        },
      };
    }
    const isFavorite = dto.is_favorite;

    const trackResult = gps_track;

    let locationResult = null;
    if (dto.location) {
      locationResult = ShortText.create({ value: dto.location, name: 'location' });
      if (locationResult.isFailure) {
        logger.error(
          '[Mappers/Session] toDomainFromDTO - Error wrong value for session location: %s',
          dto.location
        );
        return {
          isSuccess: false,
          isFailure: true,
          getValue: null,
          getError: {
            cause: 'Wrong value',
            value: `location: ${dto.location}`,
          },
        };
      }
    }

    // const commentsDetails = comments;

    if (typeof dto.nb_comments !== 'number') {
      logger.error(
        '[Mappers/Session] toDomainFromDTO - Error wrong value for session nb_comments: %s',
        dto.nb_comments
      );
      return {
        isSuccess: false,
        isFailure: true,
        getValue: null,
        getError: {
          cause: 'Wrong value',
          value: `nb_comments: ${dto.nb_comments}`,
        },
      };
    }
    const nb_comments = dto.nb_comments;

    if (typeof dto.nb_likes !== 'number') {
      logger.error(
        '[Mappers/Session] toDomainFromDTO - Error wrong value for session nb_likes: %s',
        dto.nb_likes
      );
      return {
        isSuccess: false,
        isFailure: true,
        getValue: null,
        getError: {
          cause: 'Wrong value',
          value: `nb_likes: ${dto.nb_likes}`,
        },
      };
    }
    const nbLikes = dto.nb_likes;

    const nameResult = ShortText.create({ value: dto.name, name: 'name' });
    if (nameResult.isFailure) {
      logger.error(
        '[Mappers/Session] toDomainFromDTO - Error wrong value for session name: %s',
        dto.name
      );
      return {
        isSuccess: false,
        isFailure: true,
        getValue: null,
        getError: {
          cause: 'Wrong value',
          value: `name: ${dto.name}`,
        },
      };
    }

    const startDateTimeResult = DateTime.create({ value: dto.start_date_time });
    if (startDateTimeResult.isFailure) {
      logger.error(
        '[Mappers/Session] toDomainFromDTO - Error wrong value for session start_date_time: %s',
        dto.start_date_time
      );
      return {
        isSuccess: false,
        isFailure: true,
        getValue: null,
        getError: {
          cause: 'Wrong value',
          value: `start_date_time: ${dto.start_date_time}`,
        },
      };
    }

    const userDetails = user;

    let burnedCalories = 0;
    if (dto.burned_calories) {
      burnedCalories = dto.burned_calories;
      logger.debug(`[Mappers/Session] toDomainFromDTO - DTO burnedCalories: ${burnedCalories}`);
    } else {
      burnedCalories = BurnedCalories.calculate({
        weight: profile.weight.value,
        duration: durationResult.getValue().value,
        distance: distanceResult ? distanceResult.getValue().value : null,
        activity_name: activityDetails.name,
      });
      logger.debug(
        `[Mappers/Session] toDomainFromDTO - Calculated burnedCalories: ${burnedCalories}`
      );
    }
    const burnedCaloriesResult = BurnedCalories.create({
      value: burnedCalories,
    });
    if (burnedCaloriesResult.isFailure) {
      logger.error('[Mappers/Session] toDomainFromDTO - Error missing session burned_calories');
      return {
        isSuccess: false,
        isFailure: true,
        getValue: null,
        getError: {
          cause: 'Wrong value',
          value: `burned_calories: ${dto.burned_calories}`,
        },
      };
    }

    const sessionProps: SessionProps = {
      activity: activityDetails,
      average_heart_rate: averageHeartRateResult ? averageHeartRateResult.getValue() : null,
      average_speed: averageSpeedResult ? averageSpeedResult.getValue() : null,
      burned_calories: burnedCaloriesResult.getValue(),
      description: descriptionResult ? descriptionResult.getValue() : null,
      distance: distanceResult.getValue(),
      duration: durationResult ? durationResult.getValue() : null,
      elevation_gain: elevationGainResult ? elevationGainResult.getValue() : null,
      images: imagesDetails ? imagesDetails : null,
      is_favorite: isFavorite,
      gps_track: trackResult,
      location: locationResult ? locationResult.getValue() : null,
      name: nameResult.getValue(),
      nb_comments: nb_comments,
      nb_likes: nbLikes,
      start_date_time: startDateTimeResult.getValue(),
      user: userDetails,
    };

    const sessionResult = Session.create(sessionProps, idResult);

    return sessionResult.isSuccess
      ? {
          isSuccess: true,
          isFailure: false,
          getValue: sessionResult.getValue(),
          getError: null,
        }
      : {
          isSuccess: false,
          isFailure: true,
          getValue: null,
          getError: { cause: 'Wrong value', value: sessionResult.error.toString() },
        };
  }

  public static toMetadaDTO(session: Session): SessionMetadataDTO {
    return {
      activity_name: session.activity.name,
      average_heart_rate: session.average_heart_rate ? session.average_heart_rate.value : null,
      average_speed: session.average_speed ? session.average_speed.speed : null,
      burned_calories: session.burned_calories.value,
      description: session.description ? session.description.value : null,
      distance: session.distance ? session.distance.value : null,
      duration: session.duration.value,
      elevation_gain: session.elevation_gain ? session.elevation_gain.value : null,
      id: session.id.id.toString(),
      images: session.images.length !== 0 ? true : false,
      is_favorite: session.is_favorite,
      gps_track: session.gps_track ? true : false,
      location: session.location ? session.location.value : null,
      name: session.name.value,
      nb_comments: session.nb_comments,
      nb_likes: session.nb_likes,
      start_date_time: session.start_date_time.date_time,
      user_id: session.user.id.toString(),
    };
  }
}
