import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();
import { DateTime as DT } from 'luxon';

import { GPSFile } from '@/app/domains/sessions/gpsFile';
import { TrackMap } from './trackMap';
import { fakeSessionGPX } from '@/fake_data/gpsSession/gpx';
import { fakeSessionJSON } from '@/fake_data/gpsSession/json';
import { fakeSessionMapGraphs } from '@/fake_data/gpsSession/map_graphs';
import { Track } from '@/app/domains/sessions/track';
import { TrackPoint } from '@/app/domains/sessions/trackPoint';
import { DateTime } from '@/app/domains/dateTime';
import { Coordinate } from '@/app/domains/sessions/coordinate';
import { Distance } from '@/app/domains/sessions/distance';
import { Speed } from '@/app/domains/sessions/speed';
import { HeartRate } from '@/app/domains/sessions/heartRate';

const parsedGPSFile = GPSFile.parseGPX(fakeSessionGPX);

// const domainTrack = TrackMap.toDomain(JSON.parse(fakeSessionJSON));

const raw = [
  [
    {
      date_time: DT.fromISO('2023-10-29T15:09').toISO(),
      latitude: 40.741895,
      longitude: -73.989308,
      altitude: 13,
      speed: 2,
      heart_rate: 120,
    },
    {
      date_time: DT.fromISO('2023-10-29T15:11').toISO(),
      latitude: 40.741885,
      longitude: -73.989308,
      altitude: 27,
      speed: 3,
      heart_rate: 130,
    },
    {
      date_time: DT.fromISO('2023-10-29T15:13').toISO(),
      latitude: 40.741875,
      longitude: -73.989308,
      altitude: 30,
      speed: 4,
      heart_rate: 140,
    },
  ],
];

const domain = Track.create({
  track: [
    [
      TrackPoint.create({
        date_time: DateTime.create({ value: DT.fromISO('2023-10-29T15:09').toISO() }).getValue(),
        latitude: Coordinate.create({ value: 40.741895, name: 'latitude' }).getValue(),
        longitude: Coordinate.create({ value: -73.989308, name: 'longitude' }).getValue(),
        altitude: Distance.create({ value: 13, unit: 'm' }).getValue(),
        speed: Speed.create({ value: 2 }).getValue(),
        heart_rate: HeartRate.create({ value: 120 }).getValue(),
      }).getValue(),
      TrackPoint.create({
        date_time: DateTime.create({ value: DT.fromISO('2023-10-29T15:11').toISO() }).getValue(),
        latitude: Coordinate.create({ value: 40.741885, name: 'latitude' }).getValue(),
        longitude: Coordinate.create({ value: -73.989308, name: 'longitude' }).getValue(),
        altitude: Distance.create({ value: 27, unit: 'm' }).getValue(),
        speed: Speed.create({ value: 3 }).getValue(),
        heart_rate: HeartRate.create({ value: 130 }).getValue(),
      }).getValue(),
      TrackPoint.create({
        date_time: DateTime.create({ value: DT.fromISO('2023-10-29T15:13').toISO() }).getValue(),
        latitude: Coordinate.create({ value: 40.741875, name: 'latitude' }).getValue(),
        longitude: Coordinate.create({ value: -73.989308, name: 'longitude' }).getValue(),
        altitude: Distance.create({ value: 30, unit: 'm' }).getValue(),
        speed: Speed.create({ value: 4 }).getValue(),
        heart_rate: HeartRate.create({ value: 140 }).getValue(),
      }).getValue(),
    ],
  ],
}).getValue();

const date_times = [
  DT.fromISO('2023-10-29T15:09').toISO(),
  DT.fromISO('2023-10-29T15:11').toISO(),
  DT.fromISO('2023-10-29T15:13').toISO(),
];
const coordinates = [
  [40.741895, -73.989308],
  [40.741885, -73.989308],
  [40.741875, -73.989308],
];

describe('App / Mappers / Sessions / Track', () => {
  it('toPersistence', () => {
    const result = TrackMap.toPersistence(domain);
    result.should.be.eql(JSON.stringify(raw));
  });

  it('toDTO', () => {
    const result = TrackMap.toDTO(parsedGPSFile.getValue().gps_track);
    result.should.be.eql(JSON.parse(fakeSessionJSON));
  });

  it('toDomain', () => {
    const result = TrackMap.toDomain(raw);
    result.should.eql(domain);
  });

  it('toGrapsAndMap', () => {
    const result = TrackMap.toGrapsAndMap(parsedGPSFile.getValue().gps_track);
    result.should.eql({
      date_times: JSON.parse(fakeSessionMapGraphs).date_times,
      coordinates: JSON.parse(fakeSessionMapGraphs).coordinates,
      altitudes: JSON.parse(fakeSessionMapGraphs).altitudes,
      speeds: JSON.parse(fakeSessionMapGraphs).speeds,
      heart_rates: JSON.parse(fakeSessionMapGraphs).heart_rates,
    });
  });

  it('toCoordinates', () => {
    const result = TrackMap.toCoordinates(domain);

    result.should.eql({
      date_times: date_times,
      coordinates: coordinates,
    });
  });
});
