/* eslint-disable @typescript-eslint/no-explicit-any */
import { DateTime } from '@/app/domains/dateTime';
import { Coordinate } from '@/app/domains/sessions/coordinate';
import { Distance } from '@/app/domains/sessions/distance';
import { HeartRate } from '@/app/domains/sessions/heartRate';
import { Speed } from '@/app/domains/sessions/speed';
import { Track, TrackProps } from '@/app/domains/sessions/track';
import { TrackPoint } from '@/app/domains/sessions/trackPoint';
import { SessionChartsDataDTO, SessionMapDataDTO } from '@/app/dtos/sessionsDTO';
import { Result } from '@/shared/core/Result';
import logger from '@/shared/utils/logger';

export class TrackMap extends Track {
  public static toPersistence(track: Track): string {
    const dto = this.toDTO(track);
    return JSON.stringify(dto);
  }

  public static toDTO(track: Track): any {
    const trackDTO = [];

    const segments = track.track;
    for (let nb_seg = 0; nb_seg < segments.length; nb_seg++) {
      const segment = segments[nb_seg];
      trackDTO[nb_seg] = [];

      for (let nb_pt = 0; nb_pt < segment.length; nb_pt++) {
        const pt = segment[nb_pt];

        let point = {
          date_time: pt.date_time.value,
          latitude: pt.latitude.value,
          longitude: pt.longitude.value,
          altitude: pt.altitude.value,
          // speed: pt.speed ? pt.speed.speed : null,
          // heart_rate: pt.heart_rate ? pt.heart_rate.value : null,
          // distance: pt.distance ? pt.distance.value : null,
        };

        if (pt.speed) {
          point = { ...point, ...{ speed: pt.speed.speed } };
        }

        if (pt.heart_rate) {
          point = { ...point, ...{ heart_rate: pt.heart_rate.value } };
        }

        if (pt.distance) {
          point = { ...point, ...{ distance: pt.distance.value } };
        }

        trackDTO[nb_seg][nb_pt] = point;
      }
    }

    logger.debug(`[Mappers/Track] toDTO - trackDTO[0][1]: ${JSON.stringify(trackDTO[0][1])}`);
    return trackDTO;
  }

  public static toDomain(track: any): Track {
    logger.debug(`[Mappers/Track] toDomain - track.length: ${track.length}`);

    const segments = [];
    for (let nb_seg = 0; nb_seg < track.length; nb_seg++) {
      logger.debug(`[Mappers/Track] toDomain - track${nb_seg}`);
      const segment = track[nb_seg];
      logger.debug(`[Mappers/Track] toDomain - segment.length: ${segment.length}`);
      segments[nb_seg] = [];

      for (let nb_pt = 0; nb_pt < segment.length; nb_pt++) {
        const pt = segment[nb_pt];
        // logger.debug(`[Mappers/Track] toDomain - Segment/Point: ${nb_seg}/${nb_pt}`);
        // logger.debug(`[Mappers/Track] toDomain - pt: ${JSON.stringify(pt)}`);

        const dateTimeResult = DateTime.create({ value: pt.date_time });
        const latitudeResult = Coordinate.create({ value: pt.latitude, name: 'latitude' });
        const longitudeResult = Coordinate.create({ value: pt.longitude, name: 'longitude' });
        const altitudeResult = Distance.create({ value: pt.altitude });

        const domainResult = Result.combine([
          dateTimeResult,
          latitudeResult,
          longitudeResult,
          altitudeResult,
        ]);
        // logger.debug(`[Mappers/Track] toDomain - domainResult: ${JSON.stringify(domainResult)}`);
        if (domainResult.isFailure) {
          logger.error(
            `[Mappers/Track] toDomain Error - Segment/Point ${nb_seg}/${nb_pt}: ${domainResult.errorValue()}`
          );
          return null;
        }

        let point = {
          date_time: dateTimeResult.getValue(),
          latitude: latitudeResult.getValue(),
          longitude: longitudeResult.getValue(),
          altitude: altitudeResult.getValue(),
        };

        if (pt.speed) {
          const speedResult = Speed.create({ value: pt.speed });
          if (speedResult.isFailure) {
            logger.error(
              `[Mappers/Track] toDomain Error - Segment/Point ${nb_seg}/${nb_pt} - Speed: ${speedResult.errorValue()}`
            );
            return null;
          }
          point = { ...point, ...{ speed: speedResult.getValue() } };
        }

        if (pt.heart_rate) {
          const hearRateResult = HeartRate.create({ value: pt.heart_rate });
          if (hearRateResult.isFailure) {
            logger.error(
              `[Mappers/Track] toDomain Error - Segment/Point ${nb_seg}/${nb_pt} - Heart Rate: ${hearRateResult.errorValue()}`
            );
            return null;
          }
          point = { ...point, ...{ heart_rate: hearRateResult.getValue() } };
        }

        if (pt.distance) {
          const distanceResult = Distance.create({ value: pt.distance });
          if (distanceResult.isFailure) {
            logger.error(
              `[Mappers/Track] toDomain Error - Segment/Point ${nb_seg}/${nb_pt} - Distance: ${distanceResult.errorValue()}`
            );
            return null;
          }
          point = { ...point, ...{ distance: distanceResult.getValue() } };
        }
        // logger.debug(`[Mappers/Track] toDomain - point: ${JSON.stringify(point)}`);
        const trackPointResult = TrackPoint.create(point);
        // logger.debug(
        //   `[Mappers/Track] toDomain - trackPointResult: ${JSON.stringify(trackPointResult)}`
        // );
        if (trackPointResult.isFailure) {
          logger.error(
            `[Mappers/Track] toDomain Error - Segment/Point ${nb_seg}/${nb_pt}: ${trackPointResult.errorValue()} - ${JSON.stringify(
              point
            )}`
          );
          return null;
        }
        segments[nb_seg].push(trackPointResult.getValue());
        // trackProps.track[nb_seg][nb_pt] = trackPointResult.getValue();
      }
    }
    logger.debug(`[Mappers/Track] toDomain - segments[0][1]: ${JSON.stringify(segments[0][1])}`);
    const trackProps: TrackProps = { track: segments };
    const trackResult = Track.create(trackProps);

    logger.debug(
      `[Mappers/Track] toDomain - trackResult.isSuccess: ${JSON.stringify(trackResult.isSuccess)}`
    );
    // logger.debug(
    //   `[Mappers/Track] toDomain - trackResult.getValue(): ${JSON.stringify(trackResult.getValue())}`
    // );
    if (trackResult.isFailure) {
      logger.error(`[Mappers/Track] toDomain Error - Track: ${trackResult.errorValue()}`);
      return null;
    }

    return trackResult.getValue();
  }

  /*public static toGraphs(track: Track): any {
    const graphs = [];

    const segments = track.track;
    for (let nb_seg = 0; nb_seg < segments.length; nb_seg++) {
      const segment = segments[nb_seg];

      for (let nb_pt = 0; nb_pt < segment.length; nb_pt++) {
        const pt = segment[nb_pt];

        graphs.push({
          date_time: pt.date_time.value,
          altitude: pt.altitude.value,
          speed: pt.speed ? pt.speed.speed : null,
          heart_rate: pt.heart_rate ? pt.heart_rate.value : null,
        });
      }
    }

    return graphs;
  }*/

  public static toMap(track: Track): SessionMapDataDTO {
    const date_times = [];
    const coordinates = [];

    const segments = track.track;
    for (let nb_seg = 0; nb_seg < segments.length; nb_seg++) {
      const segment = segments[nb_seg];

      for (let nb_pt = 0; nb_pt < segment.length; nb_pt++) {
        const pt = segment[nb_pt];
        if (nb_seg === 0 && nb_pt === 10)
          logger.debug(`[Mappees/TrackMap] toCoordinates - pt ${JSON.stringify(pt)}`);

        date_times.push(pt.date_time.value);
        coordinates.push([pt.latitude.value, pt.longitude.value]);
      }
    }
    return {
      date_times,
      coordinates,
    };
  }

  public static toGrapsAndMap(track: Track): any {
    const date_times = [];
    const coordinates = [];
    const altitudes = [];
    const speeds = [];
    const heart_rates = [];

    const segments = track.track;
    for (let nb_seg = 0; nb_seg < segments.length; nb_seg++) {
      const segment = segments[nb_seg];

      for (let nb_pt = 0; nb_pt < segment.length; nb_pt++) {
        const pt = segment[nb_pt];

        date_times.push(pt.date_time.value);
        coordinates.push([pt.latitude.value, pt.longitude.value]);
        altitudes.push(pt.altitude.value);
        speeds.push(pt.speed ? pt.speed.speed : null);
        if (pt.heart_rate) {
          heart_rates.push(pt.heart_rate.value);
        }
      }
    }

    return {
      date_times,
      coordinates,
      altitudes,
      speeds,
      heart_rates: heart_rates.length !== 0 ? heart_rates : false,
    };
  }

  public static toCharts(track: Track): SessionChartsDataDTO {
    const date_times = [];
    const altitudes = [];
    const speeds = [];
    const heart_rates = [];

    const segments = track.track;
    for (let nb_seg = 0; nb_seg < segments.length; nb_seg++) {
      const segment = segments[nb_seg];

      for (let nb_pt = 0; nb_pt < segment.length; nb_pt++) {
        const pt = segment[nb_pt];

        date_times.push(pt.date_time.value);
        altitudes.push(pt.altitude.value);
        speeds.push(pt.speed ? pt.speed.speed : null);
        if (pt.heart_rate) {
          heart_rates.push(pt.heart_rate.value);
        }
      }
    }

    return {
      date_times,
      altitudes,
      speeds,
      heart_rates: heart_rates.length !== 0 ? heart_rates : null,
    };
  }

  public static toCoordinates(track: Track) {
    const date_times = [];
    const coordinates = [];

    const segments = track.track;
    for (let nb_seg = 0; nb_seg < segments.length; nb_seg++) {
      const segment = segments[nb_seg];

      for (let nb_pt = 0; nb_pt < segment.length; nb_pt++) {
        const pt = segment[nb_pt];
        if (nb_seg === 0 && nb_pt === 10)
          logger.debug(`[Mappers/TrackMap] toCoordinates - pt ${JSON.stringify(pt)}`);

        date_times.push(pt.date_time.value);
        coordinates.push([pt.latitude.value, pt.longitude.value]);
      }
    }

    return {
      date_times,
      coordinates,
    };
  }
}
