import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from '@/shared/core/Result';
import { Image } from './image';
import { fakeAvatarsData } from '@/fake_data/profilesAvatars';

let imageResult: Result<Image>;

describe('App / Domains / Image', () => {
  describe('create', () => {
    it('should return an Image', () => {
      imageResult = Image.create({
        mimetype: fakeAvatarsData.casquette_verte.type,
        data: fakeAvatarsData.casquette_verte.data,
      });
      imageResult.isSuccess.should.be.true;
      imageResult.getValue().mimetype.should.equal(fakeAvatarsData.casquette_verte.type);
      imageResult.getValue().data.should.equal(fakeAvatarsData.casquette_verte.data);
    });
  });
});
