import { ValueObject } from '@/shared/domains/ValueObject';
import { Result } from '@/shared/core/Result';
import { Guard } from '@/shared/core/Guard';

interface LongTextProps {
  value: string;
  name: string;
}

export class LongText extends ValueObject<LongTextProps> {
  public static minLength = 2;
  public static maxLength = 10000;

  get value(): string {
    return this.props.value;
  }

  private constructor(props: LongTextProps) {
    super(props);
  }

  public static create(props: LongTextProps): Result<LongText> {
    const nullGuardResult = Guard.againstNullOrUndefined(props.value, props.name);

    if (!nullGuardResult.succeeded) {
      return Result.fail<LongText>(nullGuardResult.message);
    }

    const minGuardResult = Guard.againstAtLeast(this.minLength, props.value, props.name);
    const maxGuardResult = Guard.againstAtMost(this.maxLength, props.value, props.name);

    if (!minGuardResult.succeeded) {
      return Result.fail<LongText>(minGuardResult.message);
    }

    if (!maxGuardResult.succeeded) {
      return Result.fail<LongText>(maxGuardResult.message);
    }

    return Result.ok<LongText>(new LongText(props));
  }
}
