import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from '@/shared/core/Result';
import { LongText } from './longText';

let long_text: LongText;
let longTextResult: Result<LongText>;

describe('Modules / App / Domain / Session / LongText', () => {
  it('should pass if length is between 2 and 10000.', () => {
    longTextResult = LongText.create({ value: 'xxx', name: 'xxx' });
    longTextResult.isSuccess.should.be.true;
  });

  it('should fail if argument is null or undefined.', () => {
    longTextResult = LongText.create({ value: null, name: 'xxx' });
    longTextResult.isSuccess.should.be.false;
  });

  it('should fail if length is less than 2.', () => {
    longTextResult = LongText.create({ value: 'x', name: 'xxx' });
    longTextResult.isSuccess.should.be.false;
  });

  it('should fail if length is greater than 10000.', () => {
    longTextResult = LongText.create({
      value: Array(11000).join('x'),
      name: 'xxx',
    });
    longTextResult.isSuccess.should.be.false;
  });

  it('should return a message if fails if length is greater than 10000.', () => {
    longTextResult = LongText.create({ value: 'x', name: 'xxx' });
    longTextResult.error.should.equal('Text xxx is not at least 2 chars.');
  });

  it('should return value', () => {
    longTextResult = LongText.create({ value: 'xxx', name: 'xxx' });
    long_text = longTextResult.getValue();
    long_text.value.should.equal('xxx');
  });
});
