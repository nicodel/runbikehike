import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from '@/shared/core/Result';
import { DateTime } from './dateTime';

let dateTimeResult: Result<DateTime>;

const now = new Date().toISOString();

describe('Modules / App / Domain / Session / DateTime', () => {
  it('should be able to create a DateTime.', () => {
    dateTimeResult = DateTime.create({ value: now });
    dateTimeResult.isSuccess.should.be.true;
    dateTimeResult.getValue().value.should.equal(now);
  });

  it('should fail if value is null or undefined.', () => {
    dateTimeResult = DateTime.create({ value: null });
    dateTimeResult.isSuccess.should.be.false;
  });

  it('should fail if value is an invalid Date string.', () => {
    dateTimeResult = DateTime.create({ value: 'xxx' });
    dateTimeResult.isSuccess.should.be.false;
    // eslint-disable-next-line quotes
    dateTimeResult.error.should.equal("'xxx' is not a valid Date string.");
  });

  it('should fail if value is a future Date.', () => {
    const year = new Date(now).getFullYear() + 1;
    const futur = new Date(new Date(now).setFullYear(year)).toISOString();
    dateTimeResult = DateTime.create({ value: futur });
    dateTimeResult.isSuccess.should.be.false;
    dateTimeResult.error.should.equal(`'${futur}' is a future date, and cannot be used.`);
  });
});
