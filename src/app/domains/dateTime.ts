import { Guard } from '@/shared/core/Guard';
import { Result } from '@/shared/core/Result';
import { ValueObject } from '@/shared/domains/ValueObject';

interface DateTimeProps {
  value: string;
}

export class DateTime extends ValueObject<DateTimeProps> {
  private constructor(props: DateTimeProps) {
    super(props);
  }

  get value(): string {
    return this.props.value;
  }

  get date_time(): string {
    return this.props.value;
  }

  public static create(props: DateTimeProps): Result<DateTime> {
    const nullGuardResult = Guard.againstNullOrUndefined(props.value, 'date_time');

    if (!nullGuardResult.succeeded) {
      return Result.fail<DateTime>(nullGuardResult.message);
    }

    const date_time = new Date(props.value);

    if (date_time.toString() === 'Invalid Date') {
      return Result.fail<DateTime>(`'${props.value}' is not a valid Date string.`);
    }

    const now = new Date();
    if (date_time > now) {
      return Result.fail<DateTime>(`'${props.value}' is a future date, and cannot be used.`);
    }

    return Result.ok<DateTime>(new DateTime(props));
  }
}
