import { ValueObject } from '@/shared/domains/ValueObject';
import { Result } from '@/shared/core/Result';
import { Guard } from '@/shared/core/Guard';

export interface EmailProps {
  value: string;
}

export class Email extends ValueObject<EmailProps> {
  get value(): string {
    return this.props.value;
  }

  private constructor(props: EmailProps) {
    super(props);
  }

  private static isValidEmail(email: string) {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  private static format(email: string): string {
    return email.trim().toLowerCase();
  }

  public static create(props: EmailProps): Result<Email> {
    const nullGuardResult = Guard.againstNullOrUndefined(props.value, 'email');

    if (!nullGuardResult.succeeded) {
      return Result.fail<Email>(nullGuardResult.message);
    }

    if (!this.isValidEmail(props.value)) {
      return Result.fail<Email>('Email address not valid');
    } else {
      return Result.ok<Email>(new Email({ value: this.format(props.value) }));
    }
  }
}
