import { Entity } from '@/shared/domains/Entity';
import { Result } from '@/shared/core/Result';
import { Guard } from '@/shared/core/Guard';

import { ShortText } from './../shortText';
import { Gender } from './gender';
import { Birthyear } from './birthyear';
import { Height } from './height';
import { Weight } from './weight';
import { UniqueId } from './../uniqueId';
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';
import { LongText } from './../longText';
import { User } from '../auth/user';
import { Image } from '../image';

export interface ProfileProps {
  name: ShortText;
  gender: Gender;
  birthyear: Birthyear;
  height: Height;
  weight: Weight;
  location?: ShortText;
  biography?: LongText;
  user: User;
  avatar?: Image;
}

export class Profile extends Entity<ProfileProps> {
  constructor(props: ProfileProps, id?: UniqueEntityID) {
    super(props, id);
  }

  get id(): UniqueId {
    return UniqueId.create(this._id).getValue();
  }

  get name(): ShortText {
    return this.props.name;
  }

  get gender(): Gender {
    return this.props.gender;
  }

  get birthyear(): Birthyear {
    return this.props.birthyear;
  }

  get height(): Height {
    return this.props.height;
  }

  get weight(): Weight {
    return this.props.weight;
  }

  get location(): ShortText {
    return this.props.location;
  }

  get biography(): LongText {
    return this.props.biography;
  }

  get user(): User {
    return this.props.user;
  }

  get avatar(): Image {
    return this.props.avatar;
  }

  public static create(props: ProfileProps, id?: UniqueEntityID): Result<Profile> {
    const nullGuard = Guard.againstNullOrUndefinedBulk([
      { argument: props.name, argumentName: 'name' },
      { argument: props.gender, argumentName: 'gender' },
      { argument: props.birthyear, argumentName: 'birthyear' },
      { argument: props.height, argumentName: 'height' },
      { argument: props.weight, argumentName: 'weight' },
      { argument: props.user, argumentName: 'user' },
    ]);

    if (!nullGuard.succeeded) {
      return Result.fail<Profile>(nullGuard.message);
    }

    const defaultProfileProps: ProfileProps = {
      ...props,
    };

    const profile = new Profile(defaultProfileProps, id);

    return Result.ok<Profile>(profile);
  }
}
