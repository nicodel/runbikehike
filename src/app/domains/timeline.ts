import { Guard } from '@/shared/core/Guard';
import logger from '@/shared/utils/logger';
import { Entity } from '@/shared/domains/Entity';
import { Result } from '@/shared/core/Result';
import { SessionItemDTO } from '../dtos/timelineItemsDTO';

export interface TimelineProps {
  timeline: SessionItemDTO[];
}

export class Timeline extends Entity<TimelineProps> {
  constructor(props: TimelineProps) {
    super(props);
  }

  get timeline(): SessionItemDTO[] {
    return this.timeline;
  }

  public static create(props: TimelineProps): Result<Timeline> {
    for (let index = 0; index < props.timeline.length; index++) {
      const item = props.timeline[index];
      const nullGuard = Guard.againstNullOrUndefined(item, item.id);
      if (!nullGuard.succeeded) {
        logger.error('[Domain/Sessions/Session] create - Error: %s', nullGuard.message);
        return Result.fail<Timeline>(nullGuard.message);
      }
    }

    const timeline = new Timeline(props);

    return Result.ok<Timeline>(timeline);
  }
}
