import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from '@/shared/core/Result';

import { fakeComments } from '@/fake_data/comments';

import { Comment } from './comment';

let commentResult: Result<Comment>;

describe('App / Domains / Comment', () => {
  describe('create', () => {
    it('should return a Comment', () => {
      commentResult = Comment.create({
        date_time: fakeComments[0].date_time,
        value: fakeComments[0].text,
        user_id: fakeComments[0].user.id,
        session_id: fakeComments[0].session.id,
      });
      commentResult.isSuccess.should.be.true;
      commentResult.getValue().value.should.be.equal(fakeComments[0].text);
    });
  });
});
