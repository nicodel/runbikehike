import { Guard } from '@/shared/core/Guard';
import { ValueObject } from '@/shared/domains/ValueObject';
import { Result } from '@/shared/core/Result';

import { TrackPoint } from './trackPoint';

export interface TrackProps {
  track: TrackPoint[][];
}

export class Track extends ValueObject<TrackProps> {
  constructor(props: TrackProps) {
    super(props);
  }

  get track(): TrackPoint[][] {
    return this.props.track;
  }

  get asJSON(): string {
    return JSON.stringify(this.props.track);
  }

  public static create(props: TrackProps): Result<Track> {
    const nullGuard = Guard.againstNullOrUndefined(props.track, 'track');

    if (!nullGuard.succeeded) {
      return Result.fail<Track>(nullGuard.message);
    }

    const trackProps: TrackProps = {
      ...props,
    };

    const track = new Track(trackProps);
    return Result.ok<Track>(track);
  }
}
