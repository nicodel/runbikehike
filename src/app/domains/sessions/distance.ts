import { ValueObject } from '@/shared/domains/ValueObject';
import { Result } from '@/shared/core/Result';
import { Guard } from '@/shared/core/Guard';

interface DistanceProps {
  value: number;
  unit?: string;
}

export class Distance extends ValueObject<DistanceProps> {
  private constructor(props: DistanceProps) {
    super(props);
  }

  get value(): number {
    return this.props.value;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private static isValidFloatNumber(raw: any): boolean {
    return !isNaN(parseFloat(raw));
  }

  public static create(props: DistanceProps): Result<Distance> {
    const nullGuardResult = Guard.againstNullOrUndefined(props.value, 'distance');

    if (!nullGuardResult.succeeded) {
      return Result.fail<Distance>(nullGuardResult.message);
    }

    const nanGuardResult = Guard.againstNullOrNaN(props.value, 'distance');
    if (!nanGuardResult.succeeded) {
      return Result.fail<Distance>('Distance should be a number.');
    }

    if (!this.isValidFloatNumber(props.value)) {
      return Result.fail<Distance>('Distance should be a number.');
    }

    const distance = new Distance({
      value: props.value,
      unit: 'm',
    });

    return Result.ok<Distance>(distance);
  }
}
