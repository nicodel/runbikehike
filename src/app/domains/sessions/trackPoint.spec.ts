import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { TrackPoint } from './trackPoint';
import { DateTime } from './../dateTime';
import { Coordinate } from './coordinate';
import { Distance } from './distance';
import { Speed } from './speed';
import { HeartRate } from './heartRate';

let trackPointProps;

describe('Modules / App / Domain / Session / TrackPoint', () => {
  beforeEach(() => {
    trackPointProps = {
      date_time: DateTime.create({ value: '2019-10-26T14:22:17.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72860253788530826568603515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2979445196688175201416015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 99.1999969482421875 }).getValue(),
      speed: Speed.create({ value: 9.499 }).getValue(),
      heart_rate: HeartRate.create({ value: 133 }).getValue(),
      previous_point: {
        latitude: Coordinate.create({
          value: 48.72849022038280963897705078125,
          name: 'latitude',
        }).getValue(),
        longitude: Coordinate.create({
          value: 2.2978114150464534759521484375,
          name: 'longitude',
        }).getValue(),
        // altitude: 98.59999847412109375
        date_time: DateTime.create({ value: '2019-10-26T14:22:11.000Z' }).getValue(),
      },
    };
  });

  it('should return trackPoint', () => {
    const trackPointResult = TrackPoint.create(trackPointProps);
    trackPointResult.isSuccess.should.be.true;
  });

  it('should return trackPoint, even if "speed" is missing', () => {
    delete trackPointProps['speed'];
    const trackPointResult = TrackPoint.create(trackPointProps);
    trackPointResult.isSuccess.should.be.true;
  });

  it('should return trackPoint, even if "heart_rate" is missing', () => {
    delete trackPointProps['heart_rate'];
    const trackPointResult = TrackPoint.create(trackPointProps);
    trackPointResult.isSuccess.should.be.true;
  });

  it('should return error, if "date_time" is missing', () => {
    delete trackPointProps['date_time'];
    const trackPointResult = TrackPoint.create(trackPointProps);
    trackPointResult.isSuccess.should.be.false;
  });

  it('should return error, if "latitude" is missing', () => {
    delete trackPointProps['latitude'];
    const trackPointResult = TrackPoint.create(trackPointProps);
    trackPointResult.isSuccess.should.be.false;
  });

  it('should return error, if "longitude" is missing', () => {
    delete trackPointProps['longitude'];
    const trackPointResult = TrackPoint.create(trackPointProps);
    trackPointResult.isSuccess.should.be.false;
  });

  it('should return error, if "altitude" is missing', () => {
    delete trackPointProps['altitude'];
    const trackPointResult = TrackPoint.create(trackPointProps);
    trackPointResult.isSuccess.should.be.false;
  });

  it('should return trackPoint, with calculated distance', () => {
    const trackPointResult = TrackPoint.create(trackPointProps);
    trackPointResult.isSuccess.should.be.true;
    trackPointResult.getValue().props.distance.value.should.equal(15.852186461242198);
  });

  it('should return trackPoint, with calculated speed', () => {
    delete trackPointProps['speed'];
    const trackPointResult = TrackPoint.create(trackPointProps);
    trackPointResult.isSuccess.should.be.true;
    trackPointResult.getValue().props.speed.speed.should.equal(2.6420310768736996);
  });

  it('should return trackPoint, if "previous_point" is missing', () => {
    delete trackPointProps['previous_point'];
    const trackPointResult = TrackPoint.create(trackPointProps);
    trackPointResult.isSuccess.should.be.true;
  });
});
