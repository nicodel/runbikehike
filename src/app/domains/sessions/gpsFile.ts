import { Result } from '@/shared/core/Result';
import { Guard } from '@/shared/core/Guard';
import { ValueObject } from '@/shared/domains/ValueObject';

import { XMLParser } from 'fast-xml-parser';

import { DateTime } from '../dateTime';
import { LongText } from '../longText';
import { ShortText } from '../shortText';
import { Distance } from './distance';
import { Duration } from './duration';
import { Speed } from './speed';

import logger from '@/shared/utils/logger';
import { Coordinate } from './coordinate';
import { HeartRate } from './heartRate';
import { TrackPoint } from './trackPoint';
import { Track } from './track';

export interface GPSFileProps {
  name: ShortText;
  start_date_time: DateTime;
  duration: Duration;
  distance: Distance;
  average_speed: Speed;
  description?: LongText;
  gps_track: Track;
  activity_name: string;
}

export class GPSFile extends ValueObject<GPSFileProps> {
  private constructor(gpsFile: GPSFileProps) {
    super(gpsFile);
  }

  get name(): ShortText {
    return this.props.name;
  }

  get start_date_time(): DateTime {
    return this.props.start_date_time;
  }

  get duration(): Duration {
    return this.props.duration;
  }

  get distance(): Distance {
    return this.props.distance;
  }

  get average_speed(): Speed {
    return this.props.average_speed;
  }

  get description(): LongText {
    return this.props.description;
  }

  get gps_track(): Track {
    return this.props.gps_track;
  }

  get activity_name(): string {
    return this.props.activity_name;
  }

  private static _extractData(key: string, json: object) {
    // logger.debug(`[Domains/GPSFile] _extractData - key: ${JSON.stringify(key)}`);
    // logger.debug(`[Domains/GPSFile] _extractData - json: ${JSON.stringify(json)}`);
    // logger.debug(`[Domains/GPSFile] _extractData - json[${key}]: ${JSON.stringify(json[key])}`);
    if (!json[key]) {
      return { isFailure: true, isSuccess: false, value: null };
    }
    return { isFailure: false, isSuccess: true, value: json[key] };
  }

  private static _nameSession(start_date_time: DateTime, activity_name: string): string {
    // logger.debug(
    //   `[TEST _nameSession] latitudeResult: ${JSON.stringify(
    //     ShortText.create({
    //       value: `${activity_name} - ${new Date(start_date_time.value).toLocaleString()}`,
    //       name: 'session.name',
    //     })
    //   )}`
    // );

    return ShortText.create({
      value: `${activity_name} - ${new Date(start_date_time.value).toLocaleString()}`,
      name: 'session.name',
    }).getValue().value;
  }

  public static parseGPX(jsonFile: string): Result<GPSFile> {
    // logger.debug(`[Domains/GPSFile] parseGPX - jsonFile: ${JSON.stringify(jsonFile)}`);

    const alwaysArray = ['gpx.trk.trkseg', 'gpx.trk.trkseg.trkpt'];

    const parser = new XMLParser({
      ignoreAttributes: false,
      attributeNamePrefix: '',
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      isArray: (name, jpath, isLeafNode, isAttribute) => {
        if (alwaysArray.indexOf(jpath) !== -1) return true;
      },
    });
    // logger.debug(
    //   `[Domains/GPSFile] parseGPX - parser.parse(jsonFile): ${JSON.stringify(
    //     parser.parse(jsonFile)
    //   )}`
    // );
    const nullGuardResult = Guard.againstNullOrUndefined(jsonFile, 'jsonFile');
    if (!nullGuardResult.succeeded) {
      logger.error(`[Domains/GPSFile] parseGPX- GPX Error: ${JSON.stringify(nullGuardResult)}`);
      return Result.fail<GPSFile>(nullGuardResult.message);
    }

    let parsedProps = null;
    try {
      // parsedProps = JSON.parse(jsonFile);
      parsedProps = parser.parse(jsonFile);
    } catch (err) {
      logger.error(`[Domains/GPSFile] parseGPX: Provided file is not JSON: ${err}`);
      return Result.fail<GPSFile>(`Provided file is not JSON: ${err}`);
    }

    const gpxResult = this._extractData('gpx', parsedProps);
    if (gpxResult.isFailure) {
      logger.error('[Domains/GPSFile] parseGPX: Provided JSON file is missing "gpx" key.');
      return Result.fail<GPSFile>('Provided JSON file is missing "gpx" key.');
    }

    const metadataParsed = this._extractData('metadata', parsedProps['gpx']);
    let startDateTimeResult = null;
    let endDateTimeResult = null;
    let descriptionResult = null;
    let nameResult = null;
    if (metadataParsed.isSuccess) {
      const metadata = metadataParsed.value;

      const timeParsed = this._extractData('time', metadata);
      startDateTimeResult = timeParsed.isSuccess
        ? DateTime.create({ value: timeParsed.value })
        : null;

      const descParsed = this._extractData('desc', metadata);
      descriptionResult = descParsed.isSuccess
        ? LongText.create({ value: descParsed.value, name: 'description' })
        : null;

      const nameParsed = this._extractData('name', metadata);
      nameResult = nameParsed.isSuccess
        ? ShortText.create({ value: nameParsed.value, name: 'session.name' })
        : null;
    }

    const trkParsed = this._extractData('trk', parsedProps['gpx']);
    if (trkParsed.isFailure) {
      logger.error('[Domains/GPSFile] parseGPX: Provided JSON file is missing "trk" key.');
      return Result.fail<GPSFile>('Provided JSON file is missing "trk" key.');
    }
    const trk = trkParsed.value;

    const trkNameParsed = this._extractData('name', trk);
    nameResult = trkNameParsed.isSuccess
      ? ShortText.create({ value: trkNameParsed.value, name: 'session.name' })
      : nameResult;

    const typeParsed = this._extractData('type', trk);
    const activityName = typeParsed.isSuccess ? typeParsed.value : 'unknown';

    const trkDescParsed = this._extractData('desc', trk);
    if (!descriptionResult) {
      descriptionResult = trkDescParsed.isSuccess
        ? LongText.create({ value: trkDescParsed.value, name: 'description' })
        : null;
    }

    /*
     * Extracting file Track Segment(s) (trkseg) to create tracks
     */
    const trksegParsed = this._extractData('trkseg', trk);
    if (trksegParsed.isFailure) {
      logger.error('[Domains/GPSFile] parseGPX: Provided JSON file is missing "trkseg" key.');
      return Result.fail<GPSFile>('Provided JSON file is missing "trkseg" key.');
    }
    const trksegs = trksegParsed.value;
    if (trksegs.length === 0) {
      logger.error('[Domains/GPSFile] parseGPX: Provided JSON file has an empty "trkseg" key.');
      return Result.fail<GPSFile>('Provided JSON file has an empty "trkseg" key.');
    }
    // logger.debug(`[TEST gpsFile] trksegs: ${JSON.stringify(trksegs)}`);
    // logger.debug(`[TEST gpsFile] trksegs.length: ${JSON.stringify(trksegs.length)}`);

    const trackProps = [];
    let distance = 0;

    for (let seg_nb = 0; seg_nb < trksegs.length; seg_nb++) {
      const seg = trksegs[seg_nb];
      const segmentsProps = [];

      /*
       * Extracting file Track Point(s) (trkpt)
       */
      const trkptParsed = this._extractData('trkpt', seg);
      if (trkptParsed.isSuccess) {
        const trkpts = trkptParsed.value;
        if (trkpts.length === 0) {
          logger.error('[Domains/GPSFile] parseGPX: Provided JSON file has an empty "trkpt" key.');
          return Result.fail<GPSFile>('Provided JSON file has an empty "trkpt" key.');
        }
        // logger.debug(`[TEST gpsFile] trkpts: ${JSON.stringify(trkpts)}`);
        // logger.debug(`[TEST gpsFile] trkpts.length: ${JSON.stringify(trkpts.length)}`);

        let previousPoint = null;
        for (let pt_nb = 0; pt_nb < trkpts.length; pt_nb++) {
          const pt = trkpts[pt_nb];
          // logger.debug(`[TEST gpsFile] pt: ${JSON.stringify(pt)}`);

          let speedResult = null;
          let heartRateResult = null;

          /*
           * Trackpoint - Geoposition (latitude & longitude)
           */
          const latitudeParsed = this._extractData('lat', pt);
          if (latitudeParsed.isFailure) {
            logger.error('[Domains/GPSFile] parseGPX: Provided JSON file is missing "lat" key.');
            return Result.fail<GPSFile>('Provided JSON file is missing "lat" key.');
          }
          const latitudeResult = Coordinate.create({
            name: 'latitude',
            value: latitudeParsed.value,
          });
          if (latitudeResult.isFailure) {
            logger.error(
              '[Domains/GPSFile] parseGPX: Provided JSON file has a wrong "latitude" value.'
            );
            return Result.fail<GPSFile>('Provided JSON file has a wrong "latitude" value.');
          }

          const longitudeParsed = this._extractData('lon', pt);
          if (longitudeParsed.isFailure) {
            logger.error('[Domains/GPSFile] parseGPX: Provided JSON file is missing "lon" key.');
            return Result.fail<GPSFile>('Provided JSON file is missing "lon" key.');
          }
          const longitudeResult = Coordinate.create({
            name: 'longitude',
            value: longitudeParsed.value,
          });
          if (longitudeResult.isFailure) {
            logger.error(
              '[Domains/GPSFile] parseGPX: Provided JSON file has a wrong "longitude" value.'
            );
            return Result.fail<GPSFile>('Provided JSON file has a wrong "longitude" value.');
          }

          /*
           * Trackpoint - Time
           */
          const timeParsed = this._extractData('time', pt);
          if (timeParsed.isFailure) {
            logger.error('[Domains/GPSFile] parseGPX: Provided JSON file is missing "time" key.');
            return Result.fail<GPSFile>('Provided JSON file is missing "time" key.');
          }
          const DateTimeResult = DateTime.create({ value: timeParsed.value });
          if (DateTimeResult.isFailure) {
            logger.error(
              '[Domains/GPSFile] parseGPX: Provided JSON file has a wrong "time" value.'
            );
            return Result.fail<GPSFile>('Provided JSON file has a wrong "time" value.');
          }
          if (!startDateTimeResult) {
            if (seg_nb === 0 && pt_nb === 0) {
              startDateTimeResult = DateTimeResult;
            }
          }
          endDateTimeResult = DateTimeResult;

          /*
           * Trackpoint - Altitude (ele)
           * TODO: Positive Climb / Negative Climb
           */
          const altitudParsed = this._extractData('ele', pt);
          if (altitudParsed.isFailure) {
            logger.error('[Domains/GPSFile] parseGPX: Provided JSON file is missing "ele" key.');
            return Result.fail<GPSFile>('Provided JSON file is missing "ele" key.');
          }
          const altitudeResult = Distance.create({ value: altitudParsed.value });
          if (altitudeResult.isFailure) {
            logger.error(
              '[Domains/GPSFile] parseGPX: Provided JSON file has a wrong "altitide" value.'
            );
            return Result.fail<GPSFile>('Provided JSON file has a wrong "altitide" value.');
          }
          /*
           * Trackpoint - Speed (speed)
           */
          const speedParsed = this._extractData('speed', pt);
          if (speedParsed.isSuccess) {
            speedResult = Speed.create({ value: speedParsed.value });
            if (speedResult.isFailure) {
              logger.error(
                '[Domains/GPSFile] parseGPX: Provided JSON file has a wrong "speed" value.'
              );
              return Result.fail<GPSFile>('Provided JSON file has a wrong "speed" value.');
            }
          }

          /*
           * Extensions
           */
          const extensionsParsed = this._extractData('extensions', pt);
          if (extensionsParsed.isSuccess) {
            const extensions = extensionsParsed.value;

            /*
             * Trackpoint - Extensions ns3:TrackPointExtension - GARMIN GPX Files
             */
            const ns3Parsed = this._extractData('ns3:TrackPointExtension', extensions);
            if (ns3Parsed.isSuccess) {
              const ns3 = ns3Parsed.value;

              /*
               * Trackpoint - Extensions ns3:TrackPointExtension - Speed
               */
              const speedExtensionParsed = this._extractData('ns3:speed', ns3);
              if (speedExtensionParsed.isSuccess) {
                const speedExtensionResult = Speed.create({ value: speedExtensionParsed.value });
                if (speedExtensionResult.isFailure) {
                  logger.error(
                    '[Domains/GPSFile] parseGPX: Provided JSON file has a wrong "ns3:speed" value.'
                  );
                  return Result.fail<GPSFile>('Provided JSON file has a wrong "ns3:speed" value.');
                }
                speedResult = speedExtensionResult;
              }

              /*
               * Trackpoint - Extensions ns3:TrackPointExtension - Heart Rate (hr)
               */
              const heartRateParsed = this._extractData('ns3:hr', ns3);
              if (heartRateParsed.isSuccess) {
                heartRateResult = HeartRate.create({ value: heartRateParsed.value });
                if (heartRateResult.isFailure) {
                  logger.error(
                    '[Domains/GPSFile] parseGPX: Provided JSON file has a wrong "ns3:hr" value.'
                  );
                  return Result.fail<GPSFile>('Provided JSON file has a wrong "ns3:hr" value.');
                }
              }

              /*
                * Trackpoint - Extensions ns3:TrackPointExtension - STRAVA GPX Files
                *  <gpxtpx:TrackPointExtension>
                      <gpxtpx:atemp>18</gpxtpx:atemp>
                      <gpxtpx:hr>122</gpxtpx:hr>
                      <gpxtpx:cad>80</gpxtpx:cad>
                      </gpxtpx:TrackPointExtension>
                */
            }
          }

          const trackPointProps = {
            date_time: DateTimeResult.getValue(),
            latitude: latitudeResult.getValue(),
            longitude: longitudeResult.getValue(),
            altitude: altitudeResult.getValue(),
            speed: speedResult ? speedResult.getValue() : null,
            heart_rate: heartRateResult ? heartRateResult.getValue() : null,
            previous_point: previousPoint
              ? {
                  latitude: previousPoint.latitude,
                  longitude: previousPoint.longitude,
                  date_time: previousPoint.date_time,
                }
              : null,
          };
          const trackPointResult = TrackPoint.create(trackPointProps);
          if (trackPointResult.isFailure) {
            logger.error(
              `[Domains/GPSFile] parseGPX: Error while create a TrackPoint: ${JSON.stringify(
                trackPointResult.errorValue
              )}`
            );
            return Result.fail<GPSFile>(
              `[Domains/GPSFile] parseGPX: Error while create a TrackPoint: ${JSON.stringify(
                trackPointResult.errorValue
              )}`
            );
          }

          const distanceResult = trackPointResult.getValue().distance;
          distance = distanceResult ? distance + distanceResult.value : distance;

          segmentsProps.push(trackPointResult.getValue());
          previousPoint = trackPointResult.getValue();
        }
      }
      trackProps.push(segmentsProps);
    }
    const trackResult = Track.create({ track: trackProps });
    if (trackResult.isFailure) {
      logger.error(
        `[Domains/GPSFile] parseGPX: Error while create a Track: ${JSON.stringify(
          trackResult.errorValue
        )}`
      );
      return Result.fail<GPSFile>(
        `[Domains/GPSFile] parseGPX: Error while create a Track: ${JSON.stringify(
          trackResult.errorValue
        )}`
      );
    }

    /*
     * Preparing Session Props
     */
    let durationResult = null;
    let duration = 0;
    if (endDateTimeResult) {
      duration =
        (+new Date(endDateTimeResult.getValue().value) -
          +new Date(startDateTimeResult.getValue().value)) /
        1000;
      durationResult = Duration.create({ value: duration });
      if (durationResult.isFailure) {
        logger.error(
          `[Domains/GPSFile] parseGPX: Error while create a Track: ${JSON.stringify(
            durationResult.errorValue
          )}`
        );
        return Result.fail<GPSFile>(
          `[Domains/GPSFile] parseGPX: Error while create a Track: ${JSON.stringify(
            durationResult.errorValue
          )}`
        );
      }
    }

    let distanceResult = null;
    let averageSpeedResult = null;
    if (distance) {
      distanceResult = Distance.create({ value: distance });
      if (distanceResult.isFailure) {
        logger.error(
          `[Domains/GPSFile] parseGPX: Error while create a Track: ${JSON.stringify(
            distanceResult.errorValue
          )}`
        );
        return Result.fail<GPSFile>(
          `[Domains/GPSFile] parseGPX: Error while create a Track: ${JSON.stringify(
            distanceResult.errorValue
          )}`
        );
      }

      averageSpeedResult = Speed.create({ value: distance / duration });
      if (averageSpeedResult.isFailure) {
        logger.error(
          `[Domains/GPSFile] parseGPX: Error while create a Track: ${JSON.stringify(
            averageSpeedResult.errorValue
          )}`
        );
        return Result.fail<GPSFile>(
          `[Domains/GPSFile] parseGPX: Error while create a Track: ${JSON.stringify(
            averageSpeedResult.errorValue
          )}`
        );
      }
    }

    // logger.debug(
    //   `[Domains/GPSFile] parseGPX: durationResult: ${JSON.stringify(
    //     durationResult.getValue()
    //   )}`
    // );
    // logger.debug(`[TEST gpsFile] nameResult: ${JSON.stringify(nameResult)}`);
    // logger.debug(`[TEST gpsFile] startDateTimeResult: ${JSON.stringify(startDateTimeResult)}`);
    // logger.debug(`[TEST gpsFile] durationResult: ${JSON.stringify(durationResult)}`);
    // logger.debug(`[TEST gpsFile] distanceResult: ${JSON.stringify(distanceResult)}`);
    // logger.debug(`[TEST gpsFile] averageSpeedResult: ${JSON.stringify(averageSpeedResult)}`);
    // logger.debug(`[TEST gpsFile] trackResult: ${JSON.stringify(trackResult)}`);
    // logger.debug(
    //   `[Domains/GPSFile] parseGPX - descriptionResult: ${JSON.stringify(descriptionResult)}`
    // );
    const gpsFileProps = {
      name: nameResult
        ? nameResult.getValue()
        : this._nameSession(startDateTimeResult.getValue(), activityName),
      activity_name: activityName,
      start_date_time: startDateTimeResult.getValue(),
      duration: duration ? durationResult.getValue() : null,
      distance: distance ? distanceResult.getValue() : null,
      average_speed: distance ? averageSpeedResult.getValue() : null,
      gps_track: trackResult.getValue(),
      description: descriptionResult ? descriptionResult.getValue() : null,
    };
    return Result.ok<GPSFile>(new GPSFile(gpsFileProps));
  }
}
