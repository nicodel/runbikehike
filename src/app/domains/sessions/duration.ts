import { Guard } from '@/shared/core/Guard';
import { Result } from '@/shared/core/Result';
import { ValueObject } from '@/shared/domains/ValueObject';

interface DurationProps {
  value: number;
  unit?: string;
}

export class Duration extends ValueObject<DurationProps> {
  private constructor(props: DurationProps) {
    super(props);
  }

  get value(): number {
    return this.props.value;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private static isValidFloatNumber(raw: any): boolean {
    return !isNaN(parseFloat(raw));
  }

  public static create(props: DurationProps): Result<Duration> {
    const nullGuardResult = Guard.againstNullOrUndefined(props.value, 'duration');

    if (!nullGuardResult.succeeded) {
      return Result.fail<Duration>(nullGuardResult.message);
    }

    if (!this.isValidFloatNumber(props.value)) {
      return Result.fail<Duration>('Duration should be a number.');
    }

    const duration = new Duration({
      value: props.value,
      unit: 's',
    });

    return Result.ok<Duration>(duration);
  }
}
