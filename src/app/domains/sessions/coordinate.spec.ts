import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from '@/shared/core/Result';
import { Coordinate } from './coordinate';

let coordinate: Coordinate;
let coordinateResult: Result<Coordinate>;

describe('Modules / App / Domain / Session / Coordinate', () => {
  it('should return a coordinate', () => {
    coordinateResult = Coordinate.create({ value: 5, name: 'latitude' });
    coordinateResult.isSuccess.should.be.true;
    coordinate = coordinateResult.getValue();
    coordinate.props.should.eql({
      value: 5,
      name: 'latitude',
    });
  });

  it('should return an error if "name" is neither "latitude" nor "longitude"', () => {
    coordinateResult = Coordinate.create({ value: 5, name: 'wrong' });
    coordinateResult.isFailure.should.be.true;
  });
});
