import { Guard } from '@/shared/core/Guard';
import { Result } from '@/shared/core/Result';
import { ValueObject } from '@/shared/domains/ValueObject';
import logger from '@/shared/utils/logger';

interface BurnedCaloriesProps {
  value: number;
  unit?: string;
}

interface CalculatorProps {
  weight: number;
  distance: number;
  duration: number;
  activity_name: string;
}

export class BurnedCalories extends ValueObject<BurnedCaloriesProps> {
  private constructor(props: BurnedCaloriesProps) {
    super(props);
  }

  get value(): number {
    return this.props.value;
  }

  public static calculate(props: CalculatorProps): number {
    const speed = props.distance / props.duration;

    /*
     * The MET values below are based on "2011_Compendium_of_Physical_Activities.pdf".
     */
    let MET = 0;
    if (props.activity_name === 'walking') {
      if (speed <= 1.15) {
        MET = 3;
      } else if (speed <= 1.45) {
        MET = 3.5;
      } else if (speed <= 1.58) {
        MET = 4.3;
      } else if (speed <= 1.8) {
        MET = 5;
      } else if (speed <= 2) {
        MET = 7;
      } else if (speed <= 2.25) {
        MET = 8.3;
      } else {
        MET = 9;
      }
    } else if (props.activity_name === 'running') {
      if (speed <= 1.8) {
        MET = 6;
      } else if (speed <= 2.1) {
        MET = 7;
      } else if (speed <= 2.28) {
        MET = 8.3;
      } else if (speed <= 2.5) {
        MET = 9;
      } else if (speed <= 2.835) {
        MET = 9.8;
      } else if (speed <= 3.055) {
        MET = 10.5;
      } else if (speed <= 3.235) {
        MET = 11;
      } else if (speed <= 3.46) {
        MET = 11.5;
      } else if (speed <= 3.705) {
        MET = 11.8;
      } else if (speed <= 3.93) {
        MET = 12.3;
      } else if (speed <= 4.245) {
        MET = 12.8;
      } else if (speed <= 4.69) {
        MET = 14.5;
      } else if (speed <= 5.135) {
        MET = 16;
      } else if (speed <= 5.585) {
        MET = 19;
      } else if (speed <= 6.03) {
        MET = 19.8;
      } else {
        MET = 23;
      }
    } else if (props.activity_name === 'racing') {
      MET = 23;
    } else if (props.activity_name === 'swimming') {
      if (speed <= 0.5) {
        MET = 7;
      } else if (speed <= 0.8) {
        MET = 8.6;
      } else if (speed <= 1.1) {
        MET = 10;
      } else {
        MET = 13;
      }
    } else if (props.activity_name === 'regular_biking') {
      if (speed <= 3.325) {
        MET = 3.5;
      } else if (speed <= 4.335) {
        MET = 5.8;
      } else if (speed <= 5.335) {
        MET = 6.8;
      } else if (speed <= 6.23) {
        MET = 8;
      } else if (speed <= 7.125) {
        MET = 10;
      } else if (speed <= 8.715) {
        MET = 12;
      } else {
        MET = 15.8;
      }
    } else if (props.activity_name === 'mountain_biking') {
      MET = 8.5;
    } else if (props.activity_name === 'bmx') {
      MET = 8.5;
    } else if (props.activity_name === 'time_trial_biking') {
      MET = 16;
    } else if (props.activity_name === 'trekking') {
      MET = 7.3;
    } else if (props.activity_name === 'skiing') {
      MET = 7;
    } else if (props.activity_name === 'paddling') {
      MET = 7;
    } else if (props.activity_name === 'climbing') {
      MET = 7;
    } else if (props.activity_name === 'football') {
      MET = 7;
    } else {
      MET = 10;
    }
    logger.debug(`[Domains / BurnedCalories] MET ${JSON.stringify(MET)}`);

    const calories_burned = Math.round(((3.5 * MET * props.weight) / 200) * (props.duration / 60));
    // logger.debug(`BurnedCalories calories_burned ${JSON.stringify(calories_burned)}`);
    return calories_burned;
  }

  public static create(props: BurnedCaloriesProps): Result<BurnedCalories> {
    const nullGuardResult = Guard.againstNullOrUndefined(props.value, 'calories_burned');

    if (!nullGuardResult.succeeded) {
      return Result.fail<BurnedCalories>(nullGuardResult.message);
    }

    if (isNaN(props.value)) {
      return Result.fail<BurnedCalories>('Burned Calories should be a number.');
    }

    const calories_burned = new BurnedCalories({
      value: props.value,
      unit: 'kcal',
    });

    return Result.ok<BurnedCalories>(calories_burned);
  }
}
