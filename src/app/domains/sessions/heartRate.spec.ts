import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from '@/shared/core/Result';
import { HeartRate } from './heartRate';

let distance: HeartRate;
let distanceResult: Result<HeartRate>;

describe('Modules / App / Domain / Session / HeartRate', () => {
  it('should return a heart rate', () => {
    distanceResult = HeartRate.create({ value: 5 });
    distanceResult.isSuccess.should.be.true;
    distance = distanceResult.getValue();
    distance.props.should.eql({
      value: 5,
    });
  });
});
