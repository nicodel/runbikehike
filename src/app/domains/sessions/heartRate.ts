import { ValueObject } from '@/shared/domains/ValueObject';
import { Result } from '@/shared/core/Result';
import { Guard } from '@/shared/core/Guard';

interface HeartRateProps {
  value: number;
}

export class HeartRate extends ValueObject<HeartRateProps> {
  private constructor(props: HeartRateProps) {
    super(props);
  }

  get value(): number {
    return this.props.value;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private static isValidFloatNumber(raw: any): boolean {
    return !isNaN(parseFloat(raw));
  }

  public static create(props: HeartRateProps): Result<HeartRate> {
    const nullGuardResult = Guard.againstNullOrUndefined(props.value, 'heart rate');

    if (!nullGuardResult.succeeded) {
      return Result.fail<HeartRate>(nullGuardResult.message);
    }

    if (!this.isValidFloatNumber(props.value)) {
      return Result.fail<HeartRate>('HeartRate should be a number.');
    }

    const heart_rate = new HeartRate({
      value: props.value,
    });

    return Result.ok<HeartRate>(heart_rate);
  }
}
