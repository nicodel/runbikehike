import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from '@/shared/core/Result';
import { BurnedCalories } from './burnedCalories';

let calories_burned: BurnedCalories;
let calories_burnedResult: Result<BurnedCalories>;

describe('Modules / App / Domain / Session / BurnedCalories', () => {
  it('should return a calories burned', () => {
    calories_burnedResult = BurnedCalories.create({ value: 5 });
    calories_burnedResult.isSuccess.should.be.true;
    calories_burned = calories_burnedResult.getValue();
    calories_burned.props.should.eql({
      value: 5,
      unit: 'kcal',
    });
  });

  it('should return a fail, if wrong props are passed', () => {
    calories_burnedResult = BurnedCalories.create({ value: null });
    calories_burnedResult.isFailure.should.be.true;
  });

  it('should calculate calories burned', () => {
    const calories = BurnedCalories.calculate({
      weight: 95,
      duration: 2100,
      distance: 5000,
      activity_name: 'running',
    });
    calories.should.equal(524);
  });
});
