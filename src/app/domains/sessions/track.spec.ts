import * as chai from 'chai';
chai.should();

import { tracks } from '@/fake_data/gpsFiles/tracks';

import { Track } from './track';
import logger from '@/shared/utils/logger';

const trackProps = {
  track: tracks.chloe_mccardell.Segments,
};

// const trackFlat = JSON.parse(tracks.chloe_mccardell.Segments)[0];

describe('Modules / App / Domain / Session / Track', () => {
  it('should be able to create a track', () => {
    const trackResult = Track.create(trackProps);
    trackResult.isSuccess.should.be.true;
    const track = trackResult.getValue();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const nb_segments = track.track.length;
    logger.debug(`TEST nb_segments ${JSON.stringify(nb_segments)}`);
    nb_segments.should.equal(1);
  });

  // it('should return a flat_track', () => {
  //   const trackResult = Track.create(trackProps);
  //   trackResult.isSuccess.should.be.true;
  //   const flat_track = trackResult.getValue().flat_track;
  //   flat_track.should.eql(trackFlat);
  // });

  // it('should return a interval_track', () => {
  //   const trackResult = Track.create(trackProps);
  //   trackResult.isSuccess.should.be.true;
  //   const interval_track = trackResult.getValue().interval_track;
  //   interval_track.should.eql(tracks.chloe_mccardell.interval_track);
  // });

  // it('should return a median_track', () => {
  //   const props = { segments: JSON.parse(tracks.forrest_gump.Segments) };
  //   const trackResult = Track.create(props);
  //   trackResult.isSuccess.should.be.true;
  //   const median_track = trackResult.getValue().median_track;
  //   median_track.should.eql(tracks.forrest_gump.median_track);
  // });
});
