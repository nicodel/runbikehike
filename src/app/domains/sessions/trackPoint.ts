import { ValueObject } from '@/shared/domains/ValueObject';
import { Guard } from '@/shared/core/Guard';
import { Result } from '@/shared/core/Result';

import { DateTime } from './../dateTime';
import { Coordinate } from './coordinate';
import { Distance } from './distance';
import { Speed } from './speed';
import { HeartRate } from './heartRate';

interface TrackPointProps {
  date_time: DateTime;
  latitude: Coordinate;
  longitude: Coordinate;
  altitude: Distance;
  speed?: Speed;
  heart_rate?: HeartRate;
  previous_point?: {
    latitude: Coordinate;
    longitude: Coordinate;
    date_time: DateTime;
  };
  distance?: Distance;
}

export class TrackPoint extends ValueObject<TrackPointProps> {
  private constructor(props: TrackPointProps) {
    super(props);
  }

  get json(): object {
    const json = {
      date_time: this.props.date_time.value,
      latitude: this.props.latitude.value,
      longitude: this.props.longitude.value,
      altitude: this.props.altitude.value,
      distance: this.props.distance ? this.props.distance.value : 0,
    };
    if (this.props.speed) {
      json['speed'] = this.props.speed.speed;
    }
    if (this.props.heart_rate) {
      json['heart_rate'] = this.props.heart_rate.value;
    }
    return json;
  }

  get previousPoint(): object {
    return this.props.previous_point;
  }

  get distance(): Distance {
    return this.props.distance;
  }

  get date_time(): DateTime {
    return this.props.date_time;
  }

  get latitude(): Coordinate {
    return this.props.latitude;
  }

  get longitude(): Coordinate {
    return this.props.longitude;
  }

  get altitude(): Distance {
    return this.props.altitude;
  }

  get heart_rate(): HeartRate {
    return this.props.heart_rate;
  }

  get speed(): Speed {
    return this.props.speed;
  }

  private static calculateDistanceBetweenTwoPoints(
    previous_latitude: Coordinate,
    previous_longitude: Coordinate,
    current_latitude: Coordinate,
    current_longitude: Coordinate
  ): number {
    const lat1Rad = previous_latitude.value * (Math.PI / 180);
    const lon1Rad = previous_longitude.value * (Math.PI / 180);
    const lat2Rad = current_latitude.value * (Math.PI / 180);
    const lon2Rad = current_longitude.value * (Math.PI / 180);

    const dLat = lat2Rad - lat1Rad;
    const dLon = lon2Rad - lon1Rad;

    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(lat1Rad) * Math.cos(lat2Rad) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const R = 6371 * 1000; // Earth radius (mean) in metres {6371, 6367}
    return R * c;
  }

  private static calculateDurationbetweenTwoDateTime(
    previous_date_time: DateTime,
    current_date_time: DateTime
  ): number {
    const current = new Date(current_date_time.value).getTime();
    const previous = new Date(previous_date_time.value).getTime();
    const diffMilli = Math.abs(current - previous);
    return diffMilli / 1000; // returning duration in seconds
  }

  public static create(props: TrackPointProps): Result<TrackPoint> {
    const date_timeGuardResult = Guard.againstNullOrUndefined(props.date_time, 'date_time');
    if (!date_timeGuardResult.succeeded) {
      return Result.fail<TrackPoint>(date_timeGuardResult.message);
    }
    const latitudeGuardResult = Guard.againstNullOrUndefined(props.latitude, 'latitude');
    if (!latitudeGuardResult.succeeded) {
      return Result.fail<TrackPoint>(latitudeGuardResult.message);
    }
    const longitudeGuardResult = Guard.againstNullOrUndefined(props.longitude, 'longitude');
    if (!longitudeGuardResult.succeeded) {
      return Result.fail<TrackPoint>(longitudeGuardResult.message);
    }
    const altitudeGuardResult = Guard.againstNullOrUndefined(props.altitude, 'altitude');
    if (!altitudeGuardResult.succeeded) {
      return Result.fail<TrackPoint>(altitudeGuardResult.message);
    }

    const trackPointProps: TrackPointProps = {
      date_time: props.date_time,
      latitude: props.latitude,
      longitude: props.longitude,
      altitude: props.altitude,
    };

    if (props.speed) {
      trackPointProps.speed = props.speed;
    }

    if (props.heart_rate) {
      trackPointProps.heart_rate = props.heart_rate;
    }

    let distance = 0;
    if (props.previous_point) {
      distance = this.calculateDistanceBetweenTwoPoints(
        props.previous_point.latitude,
        props.previous_point.longitude,
        props.latitude,
        props.longitude
      );

      trackPointProps.distance = Distance.create({ value: distance }).getValue();
      if (!props.speed) {
        const duration = this.calculateDurationbetweenTwoDateTime(
          props.previous_point.date_time,
          props.date_time
        );
        trackPointProps.speed = Speed.create({ value: distance / duration }).getValue();
      }
    }

    return Result.ok<TrackPoint>(new TrackPoint(trackPointProps));
  }
}
