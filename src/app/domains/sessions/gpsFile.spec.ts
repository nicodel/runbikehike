import * as chai from 'chai';
import { expect } from 'chai';
chai.should();

import { XMLParser } from 'fast-xml-parser';
import { GPSFile } from './gpsFile';

import {
  GPS_ALL_MANDATORY,
  GPX_ERROR_GPX_MISSING,
  GPX_ERROR_TRK_MISSING,
  GPX_ERROR_TRKSEG_MISSING,
  GPX_NO_METADATA_TIME,
  GPX_METADATA_TIME,
  GPX_NO_TIME,
  GPX_METADATA_NAME,
  GPX_TRACK_NAME,
  GPX_NO_NAME,
  GPX_TYPE,
  GPX_NO_TYPE,
  GPX_NO_DURATION,
  GPX_DURATION,
  GPX_NO_DISTANCE,
  GPX_TRK_DESC,
  GPX_NO_DESC,
  GPX_GPS_NO_TIME,
  GPX_GPS_NO_LAT,
  GPX_GPS_NO_LON,
  GPX_GPS_NO_ELE,
  GPX_GPS_NO_HR,
} from '@/fake_data/gpsFiles/GpsFiles';

const _extractData = (key: string, json: object) => {
  if (!json[key]) {
    return { isFailure: true, isSuccess: false, value: null };
  }
  return { isFailure: false, isSuccess: true, value: json[key] };
};
const alwaysArray = ['gpx.trk.trkseg', 'gpx.trk.trkseg.trkpt'];

const parser = new XMLParser({
  ignoreAttributes: false,
  attributeNamePrefix: '',
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  isArray: (name, jpath, isLeafNode, isAttribute) => {
    if (alwaysArray.indexOf(jpath) !== -1) return true;
  },
});

describe('Modules / App / Domain / files / GPSFile', () => {
  describe('mandatory keys', () => {
    it('should return a parsedFile, with metadata and track', () => {
      const parsedFileResult = GPSFile.parseGPX(GPS_ALL_MANDATORY);
      parsedFileResult.isSuccess.should.be.true;
      const file = parsedFileResult.getValue();
      file.props.should.have.keys([
        'name',
        'start_date_time',
        'duration',
        'distance',
        'average_speed',
        'description',
        'gps_track',
        'activity_name',
      ]);
    });

    it('should return an error, if gpx is missing', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_ERROR_GPX_MISSING);
      parsedFileResult.isSuccess.should.be.false;
    });

    it('should return an error, if trk is missing', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_ERROR_TRK_MISSING);
      parsedFileResult.isSuccess.should.be.false;
    });

    it('should return an error, if trkseg is missing', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_ERROR_TRKSEG_MISSING);
      parsedFileResult.isSuccess.should.be.false;
    });
  });

  describe('start_date_time', () => {
    it('should equal time, if time', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_METADATA_TIME);
      parsedFileResult.isSuccess.should.be.true;

      const mt = _extractData('metadata', parser.parse(GPX_METADATA_TIME).gpx);
      const time = _extractData('time', mt.value).value;

      const file = parsedFileResult.getValue();
      file.props.start_date_time.date_time.should.equal(time);
    });

    it('should equal trk.trkseg[0].trkpt[0].time, if only trk.trkseg[0].trkpt[0].time', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_NO_METADATA_TIME);
      parsedFileResult.isSuccess.should.be.true;

      const trk = _extractData('trk', parser.parse(GPX_NO_METADATA_TIME).gpx).value;
      const seg = _extractData('trkseg', trk).value;
      const trkpt = _extractData('trkpt', seg[0]).value;
      const time = _extractData('time', trkpt[0]).value;

      const file = parsedFileResult.getValue();
      file.gps_track.track[0][0].date_time.value.should.equal(time);
    });

    it('should fails, if !time && !trk.trkseg[0].trkpt[0].time', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_NO_TIME);
      parsedFileResult.isFailure.should.be.true;
    });
  });

  describe('name', () => {
    it('should equal name, if name', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_METADATA_NAME);
      parsedFileResult.isSuccess.should.be.true;

      const mt = _extractData('metadata', parser.parse(GPX_METADATA_NAME).gpx);
      const name = _extractData('name', mt.value).value;

      const file = parsedFileResult.getValue();
      file.name.value.should.equal(name);
    });

    it('should equal trk.name, if only trk.name', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_TRACK_NAME);
      parsedFileResult.isSuccess.should.be.true;

      const trk = _extractData('trk', parser.parse(GPX_TRACK_NAME).gpx);
      const name = _extractData('name', trk.value).value;

      const file = parsedFileResult.getValue();
      file.name.value.should.equal(name);
    });

    it('should be a name build from Activity and startDateTime, if !name && !trk.name', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_NO_NAME);
      parsedFileResult.isSuccess.should.be.true;

      const file = parsedFileResult.getValue();
      const name = `${file.activity_name} - ${new Date(
        file.start_date_time.value
      ).toLocaleString()}`;
      file.name.should.equal(name);
    });
  });

  describe('activity_name', () => {
    it('should equal trk.type, if trk.type', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_TYPE);
      parsedFileResult.isSuccess.should.be.true;

      const trk = _extractData('trk', parser.parse(GPX_TRACK_NAME).gpx);
      const type = _extractData('type', trk.value).value;

      const file = parsedFileResult.getValue();
      file.activity_name.should.equal(type);
    });

    it('should equal "unknown", if !trk.type', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_NO_TYPE);
      parsedFileResult.isSuccess.should.be.true;

      const file = parsedFileResult.getValue();
      file.activity_name.should.equal('unknown');
    });
  });
  describe('duration', () => {
    it('should return a calculated duration', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_DURATION);
      parsedFileResult.isSuccess.should.be.true;

      const file = parsedFileResult.getValue();
      file.duration.value.should.equal(71);
    });

    it('should be null, if no trkpt are in file', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_NO_DURATION);
      parsedFileResult.isSuccess.should.be.true;

      const file = parsedFileResult.getValue();
      expect(file.duration).to.be.null;
    });
  });

  describe('metadate.distance', () => {
    it('should return a calculated distance', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_DURATION);
      parsedFileResult.isSuccess.should.be.true;

      const file = parsedFileResult.getValue();
      file.distance.value.should.equal(132.91242992518565);
    });

    it('should be null, if no trkpt', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_NO_DISTANCE);
      parsedFileResult.isSuccess.should.be.true;

      const file = parsedFileResult.getValue();
      expect(file.distance).to.be.null;
    });
  });

  describe('average_speed', () => {
    it('should return a calculated speed', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_DURATION);
      parsedFileResult.isSuccess.should.be.true;

      const file = parsedFileResult.getValue();
      const average_speed = file.average_speed.speed;
      const distance = file.distance.value;
      const duration = file.duration.value;
      average_speed.should.equal(distance / duration);
    });

    it('should be null, if !distance', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_NO_DISTANCE);
      parsedFileResult.isSuccess.should.be.true;

      const file = parsedFileResult.getValue();
      expect(file.average_speed).to.be.null;
    });
  });

  describe('decription', () => {
    it('should be equal trk.desc, if exist', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_TRK_DESC);
      parsedFileResult.isSuccess.should.be.true;

      const trk = _extractData('trk', parser.parse(GPX_TRK_DESC).gpx);
      const desc = _extractData('desc', trk.value).value;

      const file = parsedFileResult.getValue();
      file.description.value.should.equal(desc);
    });

    it('should be equal desc, if exist', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_METADATA_NAME);
      parsedFileResult.isSuccess.should.be.true;

      const mt = _extractData('metadata', parser.parse(GPX_METADATA_NAME).gpx);
      const desc = _extractData('desc', mt.value).value;

      const file = parsedFileResult.getValue();
      file.description.value.should.equal(desc);
    });

    it('should be null, if !desc', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_NO_DESC);
      parsedFileResult.isSuccess.should.be.true;

      const file = parsedFileResult.getValue();
      expect(file.description).to.be.null;
    });
  });

  describe('track.segments', () => {
    it('should be same length as trkseg', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_DURATION);
      parsedFileResult.isSuccess.should.be.true;

      const trk = _extractData('trk', parser.parse(GPX_TRK_DESC).gpx);
      const trkseg = _extractData('trkseg', trk.value).value;

      const file = parsedFileResult.getValue();
      file.gps_track.track.length.should.equal(trkseg.length);
    });
  });

  describe('track[X].points', () => {
    it('should be same length as trkpt', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_DURATION);
      parsedFileResult.isSuccess.should.be.true;

      const trk = _extractData('trk', parser.parse(GPX_DURATION).gpx);
      const trkseg = _extractData('trkseg', trk.value).value;

      const trkpt = _extractData('trkpt', trkseg[0]).value;

      const file = parsedFileResult.getValue();
      const segment = file.gps_track.track[0];
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      segment.length.should.equal(trkpt.length);
    });
  });

  describe('track[seg][pt].date_time', () => {
    it('should failed if !time', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_GPS_NO_TIME);
      parsedFileResult.isSuccess.should.be.false;
    });

    it('should equal trk[trkseg][trkpt].time', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_DURATION);
      parsedFileResult.isSuccess.should.be.true;

      const trk = _extractData('trk', parser.parse(GPX_DURATION).gpx);
      const trkseg = _extractData('trkseg', trk.value).value;
      const trkpt = _extractData('trkpt', trkseg[0]).value;
      const time = _extractData('time', trkpt[0]).value;

      const file = parsedFileResult.getValue();
      file.gps_track.track[0][0].date_time.value.should.equal(time);
    });
  });

  describe('track[seg][pt].latitude', () => {
    it('should failed if !lat', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_GPS_NO_LAT);
      parsedFileResult.isSuccess.should.be.false;
    });

    it('should equal trk[trkseg][trkpt].lat', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_DURATION);
      parsedFileResult.isSuccess.should.be.true;

      const trk = _extractData('trk', parser.parse(GPX_DURATION).gpx);
      const trkseg = _extractData('trkseg', trk.value).value;
      const trkpt = _extractData('trkpt', trkseg[0]).value;
      const lat = _extractData('lat', trkpt[0]).value;

      const file = parsedFileResult.getValue();
      file.gps_track.track[0][0].latitude.value.should.equal(lat);
    });
  });

  describe('track[seg][pt].longitude', () => {
    it('should failed if !lon', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_GPS_NO_LON);
      parsedFileResult.isSuccess.should.be.false;
    });

    it('should equal trk[trkseg][trkpt].lon', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_DURATION);
      parsedFileResult.isSuccess.should.be.true;

      const trk = _extractData('trk', parser.parse(GPX_DURATION).gpx);
      const trkseg = _extractData('trkseg', trk.value).value;
      const trkpt = _extractData('trkpt', trkseg[0]).value;
      const lon = _extractData('lon', trkpt[0]).value;

      const file = parsedFileResult.getValue();
      file.gps_track.track[0][0].longitude.value.should.equal(lon);
    });
  });

  describe('track[seg][pt].altitude', () => {
    it('should failed if !ele', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_GPS_NO_ELE);
      parsedFileResult.isSuccess.should.be.false;
    });

    it('should equal trk[trkseg][trkpt].ele', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_DURATION);
      parsedFileResult.isSuccess.should.be.true;

      const trk = _extractData('trk', parser.parse(GPX_DURATION).gpx);
      const trkseg = _extractData('trkseg', trk.value).value;
      const trkpt = _extractData('trkpt', trkseg[0]).value;
      const ele = _extractData('ele', trkpt[0]).value;

      const file = parsedFileResult.getValue();
      file.gps_track.track[0][0].altitude.value.should.equal(ele);
    });
  });

  describe('track[seg][pt].heart_rate', () => {
    it('should equal trk[trkseg][trkpt].extensions.ns3:TrackPointExtension.hr, if exists', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_DURATION);
      parsedFileResult.isSuccess.should.be.true;

      const trk = _extractData('trk', parser.parse(GPX_DURATION).gpx);
      const trkseg = _extractData('trkseg', trk.value).value;
      const trkpt = _extractData('trkpt', trkseg[0]).value;
      const ext = _extractData('extensions', trkpt[0]).value;
      const ns3 = _extractData('ns3:TrackPointExtension', ext).value;
      const hr = _extractData('ns3:hr', ns3).value;

      const file = parsedFileResult.getValue();
      file.gps_track.track[0][0].heart_rate.value.should.equal(hr);
    });

    it('should not be present, if not provided in file', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_GPS_NO_HR);
      parsedFileResult.isSuccess.should.be.true;

      const file = parsedFileResult.getValue();
      expect(file.gps_track.track[0][0].heart_rate).to.be.undefined;
    });
  });

  describe('track[seg][pt].speed', () => {
    it('should not be present, if not provided in file', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_GPS_NO_HR);
      parsedFileResult.isSuccess.should.be.true;

      const file = parsedFileResult.getValue();
      expect(file.gps_track.track[0][0].speed).to.be.undefined;
    });
  });

  describe('track[seg][pt].distance', () => {
    it('should be calculated', () => {
      const parsedFileResult = GPSFile.parseGPX(GPX_DURATION);
      parsedFileResult.isSuccess.should.be.true;

      const file = parsedFileResult.getValue();
      expect(file.gps_track.track[0][0].distance).to.be.undefined;
      file.gps_track.track[0][10].distance.value.should.equal(1.8970687667961021);
    });
  });
});
