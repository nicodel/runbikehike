import { Guard } from '@/shared/core/Guard';
import { Result } from '@/shared/core/Result';
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';
import { UniqueId } from '../uniqueId';
import { Entity } from '@/shared/domains/Entity';

export interface ActivityProps {
  name: string;
  icon: string;
  use_distance: boolean;
}

export class Activity extends Entity<ActivityProps> {
  constructor(props: ActivityProps, id?: UniqueEntityID) {
    super(props, id);
  }

  get name(): string {
    return this.props.name;
  }

  get icon(): string {
    return this.props.icon;
  }

  get use_distance(): boolean {
    return this.props.use_distance;
  }

  get id(): UniqueId {
    return UniqueId.create(this._id).getValue();
  }

  public static create(props: ActivityProps, id?: UniqueEntityID): Result<Activity> {
    const nullGuard = Guard.againstNullOrUndefinedBulk([
      { argument: props.name, argumentName: 'name' },
      { argument: props.icon, argumentName: 'icon' },
      { argument: props.use_distance, argumentName: 'use_distance' },
    ]);

    if (!nullGuard.succeeded) {
      return Result.fail<Activity>(nullGuard.message);
    }

    const defaultActivityProps: ActivityProps = {
      ...props,
    };

    const activity = new Activity(defaultActivityProps, id);

    return Result.ok<Activity>(activity);
  }
}
