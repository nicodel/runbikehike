import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from '@/shared/core/Result';
import { Duration } from './duration';

let duration: Duration;
let durationResult: Result<Duration>;

describe('Modules / App / Domain / Session / Duration', () => {
  it('should return a duration', () => {
    durationResult = Duration.create({ value: 5 });
    durationResult.isSuccess.should.be.true;
    duration = durationResult.getValue();
    duration.props.should.eql({
      value: 5,
      unit: 's',
    });
  });

  it('should return a fail, if wrong props are passed', () => {
    durationResult = Duration.create({ value: null });
    durationResult.isFailure.should.be.true;
  });
});
