import { Entity } from '@/shared/domains/Entity';
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';
import { Guard } from '@/shared/core/Guard';
import { Result } from '@/shared/core/Result';

import { User } from '../auth/user';
import { LongText } from '../longText';
import { ShortText } from '../shortText';
import { DateTime } from '../dateTime';
import { Duration } from './duration';
import { BurnedCalories } from './burnedCalories';
import { Distance } from './distance';
import { Speed } from './speed';
import { UniqueId } from '../uniqueId';
import { Activity } from './activity';
import { Track } from './track';
import { HeartRate } from './heartRate';
import { Image } from '../image';

import logger from '@/shared/utils/logger';

export interface SessionProps {
  name: ShortText;
  start_date_time: DateTime;
  duration: Duration;
  burned_calories?: BurnedCalories;
  distance?: Distance;
  average_speed?: Speed;
  average_heart_rate?: HeartRate;
  elevation_gain?: Distance;
  description?: LongText;
  location?: ShortText;
  gps_track?: Track;
  nb_comments: number;
  nb_likes: number;
  is_favorite: boolean;
  created_at?: DateTime;
  updated_at?: DateTime;
  user: User;
  activity: Activity;
  images?: Image[];
}

export class Session extends Entity<SessionProps> {
  constructor(props: SessionProps, id?: UniqueEntityID) {
    super(props, id);
  }

  get name(): ShortText {
    return this.props.name;
  }

  get start_date_time(): DateTime {
    return this.props.start_date_time;
  }

  get duration(): Duration {
    return this.props.duration;
  }

  get burned_calories(): BurnedCalories {
    return this.props.burned_calories;
  }

  get distance(): Distance {
    return this.props.distance;
  }

  get average_speed(): Speed {
    return this.props.average_speed;
  }

  get average_heart_rate(): HeartRate {
    return this.props.average_heart_rate;
  }

  get elevation_gain(): Distance {
    return this.props.elevation_gain;
  }

  get description(): LongText {
    return this.props.description;
  }

  get location(): ShortText {
    return this.props.location;
  }

  get gps_track(): Track {
    return this.props.gps_track;
  }

  get nb_comments(): number {
    return this.props.nb_comments;
  }

  public set nb_comments(new_value: number) {
    this.props.nb_comments = new_value;
  }

  get nb_likes(): number {
    return this.props.nb_likes;
  }

  get is_favorite(): boolean {
    return this.props.is_favorite;
  }

  get activity(): Activity {
    return this.props.activity;
  }

  get id(): UniqueId {
    return UniqueId.create(this._id).getValue();
  }

  get user(): User {
    return this.props.user;
  }

  get images(): Image[] {
    return this.props.images;
  }

  public static create(props: SessionProps, id?: UniqueEntityID): Result<Session> {
    const nullGuard = Guard.againstNullOrUndefinedBulk([
      { argument: props.name, argumentName: 'name' },
      { argument: props.start_date_time, argumentName: 'start_date_time' },
      { argument: props.duration, argumentName: 'duration' },
      { argument: props.burned_calories, argumentName: 'burned_calories' },
      { argument: props.nb_comments, argumentName: 'nb_comments' },
      { argument: props.nb_likes, argumentName: 'nb_likes' },
      { argument: props.is_favorite, argumentName: 'is_favorite' },
      { argument: props.activity, argumentName: 'activity' },
      { argument: props.user, argumentName: 'user' },
    ]);
    if (!nullGuard.succeeded) {
      logger.error('[Domain/Sessions/Session] create - Error: %s', nullGuard.message);
      return Result.fail<Session>(nullGuard.message);
    }

    const defaultSessionProps: SessionProps = {
      ...props,
    };

    const session = new Session(defaultSessionProps, id);

    return Result.ok<Session>(session);
  }
}
