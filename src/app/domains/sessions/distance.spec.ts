import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from '@/shared/core/Result';
import { Distance } from './distance';

let distance: Distance;
let distanceResult: Result<Distance>;

describe('Modules / App / Domain / Session / Distance', () => {
  it('should return a distance', () => {
    distanceResult = Distance.create({ value: 5 });
    distanceResult.isSuccess.should.be.true;
    distance = distanceResult.getValue();
    distance.props.should.eql({
      value: 5,
      unit: 'm',
    });
  });

  it('should return a fail, if wrong props are passed', () => {
    distanceResult = Distance.create({ value: null });
    distanceResult.isFailure.should.be.true;
  });
});
