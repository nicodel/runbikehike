import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';

import { fakeSessions } from '@/fake_data/sessions';
import { fakeActivities } from '@/fake_data/activities';
import { fakeImages } from '@/fake_data/images';

import { ShortText } from '../shortText';
import { DateTime } from '../dateTime';
import { Duration } from './duration';
import { Distance } from './distance';
import { Speed } from './speed';
import { BurnedCalories } from './burnedCalories';
import { LongText } from './../longText';
import { User } from '../auth/user';
import { Email } from '../email';
import { Password } from '../auth/password';
import { Activity } from './activity';
import { HeartRate } from './heartRate';
import { Image } from '../image';

import { Session, SessionProps } from './session';

const activity = Activity.create({
  name: fakeActivities.running.name,
  icon: fakeActivities.running.icon,
  use_distance: fakeActivities.running.use_distance,
}).getValue();

const images = [
  Image.create({ mimetype: fakeImages[0].mimetype, data: fakeImages[0].data }).getValue(),
  Image.create({ mimetype: fakeImages[1].mimetype, data: fakeImages[1].data }).getValue(),
];

const props: SessionProps = {
  name: ShortText.create({
    value: fakeSessions[2].name,
    name: 'session_name',
  }).getValue(),
  start_date_time: DateTime.create({ value: fakeSessions[2].start_date_time }).getValue(),
  duration: Duration.create({ value: fakeSessions[2].duration, unit: 's' }).getValue(),
  distance: Distance.create({ value: fakeSessions[2].distance, unit: 'm' }).getValue(),
  average_speed: Speed.create({ value: fakeSessions[2].average_speed }).getValue(),
  burned_calories: BurnedCalories.create({
    value: fakeSessions[2].burned_calories,
  }).getValue(),
  description: LongText.create({
    value: fakeSessions[2].description,
    name: 'session_description',
  }).getValue(),
  user: User.create({
    email: Email.create({ value: fakeSessions[2].user.email }).getValue(),
    password: Password.create({ value: fakeSessions[2].user.password }).getValue(),
    isAdmin: fakeSessions[2].user.is_admin,
    isDisabled: fakeSessions[2].user.is_disabled,
  }).getValue(),
  activity: activity,
  average_heart_rate: HeartRate.create({ value: fakeSessions[2].average_heart_rate }).getValue(),
  elevation_gain: Distance.create({ value: fakeSessions[2].elevation_gain, unit: 'm' }).getValue(),
  nb_comments: fakeSessions[2].nb_comments,
  nb_likes: fakeSessions[2].nb_likes,
  is_favorite: fakeSessions[2].is_favorite,
  images: images,
};

describe('Modules / App / Domains / Sessions / Session', () => {
  it('create() should return a Session', () => {
    const sessionResult = Session.create(props);
    sessionResult.isSuccess.should.be.true;
  });

  it('create() should return an Error, if a mandatory value is missing', () => {
    const wrongProps: SessionProps = {
      name: null,
      start_date_time: DateTime.create({
        value: fakeSessions[2].start_date_time,
      }).getValue(),
      duration: Duration.create({
        value: fakeSessions[2].duration,
        unit: 's',
      }).getValue(),
      distance: Distance.create({
        value: fakeSessions[2].distance,
        unit: 'm',
      }).getValue(),
      average_speed: Speed.create({ value: fakeSessions[2].average_speed }).getValue(),
      burned_calories: BurnedCalories.create({
        value: fakeSessions[2].burned_calories,
      }).getValue(),
      description: LongText.create({
        value: fakeSessions[2].description,
        name: 'session_description',
      }).getValue(),
      user: User.create({
        email: Email.create({ value: fakeSessions[2].user.email }).getValue(),
        password: Password.create({ value: fakeSessions[2].user.password }).getValue(),
        isAdmin: fakeSessions[2].user.is_admin,
        isDisabled: fakeSessions[2].user.is_disabled,
      }).getValue(),
      activity: Activity.create({
        name: fakeActivities.running.name,
        icon: fakeActivities.running.icon,
        use_distance: fakeActivities.running.use_distance,
      }).getValue(),
      average_heart_rate: HeartRate.create({
        value: fakeSessions[2].average_heart_rate,
      }).getValue(),
      elevation_gain: Distance.create({
        value: fakeSessions[2].elevation_gain,
        unit: 'm',
      }).getValue(),
      nb_comments: fakeSessions[2].nb_comments,
      nb_likes: fakeSessions[2].nb_likes,
      is_favorite: fakeSessions[2].is_favorite,
    };
    const sessionResult = Session.create(wrongProps);
    sessionResult.isFailure.should.be.true;
    sessionResult.error.should.equal('name is null or undefined.');
  });

  it('.name.value should return Session name', () => {
    const session = Session.create(props).getValue();
    session.name.value.should.equal(fakeSessions[2].name);
  });

  it('.start_date_time.value should return Session start_date_time', () => {
    const session = Session.create(props).getValue();
    session.start_date_time.value.should.equal(fakeSessions[2].start_date_time);
  });

  it('.duration.value should return Session duration', () => {
    const session = Session.create(props).getValue();
    session.duration.value.should.equal(fakeSessions[2].duration);
  });

  it('.distance.value should return Session distance', () => {
    const session = Session.create(props).getValue();
    session.distance.value.should.equal(fakeSessions[2].distance);
  });

  it('.average_speed.speed should return Session average_speed', () => {
    const session = Session.create(props).getValue();
    session.average_speed.speed.should.equal(fakeSessions[2].average_speed);
  });

  it('.burned_calories.speed should return Session burned_calories', () => {
    const session = Session.create(props).getValue();
    session.burned_calories.value.should.equal(fakeSessions[2].burned_calories);
  });

  it('.description.value should return Session description', () => {
    const session = Session.create(props).getValue();
    session.description.value.should.equal(fakeSessions[2].description);
  });

  it('.id.id.toString() should return Session id', () => {
    const session = Session.create(props, new UniqueEntityID(fakeSessions[2].id)).getValue();
    session.id.id.toString().should.equal(fakeSessions[2].id);
  });

  it('.activity.name should return Session activity.name', () => {
    const session = Session.create(props).getValue();
    session.activity.should.equal(activity);
  });

  it('.images should return Session images', () => {
    const session = Session.create(props).getValue();
    session.images.should.equal(images);
  });
});
