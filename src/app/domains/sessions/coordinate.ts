import { Guard } from '@/shared/core/Guard';
import { Result } from '@/shared/core/Result';
import { ValueObject } from '@/shared/domains/ValueObject';

interface CoordinateProps {
  value: number;
  name: string;
}

export class Coordinate extends ValueObject<CoordinateProps> {
  private constructor(props: CoordinateProps) {
    super(props);
  }

  get value(): number {
    return this.props.value;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private static isValidFloatNumber(raw: any): boolean {
    return !isNaN(parseFloat(raw));
  }

  private static isValidName(rawName: string): boolean {
    return rawName === 'latitude' || rawName === 'longitude';
  }

  public static create(props: CoordinateProps): Result<Coordinate> {
    const nullGuardResult = Guard.againstNullOrNaN(props.value, props.name);

    if (!nullGuardResult.succeeded) {
      return Result.fail<Coordinate>(nullGuardResult.message);
    }

    if (!this.isValidFloatNumber(props.value)) {
      return Result.fail<Coordinate>(
        `Coordinate ${JSON.stringify(props.value)} should be a number.`
      );
    }
    if (!this.isValidName(props.name)) {
      return Result.fail<Coordinate>('Coordinate name should be either "latitude" or "longitude".');
    }

    const coordinate = new Coordinate({
      value: props.value,
      name: props.name,
    });

    return Result.ok<Coordinate>(coordinate);
  }
}
