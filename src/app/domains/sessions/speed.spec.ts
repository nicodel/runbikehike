import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from '@/shared/core/Result';
import { Speed } from './speed';
import { Distance } from './distance';
import { Duration } from './duration';

let speedResult: Result<Speed>;

describe('Modules / App / Domain / Session / Speed', () => {
  it('should return a speed', () => {
    speedResult = Speed.create({ value: 5 });
    speedResult.isSuccess.should.be.true;
    const speed = speedResult.getValue();
    speed.props.should.eql({
      value: 5,
      unit: 'm/s',
    });
  });

  it('should calculate speed', () => {
    const speed = Speed.calculateSpeed(
      Distance.create({ value: 5000 }).getValue(),
      Duration.create({ value: 2100 }).getValue()
    );
    speed.should.equal(2.380952380952381);
  });

  it('should return pace', () => {
    speedResult = Speed.create({ value: 5 });
    speedResult.getValue().pace.should.equal(0.3);
  });
});
