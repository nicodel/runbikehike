import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from '@/shared/core/Result';
import { Activity, ActivityProps } from './activity';

let activity: Activity;
let activityResult: Result<Activity>;

const props: ActivityProps = {
  name: 'running',
  icon: 'some svg icon',
  use_distance: true,
};

describe('Modules / App / Domain / Sessions / Activity', () => {
  it('should return an activity', () => {
    activityResult = Activity.create(props);
    activityResult.isSuccess.should.be.true;
    activity = activityResult.getValue();
    activity.should.have.property('name');
    activity.should.have.property('icon');
    activity.should.have.property('use_distance');
  });
});
