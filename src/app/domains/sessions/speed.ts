import { ValueObject } from '@/shared/domains/ValueObject';
import { Result } from '@/shared/core/Result';
import { Guard } from '@/shared/core/Guard';
import { Distance } from './distance';
import { Duration } from './duration';

interface SpeedProps {
  value: number;
  unit?: string;
}

export class Speed extends ValueObject<SpeedProps> {
  private constructor(props: SpeedProps) {
    super(props);
  }

  get speed(): number {
    return this.props.value;
  }

  get pace(): number {
    return this.props.value * 0.06;
  }

  public static calculateSpeed(distance: Distance, duration: Duration): number {
    return distance.value / duration.value;
  }

  public static create(props: SpeedProps): Result<Speed> {
    const nullGuardResult = Guard.againstNullOrUndefined(props.value, 'speed');

    if (!nullGuardResult.succeeded) {
      return Result.fail<Speed>(nullGuardResult.message);
    }

    if (isNaN(props.value)) {
      return Result.fail<Speed>('Speed should be a number.');
    }

    const speed = new Speed({
      value: props.value,
      unit: 'm/s',
    });

    return Result.ok<Speed>(speed);
  }
}
