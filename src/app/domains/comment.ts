import { Guard } from '@/shared/core/Guard';
import { Result } from '@/shared/core/Result';
import { Entity } from '@/shared/domains/Entity';
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';

import { UniqueId } from './uniqueId';

interface CommentProps {
  date_time: string;
  user_id: string;
  session_id: string;
  value: string;
}

export class Comment extends Entity<CommentProps> {
  constructor(props: CommentProps, id?: UniqueEntityID) {
    super(props, id);
  }

  get id(): UniqueId {
    return UniqueId.create(this._id).getValue();
  }

  get date_time(): string {
    return this.props.date_time;
  }

  get value(): string {
    return this.props.value;
  }

  get user_id(): string {
    return this.props.user_id;
  }

  get session_id(): string {
    return this.props.session_id;
  }

  public static create(commentProps: CommentProps, id?: UniqueEntityID): Result<Comment> {
    const nullGuard = Guard.againstNullOrUndefinedBulk([
      { argument: commentProps.date_time, argumentName: 'date_time' },
      { argument: commentProps.value, argumentName: 'comment' },
      { argument: commentProps.user_id, argumentName: 'user_id' },
      { argument: commentProps.session_id, argumentName: 'session_id' },
    ]);
    if (!nullGuard.succeeded) {
      return Result.fail<Comment>(nullGuard.message);
    }

    const comment = new Comment(commentProps, id);

    return Result.ok<Comment>(comment);
  }
}
