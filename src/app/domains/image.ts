import { Guard } from '@/shared/core/Guard';
import { Result } from '@/shared/core/Result';
import { Entity } from '@/shared/domains/Entity';
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';
import { UniqueId } from './uniqueId';

export interface ImageProps {
  mimetype: string;
  data: string;
}

export class Image extends Entity<ImageProps> {
  constructor(props: ImageProps, id?: UniqueEntityID) {
    super(props, id);
  }

  get id(): UniqueId {
    return UniqueId.create(this._id).getValue();
  }

  get mimetype(): string {
    return this.props.mimetype;
  }

  get data(): string {
    return this.props.data;
  }

  get base64Buffer(): Buffer {
    return Buffer.from(this.data, 'base64');
  }

  get base64String(): string {
    return Buffer.from(this.data, 'base64').toString('base64');
  }

  public static create(imageProps: ImageProps, id?: UniqueEntityID): Result<Image> {
    const nullGuard = Guard.againstNullOrUndefinedBulk([
      { argument: imageProps.mimetype, argumentName: 'mimetype' },
      { argument: imageProps.data, argumentName: 'data' },
    ]);

    if (!nullGuard.succeeded) {
      return Result.fail<Image>(nullGuard.message);
    }

    const image = new Image(
      {
        mimetype: imageProps.mimetype,
        data: imageProps.data,
      },
      id
    );

    return Result.ok<Image>(image);
  }

  /*public static createFromBase64(props: IBase64Image, id?: UniqueEntityID): Result<Image> {
    const nullGuardResult = Guard.againstNullOrUndefinedBulk([
      { argument: props.data, argumentName: 'data' },
      { argument: props.type, argumentName: 'type' },
    ]);

    if (!nullGuardResult.succeeded) {
      return Result.fail<Image>(nullGuardResult.message);
    }

    const imageBuffer = Buffer.from(props.data, 'base64');
    // logger.debug(
    //   '[Domains/Image] createFromBase64 - imageBuffer[0], [1], [2]: %s, %s, %s',
    //   imageBuffer[0],
    //   imageBuffer[1],
    //   imageBuffer[2]
    // );

    let imageExtension = '';
    if (props.type === 'image/jpeg') {
      imageExtension = 'jpeg';
    } else if (props.type === 'image/png') {
      imageExtension = 'png';
    } else if (props.type === 'image/gif') {
      imageExtension = 'gif';
    } else if (props.type === 'image/svg+xml') {
      imageExtension = 'svg';
    } else {
      return Result.fail<Image>('Image format not supported.');
    }

    let imageFilename = `${UniqueId.create().getValue().id.toString()}.${imageExtension}`;
    if (props.filename) {
      if (uuidValidate(props.filename.replace(/\.[^/.]+$/, ''))) {
        imageFilename = props.filename;
      }
    }

    const imageProps = {
      filename: imageFilename,
      mimetype: props.type,
      size: imageBuffer.byteLength,
      data: imageBuffer,
    };
    // logger.debug('[Domains/Image] createFromBase64 - imageProps: %s', imageProps);

    const image = new Image(imageProps, id);
    // logger.debug('[Domains/Image] createFromBase64 - image: %s', image);

    return Result.ok<Image>(image);
  }*/
}
