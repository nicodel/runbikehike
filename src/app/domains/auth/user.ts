import { Guard } from '@/shared/core/Guard';
import { Result } from '@/shared/core/Result';
import { AggregateRoot } from '@/shared/domains/AggregateRoots';
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';

import { Password } from './password';
import { Email } from '../email';
import { UniqueId } from '../uniqueId';

import { JWTToken, RefreshToken } from './jwt';

interface UserProps {
  email: Email;
  password: Password;
  // isEmailVerified?: boolean;
  isAdmin: boolean;
  isDisabled: boolean;
  accessToken?: JWTToken;
  refreshToken?: RefreshToken;
  isDeleted?: boolean;
  // lastLogin?: Date;
}

export class User extends AggregateRoot<UserProps> {
  constructor(props: UserProps, id?: UniqueEntityID) {
    super(props, id);
  }

  get userId(): UniqueId {
    return UniqueId.create(this._id).getValue();
  }

  get email(): Email {
    return this.props.email;
  }

  get password(): Password {
    return this.props.password;
  }

  get accessToken(): string {
    return this.props.accessToken;
  }

  get isDeleted(): boolean {
    return this.props.isDeleted;
  }

  // get isEmailVerified(): boolean {
  //   return this.props.isEmailVerified;
  // }

  get isAdmin(): boolean {
    return this.props.isAdmin;
  }

  get isDisabled(): boolean {
    return this.props.isDisabled;
  }

  public set isDisabled(value: boolean) {
    this.props.isDisabled = value;
  }

  // get lastLogin(): Date {
  //   return this.props.lastLogin;
  // }

  get refreshToken(): RefreshToken {
    return this.props.refreshToken;
  }

  public isLoggedIn(): boolean {
    return !!this.props.accessToken && !!this.props.refreshToken;
  }

  public setAccessToken(token: JWTToken, refreshToken: RefreshToken): void {
    /*this.addDomainEvent(new UserLoggedIn(this));*/
    this.props.accessToken = token;
    this.props.refreshToken = refreshToken;
    // this.props.lastLogin = new Date();
  }

  public delete(): void {
    if (!this.props.isDeleted) {
      /*this.addDomainEvent(new UserDeleted(this));*/
      this.props.isDeleted = true;
    }
  }

  public static create(props: UserProps, id?: UniqueEntityID): Result<User> {
    const guardResult = Guard.againstNullOrUndefinedBulk([
      { argument: props.email, argumentName: 'email' },
      { argument: props.password, argumentName: 'password' },
    ]);

    if (!guardResult.succeeded) {
      return Result.fail<User>(guardResult.message);
    }

    /*const isNewUser = !!id === false;*/
    const user = new User(
      {
        ...props,
        isDeleted: props.isDeleted ? props.isDeleted : false,
        isAdmin: props.isAdmin ? props.isAdmin : false,
        // isEmailVerified: props.isEmailVerified ? props.isEmailVerified : false,
      },
      id
    );

    /*if (isNewUser) {
      user.addDomainEvent(new UserCreated(user));
    }*/

    return Result.ok<User>(user);
  }
}
