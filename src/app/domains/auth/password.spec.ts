import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import * as bcrypt from 'bcrypt-nodejs';

import { Result } from '@/shared/core/Result';
import { Password } from './password';

let password: Password;
let passwordResult: Result<Password>;

describe('Modules / Users / Domain / Password', () => {
  describe('create()', () => {
    it('should pass if length is greater than 8.', () => {
      passwordResult = Password.create({ value: 'some password' });
      passwordResult.isSuccess.should.be.true;
    });

    it('should return value', () => {
      passwordResult = Password.create({ value: 'some password' });
      password = passwordResult.getValue();
      password.value.should.equal('some password');
    });

    it('should fail if argument is null or undefined.', () => {
      passwordResult = Password.create({ value: null });
      passwordResult.isSuccess.should.be.false;
    });

    it('should fail if length is less than 8.', () => {
      passwordResult = Password.create({ value: 'x' });
      passwordResult.isSuccess.should.be.false;
    });

    it('should return a error message if length is less than 8.', () => {
      passwordResult = Password.create({ value: 'x' });
      passwordResult.error.should.equal('Text password is not at least 8 chars.');
    });
  });

  describe('comparePassword()', () => {
    it('should return true if !isAlreadyHashed and password is equal', async () => {
      passwordResult = Password.create({ value: 'some password' });
      password = passwordResult.getValue();
      const comparedPassword = await password.comparePassword('some password');
      comparedPassword.should.be.true;
    });

    it('should return false if !isAlreadyHashed and password is not equal', async () => {
      passwordResult = Password.create({ value: 'some password' });
      password = passwordResult.getValue();
      const comparedPassword = await password.comparePassword('other password');
      comparedPassword.should.be.false;
    });

    it('should return true if isAlreadyHashed and password is equal', async () => {
      bcrypt.hash('some password', null, null, async (err, hash) => {
        passwordResult = Password.create({ value: hash, hashed: true });
        password = passwordResult.getValue();
        const comparedPassword = await password.comparePassword('some password');
        comparedPassword.should.be.true;
      });
    });

    it('should return false if isAlreadyHashed and password is not equal', async () => {
      bcrypt.hash('some password', null, null, async (err, hash) => {
        passwordResult = Password.create({ value: hash, hashed: true });
        password = passwordResult.getValue();
        const comparedPassword = await password.comparePassword('other password');
        comparedPassword.should.be.false;
      });
    });
  });

  describe('getHashedValue()', () => {
    it('should return the hashed value of the password', async () => {
      passwordResult = Password.create({ value: 'some password' });
      password = passwordResult.getValue();
      const hash = await password.getHashedValue();
      hash.should.be.string;
    });

    it('should return the hashed value of the password, even is already hashed', async () => {
      passwordResult = Password.create({
        value: 'some password',
        hashed: true,
      });
      password = passwordResult.getValue();
      const hash = await password.getHashedValue();
      hash.should.be.string;
    });
  });
});
