import { SessionItemDTO } from '@/app/dtos/timelineItemsDTO';

export interface GetTimelineResponseDTO {
  timeline: SessionItemDTO[];
}
