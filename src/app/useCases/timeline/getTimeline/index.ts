import { sessionsRepo } from '@/app/repositories/sessions';
import { GetTimeline } from './getTimeline';
import { GetTimelineController } from './getTimelineControler';

const getTimeline = new GetTimeline(sessionsRepo);

const getTimelineController = new GetTimelineController(getTimeline);

export { getTimelineController };
