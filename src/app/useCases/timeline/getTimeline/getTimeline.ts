import { Either, Result, left, right } from '@/shared/core/Result';
import { AppError } from '@/shared/core/AppError';
import { UseCase } from '@/shared/core/UseCase';

import { Timeline, TimelineProps } from '@/app/domains/timeline';
import { ISessionsRepo } from '@/app/repositories/sessions/ISessionsRepo';
import { SessionMap } from '@/app/mappers/sessions/sessionMap';
import { SessionItemDTO } from '@/app/dtos/timelineItemsDTO';

import { GetTimelineRequestDTO } from './getTimelineRequestDTO';
import { GetTimelineErrors } from './getTimelineErrors';

import logger from '@/shared/utils/logger';

type Response = Either<
  | GetTimelineErrors.NotAuthorizedToGetTimelineError
  | AppError.UnauthorizedError
  | AppError.UnexpectedError,
  Result<Timeline>
>;

export class GetTimeline implements UseCase<GetTimelineRequestDTO, Promise<Response>> {
  private sessionRepo: ISessionsRepo;

  constructor(sessionRepo: ISessionsRepo) {
    this.sessionRepo = sessionRepo;
  }

  public async execute(req: GetTimelineRequestDTO): Promise<Response> {
    const { user_id, period } = req;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    let days = 0;
    if (period === '7days') {
      days = 7;
    } else if (period === '14days') {
      days = 14;
    } else {
      days = 7;
    }

    try {
      const sessionsDetails = await this.sessionRepo.getSessionsForPeriod(days);

      const sessionsItems: SessionItemDTO[] = [];
      for (let index = 0; index < sessionsDetails.length; index++) {
        const session = sessionsDetails[index];
        const item = SessionMap.toTimelineItem(session);
        sessionsItems.push(item);
      }

      const timelineProps: TimelineProps = { timeline: sessionsItems };

      const timeline = Timeline.create(timelineProps);

      return right(Result.ok<Timeline>(timeline.getValue()));
    } catch (err) {
      logger.error(`[UseCase/GetTimeline] UseCase Error: ${JSON.stringify(err)}`);
      return left(new AppError.UnexpectedError(err));
    }
  }
}
