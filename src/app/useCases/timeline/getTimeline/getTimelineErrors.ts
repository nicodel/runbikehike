import { Result } from '@/shared/core/Result';
import { UseCaseError } from '@/shared/core/UseCaseError';

/* eslint-disable @typescript-eslint/no-namespace */
export namespace GetTimelineErrors {
  export class NotAuthorizedToGetTimelineError extends Result<UseCaseError> {
    constructor() {
      super(false, {
        message: 'You do not have permission to perform such action.',
      } as UseCaseError);
    }
  }
}
