import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { GetTimeline } from './getTimeline';
import { GetTimelineRequestDTO } from './getTimelineRequestDTO';
import { GetTimelineResponseDTO } from './getTimelineResponseDTO';

export class GetTimelineController extends BaseController {
  private useCase: GetTimeline;

  constructor(useCase: GetTimeline) {
    super();
    this.useCase = useCase;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected async executeImpl(req: Request | any, res: Response): Promise<Response> {
    const dto: GetTimelineRequestDTO = {
      period: req.params.period,
      user_id: req.user,
    };

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res, errMessage);
        } else {
          return this.badRequest(res, errMessage ? errMessage : error.errorValue().toString());
        }
      } else {
        const responseDTO: GetTimelineResponseDTO = result.value.getValue().props;
        return this.ok<GetTimelineResponseDTO>(res, responseDTO);
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
