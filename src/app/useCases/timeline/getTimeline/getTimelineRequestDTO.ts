export interface GetTimelineRequestDTO {
  user_id: string;
  period: string;
}
