import { GetImage } from './getImage';
import { imagesRepo } from '@/app/repositories/images';
import { GetImageController } from './getImageController';

const getImage = new GetImage(imagesRepo);

export const getImageController = new GetImageController(getImage);
