/* eslint-disable @typescript-eslint/no-namespace */
import { Result } from '@/shared/core/Result';
import { UseCaseError } from '@/shared/core/UseCaseError';

export namespace GetImageErrors {
  export class ImageNotFoundError extends Result<UseCaseError> {
    constructor() {
      super(false, {
        message: 'Image not found.',
      } as UseCaseError);
    }
  }

  export class NotAuthorizedToGetImageError extends Result<UseCaseError> {
    constructor() {
      super(false, {
        message: 'You do not have permission.',
      } as UseCaseError);
    }
  }
}
