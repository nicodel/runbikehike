import { ImagesDTO } from '@/app/dtos/imagesDTO';

export interface GetImageRequestDTO {
  image_id: string;
  user_id: string;
}

export type GetImageResponseDTO = ImagesDTO;
