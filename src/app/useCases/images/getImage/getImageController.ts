import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { ImageMap } from '@/app/mappers/imageMap';

import { GetImage } from './getImage';
import { GetImageRequestDTO, GetImageResponseDTO } from './getImageDTO';

import logger from '@/shared/utils/logger';

export class GetImageController extends BaseController {
  private useCase: GetImage;

  constructor(useCase: GetImage) {
    super();
    this.useCase = useCase;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected async executeImpl(req: Request | any, res: Response): Promise<any> {
    logger.debug('[UseCases/GetImage] Controller - req.params %s', req.params);
    logger.debug('[UseCases/GetImage] Controller - req.user %s', req.user);

    const dto: GetImageRequestDTO = {
      image_id: req.params.image_id,
      user_id: req.user,
    };

    try {
      const result = await this.useCase.execute(dto);
      logger.debug(`[UseCases/GetImage] Controller - result ${JSON.stringify(result)}`);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        logger.error(`[UseCases/GetImage] Controller Error ${JSON.stringify(error)}`);
        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res);
        } else if (errMessage === 'You do not have permission to perform such action.') {
          return this.forbidden(res, errMessage);
        } else if (errMessage === 'Image not found.') {
          return this.notFound(res, errMessage);
        } else {
          return this.fail(res, errMessage);
        }
      } else {
        const image = result.value.getValue();

        const imageDTO = ImageMap.toDTO(image);

        return this.ok<{ image: GetImageResponseDTO }>(res, { image: imageDTO });
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
