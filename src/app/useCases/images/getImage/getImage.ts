import { UseCase } from '@/shared/core/UseCase';
import { AppError } from '@/shared/core/AppError';
import { Either, Result, left, right } from '@/shared/core/Result';

import { Image } from '@/app/domains/image';
import { IImagesRepo } from '@/app/repositories/images/IImagesRepo';

import { GetImageRequestDTO } from './getImageDTO';
import { GetImageErrors } from './getImagesErrors';

import logger from '@/shared/utils/logger';

type Response = Either<
  | GetImageErrors.NotAuthorizedToGetImageError
  | GetImageErrors.ImageNotFoundError
  | AppError.UnexpectedError,
  Result<Image>
>;

export class GetImage implements UseCase<GetImageRequestDTO, Promise<Response>> {
  private imageRepo: IImagesRepo;

  constructor(imageRepo: IImagesRepo) {
    this.imageRepo = imageRepo;
  }

  public async execute(req: GetImageRequestDTO): Promise<Response> {
    const { user_id, image_id } = req;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    try {
      const imageDetails = await this.imageRepo.getByImageId(image_id);
      logger.debug(`[UseCase/GetImage] imageDetails ${JSON.stringify(imageDetails)}`);

      return right(Result.ok<Image>(imageDetails));
    } catch (err) {
      if (err === `Error: Image not found for id: ${image_id}`) {
        logger.error('[UseCase/GetImage] Image not found.: %s', image_id);
        return left(new GetImageErrors.ImageNotFoundError());
      }
      logger.error('[UseCase/GetImage] Unexpected Error.: %s', err);
      return left(new AppError.UnexpectedError(err));
    }
  }
}
