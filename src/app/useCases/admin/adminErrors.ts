/* eslint-disable @typescript-eslint/no-namespace */
import { Result } from '@/shared/core/Result';
import { UseCaseError } from '@/shared/core/UseCaseError';

export namespace AdminErrors {
  export class UserNotFoundError extends Result<UseCaseError> {
    constructor(id: string) {
      super(false, {
        message: `Could not find a user by id ${id}.`,
      } as UseCaseError);
    }
  }
}
