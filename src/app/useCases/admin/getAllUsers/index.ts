import { usersRepo } from '@/app/repositories/auth';
import { GetAllUsers } from './getAllUsers';
import { GetAllUsersController } from './getAllUsersController';

const getAllUsers = new GetAllUsers(usersRepo);
const getAllUsersController = new GetAllUsersController(getAllUsers);

export { getAllUsersController };
