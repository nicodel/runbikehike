import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { GetAllUsers } from './getAllUsers';
import { GetAllUsersRequestDTO, GetAllUsersResponseDTO } from './getAllUsersDTO';

import logger from '@/shared/utils/logger';
import { UsersMap } from '@/app/mappers/auth/usersMap';

export class GetAllUsersController extends BaseController {
  private useCase: GetAllUsers;

  constructor(useCase: GetAllUsers) {
    super();
    this.useCase = useCase;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected async executeImpl(req: Request | any, res: Response): Promise<Response> {
    const dto: GetAllUsersRequestDTO = {
      user_id: req.user,
    };

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res, errMessage);
        } else {
          return this.badRequest(res, errMessage ? errMessage : error.errorValue().toString());
        }
      } else {
        logger.debug(
          `[UseCase/GetAllUsers] Controller - result.value.getValue(): ${JSON.stringify(
            result.value.getValue()
          )}`
        );

        const usersAndProfiles = result.value.getValue();

        const users = [];
        for (let index = 0; index < usersAndProfiles.length; index++) {
          const userAndProfile = usersAndProfiles[index];
          const user = UsersMap.toAdminListing(userAndProfile);
          users.push(user);
        }

        const responseDTO: GetAllUsersResponseDTO = { all_users: users };

        logger.debug(
          `[UseCase/GetAllUsers] Controller - responseDTO: ${JSON.stringify(responseDTO)}`
        );

        return this.ok<GetAllUsersResponseDTO>(res, responseDTO);
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
