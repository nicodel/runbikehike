export interface GetAllUsersRequestDTO {
  user_id: string;
}

interface UserDTO {
  id: string;
  name: string;
  email: string;
  is_admin: boolean;
  is_disabled: boolean;
  avatar: {
    mimetype: string;
    data: string;
  };
}

export interface GetAllUsersResponseDTO {
  all_users: UserDTO[];
}
