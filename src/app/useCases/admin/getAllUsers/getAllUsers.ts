import { AppError } from '@/shared/core/AppError';
import { Either, Result, left, right } from '@/shared/core/Result';
import { UseCase } from '@/shared/core/UseCase';

import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';

import { GetAllUsersRequestDTO } from './getAllUsersDTO';

import logger from '@/shared/utils/logger';
import { User } from '@/app/domains/auth/user';
import { Profile } from '@/app/domains/users/profile';

type Response = Either<
  AppError.ForbiddenError | AppError.UnauthorizedError | AppError.UnexpectedError,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Result<any>
>;

export class GetAllUsers implements UseCase<GetAllUsersRequestDTO, Promise<Response>> {
  private usersRepo: IUsersRepo;

  constructor(usersRepo: IUsersRepo) {
    this.usersRepo = usersRepo;
  }

  public async execute(req: GetAllUsersRequestDTO): Promise<Response> {
    const { user_id } = req;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    const isAdmin = await this.usersRepo.isAdmin(user_id);
    if (!isAdmin) {
      return left(new AppError.ForbiddenError('get all users', user_id));
    }

    try {
      const usersAndProfiles = await this.usersRepo.getAllUsers();

      logger.debug(`[UseCase/GetAllUsers] usersAndProfiles: ${JSON.stringify(usersAndProfiles)}`);

      return right(Result.ok<{ user: User; profile: Profile }[]>(usersAndProfiles));
    } catch (err) {
      logger.error(`[UseCase/GetAllUsers] UseCase Error: ${JSON.stringify(err)}`);
      return left(new AppError.UnexpectedError(err));
    }
  }
}
