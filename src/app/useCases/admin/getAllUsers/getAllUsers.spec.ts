import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { usersRepo } from '@/app/repositories/auth';

import { fakeDestroy, fakeInjectData } from '@/fake_data';

import { GetAllUsers } from './getAllUsers';
import { fakeUsers } from '@/fake_data/users';

const getAllUsers = new GetAllUsers(usersRepo);

const dto = { user_id: fakeUsers.antoine_albeau.id };

describe('App / UseCases / Admin / GetAllUsers', function () {
  this.timeout(5000);

  before(async () => {
    await fakeInjectData();
  });

  after(async () => {
    await fakeDestroy();
  });
  it('should return Right result as a success', async () => {
    const result = await getAllUsers.execute(dto);
    result.isRight().should.be.true;
  });
});
