import { AppError } from '@/shared/core/AppError';
import { Either, Result, left, right } from '@/shared/core/Result';
import { UseCase } from '@/shared/core/UseCase';

import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';

import { DisableUserRequestDTO } from './disableUserDTO';

import logger from '@/shared/utils/logger';
import { AdminErrors } from '../adminErrors';

type Response = Either<
  | AppError.ForbiddenError
  | AppError.UnauthorizedError
  | AppError.UnexpectedError
  | AdminErrors.UserNotFoundError,
  Result<void>
>;

export class DisableUser implements UseCase<DisableUserRequestDTO, Promise<Response>> {
  private usersRepo: IUsersRepo;

  constructor(usersRepo: IUsersRepo) {
    this.usersRepo = usersRepo;
  }

  public async execute(req: DisableUserRequestDTO): Promise<Response> {
    const { user_id, user } = req;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    const isAdmin = await this.usersRepo.isAdmin(user_id);
    if (!isAdmin) {
      return left(new AppError.ForbiddenError('disable user', user_id));
    }

    try {
      const userResult = await this.usersRepo.getUserById(user);
      logger.debug(`[UseCase/DisableUser] userResult: ${JSON.stringify(userResult)}`);

      if (!userResult) {
        return left(new AdminErrors.UserNotFoundError(user));
      }

      userResult.isDisabled = true;
      logger.debug(`[UseCase/DisableUser] userResult disabled: ${JSON.stringify(userResult)}`);

      await this.usersRepo.save(userResult);

      return right(Result.ok<void>());
    } catch (err) {
      logger.error(`[UseCase/DisableUser] UseCase Error: ${JSON.stringify(err)}`);
      return left(new AppError.UnexpectedError(err));
    }
  }
}
