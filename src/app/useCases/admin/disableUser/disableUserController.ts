import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { DisableUser } from './disableUser';
import { DisableUserRequestDTO } from './disableUserDTO';

export class DisableUserController extends BaseController {
  private useCase: DisableUser;

  constructor(useCase: DisableUser) {
    super();
    this.useCase = useCase;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected async executeImpl(req: Request | any, res: Response): Promise<Response> {
    const dto: DisableUserRequestDTO = {
      user_id: req.user,
      user: req.params.user,
    };

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        if (errMessage === `Could not find a user by id ${dto.user}.`) {
          return this.notFound(res, 'User not found.');
        } else if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res, errMessage);
        } else {
          return this.badRequest(res, errMessage ? errMessage : error.errorValue().toString());
        }
      } else {
        return this.ok(res);
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
