import { usersRepo } from '@/app/repositories/auth';

import { DisableUser } from './disableUser';
import { DisableUserController } from './disableUserController';

const disableUser = new DisableUser(usersRepo);
const disableUserController = new DisableUserController(disableUser);

export { disableUserController };
