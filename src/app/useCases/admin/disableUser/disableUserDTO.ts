export interface DisableUserRequestDTO {
  user_id: string;
  user: string;
}
