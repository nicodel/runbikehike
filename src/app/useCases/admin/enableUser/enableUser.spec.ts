import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { fakeDestroy, fakeInjectData } from '@/fake_data';
import { EnableUser } from './enableUser';
import { usersRepo } from '@/app/repositories/auth';
import { fakeUsers } from '@/fake_data/users';

const enableUser = new EnableUser(usersRepo);

describe('App / UseCases / Admin / DisableUser', function () {
  this.timeout(5000);

  before(async () => {
    await fakeInjectData();
  });

  after(async () => {
    await fakeDestroy();
  });
  it('should return Right result as a success', async () => {
    const result = await enableUser.execute({
      user_id: fakeUsers.antoine_albeau.id,
      user: fakeUsers.chloe_mccardell.id,
    });
    result.isRight().should.be.true;
  });
});
