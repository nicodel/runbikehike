import { AppError } from '@/shared/core/AppError';
import { Either, Result, left, right } from '@/shared/core/Result';
import { UseCase } from '@/shared/core/UseCase';

import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';

import { EnableUserRequestDTO } from './enableUserDTO';

import logger from '@/shared/utils/logger';
import { AdminErrors } from '../adminErrors';

type Response = Either<
  | AppError.ForbiddenError
  | AppError.UnauthorizedError
  | AppError.UnexpectedError
  | AdminErrors.UserNotFoundError,
  Result<void>
>;

export class EnableUser implements UseCase<EnableUserRequestDTO, Promise<Response>> {
  private usersRepo: IUsersRepo;

  constructor(usersRepo: IUsersRepo) {
    this.usersRepo = usersRepo;
  }

  public async execute(req: EnableUserRequestDTO): Promise<Response> {
    const { user_id, user } = req;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    const isAdmin = await this.usersRepo.isAdmin(user_id);
    if (!isAdmin) {
      return left(new AppError.ForbiddenError('enable user', user_id));
    }

    try {
      const userResult = await this.usersRepo.getUserById(user);
      logger.debug(`[UseCase/EnableUser] userResult: ${JSON.stringify(userResult)}`);

      if (!userResult) {
        return left(new AdminErrors.UserNotFoundError(user));
      }

      userResult.isDisabled = false;
      logger.debug(`[UseCase/EnableUser] userResult disabled: ${JSON.stringify(userResult)}`);

      await this.usersRepo.save(userResult);

      /*
        return profileDetails === null
        ? left(new GetProfileErrors.ProfileNotFoundError(user_id))
        : right(Result.ok<Profile>(profileDetails));
      */
      return right(Result.ok<void>());
    } catch (err) {
      logger.error(`[UseCase/EnableUser] UseCase Error: ${JSON.stringify(err)}`);
      return left(new AppError.UnexpectedError(err));
    }
  }
}
