import { usersRepo } from '@/app/repositories/auth';

import { EnableUser } from './enableUser';
import { EnableUserController } from './enableUserController';

const enableUser = new EnableUser(usersRepo);
const enableUserController = new EnableUserController(enableUser);

export { enableUserController };
