import { AppError } from '@/shared/core/AppError';
import { Either, Result, left, right } from '@/shared/core/Result';
import { UseCase } from '@/shared/core/UseCase';

import { IProfilesRepo } from '@/app/repositories/users/IProfilesRepo';
import { IProfileAvatarsRepo } from '@/app/repositories/users/IProfileAvatarsRepo';

import { SearchUsersReponseDTO, SearchUsersRequestDTO } from './searchUsersDTO';
import { SearchUsersErrors } from './searchUsersErrors';

import logger from '@/shared/utils/logger';

type Response = Either<
  AppError.UnauthorizedError | AppError.UnexpectedError | SearchUsersErrors.NotEnoughLettersError,
  Result<SearchUsersReponseDTO>
>;

export class SearchUsers implements UseCase<SearchUsersRequestDTO, Promise<Response>> {
  private userProfilesRepo: IProfilesRepo;
  private profilesAvatarRepo: IProfileAvatarsRepo;

  constructor(userProfilesRepo: IProfilesRepo, profilesAvatarRepo: IProfileAvatarsRepo) {
    this.userProfilesRepo = userProfilesRepo;
    this.profilesAvatarRepo = profilesAvatarRepo;
  }

  public async execute(req: SearchUsersRequestDTO): Promise<Response> {
    const { user_id, search_letters } = req;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    if (search_letters.length < 3) {
      return left(new SearchUsersErrors.NotEnoughLettersError());
    }

    try {
      logger.debug(`[UseCase/SearchUsers] Searching for: ${JSON.stringify(search_letters)}`);

      const searchResults = await this.userProfilesRepo.searchByName(search_letters);
      logger.debug(`[UseCase/SearchUsers] Results: ${JSON.stringify(searchResults)}`);

      return right(Result.ok<SearchUsersReponseDTO>({ users: searchResults }));
    } catch (err) {
      logger.error(`[UseCase/SearchUsers] UseCase Error: ${JSON.stringify(err)}`);
      return left(new AppError.UnexpectedError(err));
    }
  }
}
