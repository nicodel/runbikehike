import { Result } from '@/shared/core/Result';
import { UseCaseError } from '@/shared/core/UseCaseError';

/* eslint-disable @typescript-eslint/no-namespace */
export namespace SearchUsersErrors {
  export class NotEnoughLettersError extends Result<UseCaseError> {
    constructor() {
      super(false, {
        message: 'You need to provide at least 3 letters to do a search',
      } as UseCaseError);
    }
  }
}
