import { avatarsRepo, profilesRepo } from '@/app/repositories/users';

import { SearchUsers } from './searchUsers';
import { SearchUsersController } from './searchUsersController';

const searchUsers = new SearchUsers(profilesRepo, avatarsRepo);
const searchUsersController = new SearchUsersController(searchUsers);

export { searchUsersController };
