/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { SearchUsers } from './searchUsers';
import { SearchUsersReponseDTO, SearchUsersRequestDTO } from './searchUsersDTO';

import logger from '@/shared/utils/logger';

export class SearchUsersController extends BaseController {
  private useCase: SearchUsers;

  constructor(useCase: SearchUsers) {
    super();
    this.useCase = useCase;
  }
  protected async executeImpl(req: Request | any, res: Response): Promise<Response> {
    const dto: SearchUsersRequestDTO = {
      user_id: req.user,
      search_letters: req.query.search,
    };

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res, errMessage);
        } else {
          return this.badRequest(res, errMessage ? errMessage : error.errorValue().toString());
        }
      } else {
        const responseDTO: SearchUsersReponseDTO = result.value.getValue();
        logger.debug(
          `[UseCase/SearchUsersController] Controller responseDTO: ${JSON.stringify(responseDTO)}`
        );

        return this.ok<SearchUsersReponseDTO>(res, responseDTO);
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
