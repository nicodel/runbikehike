export interface SearchUsersRequestDTO {
  user_id: string;
  search_letters: string;
}

export interface SearchUserReponseDTO {
  user: { id: string };
  name: string;
  avatar: {
    mimetype: string;
    data: string;
  } | null;
}

export interface SearchUsersReponseDTO {
  users: SearchUserReponseDTO[] | void;
}
