import { InitiateActivities } from './initiateActivities';
import { activitiesRepo } from '@/app/repositories/sessions';

const initiateActivities = new InitiateActivities(activitiesRepo);

export { initiateActivities };
