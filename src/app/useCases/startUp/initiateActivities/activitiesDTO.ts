export interface ActivityDTO {
  name: string;
  icon: string;
  use_distance: boolean;
}
