/* eslint-disable @typescript-eslint/no-explicit-any */
import { AppError } from '@/shared/core/AppError';
import { Either, Result, left, right } from '@/shared/core/Result';
import { UseCase } from '@/shared/core/UseCase';

import { ActivityDTO } from './activitiesDTO';
import { Activity } from '@/app/domains/sessions/activity';
import { IActivitiesRepo } from '@/app/repositories/sessions/IActivitiesRepo';

import logger from '@/shared/utils/logger';

type Response = Either<AppError.UnexpectedError | Result<any>, Result<void>>;

export class InitiateActivities implements UseCase<ActivityDTO[], Promise<Response>> {
  private activitiesRepo: IActivitiesRepo;

  constructor(activitiesRepo: IActivitiesRepo) {
    this.activitiesRepo = activitiesRepo;
  }

  public async execute(activities: ActivityDTO[]): Promise<Response> {
    try {
      logger.debug(
        `[UseCase/initiateActivities] UseCase - activities: ${JSON.stringify(activities)}`
      );
      for (let index = 0; index < activities.length; index++) {
        const activity = activities[index];

        const activityResult = Activity.create({
          name: activity.name,
          icon: activity.icon,
          use_distance: activity.use_distance,
        });
        if (activityResult.isFailure) {
          logger.error(
            `[UseCase/initiateActivities] UseCase - Error: ${JSON.stringify(activityResult)}`
          );
          return left(activityResult);
        }
        logger.debug(
          `[UseCase/initiateActivities] UseCase - activityResult: ${JSON.stringify(
            activityResult.getValue()
          )}`
        );
        await this.activitiesRepo.save(activityResult.getValue());
      }

      return right(Result.ok<void>());
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
