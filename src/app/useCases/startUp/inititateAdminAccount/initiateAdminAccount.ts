/* eslint-disable @typescript-eslint/no-explicit-any */
import { Either, Result, left, right } from '@/shared/core/Result';
import { UseCase } from '@/shared/core/UseCase';
import { Guard } from '@/shared/core/Guard';
import { AppError } from '@/shared/core/AppError';

import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';

import { Password } from '@/app/domains/auth/password';
import { User } from '@/app/domains/auth/user';
import { Email } from '@/app/domains/email';

import { AdminDTO } from './adminDTO';

import logger from '@/shared/utils/logger';
import { IProfilesRepo } from '@/app/repositories/users/IProfilesRepo';
import { Profile } from '@/app/domains/users/profile';
import { ShortText } from '@/app/domains/shortText';
import { Gender } from '@/app/domains/users/gender';
import { Birthyear } from '@/app/domains/users/birthyear';
import { Height } from '@/app/domains/users/height';
import { Weight } from '@/app/domains/users/weight';

type Response = Either<AppError.UnexpectedError | Result<any>, Result<void>>;

export class InitiateAdminAccount implements UseCase<AdminDTO, Promise<Response>> {
  private usersRepo: IUsersRepo;
  private profilesRepo: IProfilesRepo;

  constructor(usersRepo: IUsersRepo, profilesRepo: IProfilesRepo) {
    this.usersRepo = usersRepo;
    this.profilesRepo = profilesRepo;
  }

  public async execute(dto: AdminDTO): Promise<Response> {
    const dtoResult = Guard.againstNullOrUndefined(dto, 'dto');
    if (!dtoResult.succeeded) {
      logger.error(
        '[UseCase/InitiateAdminAccount] UseCase - Error againstNullOrUndefined(dto) error: %s',
        dtoResult.message
      );
      return left(new AppError.UnexpectedError(dtoResult.message));
    }
    const adminEmailResult = Email.create({ value: dto.email });
    const adminPasswordResult = Password.create({ value: dto.password, hashed: false });

    const adminDtoResult = Result.combine([adminEmailResult, adminPasswordResult]);
    if (adminDtoResult.isFailure) {
      const err = adminDtoResult.errorValue();
      logger.error(`[UseCase/InitiateAdminAccount] UseCase - adminDto Error: ${err}`);
      return left(new AppError.UnexpectedError(err));
    }

    const adminUserResult = User.create({
      email: adminEmailResult.getValue(),
      password: adminPasswordResult.getValue(),
      isAdmin: true,
      isDisabled: false,
    });
    if (adminUserResult.isFailure) {
      const err = adminUserResult.errorValue();
      logger.error(`[UseCase/InitiateAdminAccount] UseCase - adminUser Error: ${err}`);
      return left(new AppError.UnexpectedError(err));
    }
    try {
      const adminUser = adminUserResult.getValue();
      await this.usersRepo.save(adminUser);

      const emptyProfileResult = Profile.create({
        name: ShortText.create({ name: 'name', value: 'to be updated' }).getValue(),
        gender: Gender.create({ value: 'to be updated' }).getValue(),
        birthyear: Birthyear.create({ value: new Date().getFullYear() - 18 }).getValue(),
        height: Height.create({ value: 0 }).getValue(),
        weight: Weight.create({ value: 0 }).getValue(),
        user: adminUser,
      });
      if (emptyProfileResult.isFailure) {
        const err = emptyProfileResult.error.toString();
        logger.error(`[UseCase/InitiateAdminAccount] UseCase - emptyProfile Error: ${err}`);
        return left(new AppError.UnexpectedError(err));
      }

      const profile: Profile = emptyProfileResult.getValue();
      await this.profilesRepo.save(profile);

      return right(Result.ok<void>());
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
