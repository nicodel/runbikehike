import { usersRepo } from '@/app/repositories/auth';
import { profilesRepo } from '@/app/repositories/users';

import { InitiateAdminAccount } from './initiateAdminAccount';

const initiateAdminAccount = new InitiateAdminAccount(usersRepo, profilesRepo);

export { initiateAdminAccount };
