import { UseCase } from '@/shared/core/UseCase';
import { Guard } from '@/shared/core/Guard';
import { Either, Result, left, right } from '@/shared/core/Result';
import { AppError } from '@/shared/core/AppError';

import { SessionMap } from '@/app/mappers/sessions/sessionMap';
import { Session, SessionProps } from '@/app/domains/sessions/session';
import { GPSFile } from '@/app/domains/sessions/gpsFile';
import { BurnedCalories } from '@/app/domains/sessions/burnedCalories';
import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';
import { IActivitiesRepo } from '@/app/repositories/sessions/IActivitiesRepo';
import { IProfilesRepo } from '@/app/repositories/users/IProfilesRepo';

import { ImportGPSFileRequestDTO, ImportGPSFileResponseDTO } from './importGPSFileDTO';
import { ImportGPSFileErrors } from './importGPSFileErrors';

import logger from '@/shared/utils/logger';

type Response = Either<AppError.UnexpectedError, Result<ImportGPSFileResponseDTO>>;

export class ImportGPSFile implements UseCase<ImportGPSFileRequestDTO, Promise<Response>> {
  private usersRepo: IUsersRepo;
  private profilesRepo: IProfilesRepo;
  private activitiesRepo: IActivitiesRepo;

  constructor(usersRepo: IUsersRepo, profilesRepo: IProfilesRepo, activitiesRepo: IActivitiesRepo) {
    this.usersRepo = usersRepo;
    this.profilesRepo = profilesRepo;
    this.activitiesRepo = activitiesRepo;
  }

  public async execute(dto: ImportGPSFileRequestDTO): Promise<Response> {
    const dtoResult = Guard.againstNullOrUndefined(dto, 'dto');
    if (!dtoResult.succeeded) return left(new AppError.UnexpectedError(dtoResult.message));
    // logger.debug(`[UseCase ImportGPSFile] UseCase - dto: ${JSON.stringify(dto)}`);

    if (!dto.user_id) {
      return left(new AppError.UnauthorizedError(dto.user_id));
    }

    try {
      let parseGPSFile = null;
      if (dto.extension === 'gpx') {
        const parseGPSFileResult = GPSFile.parseGPX(dto.data);
        if (parseGPSFileResult.isFailure) {
          logger.error(
            `[UseCase ImportGPSFile] UseCase Error: Parsing XML error, ${JSON.stringify(
              parseGPSFileResult.error
            )}`
          );
          return left(new ImportGPSFileErrors.parsingXMLError(parseGPSFileResult.error));
        }
        parseGPSFile = parseGPSFileResult.getValue();
      } else {
        logger.error(
          `[UseCase ImportGPSFile] UseCase Error: Format not supported ${dto.extension}`
        );
        return left(new ImportGPSFileErrors.FormatNotSupported(dto.extension));
      }

      const userDetails = await this.usersRepo.getUserById(dto.user_id);
      const userFound = !!userDetails;
      if (!userFound) {
        logger.error(`[UseCase ImportGPSFile] UseCase Error: Unknown user ${dto.user_id}`);
        return left(new ImportGPSFileErrors.UnknownUser(dto.user_id));
      }

      const activityDetails = await this.activitiesRepo.getActivityByName(
        String(parseGPSFile.activity_name)
      );
      logger.debug(
        `[UseCase / ImportGPSFile] UseCase - activityDetails: ${JSON.stringify(activityDetails)} `
      );
      const activityFound = !!activityDetails;
      if (!activityFound) {
        logger.error(
          `[UseCase ImportGPSFile] UseCase Error: Activity not found ${parseGPSFile.activity_name}`
        );
        return left(new AppError.UnexpectedError(parseGPSFile.activity_name));
      }

      let burnedCalories = 0;
      let burnedCaloriesResult = null;
      if (parseGPSFile.distance) {
        const profileDetails = await this.profilesRepo.getProfile(dto.user_id);
        burnedCalories = BurnedCalories.calculate({
          weight: profileDetails.weight.value,
          distance: parseGPSFile.distance.value,
          duration: parseGPSFile.duration.value,
          activity_name: activityDetails.name,
        });
        logger.debug(
          `[UseCase / ImportGPSFile] UseCase - burnedCalories: ${JSON.stringify(burnedCalories)} `
        );
        burnedCaloriesResult = BurnedCalories.create({ value: burnedCalories });
        if (burnedCaloriesResult.isFailure) {
          logger.error(
            `[UseCase ImportGPSFile] UseCase Error: Burned Calories, ${burnedCaloriesResult.error.toString()}`
          );
          return left(new AppError.UnexpectedError(burnedCaloriesResult.error.toString()));
        }
        // logger.debug(
        //   `[UseCase / ImportGPSFile] UseCase - burnedCaloriesResult: ${JSON.stringify(
        //     burnedCaloriesResult
        //   )} `
        // );
      }

      const sessionProps: SessionProps = {
        name: parseGPSFile.name,
        start_date_time: parseGPSFile.start_date_time,
        duration: parseGPSFile.duration,
        distance: parseGPSFile.distance ? parseGPSFile.distance : null,
        average_speed: parseGPSFile.average_speed ? parseGPSFile.average_speed : null,
        burned_calories: burnedCalories ? burnedCaloriesResult.getValue() : null,
        description: parseGPSFile.description ? parseGPSFile.description : null,
        gps_track: parseGPSFile.gps_track,
        activity: activityDetails,
        user: userDetails,
        is_favorite: false,
        nb_comments: 0,
        nb_likes: 0,
      };

      const sessionResult = Session.create(sessionProps);

      return right(Result.ok<ImportGPSFileResponseDTO>(SessionMap.toDTO(sessionResult.getValue())));
    } catch (err) {
      logger.error(`[UseCase / ImportGPSFile] UseCase Error: ${JSON.stringify(err)}`);

      return left(new AppError.UnexpectedError(err));
    }
  }
}
