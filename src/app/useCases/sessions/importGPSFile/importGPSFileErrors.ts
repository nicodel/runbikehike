/* eslint-disable @typescript-eslint/no-namespace */
import { Result } from '@/shared/core/Result';
import { UseCaseError } from '@/shared/core/UseCaseError';

export namespace ImportGPSFileErrors {
  export class FormatNotSupported extends Result<UseCaseError> {
    constructor(extension: string) {
      super(false, { message: `Format "${extension}" not supported.` } as UseCaseError);
    }
  }

  export class parsingXMLError extends Result<UseCaseError> {
    constructor(err) {
      super(false, { message: `Error parsing XML: ${err}` } as UseCaseError);
    }
  }

  export class UnknownUser extends Result<UseCaseError> {
    constructor(user: string) {
      super(false, {
        message: `Unknown user "${user}".`,
      } as UseCaseError);
    }
  }
}
