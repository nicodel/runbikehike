import { SessionsDTO } from '@/app/dtos/sessionsDTO';

export interface ImportGPSFileRequestDTO {
  user_id: string;
  extension: string;
  data: string;
}

export type ImportGPSFileResponseDTO = SessionsDTO;
