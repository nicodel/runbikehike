import { usersRepo } from '@/app/repositories/auth';
import { activitiesRepo } from '@/app/repositories/sessions';
import { ImportGPSFileController } from './importGPSFileController';
import { ImportGPSFile } from './importGPSFile';
import { profilesRepo } from '@/app/repositories/users';

const importGPSFile = new ImportGPSFile(usersRepo, profilesRepo, activitiesRepo);

const importGPSFileController = new ImportGPSFileController(importGPSFile);

export { importGPSFileController };
