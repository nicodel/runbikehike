import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { fakeInjectData, fakeDestroy } from '@/fake_data';
import { fakeUsers } from '@/fake_data/users';
import {
  GPX_DURATION,
  GPX_ERROR_GPX_MISSING,
  GPX_ERROR_TRKSEG_MISSING,
  GPX_ERROR_TRK_MISSING,
  GPX_GPS_NO_ELE,
  GPX_GPS_NO_LAT,
  GPX_GPS_NO_LON,
  GPX_GPS_NO_TIME,
  GPX_NO_TIME,
} from '@/fake_data/gpsFiles/GpsFiles';

import { usersRepo } from '@/app/repositories/auth';
import { activitiesRepo } from '@/app/repositories/sessions';
import { profilesRepo } from '@/app/repositories/users';

import { ImportGPSFile } from './importGPSFile';

const importGPSFile = new ImportGPSFile(usersRepo, profilesRepo, activitiesRepo);

describe('App / UseCases / Sessions / ImportGPSFile', () => {
  describe('execute()', () => {
    before(async () => {
      await fakeInjectData('light');
    });

    after(async () => {
      await fakeDestroy();
    });

    it('should return Right result as a success', async () => {
      const result = await importGPSFile.execute({
        user_id: fakeUsers.chloe_mccardell.id,
        extension: 'gpx',
        data: GPX_DURATION,
      });
      result.value.isSuccess.should.be.true;
    });

    it('should return an ImportGPSFileResponseDTO', async () => {
      const result = await importGPSFile.execute({
        user_id: fakeUsers.chloe_mccardell.id,
        extension: 'gpx',
        data: GPX_DURATION,
      });
      const track = result.value.getValue();
      track.should.have.keys([
        'activity_name',
        'average_heart_rate',
        'average_speed',
        'burned_calories',
        'description',
        'distance',
        'duration',
        'elevation_gain',
        'gps_charts',
        'gps_track',
        'id',
        'images_ids',
        'is_favorite',
        'location',
        'name',
        'nb_comments',
        'nb_likes',
        'start_date_time',
        'user_id',
      ]);
    });

    it('should return an Error, if extension is not supported', async () => {
      const result = await importGPSFile.execute({
        user_id: fakeUsers.chloe_mccardell.id,
        extension: 'notgpx',
        data: GPX_DURATION,
      });
      result.value.isFailure.should.be.true;
      const error = result.value.errorValue();
      error['message'].should.equal('Format "notgpx" not supported.');
    });

    it('should return an Error, if file is malformed (missing "gpx")', async () => {
      const result = await importGPSFile.execute({
        user_id: fakeUsers.chloe_mccardell.id,
        extension: 'gpx',
        data: GPX_ERROR_GPX_MISSING,
      });
      result.value.isFailure.should.be.true;
      const error = result.value.errorValue();
      error['message'].should.equal('Error parsing XML: Provided JSON file is missing "gpx" key.');
    });

    it('should return an Error, if file is malformed (missing "trk")', async () => {
      const result = await importGPSFile.execute({
        user_id: fakeUsers.chloe_mccardell.id,
        extension: 'gpx',
        data: GPX_ERROR_TRK_MISSING,
      });
      result.value.isFailure.should.be.true;
      const error = result.value.errorValue();
      error['message'].should.equal('Error parsing XML: Provided JSON file is missing "trk" key.');
    });

    it('should return an Error, if file is malformed (missing "trkseg")', async () => {
      const result = await importGPSFile.execute({
        user_id: fakeUsers.chloe_mccardell.id,
        extension: 'gpx',
        data: GPX_ERROR_TRKSEG_MISSING,
      });
      result.value.isFailure.should.be.true;
      const error = result.value.errorValue();
      error['message'].should.equal(
        'Error parsing XML: Provided JSON file is missing "trkseg" key.'
      );
    });

    it('should return an Error, if file is malformed (missing "time")', async () => {
      const result = await importGPSFile.execute({
        user_id: fakeUsers.chloe_mccardell.id,
        extension: 'gpx',
        data: GPX_NO_TIME,
      });
      result.value.isFailure.should.be.true;
      const error = result.value.errorValue();
      error['message'].should.equal('Error parsing XML: Provided JSON file is missing "time" key.');
    });

    it('should return an Error, if file is malformed (missing "trkpt[time]")', async () => {
      const result = await importGPSFile.execute({
        user_id: fakeUsers.chloe_mccardell.id,
        extension: 'gpx',
        data: GPX_GPS_NO_TIME,
      });
      result.value.isFailure.should.be.true;
      const error = result.value.errorValue();
      error['message'].should.equal('Error parsing XML: Provided JSON file is missing "time" key.');
    });

    it('should return an Error, if file is malformed (missing "trkpt[lat]")', async () => {
      const result = await importGPSFile.execute({
        user_id: fakeUsers.chloe_mccardell.id,
        extension: 'gpx',
        data: GPX_GPS_NO_LAT,
      });
      result.value.isFailure.should.be.true;
      const error = result.value.errorValue();
      error['message'].should.equal('Error parsing XML: Provided JSON file is missing "lat" key.');
    });

    it('should return an Error, if file is malformed (missing "trkpt[lon]")', async () => {
      const result = await importGPSFile.execute({
        user_id: fakeUsers.chloe_mccardell.id,
        extension: 'gpx',
        data: GPX_GPS_NO_LON,
      });
      result.value.isFailure.should.be.true;
      const error = result.value.errorValue();
      error['message'].should.equal('Error parsing XML: Provided JSON file is missing "lon" key.');
    });

    it('should return an Error, if file is malformed (missing "trkpt[ele]")', async () => {
      const result = await importGPSFile.execute({
        user_id: fakeUsers.chloe_mccardell.id,
        extension: 'gpx',
        data: GPX_GPS_NO_ELE,
      });
      result.value.isFailure.should.be.true;
      const error = result.value.errorValue();
      error['message'].should.equal('Error parsing XML: Provided JSON file is missing "ele" key.');
    });
  });
});
