/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { ImportGPSFile } from './importGPSFile';
import { ImportGPSFileRequestDTO, ImportGPSFileResponseDTO } from './importGPSFileDTO';

export class ImportGPSFileController extends BaseController {
  private useCase: ImportGPSFile;

  constructor(useCase: ImportGPSFile) {
    super();
    this.useCase = useCase;
  }

  protected async executeImpl(req: Request | any, res: Response): Promise<any> {
    const dto: ImportGPSFileRequestDTO = { ...req.body, user_id: req.user as Request };
    // logger.debug(`[UseCase ImportGPSFile] Controller - req.body: ${JSON.stringify(req.body)}`);

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res, errMessage);
        } else {
          return this.badRequest(res, errMessage ? errMessage : error.errorValue().toString());
        }
      } else {
        const responseDTO: ImportGPSFileResponseDTO = result.value.getValue();

        // logger.debug(
        //   `[UseCase / ImportGPSFile] Controller - responseDTO: ${JSON.stringify(responseDTO)}`
        // );
        return this.ok<ImportGPSFileResponseDTO>(res, responseDTO);
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
