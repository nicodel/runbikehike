import { SessionMetadataDTO } from '@/app/dtos/sessionsDTO';

export interface GetSessionMetadataRequestDTO {
  user_id: string;
  session_id: string;
}

export interface GetSessionMetadataResponseDTO {
  session_metadata: SessionMetadataDTO;
}
