import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { usersRepo } from '@/app/repositories/auth';
import { sessionsRepo } from '@/app/repositories/sessions';

import { fakeDestroy, fakeInjectData } from '@/fake_data';
import { fakeSessions } from '@/fake_data/sessions';

import { GetSessionMetadata } from './getSessionMetadata';
import { GetSessionMetadataRequestDTO } from './getSessionMetadataDTO';

const getSessionMetadata = new GetSessionMetadata(sessionsRepo, usersRepo);

const dto: GetSessionMetadataRequestDTO = {
  user_id: fakeSessions[0].user.id,
  session_id: fakeSessions[0].id,
};

describe('App / UseCases / Session / GetSessionMetadata', async () => {
  before(async () => {
    await fakeInjectData('light');
  });

  after(async () => {
    await fakeDestroy();
  });

  it('should return Right result as a success', async () => {
    const result = await getSessionMetadata.execute(dto);
    result.isRight().should.be.true;
  });
});
