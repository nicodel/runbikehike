import { sessionsRepo } from '@/app/repositories/sessions';
import { usersRepo } from '@/app/repositories/auth';

import { GetSessionMetadata } from './getSessionMetadata';
import { GetSessionMetadataController } from './getSessionMetadataController';

const getSessionMetadata = new GetSessionMetadata(sessionsRepo, usersRepo);

export const getSessionMetadataController = new GetSessionMetadataController(getSessionMetadata);
