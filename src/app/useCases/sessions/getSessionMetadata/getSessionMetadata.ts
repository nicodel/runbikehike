import { UseCase } from '@/shared/core/UseCase';
import { AppError } from '@/shared/core/AppError';
import { Either, Result, left, right } from '@/shared/core/Result';

import { Session } from '@/app/domains/sessions/session';

import { ISessionsRepo } from '@/app/repositories/sessions/ISessionsRepo';
import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';

import { SessionErrors } from '../sessionErrors';
import { GetSessionMetadataRequestDTO } from './getSessionMetadataDTO';

import logger from '@/shared/utils/logger';

type Response = Either<
  | AppError.UnexpectedError
  | AppError.ForbiddenError
  | SessionErrors.UnknownSession
  | SessionErrors.UnknownUser
  | SessionErrors.WrongValue
  | SessionErrors.MissingMandatoryValue
  | SessionErrors.UnknownComment,
  Result<Session>
>;

export class GetSessionMetadata
  implements UseCase<GetSessionMetadataRequestDTO, Promise<Response>>
{
  private sessionRepo: ISessionsRepo;
  private usersRepo: IUsersRepo;

  constructor(sessionRepo: ISessionsRepo, usersRepo: IUsersRepo) {
    this.sessionRepo = sessionRepo;
    this.usersRepo = usersRepo;
  }

  public async execute(req: GetSessionMetadataRequestDTO): Promise<Response> {
    const { user_id } = req;
    const { session_id } = req;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    try {
      const sessionDetails = await this.sessionRepo.getBySessionId(session_id);
      logger.debug('[UseCase/GetSessionMetadata] sessionDetails: %s', sessionDetails);

      logger.debug(
        '[UseCase/GetSessionMetadata] sessionDetails.user.id.toString(): %s',
        sessionDetails.user.id.toString()
      );
      logger.debug('[UseCase/GetSessionMetadata] user_id: %s', user_id);

      const isUserAdmin = await this.usersRepo.isAdmin(user_id);
      if (isUserAdmin === false && user_id !== sessionDetails.user.id.toString()) {
        return left(new AppError.ForbiddenError(`get session metadata ${session_id}`, user_id));
      }

      return right(Result.ok<Session>(sessionDetails));
    } catch (err) {
      if (err === `Error: Session not found for id: ${session_id}`) {
        return left(new SessionErrors.UnknownSession(session_id));
      }
      return left(new AppError.UnexpectedError(err));
    }
  }
}
