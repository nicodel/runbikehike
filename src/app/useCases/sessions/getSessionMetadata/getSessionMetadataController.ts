/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { SessionMap } from '@/app/mappers/sessions/sessionMap';

import { GetSessionMetadata } from './getSessionMetadata';
import {
  GetSessionMetadataRequestDTO,
  GetSessionMetadataResponseDTO,
} from './getSessionMetadataDTO';

import logger from '@/shared/utils/logger';

export class GetSessionMetadataController extends BaseController {
  private useCase: GetSessionMetadata;

  constructor(useCase: GetSessionMetadata) {
    super();
    this.useCase = useCase;
  }

  protected async executeImpl(req: Request | any, res: Response): Promise<any> {
    logger.debug('[UseCases/GetSessionMetadata] Controller - req.params %s', req.params);
    logger.debug('[UseCases/GetSessionMetadata] Controller - req.user %s', req.user);

    const dto: GetSessionMetadataRequestDTO = {
      session_id: req.params.session_id,
      user_id: req.user,
    };

    try {
      const result = await this.useCase.execute(dto);
      logger.debug('[UseCases/GetSessionMetadata] Controller - result %s', result);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        logger.error(
          `[UseCases/GetSessionMetadata] Controller Error ${JSON.stringify(errMessage)}`
        );
        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res);
        } else if (errMessage === 'You do not have permission to perform such action.') {
          return this.forbidden(res, errMessage);
        } else if (errMessage === 'Session not found.') {
          return this.notFound(res, errMessage);
        } else {
          return this.fail(res, errMessage);
        }
      } else {
        const session = result.value.getValue();

        const metadataDTO = SessionMap.toMetadaDTO(session);
        logger.debug('[UseCases/GetSessionMetadata] Controller - metadataDTO %s', metadataDTO);

        return this.ok<GetSessionMetadataResponseDTO>(res, { session_metadata: metadataDTO });
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
