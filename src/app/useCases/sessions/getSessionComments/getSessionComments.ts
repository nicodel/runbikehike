import { AppError } from '@/shared/core/AppError';
import { Either, Result, left, right } from '@/shared/core/Result';
import { UseCase } from '@/shared/core/UseCase';

import { ICommentsRepo } from '@/app/repositories/sessions/ICommentsRepo';
import { ISessionsRepo } from '@/app/repositories/sessions/ISessionsRepo';
import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';
import { Comment } from '@/app/domains/comment';

import { SessionErrors } from '../sessionErrors';
import { GetSessionCommentsRequestDTO } from './getSessionCommentsDTO';

import logger from '@/shared/utils/logger';

type Response = Either<
  | AppError.UnexpectedError
  | AppError.ForbiddenError
  | SessionErrors.UnknownSession
  | SessionErrors.UnknownUser
  | SessionErrors.WrongValue
  | SessionErrors.MissingMandatoryValue
  | SessionErrors.UnknownComment,
  Result<Comment[]>
>;

export class GetSessionComments
  implements UseCase<GetSessionCommentsRequestDTO, Promise<Response>>
{
  private sessionRepo: ISessionsRepo;
  private usersRepo: IUsersRepo;
  private commentsRepo: ICommentsRepo;

  constructor(sessionRepo: ISessionsRepo, usersRepo: IUsersRepo, commentsRepo: ICommentsRepo) {
    this.sessionRepo = sessionRepo;
    this.usersRepo = usersRepo;
    this.commentsRepo = commentsRepo;
  }

  public async execute(req: GetSessionCommentsRequestDTO): Promise<Response> {
    const { user_id } = req;
    const { session_id } = req;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    try {
      const session_owner_id = await this.sessionRepo.getUserId(session_id);
      if (session_owner_id !== user_id) {
        return left(new AppError.ForbiddenError(`get session ${session_id}`, user_id));
      }

      const comments = await this.commentsRepo.getCommentsBySessionId(session_id);
      logger.debug(`[UseCase/GetSessionComments] comments ${JSON.stringify(comments)}`);

      return right(Result.ok<Comment[]>(comments));
    } catch (err) {
      if (err === `Error: Session not found for id: ${session_id}`) {
        return left(new SessionErrors.UnknownSession(session_id));
      }
      return left(new AppError.UnexpectedError(err));
    }
  }
}
