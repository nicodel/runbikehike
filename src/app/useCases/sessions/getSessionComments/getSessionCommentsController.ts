/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { CommentMap } from '@/app/mappers/sessions/commentMap';

import {
  GetSessionCommentsRequestDTO,
  GetSessionCommentsResponseDTO,
} from './getSessionCommentsDTO';
import { GetSessionComments } from './getSessionComments';

import logger from '@/shared/utils/logger';

export class GetSessionCommentsController extends BaseController {
  private useCase: GetSessionComments;

  constructor(useCase: GetSessionComments) {
    super();
    this.useCase = useCase;
  }

  protected async executeImpl(req: Request | any, res: Response): Promise<any> {
    logger.debug('[UseCases/GetSessionComments] Controller - req.params %s', req.params);
    logger.debug('[UseCases/GetSessionComments] Controller - req.user %s', req.user);

    const dto: GetSessionCommentsRequestDTO = {
      session_id: req.params.session_id,
      user_id: req.user,
    };

    try {
      const result = await this.useCase.execute(dto);
      logger.debug('[UseCases/GetSessionComments] Controller - result %s', result);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        logger.error(
          `[UseCases/GetSessionComments] Controller Error ${JSON.stringify(errMessage)}`
        );
        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res);
        } else if (errMessage === 'You do not have permission to perform such action.') {
          return this.forbidden(res, errMessage);
        } else if (errMessage === 'Session not found.') {
          return this.notFound(res, errMessage);
        } else {
          return this.fail(res, errMessage);
        }
      } else {
        const comments = result.value.getValue();
        logger.debug('[UseCases/GetSessionComments] Controller - comments %s', comments);

        const commentsDTO = comments.map((image) => CommentMap.toDTO(image));
        logger.debug('[UseCases/GetSessionComments] Controller - commentsDTO %s', commentsDTO);

        return this.ok<GetSessionCommentsResponseDTO>(res, { session_comments: commentsDTO });
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
