import { commentsRepo, sessionsRepo } from '@/app/repositories/sessions';
import { usersRepo } from '@/app/repositories/auth';

import { GetSessionComments } from './getSessionComments';
import { GetSessionCommentsController } from './getSessionCommentsController';

const getSessionComments = new GetSessionComments(sessionsRepo, usersRepo, commentsRepo);

export const getSessionCommentsController = new GetSessionCommentsController(getSessionComments);
