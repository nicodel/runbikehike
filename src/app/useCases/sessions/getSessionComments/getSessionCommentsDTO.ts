import { CommentDTO } from '@/app/dtos/commentDTO';

export interface GetSessionCommentsRequestDTO {
  user_id: string;
  session_id: string;
}

export interface GetSessionCommentsResponseDTO {
  session_comments: CommentDTO[];
}
