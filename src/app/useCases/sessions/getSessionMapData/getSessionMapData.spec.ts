import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { usersRepo } from '@/app/repositories/auth';
import { sessionsRepo } from '@/app/repositories/sessions';

import { fakeSessions } from '@/fake_data/sessions';
import { fakeDestroy, fakeInjectData } from '@/fake_data';

import { GetSessionMapData } from './getSessionMapData';

const getSessionMapData = new GetSessionMapData(sessionsRepo, usersRepo);

describe('App / UseCases / Session / GetSessionMapData', function () {
  this.timeout(5000);

  before(async () => {
    await fakeInjectData();
  });

  after(async () => {
    await fakeDestroy();
  });

  it('should return Right result as a success', async () => {
    const result = await getSessionMapData.execute({
      user_id: fakeSessions[2].user.id,
      session_id: fakeSessions[2].id,
    });
    result.isRight().should.be.true;
  });

  it('should return Right result as a success, even if no data is retreived from session', async () => {
    const result = await getSessionMapData.execute({
      user_id: fakeSessions[1].user.id,
      session_id: fakeSessions[1].id,
    });
    result.isRight().should.be.true;
  });
});
