import { SessionMapDataDTO } from '@/app/dtos/sessionsDTO';

export interface GetSessionMapDataRequestDTO {
  user_id: string;
  session_id: string;
}

export interface GetSessionMapDataResponseDTO {
  map_data: SessionMapDataDTO;
}
