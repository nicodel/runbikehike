/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { TrackMap } from '@/app/mappers/sessions/trackMap';

import { GetSessionMapDataRequestDTO, GetSessionMapDataResponseDTO } from './getSessionMapDataDTO';
import { GetSessionMapData } from './getSessionMapData';

import logger from '@/shared/utils/logger';

export class GetSessionMapDataController extends BaseController {
  private useCase: GetSessionMapData;

  constructor(useCase: GetSessionMapData) {
    super();
    this.useCase = useCase;
  }

  protected async executeImpl(req: Request | any, res: Response): Promise<any> {
    logger.debug('[UseCases/GetSessionMapData] Controller - req.params %s', req.params);
    logger.debug('[UseCases/GetSessionMapData] Controller - req.user %s', req.user);

    const dto: GetSessionMapDataRequestDTO = {
      session_id: req.params.session_id,
      user_id: req.user,
    };

    try {
      const result = await this.useCase.execute(dto);
      logger.debug('[UseCases/GetSessionMapData] Controller - result %s', result);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        logger.error(`[UseCases/GetSessionMapData] Controller Error ${JSON.stringify(errMessage)}`);
        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res);
        } else if (errMessage === 'You do not have permission to perform such action.') {
          return this.forbidden(res, errMessage);
        } else if (errMessage === 'Session not found.') {
          return this.notFound(res, errMessage);
        } else {
          return this.fail(res, errMessage);
        }
      } else {
        const track = result.value.getValue();
        logger.debug('[UseCases/GetSessionMapData] Controller - track %s', track);

        const mapDTO = track ? TrackMap.toMap(track) : null;
        logger.debug('[UseCases/GetSessionMapData] Controller - mapDTO %s', mapDTO);

        return this.ok<GetSessionMapDataResponseDTO>(res, { map_data: mapDTO });
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
