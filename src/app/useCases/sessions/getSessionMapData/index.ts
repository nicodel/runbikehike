import { sessionsRepo } from '@/app/repositories/sessions';
import { usersRepo } from '@/app/repositories/auth';

import { GetSessionMapData } from './getSessionMapData';
import { GetSessionMapDataController } from './getSessionMapDataController';

const getSessionMapData = new GetSessionMapData(sessionsRepo, usersRepo);

export const getSessionMapDataController = new GetSessionMapDataController(getSessionMapData);
