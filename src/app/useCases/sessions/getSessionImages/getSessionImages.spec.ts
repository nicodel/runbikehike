import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { usersRepo } from '@/app/repositories/auth';
import { sessionsRepo } from '@/app/repositories/sessions';
import { imagesRepo } from '@/app/repositories/images';

import { fakeDestroy, fakeInjectData } from '@/fake_data';
import { fakeSessions } from '@/fake_data/sessions';

import { GetSessionImages } from './getSessionImages';
import { GetSessionImagesRequestDTO } from './getSessionImagesDTO';

const getSessionImages = new GetSessionImages(sessionsRepo, usersRepo, imagesRepo);

const dto: GetSessionImagesRequestDTO = {
  user_id: fakeSessions[2].user.id,
  session_id: fakeSessions[2].id,
};

describe('App / UseCases / Session / GetSessionImages', function () {
  this.timeout(5000);

  before(async () => {
    await fakeInjectData();
  });

  after(async () => {
    await fakeDestroy();
  });

  it('should return Right result as a success', async () => {
    const result = await getSessionImages.execute(dto);
    result.isRight().should.be.true;
  });
});
