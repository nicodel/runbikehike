/* eslint-disable @typescript-eslint/no-explicit-any */
import { BaseController } from '@/shared/infra/BaseController';
import { Request, Response } from 'express';

import { ImageMap } from '@/app/mappers/imageMap';

import { GetSessionImagesRequestDTO, GetSessionImagesResponseDTO } from './getSessionImagesDTO';
import { GetSessionImages } from './getSessionImages';

import logger from '@/shared/utils/logger';

export class GetSessionImagesController extends BaseController {
  private useCase: GetSessionImages;

  constructor(useCase: GetSessionImages) {
    super();
    this.useCase = useCase;
  }

  protected async executeImpl(req: Request | any, res: Response): Promise<any> {
    logger.debug('[UseCases/GetSessionImages] Controller - req.params %s', req.params);
    logger.debug('[UseCases/GetSessionImages] Controller - req.user %s', req.user);

    const dto: GetSessionImagesRequestDTO = {
      session_id: req.params.session_id,
      user_id: req.user,
    };

    try {
      const result = await this.useCase.execute(dto);
      logger.debug('[UseCases/GetSessionImages] Controller - result %s', result);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        logger.error(`[UseCases/GetSessionImages] Controller Error ${JSON.stringify(errMessage)}`);
        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res);
        } else if (errMessage === 'You do not have permission to perform such action.') {
          return this.forbidden(res, errMessage);
        } else if (errMessage === 'Session not found.') {
          return this.notFound(res, errMessage);
        } else {
          return this.fail(res, errMessage);
        }
      } else {
        const images = result.value.getValue();
        logger.debug('[UseCases/GetSessionImages] Controller - images %s', images);

        const imagesDTO = images.map((image) => ImageMap.toDTO(image));
        logger.debug('[UseCases/GetSessionImages] Controller - imagesDTO %s', imagesDTO);

        return this.ok<GetSessionImagesResponseDTO>(res, { session_images: imagesDTO });
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
