import { ImagesDTO } from '@/app/dtos/imagesDTO';

export interface GetSessionImagesRequestDTO {
  user_id: string;
  session_id: string;
}

export interface GetSessionImagesResponseDTO {
  session_images: ImagesDTO[];
}
