import { sessionsRepo } from '@/app/repositories/sessions';
import { usersRepo } from '@/app/repositories/auth';
import { imagesRepo } from '@/app/repositories/images';

import { GetSessionImagesController } from './getSessionImagesController';
import { GetSessionImages } from './getSessionImages';

const getSessionImages = new GetSessionImages(sessionsRepo, usersRepo, imagesRepo);

export const getSessionImagescontroller = new GetSessionImagesController(getSessionImages);
