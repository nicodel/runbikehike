import { AppError } from '@/shared/core/AppError';
import { Either, Result, left, right } from '@/shared/core/Result';
import { UseCase } from '@/shared/core/UseCase';

import { ISessionsRepo } from '@/app/repositories/sessions/ISessionsRepo';
import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';
import { IImagesRepo } from '@/app/repositories/images/IImagesRepo';

import { Image } from '@/app/domains/image';

import { SessionErrors } from '../sessionErrors';
import { GetSessionImagesRequestDTO } from './getSessionImagesDTO';

import logger from '@/shared/utils/logger';

type Response = Either<
  | AppError.UnexpectedError
  | AppError.ForbiddenError
  | SessionErrors.UnknownSession
  | SessionErrors.UnknownUser
  | SessionErrors.WrongValue
  | SessionErrors.MissingMandatoryValue
  | SessionErrors.UnknownComment,
  Result<Image[]>
>;

export class GetSessionImages implements UseCase<GetSessionImagesRequestDTO, Promise<Response>> {
  private sessionRepo: ISessionsRepo;
  private usersRepo: IUsersRepo;
  private imageRepo: IImagesRepo;

  constructor(sessionRepo: ISessionsRepo, usersRepo: IUsersRepo, imageRepo: IImagesRepo) {
    this.sessionRepo = sessionRepo;
    this.usersRepo = usersRepo;
    this.imageRepo = imageRepo;
  }

  public async execute(req: GetSessionImagesRequestDTO): Promise<Response> {
    const { user_id } = req;
    const { session_id } = req;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    try {
      const session_owner_id = await this.sessionRepo.getUserId(session_id);
      const isUserAdmin = await this.usersRepo.isAdmin(user_id);
      if (isUserAdmin === false && user_id !== session_owner_id) {
        return left(new AppError.ForbiddenError(`get session images ${session_id}`, user_id));
      }

      const images = await this.imageRepo.getImagesBySessionId(session_id);
      logger.debug(`[UseCase/GetSessionImages] images ${JSON.stringify(images)}`);

      return right(Result.ok<Image[]>(images));
    } catch (err) {
      if (err === `Error: Session not found for id: ${session_id}`) {
        return left(new SessionErrors.UnknownSession(session_id));
      }
      return left(new AppError.UnexpectedError(err));
    }
  }
}
