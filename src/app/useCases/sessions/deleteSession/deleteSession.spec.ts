import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { fakeDestroy, fakeInjectData } from '@/fake_data';
import { fakeSessions } from '@/fake_data/sessions';

import { DeleteSession } from './deleteSession';
import { sessionsRepo } from '@/app/repositories/sessions';
import { usersRepo } from '@/app/repositories/auth';
import { fakeUsers } from '@/fake_data/users';

const deleteSession = new DeleteSession(sessionsRepo, usersRepo);

describe('App / UseCases / Sessions / DeleteSession', () => {
  before(async () => {
    await fakeInjectData('light');
  });

  after(async () => {
    await fakeDestroy();
  });
  it('should return Right result as a success, when user is owner', async () => {
    const result = await deleteSession.execute({
      session_id: fakeSessions[0].id,
      user_id: fakeSessions[0].user.id,
    });
    result.isRight().should.be.true;
  });

  it('should return Right result as a success, when user is admin', async () => {
    const result = await deleteSession.execute({
      session_id: fakeSessions[0].id,
      user_id: fakeUsers.antoine_albeau.id,
    });
    result.isRight().should.be.true;
  });

  it('should return Left result as a failure, if user is not owner neither admin', async () => {
    const result = await deleteSession.execute({
      session_id: fakeSessions[0].id,
      user_id: fakeUsers.forrest_gump.id,
    });
    result.isLeft().should.be.true;
  });
});
