/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { DeleteSession } from './deleteSession';
import { DeleteSessionRequestDTO } from './deleteSessionDTO';

import logger from '@/shared/utils/logger';

export class DeleteSessionController extends BaseController {
  private useCase: DeleteSession;

  constructor(useCase: DeleteSession) {
    super();
    this.useCase = useCase;
  }

  protected async executeImpl(req: Request | any, res: Response): Promise<any> {
    logger.debug('[UseCases/DeleteSession] Controller - req.params %s', req.params);
    logger.debug('[UseCases/DeleteSession] Controller - req.user %s', req.user);

    const dto: DeleteSessionRequestDTO = {
      session_id: req.params.session_id,
      user_id: req.user,
    };

    try {
      const result = await this.useCase.execute(dto);
      logger.debug('[UseCases/DeleteSession] Controller - result %s', result);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        logger.error(`[UseCases/DeleteSession] Controller Error ${JSON.stringify(errMessage)}`);
        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res);
        } else if (errMessage === 'You do not have permission to perform such action.') {
          return this.forbidden(res, errMessage);
        } else if (errMessage === 'Session not found.') {
          return this.notFound(res, errMessage);
        } else {
          return this.fail(res, errMessage);
        }
      } else {
        return this.ok<void>(res);
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
