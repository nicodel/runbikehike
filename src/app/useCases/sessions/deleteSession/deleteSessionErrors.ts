/* eslint-disable @typescript-eslint/no-namespace */
import { Result } from '@/shared/core/Result';
import { UseCaseError } from '@/shared/core/UseCaseError';

export namespace DeleteSessionErrors {
  export class SessionNotFoundError extends Result<UseCaseError> {
    constructor() {
      super(false, {
        message: 'Session not found.',
      } as UseCaseError);
    }
  }
}
