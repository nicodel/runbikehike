import { UseCase } from '@/shared/core/UseCase';
import { Either, Result, left, right } from '@/shared/core/Result';
import { AppError } from '@/shared/core/AppError';

import { ISessionsRepo } from '@/app/repositories/sessions/ISessionsRepo';
// import { IImagesRepo } from '@/app/repositories/images/IImagesRepo';
// import { Session } from '@/app/domains/sessions/session';

import { DeleteSessionRequestDTO } from './deleteSessionDTO';
import { DeleteSessionErrors } from './deleteSessionErrors';

import logger from '@/shared/utils/logger';
import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';

type Response = Either<
  DeleteSessionErrors.SessionNotFoundError | AppError.ForbiddenError | AppError.UnexpectedError,
  Result<void>
>;

export class DeleteSession implements UseCase<DeleteSessionRequestDTO, Promise<Response>> {
  private sessionRepo: ISessionsRepo;
  private usersRepo: IUsersRepo;

  constructor(sessionRepo: ISessionsRepo /* , imageRepo: IImagesRepo */, usersRepo: IUsersRepo) {
    this.sessionRepo = sessionRepo;
    this.usersRepo = usersRepo;
  }

  public async execute(req: DeleteSessionRequestDTO): Promise<Response> {
    const { user_id } = req;
    const { session_id } = req;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    try {
      const sessionOwner = await this.sessionRepo.getUserId(session_id);
      const isUserAdmin = await this.usersRepo.isAdmin(user_id);
      if (isUserAdmin === false && user_id !== sessionOwner) {
        return left(new AppError.ForbiddenError(`delete session ${session_id}`, user_id));
      }

      await this.sessionRepo.removeSessionById(session_id);

      return right(Result.ok<void>());
    } catch (err) {
      if (err === `Error: Session not found for id: ${session_id}`) {
        logger.error('[UseCase/DeleteSession] Session not found.: %s', session_id);
        return left(new DeleteSessionErrors.SessionNotFoundError());
      }
      logger.error('[UseCase/DeleteSession] Unexpected Error.: %s', err);
      return left(new AppError.UnexpectedError(err));
    }
  }
}
