import { sessionsRepo } from '@/app/repositories/sessions';

import { DeleteSession } from './deleteSession';
import { DeleteSessionController } from './deleteSessionController';
// import { imagesRepo } from '@/app/repositories/images';
import { usersRepo } from '@/app/repositories/auth';

const deleteSession = new DeleteSession(sessionsRepo, /*  imagesRepo,  */ usersRepo);

export const deleteSessionController = new DeleteSessionController(deleteSession);
