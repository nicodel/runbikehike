export interface DeleteSessionRequestDTO {
  session_id: string;
  user_id: string;
}
