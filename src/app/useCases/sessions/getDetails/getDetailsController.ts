/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { SessionMap } from '@/app/mappers/sessions/sessionMap';

import { GetDetails } from './getDetails';
import { GetDetailsRequestDTO, GetDetailsResponseDTO } from './getDetailsDTO';

import logger from '@/shared/utils/logger';

export class GetDetailsController extends BaseController {
  private useCase: GetDetails;

  constructor(useCase: GetDetails) {
    super();
    this.useCase = useCase;
  }
  protected async executeImpl(req: Request | any, res: Response): Promise<any> {
    logger.debug('[UseCases/getDetails] Controller - req.params %s', req.params);
    logger.debug('[UseCases/getDetails] Controller - req.user %s', req.user);

    const dto: GetDetailsRequestDTO = {
      session_id: req.params.session_id,
      user_id: req.user,
    };

    try {
      const result = await this.useCase.execute(dto);
      logger.debug('[UseCases/getDetails] Controller - result %s', result);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        logger.error(`[UseCases/getDetails] Controller Error ${JSON.stringify(errMessage)}`);
        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res);
        } else if (errMessage === 'You do not have permission to perform such action.') {
          return this.forbidden(res, errMessage);
        } else if (errMessage === 'Session not found.') {
          return this.notFound(res, errMessage);
        } else {
          return this.fail(res, errMessage);
        }
      } else {
        const session = result.value.getValue();
        logger.debug('[UseCases/getDetails] Controller - session %s', session);

        const sessionDTO = SessionMap.toChartsDTO(session);
        logger.debug('[UseCases/getDetails] Controller - sessionDTO %s', sessionDTO);

        return this.ok<{ session: GetDetailsResponseDTO }>(res, { session: sessionDTO });
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
