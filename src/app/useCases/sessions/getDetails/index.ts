import { sessionsRepo } from '@/app/repositories/sessions';
import { GetDetails } from './getDetails';
import { GetDetailsController } from './getDetailsController';
import { usersRepo } from '@/app/repositories/auth';

const getDetails = new GetDetails(sessionsRepo, usersRepo);

export const getDetailsController = new GetDetailsController(getDetails);
