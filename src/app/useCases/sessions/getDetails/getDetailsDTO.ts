import { SessionsDTO } from '@/app/dtos/sessionsDTO';

export interface GetDetailsRequestDTO {
  user_id: string;
  session_id: string;
}

export type GetDetailsResponseDTO = SessionsDTO;
