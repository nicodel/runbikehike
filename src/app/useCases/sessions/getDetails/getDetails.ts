import { UseCase } from '@/shared/core/UseCase';
import { Either, Result, left, right } from '@/shared/core/Result';
import { AppError } from '@/shared/core/AppError';

import { ISessionsRepo } from '@/app/repositories/sessions/ISessionsRepo';
import { Session } from '@/app/domains/sessions/session';

import { GetDetailsRequestDTO } from './getDetailsDTO';

import logger from '@/shared/utils/logger';
import { SessionErrors } from '../sessionErrors';
import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';

type Response = Either<
  AppError.ForbiddenError | AppError.UnexpectedError | SessionErrors.UnknownSession,
  Result<Session>
>;

export class GetDetails implements UseCase<GetDetailsRequestDTO, Promise<Response>> {
  private sessionRepo: ISessionsRepo;
  private usersRepo: IUsersRepo;

  constructor(sessionRepo: ISessionsRepo, usersRepo: IUsersRepo) {
    this.sessionRepo = sessionRepo;
    this.usersRepo = usersRepo;
  }

  public async execute(req: GetDetailsRequestDTO): Promise<Response> {
    const { user_id } = req;
    const { session_id } = req;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    try {
      const sessionDetails = await this.sessionRepo.getBySessionId(session_id);
      logger.debug('[UseCase/GetDetails] sessionDetails: %s', sessionDetails);

      logger.debug(
        '[UseCase/GetDetails] sessionDetails.user.id.toString(): %s',
        sessionDetails.user.id.toString()
      );
      logger.debug('[UseCase/GetDetails] user_id: %s', user_id);

      const isUserAdmin = await this.usersRepo.isAdmin(user_id);
      logger.debug('[UseCase/GetDetails] isUserAdmin: %s', isUserAdmin);
      if (isUserAdmin === false && user_id !== sessionDetails.user.id.toString()) {
        return left(new AppError.ForbiddenError(`get session details ${session_id}`, user_id));
      }

      return right(Result.ok<Session>(sessionDetails));
    } catch (err) {
      if (err === `Error: Session not found for id: ${session_id}`) {
        return left(new SessionErrors.UnknownSession(session_id));
      }
      return left(new AppError.UnexpectedError(err));
    }
  }
}
