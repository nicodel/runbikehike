import { UseCase } from '@/shared/core/UseCase';
import { AppError } from '@/shared/core/AppError';
import { Either, Result, left, right } from '@/shared/core/Result';

import { Track } from '@/app/domains/sessions/track';

import { ISessionsRepo } from '@/app/repositories/sessions/ISessionsRepo';
import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';

import { SessionErrors } from '../sessionErrors';
import { GetSessionChartsDataRequestDTO } from './getSessionChartsDataDTO';

import logger from '@/shared/utils/logger';

type Response = Either<
  | AppError.UnexpectedError
  | AppError.ForbiddenError
  | SessionErrors.UnknownSession
  | SessionErrors.UnknownUser
  | SessionErrors.WrongValue
  | SessionErrors.MissingMandatoryValue
  | SessionErrors.UnknownComment,
  Result<Track>
>;

export class GetSessionChartsData
  implements UseCase<GetSessionChartsDataRequestDTO, Promise<Response>>
{
  private sessionRepo: ISessionsRepo;
  private usersRepo: IUsersRepo;

  constructor(sessionRepo: ISessionsRepo, usersRepo: IUsersRepo) {
    this.sessionRepo = sessionRepo;
    this.usersRepo = usersRepo;
  }

  public async execute(req: GetSessionChartsDataRequestDTO): Promise<Response> {
    const { user_id } = req;
    const { session_id } = req;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    try {
      const sessionDetails = await this.sessionRepo.getBySessionId(session_id);
      logger.debug('[UseCase/GetSessionChartsData] sessionDetails: %s', sessionDetails);

      logger.debug(
        '[UseCase/GetSessionChartsData] sessionDetails.user.id.toString(): %s',
        sessionDetails.user.id.toString()
      );
      logger.debug('[UseCase/GetSessionChartsData] user_id: %s', user_id);

      const isUserAdmin = await this.usersRepo.isAdmin(user_id);
      if (isUserAdmin === false && user_id !== sessionDetails.user.id.toString()) {
        return left(new AppError.ForbiddenError(`get session charts data ${session_id}`, user_id));
      }

      return right(Result.ok<Track>(sessionDetails.gps_track));
    } catch (err) {
      if (err === `Error: Session not found for id: ${session_id}`) {
        return left(new SessionErrors.UnknownSession(session_id));
      }
      return left(new AppError.UnexpectedError(err));
    }
  }
}
