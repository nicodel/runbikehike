import { sessionsRepo } from '@/app/repositories/sessions';
import { usersRepo } from '@/app/repositories/auth';

import { GetSessionChartsData } from './getSessionChartsData';
import { GetSessionChartsDataController } from './getSessionChartsDataController';

const getSessionChartsData = new GetSessionChartsData(sessionsRepo, usersRepo);

export const getSessionChartsDataController = new GetSessionChartsDataController(
  getSessionChartsData
);
