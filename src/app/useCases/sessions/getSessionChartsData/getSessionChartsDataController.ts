/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { TrackMap } from '@/app/mappers/sessions/trackMap';

import { GetSessionChartsData } from './getSessionChartsData';
import {
  GetSessionChartsDataRequestDTO,
  GetSessionChartsDataResponseDTO,
} from './getSessionChartsDataDTO';

import logger from '@/shared/utils/logger';

export class GetSessionChartsDataController extends BaseController {
  private useCase: GetSessionChartsData;

  constructor(useCase: GetSessionChartsData) {
    super();
    this.useCase = useCase;
  }

  protected async executeImpl(req: Request | any, res: Response): Promise<any> {
    logger.debug('[UseCases/GetSessionChartsData] Controller - req.params %s', req.params);
    logger.debug('[UseCases/GetSessionChartsData] Controller - req.user %s', req.user);

    const dto: GetSessionChartsDataRequestDTO = {
      session_id: req.params.session_id,
      user_id: req.user,
    };

    try {
      const result = await this.useCase.execute(dto);
      logger.debug('[UseCases/GetSessionChartsData] Controller - result %s', result);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        logger.error(
          `[UseCases/GetSessionChartsData] Controller Error ${JSON.stringify(
            error.errorValue().message
          )}`
        );
        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res);
        } else if (errMessage === 'You do not have permission to perform such action.') {
          return this.forbidden(res, errMessage);
        } else if (errMessage === 'Session not found.') {
          return this.notFound(res, errMessage);
        } else {
          return this.fail(res, errMessage);
        }
      } else {
        const track = result.value.getValue();
        logger.debug('[UseCases/GetSessionChartsData] Controller - track %s', track);

        const chartsDTO = track ? TrackMap.toCharts(track) : null;
        logger.debug('[UseCases/GetSessionChartsData] Controller - chartsDTO %s', chartsDTO);

        return this.ok<GetSessionChartsDataResponseDTO>(res, { charts_data: chartsDTO });
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
