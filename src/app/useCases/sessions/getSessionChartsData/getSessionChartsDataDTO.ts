import { SessionChartsDataDTO } from '@/app/dtos/sessionsDTO';

export interface GetSessionChartsDataRequestDTO {
  user_id: string;
  session_id: string;
}

export interface GetSessionChartsDataResponseDTO {
  charts_data: SessionChartsDataDTO;
}
