import { AppError } from '@/shared/core/AppError';
import { Either, Result, left, right } from '@/shared/core/Result';
import { UseCase } from '@/shared/core/UseCase';
import { Guard } from '@/shared/core/Guard';

import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';
import { ICommentsRepo } from '@/app/repositories/sessions/ICommentsRepo';

import { DeleteACommentResquestDTO } from './deleteACommentDTO';
import { SessionErrors } from '../sessionErrors';

import logger from '@/shared/utils/logger';

type Response = Either<
  | AppError.UnexpectedError
  | AppError.ForbiddenError
  | SessionErrors.UnknownSession
  | SessionErrors.UnknownUser
  | SessionErrors.WrongValue
  | SessionErrors.MissingMandatoryValue
  | SessionErrors.UnknownComment,
  Result<void>
>;

export class DeleteAComment implements UseCase<DeleteACommentResquestDTO, Promise<Response>> {
  private usersRepo: IUsersRepo;
  private commentsRepo: ICommentsRepo;

  constructor(commentsRepo: ICommentsRepo, usersRepo: IUsersRepo) {
    this.usersRepo = usersRepo;
    this.commentsRepo = commentsRepo;
  }

  public async execute(dto: DeleteACommentResquestDTO): Promise<Response> {
    logger.debug(`[UseCase/DeleteAComment] UseCase - dto ${JSON.stringify(dto)}`);

    // check if dto is present
    const dtoResult = Guard.againstNullOrUndefined(dto, 'dto');
    if (!dtoResult.succeeded) {
      logger.error(
        '[UseCase/DeleteAComment] UseCase - Error againstNullOrUndefined(dto) error: %s',
        dtoResult.message
      );
      return left(new AppError.UnexpectedError(dtoResult.message));
    }

    // check if user_id is present
    if (!dto.user_id) {
      logger.error('[UseCase/DeleteAComment] UseCase - Error missing dto.user_id: %s', dto);
      return left(new AppError.UnauthorizedError(dto.user_id));
    }

    // check if comment_id is present
    if (!dto.comment_id) {
      logger.error('[UseCase/DeleteAComment] UseCase - Error missing dto.comment_id: %s', dto);
      return left(new SessionErrors.MissingMandatoryValue('comment_id'));
    }

    try {
      // check if user exists
      const userDetails = await this.usersRepo.getUserById(dto.user_id);
      if (!userDetails) {
        logger.error(`[UseCase/DeleteAComment] UseCase - Unknown user_id ${dto.user_id}`);
        return left(new SessionErrors.UnknownUser(dto.user_id));
      }

      // check if comment exists
      const commentDetails = await this.commentsRepo.getCommentById(dto.comment_id);
      if (!commentDetails) {
        logger.error(`[UseCase/DeleteAComment] UseCase - Unknown comment ${dto.comment_id}`);
        return left(new SessionErrors.UnknownComment(dto.comment_id));
      }

      // check is user wrote the comment
      if (commentDetails.user_id !== userDetails.id.toString()) {
        logger.error(
          `[UseCase/DeleteAComment] UseCase - User ${dto.user_id} tried to perform Delete a comment ${dto.comment_id} without permission.`
        );
        return left(new AppError.ForbiddenError(`Delete a comment ${dto.comment_id}`, dto.user_id));
      }

      // delete the comment
      await this.commentsRepo.deleteCommentById(commentDetails.id.id.toString());

      return right(Result.ok<void>());
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
