/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { DeleteAComment } from './deleteAComment';
import { DeleteACommentResquestDTO } from './deleteACommentDTO';

import logger from '@/shared/utils/logger';

export class DeleteACommentController extends BaseController {
  private useCase: DeleteAComment;

  constructor(useCase: DeleteAComment) {
    super();
    this.useCase = useCase;
  }

  protected async executeImpl(req: Request | any, res: Response): Promise<any> {
    const dto: DeleteACommentResquestDTO = {
      comment_id: req.params.comment_id,
      user_id: req.user,
    };

    logger.debug(`[UseCase/DeleteAComment] Controller - dto ${JSON.stringify(dto)}`);

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;

        if (error.errorValue().message === `Unknown user "${req.user}".`) {
          return this.unauthorized(res, error.errorValue().message);
        } else {
          const message = error.errorValue().message
            ? error.errorValue().message
            : error.errorValue();
          return this.badRequest(res, message.toString());
        }
      } else {
        return this.ok(res);
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
