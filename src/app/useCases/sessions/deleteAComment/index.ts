import { commentsRepo } from '@/app/repositories/sessions';
import { usersRepo } from '@/app/repositories/auth';

import { DeleteAComment } from './deleteAComment';
import { DeleteACommentController } from './deleteACommentController';

const deleteAComment = new DeleteAComment(commentsRepo, usersRepo);

export const deleteACommentController = new DeleteACommentController(deleteAComment);
