import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { fakeDestroy, fakeInjectData } from '@/fake_data';
import { fakeComments } from '@/fake_data/comments';

import { commentsRepo } from '@/app/repositories/sessions';
import { usersRepo } from '@/app/repositories/auth';

import { DeleteAComment } from './deleteAComment';
import { DeleteACommentResquestDTO } from './deleteACommentDTO';

const deleteAComment = new DeleteAComment(commentsRepo, usersRepo);

describe('App / UseCases / Sessions / DeleteAComment', () => {
  before(async () => {
    await fakeInjectData('light');
  });

  after(async () => {
    await fakeDestroy();
  });

  it('should return Right result as a success', async () => {
    const result = await deleteAComment.execute({
      comment_id: fakeComments[1].id,
      user_id: fakeComments[1].user.id,
    } as DeleteACommentResquestDTO);
    result.isRight().should.be.true;
  });
});
