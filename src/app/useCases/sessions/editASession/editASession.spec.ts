import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { UseCaseError } from '@/shared/core/UseCaseError';

import { fakeDestroy, fakeInjectData } from '@/fake_data';
import { fakeSessions } from '@/fake_data/sessions';

import { activitiesRepo, commentsRepo, sessionsRepo } from '@/app/repositories/sessions';
import { usersRepo } from '@/app/repositories/auth';
import { imagesRepo } from '@/app/repositories/images';
import { profilesRepo } from '@/app/repositories/users';

import { EditASession } from './editASession';

const editASession = new EditASession(
  sessionsRepo,
  usersRepo,
  activitiesRepo,
  imagesRepo,
  profilesRepo,
  commentsRepo
);

const dtoSession = {
  activity_name: fakeSessions[0].activity.name,
  average_heart_rate: fakeSessions[0].average_heart_rate,
  average_speed: fakeSessions[0].average_speed,
  burned_calories: fakeSessions[0].burned_calories,
  description: fakeSessions[0].description,
  distance: fakeSessions[0].distance,
  duration: fakeSessions[0].duration,
  elevation_gain: fakeSessions[0].elevation_gain,
  id: fakeSessions[0].id,
  images_ids: null,
  is_favorite: fakeSessions[0].is_favorite,
  gps_charts: null,
  gps_track: null,
  location: fakeSessions[0].location,
  name: fakeSessions[0].name,
  nb_comments: fakeSessions[0].nb_comments,
  nb_likes: fakeSessions[0].nb_likes,
  start_date_time: fakeSessions[0].start_date_time,
  user_id: fakeSessions[0].user.id,
};

describe('App / UseCases / Sessions / EditASession', () => {
  describe('execute()', () => {
    before(async () => {
      await fakeInjectData('light');
    });

    after(async () => {
      await fakeDestroy();
    });

    it('should return Right result as a success', async () => {
      const result = await editASession.execute(dtoSession);
      result.isRight().should.be.true;
    });

    it('should return Left result as a failure, if "dto" is null or undefined', async () => {
      const dto = null;
      const result = await editASession.execute(dto);
      result.isLeft().should.be.true;
      const error = result.value.error as UseCaseError;
      error.message.should.be.equal('An unexpected error occurred.');
    });

    it('should return Left result as a failure, if "user_id" is missing', async () => {
      const dto = { ...dtoSession, user_id: null };
      const result = await editASession.execute(dto);
      result.isLeft().should.be.true;
      const error = result.value.error as UseCaseError;
      error.message.should.be.equal('Unauthorized as unauthenticated');
    });

    it('should return Left result as a failure, if "user_id" is unknown', async () => {
      const dto = { ...dtoSession, user_id: 'wrong id' };
      const result = await editASession.execute(dto);
      result.isLeft().should.be.true;
      const error = result.value.error as UseCaseError;
      error.message.should.be.equal('You do not have permission to perform such action.');
    });

    it('should return Left result as a failure, if "user_id" is different from session.user.id', async () => {
      const dto = { ...dtoSession, user_id: fakeSessions[2].user.id };
      const result = await editASession.execute(dto);
      result.isLeft().should.be.true;
      const error = result.value.error as UseCaseError;
      error.message.should.be.equal('You do not have permission to perform such action.');
    });

    it('should return Right result as a success, when "activity_name" is changed', async () => {
      const updatedValue = 'swimming';
      const dto = { ...dtoSession, activity_name: updatedValue };
      const result = await editASession.execute(dto);
      result.isRight().should.be.true;

      const updatedSession = await sessionsRepo.getBySessionId(dto.id);
      updatedSession.activity.name.should.be.equal(updatedValue);
    });

    it('should return Right result as a success, when "average_heart_rate" is changed to a new value', async () => {
      const updatedValue = 149;
      const dto = { ...dtoSession, average_heart_rate: updatedValue };
      const result = await editASession.execute(dto);
      result.isRight().should.be.true;

      const updatedSession = await sessionsRepo.getBySessionId(dto.id);
      updatedSession.average_heart_rate.value.should.be.equal(updatedValue);
    });

    it('should return Right result as a success, when "average_heart_rate" is changed to a null', async () => {
      const updatedValue = null;
      const dto = { ...dtoSession, average_heart_rate: updatedValue };
      const result = await editASession.execute(dto);
      result.isRight().should.be.true;

      const updatedSession = await sessionsRepo.getBySessionId(dto.id);
      chai.expect(updatedSession.average_heart_rate).to.be.null;
    });

    it('should return Left result as a failure, when "average_heart_rate" is changed an invalid value', async () => {
      const dto = { ...dtoSession, average_heart_rate: 'wrong' as unknown as number };
      const result = await editASession.execute(dto);
      result.isLeft().should.be.true;
      const error = result.value.errorValue() as UseCaseError;
      // eslint-disable-next-line quotes
      error.message.should.equal('Wrong value average_heart_rate: wrong');

      // const updatedSession = await sessionsRepo.getBySessionId(dto.id);
      // chai.expect(updatedSession.average_heart_rate).to.be.null;
    });
  });
});
