/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { EditASessionRequestDTO } from './editASessionDTO';
import { EditASession } from './editASession';
import logger from '@/shared/utils/logger';

export class EditASessionController extends BaseController {
  private useCase: EditASession;

  constructor(useCase: EditASession) {
    super();
    this.useCase = useCase;
  }

  protected async executeImpl(req: Request | any, res: Response): Promise<any> {
    const dto: EditASessionRequestDTO = {
      ...req.body,
      user_id: req.user as Request,
    };

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res, errMessage);
        } else {
          const message = errMessage ? errMessage : error.errorValue();
          return this.badRequest(res, message.toString());
        }
      } else {
        const sessionId = result.value.getValue().id.toString();

        logger.debug(`[UseCase/EditASession] Controller - sessionId: ${sessionId}`);
        return this.ok(res, { id: sessionId });
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
