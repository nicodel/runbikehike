import { UseCase } from '@/shared/core/UseCase';
import { AppError } from '@/shared/core/AppError';
import { Either, Result, left, right } from '@/shared/core/Result';
import { Guard } from '@/shared/core/Guard';

import { ISessionsRepo } from '@/app/repositories/sessions/ISessionsRepo';
import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';
import { IActivitiesRepo } from '@/app/repositories/sessions/IActivitiesRepo';
import { IImagesRepo } from '@/app/repositories/images/IImagesRepo';
import { ICommentsRepo } from '@/app/repositories/sessions/ICommentsRepo';
import { IProfilesRepo } from '@/app/repositories/users/IProfilesRepo';

import { UniqueId } from '@/app/domains/uniqueId';
import { SessionMap } from '@/app/mappers/sessions/sessionMap';

import { EditASessionRequestDTO } from './editASessionDTO';
import { SessionErrors } from './../sessionErrors';

import logger from '@/shared/utils/logger';

type Response = Either<
  | AppError.UnexpectedError
  | AppError.ForbiddenError
  | SessionErrors.UnknownUser
  | SessionErrors.UnknownSession
  | SessionErrors.UnknownActivity
  | SessionErrors.WrongValue
  | SessionErrors.MissingMandatoryValue,
  Result<UniqueId>
>;

export class EditASession implements UseCase<EditASessionRequestDTO, Promise<Response>> {
  private sessionRepo: ISessionsRepo;
  private usersRepo: IUsersRepo;
  private activitiesRepo: IActivitiesRepo;
  private imagesRepo: IImagesRepo;
  private profileRepo: IProfilesRepo;
  private commentsRepo: ICommentsRepo;

  constructor(
    sessionRepo: ISessionsRepo,
    usersRepo: IUsersRepo,
    activitiesRepo: IActivitiesRepo,
    imagesRepo: IImagesRepo,
    profileRepo: IProfilesRepo,
    commentsRepo: ICommentsRepo
  ) {
    this.sessionRepo = sessionRepo;
    this.usersRepo = usersRepo;
    this.activitiesRepo = activitiesRepo;
    this.imagesRepo = imagesRepo;
    this.profileRepo = profileRepo;
    this.commentsRepo = commentsRepo;
  }

  public async execute(dto: EditASessionRequestDTO): Promise<Response> {
    logger.debug('[UseCase/EditASession] UseCase - dto: %s', dto);

    // check if dto exists
    const dtoResult = Guard.againstNullOrUndefined(dto, 'dto');
    if (!dtoResult.succeeded) {
      logger.error(
        '[UseCase/EditASession] UseCase - Error againstNullOrUndefined(dto) error: %s',
        dtoResult.message
      );
      return left(new AppError.UnexpectedError(dtoResult.message));
    }

    // check if user exists
    if (!dto.user_id) {
      logger.error('[UseCase/EditASession] UseCase - Error missing dto.user_id: %s', dto);
      return left(new AppError.UnauthorizedError(dto.user_id));
    }

    try {
      const session = await this.sessionRepo.getBySessionId(dto.id);
      // check if session exists
      if (!session) {
        logger.error(`[UseCase/EditASession] UseCase - Unknown session_id ${dto.id}`);
        return left(new SessionErrors.UnknownSession(dto.id));
      }

      // check if user can edit this session (dto.user_id === sessionExists.user.id)
      const sessionUserId = session.user.id.toString();
      if (dto.user_id !== sessionUserId) {
        logger.error(`[UseCase/EditASession] UseCase - Unknown session_id ${dto.id}`);
        return left(new AppError.ForbiddenError(`edit session ${dto.id}`, dto.user_id));
      }

      // build activityDomain
      const activityDetails = await this.activitiesRepo.getActivityByName(dto.activity_name);
      if (!activityDetails) {
        logger.error(`[UseCase/EditASession] UseCase - Unknown activity_name ${dto.activity_name}`);
        return left(new SessionErrors.UnknownActivity(dto.activity_name));
      }

      // build userDomain
      const userDetails = await this.usersRepo.getUserById(dto.user_id);
      if (!userDetails) {
        logger.error(`[UseCase/EditASession] UseCase - Unknown user_id ${dto.user_id}`);
        return left(new SessionErrors.UnknownUser(dto.user_id));
      }

      // build imagesDomain
      let imagesDetails = null;
      if (dto.images_ids) {
        imagesDetails = [];
        for (let index = 0; index < dto.images_ids.length; index++) {
          const image_id = dto.images_ids[index];
          const imageDetails = await this.imagesRepo.getByImageId(image_id);
          imagesDetails.push(imageDetails);
        }
      }

      // logger.debug(
      //   `[UseCase/EditASession] UseCase - dto.nb_comments ${JSON.stringify(dto.comments_ids)}`
      // );
      // //build commentsDomain
      // let commentsDetails = null;
      // if (dto.comments_ids) {
      //   commentsDetails = dto.comments_ids.map(
      //     async (id) => await this.commentsRepo.getCommentById(id)
      //   );
      // }

      const gpsTrackDomain = session.gps_track;

      const profileDetails = await this.profileRepo.getProfile(dto.user_id);

      // build sessionDomain
      const sessionResult = SessionMap.toDomainFromDTO(
        dto,
        activityDetails,
        userDetails,
        imagesDetails,
        profileDetails,
        gpsTrackDomain
        // commentsDetails
      );

      if (sessionResult.isFailure) {
        if (sessionResult.getError.cause === 'Missing value') {
          logger.error(
            `[UseCase/EditASession] UseCase - Missing value ${sessionResult.getError.value}`
          );
          return left(new SessionErrors.MissingMandatoryValue(sessionResult.getError.value));
        } else if (sessionResult.getError.cause === 'Wrong value') {
          logger.error(
            `[UseCase/EditASession] UseCase - Wrong value ${sessionResult.getError.value}`
          );
          return left(new SessionErrors.WrongValue(sessionResult.getError.value));
        }
      }
      const sessionDetails = sessionResult.getValue;
      // save session
      await this.sessionRepo.save(sessionDetails);

      logger.debug(
        `[UseCase/EditASession] UseCase - sessionDetails.id: ${JSON.stringify(sessionDetails.id)}`
      );
      return right(Result.ok<UniqueId>(sessionDetails.id));
    } catch (err) {
      if (err === `Error: Session not found for id: ${dto.id}`) {
        return left(new SessionErrors.UnknownSession(dto.id));
      } else if (err === `Error: Activity not found for name: ${dto.activity_name}`) {
        return left(new SessionErrors.UnknownActivity(dto.activity_name));
      } else if (err === 'Missing value') {
        return left(new SessionErrors.MissingMandatoryValue(err.value));
      } else if (err === 'Wrong value') {
        return left(new SessionErrors.WrongValue(`${err.value} ${err.label}`));
      }
      return left(new AppError.UnexpectedError(err));
    }
  }
}
