import { activitiesRepo, sessionsRepo } from '@/app/repositories/sessions';
import { usersRepo } from '@/app/repositories/auth';
import { imagesRepo } from '@/app/repositories/images';
import { profilesRepo } from '@/app/repositories/users';
import { commentsRepo } from '@/app/repositories/sessions';

import { EditASession } from './editASession';
import { EditASessionController } from './editASessionController';

const editASession = new EditASession(
  sessionsRepo,
  usersRepo,
  activitiesRepo,
  imagesRepo,
  profilesRepo,
  commentsRepo
);

export const editASessionController = new EditASessionController(editASession);
