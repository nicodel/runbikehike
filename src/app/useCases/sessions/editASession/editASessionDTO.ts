import { SessionsDTO } from '@/app/dtos/sessionsDTO';

export type EditASessionRequestDTO = SessionsDTO;
