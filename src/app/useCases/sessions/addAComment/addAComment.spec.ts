import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { fakeDestroy, fakeInjectData } from '@/fake_data';
import { fakeComments } from '@/fake_data/comments';

import { commentsRepo, sessionsRepo } from '@/app/repositories/sessions';
import { usersRepo } from '@/app/repositories/auth';

import { AddAComment } from './addAComment';
import { AddACommentRequestDTO } from './addACommentDTO';

const addAComment = new AddAComment(sessionsRepo, usersRepo, commentsRepo);

const dto: AddACommentRequestDTO = {
  text: fakeComments[3].text,
  date_time: fakeComments[3].date_time,
  user_id: fakeComments[3].user.id,
  session_id: fakeComments[3].session.id,
};

describe('App / UseCases / Sessions / AddAComment', () => {
  before(async () => {
    await fakeInjectData('light');
  });

  after(async () => {
    await fakeDestroy();
  });

  it('should return Right result as a success', async () => {
    const result = await addAComment.execute(dto);
    result.isRight().should.be.true;
  });
});
