import { usersRepo } from '@/app/repositories/auth';
import { commentsRepo, sessionsRepo } from '@/app/repositories/sessions';

import { AddAComment } from './addAComment';
import { AddACommentController } from './addACommentController';

const addAComment = new AddAComment(sessionsRepo, usersRepo, commentsRepo);

export const addACommentControler = new AddACommentController(addAComment);
