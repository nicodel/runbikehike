export interface AddACommentRequestDTO {
  text: string;
  date_time: string;
  session_id: string;
  user_id: string;
}
