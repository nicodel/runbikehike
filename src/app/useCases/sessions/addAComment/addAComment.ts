import { AppError } from '@/shared/core/AppError';
import { Guard } from '@/shared/core/Guard';
import { Either, Result, left, right } from '@/shared/core/Result';
import { UseCase } from '@/shared/core/UseCase';

import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';
import { ISessionsRepo } from '@/app/repositories/sessions/ISessionsRepo';
import { ICommentsRepo } from '@/app/repositories/sessions/ICommentsRepo';

import { UniqueId } from '@/app/domains/uniqueId';
import { Comment } from '@/app/domains/comment';

import { SessionErrors } from '../sessionErrors';
import { AddACommentRequestDTO } from './addACommentDTO';

import logger from '@/shared/utils/logger';

type Response = Either<
  | AppError.UnexpectedError
  | AppError.ForbiddenError
  | SessionErrors.UnknownSession
  | SessionErrors.UnknownUser
  | SessionErrors.WrongValue
  | SessionErrors.MissingMandatoryValue
  | SessionErrors.UnknownComment,
  Result<UniqueId>
>;

export class AddAComment implements UseCase<AddACommentRequestDTO, Promise<Response>> {
  private sessionRepo: ISessionsRepo;
  private usersRepo: IUsersRepo;
  private commentsRepo: ICommentsRepo;

  constructor(sessionRepo: ISessionsRepo, usersRepo: IUsersRepo, commentsRepo: ICommentsRepo) {
    this.sessionRepo = sessionRepo;
    this.usersRepo = usersRepo;
    this.commentsRepo = commentsRepo;
  }

  public async execute(dto: AddACommentRequestDTO): Promise<Response> {
    logger.debug(`[UseCase/AddAComment] UseCase - dto ${JSON.stringify(dto)}`);

    // check if dto is present
    const dtoResult = Guard.againstNullOrUndefined(dto, 'dto');
    if (!dtoResult.succeeded) {
      logger.error(
        '[UseCase/AddAComment] UseCase - Error againstNullOrUndefined(dto) error: %s',
        dtoResult.message
      );
      return left(new AppError.UnexpectedError(dtoResult.message));
    }

    // check if user_id is present
    if (!dto.user_id) {
      logger.error('[UseCase/AddAComment] UseCase - Error missing dto.user_id: %s', dto);
      return left(new AppError.UnauthorizedError(dto.user_id));
    }

    try {
      // check if session exists
      const sessionDetails = await this.sessionRepo.getBySessionId(dto.session_id);
      if (!sessionDetails) {
        logger.error(`[UseCase/AddAComment] UseCase - Unknown session_id ${dto.session_id}`);
        return left(new SessionErrors.UnknownSession(dto.session_id));
      }

      // build userDomain
      const userDetails = await this.usersRepo.getUserById(dto.user_id);
      if (!userDetails) {
        logger.error(`[UseCase/AddAComment] UseCase - Unknown user_id ${dto.user_id}`);
        return left(new SessionErrors.UnknownUser(dto.user_id));
      }

      // build commentDomain
      const commentResult = Comment.create({
        date_time: dto.date_time,
        value: dto.text,
        user_id: dto.user_id,
        session_id: dto.session_id,
      });
      if (commentResult.isFailure) {
        const err = commentResult.errorValue();
        logger.error(`[UseCase/AddAComment] UseCase - Error: ${err}`);
        return left(new AppError.UnexpectedError(err));
      }

      logger.debug(
        `[UseCase/AddAComment] UseCase - commentResult.getValue(): ${JSON.stringify(
          commentResult.getValue()
        )}`
      );

      // save new Comment
      await this.commentsRepo.add(commentResult.getValue(), userDetails, sessionDetails);

      // update session/nb_comments
      const nb_comments = await this.commentsRepo.countCommentsBySessionId(dto.session_id);
      sessionDetails.nb_comments = nb_comments;
      await this.sessionRepo.save(sessionDetails);

      return right(Result.ok<UniqueId>(commentResult.getValue().id));
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
