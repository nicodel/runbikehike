/* eslint-disable @typescript-eslint/no-explicit-any */
import { Guard } from '@/shared/core/Guard';
import { Either, Result, left, right } from '@/shared/core/Result';
import { UseCase } from '@/shared/core/UseCase';
import { AppError } from '@/shared/core/AppError';

import { LongText } from '@/app/domains/longText';
import { ShortText } from '@/app/domains/shortText';
import { DateTime } from '@/app/domains/dateTime';
import { Duration } from '@/app/domains/sessions/duration';
import { Distance } from '@/app/domains/sessions/distance';
import { Speed } from '@/app/domains/sessions/speed';
import { BurnedCalories } from '@/app/domains/sessions/burnedCalories';
import { ISessionsRepo } from '@/app/repositories/sessions/ISessionsRepo';
import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';

import { AddASessionErrors } from './addASessionErrors';
import { AddASessionDTO } from './addASessionDTO';
import { Session } from '@/app/domains/sessions/session';
import logger from '@/shared/utils/logger';
import { IActivitiesRepo } from '@/app/repositories/sessions/IActivitiesRepo';
import { IProfilesRepo } from '@/app/repositories/users/IProfilesRepo';
import { TrackMap } from '@/app/mappers/sessions/trackMap';
import { UniqueId } from '@/app/domains/uniqueId';

type Response = Either<
  | AppError.UnexpectedError
  | AddASessionErrors.UnknownUser
  | AddASessionErrors.MissingMandatoryValue
  | AddASessionErrors.WrongValue
  | AddASessionErrors.UnknownActivity,
  Result<UniqueId>
>;

export class AddASession implements UseCase<AddASessionDTO, Promise<Response>> {
  private sessionRepo: ISessionsRepo;
  private usersRepo: IUsersRepo;
  private activitiesRepo: IActivitiesRepo;
  private profilesRepo: IProfilesRepo;

  constructor(
    sessionRepo: ISessionsRepo,
    usersRepo: IUsersRepo,
    activitiesRepo: IActivitiesRepo,
    profilesRepo: IProfilesRepo
  ) {
    this.sessionRepo = sessionRepo;
    this.usersRepo = usersRepo;
    this.activitiesRepo = activitiesRepo;
    this.profilesRepo = profilesRepo;
  }

  public async execute(dto: AddASessionDTO): Promise<Response> {
    logger.debug('[UseCase/AddASession] UseCase - dto: %s', dto);

    const dtoResult = Guard.againstNullOrUndefined(dto, 'dto');
    if (!dtoResult.succeeded) {
      logger.error(
        '[UseCase/AddASession] UseCase - Error againstNullOrUndefined(dto) error: %s',
        dtoResult.message
      );
      return left(new AppError.UnexpectedError(dtoResult.message));
    }

    if (!dto.user_id) {
      logger.error('[UseCase/AddASession] UseCase - Error missing dto.user_id: %s', dto);
      return left(new AppError.UnauthorizedError(dto.user_id));
    }

    try {
      const userDetails = await this.usersRepo.getUserById(dto.user_id);

      const userFound = !!userDetails;
      if (!userFound) {
        logger.error('[UseCase/AddASession] UseCase - Error user not found: %s', dto.user_id);
        return left(new AddASessionErrors.UnknownUser(dto.user_id));
      }
      logger.debug(`[UseCase/AddASession] UseCase - User: ${JSON.stringify(userFound)}`);

      const nameResult = ShortText.create({ value: dto.name, name: 'session.name' });
      if (nameResult.isFailure) {
        logger.error('[UseCase/AddASession] UseCase - Error missing session name');
        return left(new AddASessionErrors.MissingMandatoryValue('name'));
      }
      logger.debug(
        `[UseCase/AddASession] UseCase - Name: ${JSON.stringify(nameResult.getValue())}`
      );

      const startDateTimeResult = DateTime.create({ value: dto.start_date_time });
      if (startDateTimeResult.isFailure) {
        logger.error('[UseCase/AddASession] UseCase - Error missing session start_date_time');
        return left(new AddASessionErrors.MissingMandatoryValue('start_date_time'));
      }
      logger.debug(
        `[UseCase/AddASession] UseCase - Start Date Time: ${JSON.stringify(
          startDateTimeResult.getValue()
        )}`
      );

      const durationResult = Duration.create({ value: parseInt(dto.duration, 10), unit: 's' });
      if (durationResult.isFailure) {
        return left(new AddASessionErrors.MissingMandatoryValue('duration'));
      }
      logger.debug(
        `[UseCase/AddASession] UseCase - Duration: ${JSON.stringify(durationResult.getValue())}`
      );

      let distanceResult = null;
      if (dto.distance) {
        distanceResult = Distance.create({ value: parseInt(dto.distance, 10), unit: 'm' });
        if (distanceResult.isFailure) {
          logger.error(
            '[UseCase/AddASession] UseCase - Error wrong value for session distance: %s',
            dto.distance
          );
          return left(new AddASessionErrors.WrongValue(dto.distance, 'distance'));
        }
        logger.debug(
          `[UseCase/AddASession] UseCase - Distance: ${JSON.stringify(distanceResult.getValue())}`
        );
      }

      let averageSpeedResult = null;
      if (dto.average_speed) {
        averageSpeedResult = Speed.create({ value: parseFloat(dto.average_speed) });
        if (averageSpeedResult.isFailure) {
          logger.error(
            '[UseCase/AddASession] UseCase - Error wrong value for session average_speed: %s',
            dto.average_speed
          );
          return left(new AddASessionErrors.WrongValue(dto.average_speed, 'average_speed'));
        }
        logger.debug(
          `[UseCase/AddASession] UseCase - Average Speed: ${JSON.stringify(
            averageSpeedResult.getValue()
          )}`
        );
      }

      let descriptionResult = null;
      if (dto.description) {
        descriptionResult = LongText.create({
          value: dto.description,
          name: 'session.description',
        });
        if (descriptionResult.isFailure) {
          logger.error(
            '[UseCase/AddASession] UseCase - Error wrong value for session description: %s',
            dto.description
          );
          return left(descriptionResult);
        }
        logger.debug(
          `[UseCase/AddASession] UseCase - Description: ${JSON.stringify(
            descriptionResult.getValue()
          )}`
        );
      }

      const activityDetails = await this.activitiesRepo.getActivityByName(
        String(dto.activity_name)
      );

      const activityFound = !!activityDetails;
      if (!activityFound) {
        logger.error(
          '[UseCase/AddASession] UseCase - Error activity not found: %s',
          dto.activity_name
        );
        return left(new AddASessionErrors.UnknownActivity(dto.activity_name));
      }
      logger.debug(`[UseCase/AddASession] UseCase - Activity: ${activityFound}`);

      let burnedCalories = 0;
      if (dto.burned_calories) {
        burnedCalories = parseInt(dto.burned_calories, 10);
        logger.debug(`[UseCase/AddASession] UseCase - DTO burnedCalories: ${burnedCalories}`);
      } else {
        const userProfileDetails = await this.profilesRepo.getProfile(dto.user_id);

        burnedCalories = BurnedCalories.calculate({
          weight: userProfileDetails.weight.value,
          duration: durationResult.getValue().value,
          distance: distanceResult ? distanceResult.getValue().value : null,
          activity_name: activityDetails.name,
        });
        logger.debug(
          `[UseCase/AddASession] UseCase - Calculated burnedCalories: ${burnedCalories}`
        );
      }
      const burnedCaloriesResult = BurnedCalories.create({
        value: burnedCalories,
      });
      if (burnedCaloriesResult.isFailure) {
        logger.error('[UseCase/AddASession] UseCase - Error missing session burned_calories');
        return left(new AddASessionErrors.MissingMandatoryValue('burned_calories'));
      }

      let gpsTrack = null;
      if (dto.gps_track) {
        gpsTrack = TrackMap.toDomain(dto.gps_track);
        logger.debug(`[UseCase/AddASession] UseCase - gpsTrack: ${gpsTrack}`);
      }

      const sessionProps = {
        name: nameResult.getValue(),
        start_date_time: startDateTimeResult.getValue(),
        duration: durationResult.getValue(),
        distance: distanceResult ? distanceResult.getValue() : null,
        average_speed: averageSpeedResult ? averageSpeedResult.getValue() : null,
        burned_calories: burnedCaloriesResult.getValue(),
        description: descriptionResult ? descriptionResult.getValue() : null,
        gps_track: gpsTrack,
        user: userDetails,
        activity: activityDetails,
        is_favorite: false,
        nb_comments: 0,
        nb_likes: 0,
      };
      logger.debug(`[UseCase/AddASession] UseCase - sessionProps: ${JSON.stringify(sessionProps)}`);

      const sessionResult = Session.create(sessionProps);
      if (sessionResult.isFailure) {
        const err = sessionResult.errorValue();
        logger.error(`[UseCase/AddASession] UseCase - gpsTrack: ${err}`);
        return left(new AppError.UnexpectedError(err));
      }

      logger.debug(
        `[UseCase/AddASession] UseCase - sessionResult.getValue(): ${JSON.stringify(
          sessionResult.getValue()
        )}`
      );
      await this.sessionRepo.add(sessionResult.getValue());

      return right(Result.ok<UniqueId>(sessionResult.getValue().id));
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
