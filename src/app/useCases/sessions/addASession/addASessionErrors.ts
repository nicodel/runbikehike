import { Result } from '@/shared/core/Result';
import { UseCaseError } from '@/shared/core/UseCaseError';

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace AddASessionErrors {
  export class WrongValue extends Result<UseCaseError> {
    constructor(value: string, label: string) {
      super(false, {
        message: `Wrong value "${value}" for ${label}.`,
      } as UseCaseError);
    }
  }
  export class MissingMandatoryValue extends Result<UseCaseError> {
    constructor(value: string) {
      super(false, {
        message: `Missing mandatory Session value "${value}".`,
      } as UseCaseError);
    }
  }
  export class UnknownUser extends Result<UseCaseError> {
    constructor(user: string) {
      super(false, {
        message: `Unknown user "${user}".`,
      } as UseCaseError);
    }
  }
  export class UnknownActivity extends Result<UseCaseError> {
    constructor(activity_name: string) {
      super(false, {
        message: `Unknown activity name "${activity_name}".`,
      } as UseCaseError);
    }
  }
}
