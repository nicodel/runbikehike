/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';
import { BaseController } from '@/shared/infra/BaseController';
import { AddASession } from './addASession';
import { AddASessionDTO } from './addASessionDTO';
import logger from '@/shared/utils/logger';

export class AddASessionController extends BaseController {
  private useCase: AddASession;

  constructor(useCase: AddASession) {
    super();
    this.useCase = useCase;
  }

  protected async executeImpl(req: Request | any, res: Response): Promise<any> {
    const dto: AddASessionDTO = {
      ...req.body,
      user_id: req.user as Request,
    };

    logger.debug('[UseCase/AddASession] Controller - dto: %s', dto);
    if (dto.gps_track)
      logger.debug(
        '[UseCase/AddASession] Controller - dto.gps_track[0][1]: %s',
        dto.gps_track[0][1]
      );

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res, errMessage);
        } else {
          const message = errMessage ? errMessage : error.errorValue();
          return this.badRequest(res, message.toString());
        }
      } else {
        const sessionId = result.value.getValue().id.toString();
        logger.debug('[UseCases/AddASession] Controller - result %s', result);
        return this.ok(res, { id: sessionId });
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
