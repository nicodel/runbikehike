export interface AddASessionDTO {
  name: string;
  start_date_time: string;
  duration: string;
  burned_calories?: string;
  distance?: string;
  average_speed?: string;
  description?: string;
  gps_track?: string[][];
  activity_name: string;
  user_id: string;
}
