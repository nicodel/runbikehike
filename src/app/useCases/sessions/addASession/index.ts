import { activitiesRepo, sessionsRepo } from '@/app/repositories/sessions';
import { AddASession } from './addASession';
import { usersRepo } from '@/app/repositories/auth';
import { AddASessionController } from './addASessionController';
import { profilesRepo } from '@/app/repositories/users';

const addASession = new AddASession(sessionsRepo, usersRepo, activitiesRepo, profilesRepo);

export const addASessionController = new AddASessionController(addASession);
