import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { fakeInjectData, fakeDestroy } from '@/fake_data';
import { AddASession } from './addASession';
import { activitiesRepo, sessionsRepo } from '@/app/repositories/sessions';
import { usersRepo } from '@/app/repositories/auth';
import { profilesRepo } from '@/app/repositories/users';
import { fakeSessions } from '@/fake_data/sessions';
import { UseCaseError } from '@/shared/core/UseCaseError';

const addASession = new AddASession(sessionsRepo, usersRepo, activitiesRepo, profilesRepo);

describe('App / UseCases / Sessions / AddASession', () => {
  describe('execute()', () => {
    before(async () => {
      await fakeInjectData('light');
    });

    after(async () => {
      await fakeDestroy();
    });

    it('should return Right result as a success', async () => {
      const result = await addASession.execute({
        name: fakeSessions[2].name,
        start_date_time: fakeSessions[2].start_date_time,
        duration: fakeSessions[2].duration.toString(),
        distance: fakeSessions[2].distance.toString(),
        average_speed: fakeSessions[2].average_speed.toString(),
        // burned_calories: '5318208',
        description: fakeSessions[2].description,
        user_id: fakeSessions[2].user.id,
        activity_name: fakeSessions[2].activity.name,
      });
      result.isRight().should.be.true;
    });

    it('should return Left result as a failure, if "user_id" is unknown', async () => {
      const result = await addASession.execute({
        name: fakeSessions[2].name,
        start_date_time: fakeSessions[2].start_date_time,
        duration: fakeSessions[2].duration.toString(),
        distance: fakeSessions[2].distance.toString(),
        average_speed: fakeSessions[2].average_speed.toString(),
        description: fakeSessions[2].description,
        user_id: 'not the right user id',
        activity_name: fakeSessions[2].activity.name,
      });
      result.isLeft().should.be.true;
      const error = result.value.error as UseCaseError;
      error.message.should.equal('Unknown user "not the right user id".');
    });

    it('should return Left result as a failure, if "name" is missing', async () => {
      const result = await addASession.execute({
        name: null,
        start_date_time: fakeSessions[2].start_date_time,
        duration: fakeSessions[2].duration.toString(),
        distance: fakeSessions[2].distance.toString(),
        average_speed: fakeSessions[2].average_speed.toString(),
        description: fakeSessions[2].description,
        user_id: fakeSessions[2].user.id,
        activity_name: fakeSessions[2].activity.name,
      });
      result.isLeft().should.be.true;
      const error = result.value.error as UseCaseError;
      error.message.should.equal('Missing mandatory Session value "name".');
    });

    it('should return Left resulat as failure, if "start_date_time" is missing', async () => {
      const result = await addASession.execute({
        name: fakeSessions[2].name,
        start_date_time: null,
        duration: fakeSessions[2].duration.toString(),
        distance: fakeSessions[2].distance.toString(),
        average_speed: fakeSessions[2].average_speed.toString(),
        description: fakeSessions[2].description,
        user_id: fakeSessions[2].user.id,
        activity_name: fakeSessions[2].activity.name,
      });
      result.isLeft().should.be.true;
      const error = result.value.error as UseCaseError;
      error.message.should.equal('Missing mandatory Session value "start_date_time".');
    });

    it('should return Left resulat as failure, if "duration" is missing', async () => {
      const result = await addASession.execute({
        name: fakeSessions[2].name,
        start_date_time: fakeSessions[2].start_date_time,
        duration: null,
        distance: fakeSessions[2].distance.toString(),
        average_speed: fakeSessions[2].average_speed.toString(),
        description: fakeSessions[2].description,
        user_id: fakeSessions[2].user.id,
        activity_name: fakeSessions[2].activity.name,
      });
      result.isLeft().should.be.true;
      const error = result.value.error as UseCaseError;
      error.message.should.equal('Missing mandatory Session value "duration".');
    });

    // it('should return Left resulat as failure, if "burned_calories" is missing', async () => {
    //   const result = await addASession.execute({
    //     name: 'Five times across the US',
    //     start_date_time: '1976-07-06T10:00:00.000Z',
    //     duration: '37987200', // 3 years, 2 months, 14 days and 16 hours
    //     distance: '25000000',
    //     average_speed: '0.658116418161907',
    //     burned_calories: null,
    //     user_id: 'a9202021-d1fe-470c-b00f-ceb85084cbea',
    //     activity_name: 'running',
    //   });
    //   result.isLeft().should.be.true;
    //   const error = result.value.error as UseCaseError;
    //   error.message.should.equal('Missing mandatory Session value "burned_calories".');
    // });

    it('should return Left resulat as failure, if "activity_name" is missing', async () => {
      const result = await addASession.execute({
        name: fakeSessions[2].name,
        start_date_time: fakeSessions[2].start_date_time,
        duration: fakeSessions[2].duration.toString(),
        distance: fakeSessions[2].distance.toString(),
        average_speed: fakeSessions[2].average_speed.toString(),
        description: fakeSessions[2].description,
        user_id: fakeSessions[2].user.id,
        activity_name: '',
      });
      result.isLeft().should.be.true;
      const error = result.value.error as UseCaseError;
      error.message.should.equal('Unknown activity name "".');
    });

    it('should return Left resulat as failure, if "activity_name" is unknown', async () => {
      const result = await addASession.execute({
        name: fakeSessions[2].name,
        start_date_time: fakeSessions[2].start_date_time,
        duration: fakeSessions[2].duration.toString(),
        distance: fakeSessions[2].distance.toString(),
        average_speed: fakeSessions[2].average_speed.toString(),
        description: fakeSessions[2].description,
        user_id: fakeSessions[2].user.id,
        activity_name: 'windsurf',
      });
      result.isLeft().should.be.true;
      const error = result.value.error as UseCaseError;
      error.message.should.equal('Unknown activity name "windsurf".');
    });

    it('should return Left resulat as failure, if "distance" is not a number', async () => {
      const result = await addASession.execute({
        name: fakeSessions[2].name,
        start_date_time: fakeSessions[2].start_date_time,
        duration: fakeSessions[2].duration.toString(),
        distance: 'null',
        average_speed: fakeSessions[2].average_speed.toString(),
        description: fakeSessions[2].description,
        user_id: fakeSessions[2].user.id,
        activity_name: fakeSessions[2].activity.name,
      });
      result.isLeft().should.be.true;
      const error = result.value.error as UseCaseError;
      error.message.should.equal('Wrong value "null" for distance.');
    });

    it('should return Left resulat as failure, if "average_speed" is not a number', async () => {
      const result = await addASession.execute({
        name: fakeSessions[2].name,
        start_date_time: fakeSessions[2].start_date_time,
        duration: fakeSessions[2].duration.toString(),
        distance: fakeSessions[2].distance.toString(),
        average_speed: 'null',
        description: fakeSessions[2].description,
        user_id: fakeSessions[2].user.id,
        activity_name: fakeSessions[2].activity.name,
      });
      result.isLeft().should.be.true;
      const error = result.value.error as UseCaseError;
      error.message.should.equal('Wrong value "null" for average_speed.');
    });
  });
});
