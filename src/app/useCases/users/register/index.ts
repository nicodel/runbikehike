import { usersRepo } from '@/app/repositories/auth';
import { profilesRepo } from '@/app/repositories/users';
import { Register } from './register';
import { RegisterController } from './registerController';

const register = new Register(usersRepo, profilesRepo);

const registerController = new RegisterController(register);

export { register, registerController };
