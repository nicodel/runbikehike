import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Register } from './register';
import { usersRepo } from '@/app/repositories/auth';
import { profilesRepo } from '@/app/repositories/users';

import { fakeDestroy, fakeInjectData } from '@/fake_data';

const createUser = new Register(usersRepo, profilesRepo);

describe('App / UseCases / Users / Register', () => {
  describe('execute()', () => {
    before(async () => {
      await fakeInjectData('light');
    });

    after(async () => {
      await fakeDestroy();
    });

    it('should return isSuccess', async () => {
      const result = await createUser.execute({
        email: 'casquette@verte.com',
        password: 'password',
      });
      result.value.isSuccess.should.be.true;
    });
  });
});
