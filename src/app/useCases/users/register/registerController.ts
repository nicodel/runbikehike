/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';
import { BaseController } from '@/shared/infra/BaseController';
import { Register } from './register';
import { RegisterDTO } from './registerDTO';

export class RegisterController extends BaseController {
  private useCase: Register;

  constructor(useCase: Register) {
    super();
    this.useCase = useCase;
  }

  protected async executeImpl(req: Request, res: Response): Promise<any> {
    const dto: RegisterDTO = {
      email: req.body.email,
      password: req.body.password,
    };

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;

        if (
          error.errorValue().message ===
          `The email ${req.body.email} is already used by another account.`
        ) {
          this.conflict(res, error.errorValue().message);
        } else if (error.errorValue().toString() === 'Email address not valid.') {
          this.badRequest(res, error.errorValue().message);
        } else if (error.errorValue().toString() === 'Text password is not at least 8 chars.') {
          this.badRequest(res, error.errorValue().toString());
        } else {
          return this.fail(
            res,
            error.errorValue().message ? error.errorValue().message : error.errorValue()
          );
        }
      } else {
        return this.ok(res);
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
