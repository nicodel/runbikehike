/* eslint-disable @typescript-eslint/no-namespace */
import { Result } from '@/shared/core/Result';
import { UseCaseError } from '@/shared/core/UseCaseError';

export namespace RegistorErrors {
  export class EmailAlreadyUsedError extends Result<UseCaseError> {
    constructor(email: string) {
      super(false, {
        message: `The email ${email} is already used by another account.`,
      } as UseCaseError);
    }
  }
}
