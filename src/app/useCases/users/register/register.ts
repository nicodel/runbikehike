/* eslint-disable @typescript-eslint/no-explicit-any */
import { Either, Result, left, right } from '@/shared/core/Result';
import { AppError } from '@/shared/core/AppError';
import { UseCase } from '@/shared/core/UseCase';
import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';
import { Email } from '@/app/domains/email';
import { Password } from '@/app/domains/auth/password';
import { User } from '@/app/domains/auth/user';
import { RegistorErrors } from './registerErrors';
import { RegisterDTO } from './registerDTO';
import { Profile } from '@/app/domains/users/profile';
import { ShortText } from '@/app/domains/shortText';
import { Gender } from '@/app/domains/users/gender';
import { Birthyear } from '@/app/domains/users/birthyear';
import { Height } from '@/app/domains/users/height';
import { Weight } from '@/app/domains/users/weight';
import { IProfilesRepo } from '@/app/repositories/users/IProfilesRepo';
import logger from '@/shared/utils/logger';

type Response = Either<
  RegistorErrors.EmailAlreadyUsedError | AppError.UnexpectedError | Result<any>,
  Result<void>
>;

export class Register implements UseCase<RegisterDTO, Promise<Response>> {
  private usersRepo: IUsersRepo;
  private profilesRepo: IProfilesRepo;

  constructor(usersRepo: IUsersRepo, profilesRepo: IProfilesRepo) {
    this.usersRepo = usersRepo;
    this.profilesRepo = profilesRepo;
  }

  async execute(dto: RegisterDTO): Promise<Response> {
    const emailResult = Email.create({ value: dto.email });
    const passwordResult = Password.create({ value: dto.password });

    const dtoResult = Result.combine([emailResult, passwordResult]);

    if (dtoResult.isFailure) {
      return left(Result.fail<void>(dtoResult.error)) as Response;
    }

    const email = emailResult.getValue();
    const password = passwordResult.getValue();

    try {
      const emailAlreadyUsed = await this.usersRepo.exists(email);

      if (emailAlreadyUsed) {
        return left(new RegistorErrors.EmailAlreadyUsedError(email.value)) as Response;
      }

      const userResult: Result<User> = User.create({
        email,
        password,
        isAdmin: false,
        isDisabled: false,
      });
      logger.debug('[UseCases/Register] Use Case - userResult: %s', userResult.getValue().props);

      if (userResult.isFailure) {
        return left(Result.fail<void>(userResult.error.toString())) as Response;
      }

      const user: User = userResult.getValue();

      await this.usersRepo.save(user);

      const emptyProfileResult = Profile.create({
        name: ShortText.create({ name: 'name', value: 'to be updated' }).getValue(),
        gender: Gender.create({ value: 'to be updated' }).getValue(),
        birthyear: Birthyear.create({ value: new Date().getFullYear() - 18 }).getValue(),
        height: Height.create({ value: 0 }).getValue(),
        weight: Weight.create({ value: 0 }).getValue(),
        user: user,
      });

      if (emptyProfileResult.isFailure) {
        return left(Result.fail<void>(emptyProfileResult.error.toString())) as Response;
      }

      const profile: Profile = emptyProfileResult.getValue();

      await this.profilesRepo.save(profile);

      return right(Result.ok<void>());
    } catch (err) {
      return left(new AppError.UnexpectedError(err)) as Response;
    }
  }
}
