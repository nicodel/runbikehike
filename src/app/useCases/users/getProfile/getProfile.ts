/* eslint-disable @typescript-eslint/no-explicit-any */
import { AppError } from '@/shared/core/AppError';
import { Either, left, Result, right } from '@/shared/core/Result';
import { UseCase } from '@/shared/core/UseCase';

import { Profile } from '@/app/domains/users/profile';
import { IProfilesRepo } from '@/app/repositories/users/IProfilesRepo';

import { GetProfileRequestDTO } from './getProfileDTO';
import { GetProfileErrors } from './getProfileErrors';

type Response = Either<
  GetProfileErrors.ProfileNotFoundError | AppError.UnauthorizedError | AppError.UnexpectedError,
  Result<Profile>
>;

export class GetProfile implements UseCase<GetProfileRequestDTO, Promise<Response>> {
  private profilesRepo: IProfilesRepo;

  constructor(profilesRepo: IProfilesRepo) {
    this.profilesRepo = profilesRepo;
  }

  public async execute(dto: GetProfileRequestDTO): Promise<Response> {
    const { user_id } = dto;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    try {
      const profileDetails: Profile = await this.profilesRepo.getProfile(user_id);

      return profileDetails === null
        ? left(new GetProfileErrors.ProfileNotFoundError(user_id))
        : right(Result.ok<Profile>(profileDetails));
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
