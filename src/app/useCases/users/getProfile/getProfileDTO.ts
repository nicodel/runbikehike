import { ProfilesDTO } from '@/app/dtos/profilesDTO';

export interface GetProfileRequestDTO {
  user_id: string;
}

export type GetProfileResponseDTO = ProfilesDTO;
