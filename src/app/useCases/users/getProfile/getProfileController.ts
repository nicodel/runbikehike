/* eslint-disable @typescript-eslint/no-explicit-any */

import { Request, Response } from 'express';

import { BaseController } from '@/shared/infra/BaseController';

import { ProfileMap } from '@/app/mappers/users/profileMap';

import { GetProfile } from './getProfile';
import { GetProfileRequestDTO, GetProfileResponseDTO } from './getProfileDTO';

import logger from '@/shared/utils/logger';

export class GetProfileController extends BaseController {
  private useCase: GetProfile;

  constructor(useCase: GetProfile) {
    super();
    this.useCase = useCase;
  }
  async executeImpl(req: Request | any, res: Response): Promise<any> {
    logger.debug('[UseCases/getProfile] Controller - req.user %s', req.user);
    const dto: GetProfileRequestDTO = {
      user_id: req.user,
    };

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        if (errMessage === `Could not find a user profile by user id ${dto.user_id}.`) {
          return this.ok(res);
        } else if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res, errMessage);
        } else {
          return this.fail(res, errMessage);
        }
      } else {
        const profile = result.value.getValue();
        logger.debug(
          `[UseCases/getProfile] Controller - profile.user: ${JSON.stringify(profile.user)}`
        );

        const profileDTO = ProfileMap.toDTO(profile);
        logger.debug('[UseCases/getProfile] Controller - profileDTO %s', profileDTO);

        return this.ok<{ profile: GetProfileResponseDTO }>(res, {
          profile: profileDTO,
        });
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
