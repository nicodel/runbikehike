import { profilesRepo } from '@/app/repositories/users';
import { GetProfile } from './getProfile';
import { GetProfileController } from './getProfileController';

const getProfile = new GetProfile(profilesRepo);

export const getProfileController = new GetProfileController(getProfile);
