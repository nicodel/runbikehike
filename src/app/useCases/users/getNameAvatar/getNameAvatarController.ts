import { BaseController } from '@/shared/infra/BaseController';
import { GetNameAvatar } from './getNameAvatar';
import { Request, Response } from 'express';
import { GetNameAvatarRequestDTO, GetNameAvatarResponseDTO } from './getNameAvatarDTO';
import logger from '@/shared/utils/logger';

export class GetNameAvatarController extends BaseController {
  private useCase: GetNameAvatar;

  constructor(useCase: GetNameAvatar) {
    super();
    this.useCase = useCase;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected async executeImpl(req: Request | any, res: Response): Promise<Response> {
    const dto: GetNameAvatarRequestDTO = {
      user_id: req.user,
      user: req.params.user,
    };

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;
        const errMessage = error.errorValue().message;

        if (
          errMessage === `Unknown user "${req.user}".` ||
          errMessage === 'Unauthorized as unauthenticated'
        ) {
          return this.unauthorized(res, errMessage);
        } else {
          return this.badRequest(res, errMessage ? errMessage : error.errorValue().toString());
        }
      } else {
        const responseDTO: GetNameAvatarResponseDTO = result.value.getValue();
        logger.debug(
          `[UseCase/GetNameAvatar] Controller responseDTO: ${JSON.stringify(responseDTO)}`
        );

        return this.ok<GetNameAvatarResponseDTO>(res, responseDTO);
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
