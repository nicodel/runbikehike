import { avatarsRepo, profilesRepo } from '@/app/repositories/users';
import { GetNameAvatar } from './getNameAvatar';
import { GetNameAvatarController } from './getNameAvatarController';

const getNameAvatar = new GetNameAvatar(profilesRepo, avatarsRepo);
const getNameAvatarController = new GetNameAvatarController(getNameAvatar);

export { getNameAvatarController };
