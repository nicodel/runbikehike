/* eslint-disable @typescript-eslint/no-explicit-any */
import { Either, Result, left, right } from '@/shared/core/Result';
import { AppError } from '@/shared/core/AppError';
import { UseCase } from '@/shared/core/UseCase';
import { GetNameAvatarRequestDTO } from './getNameAvatarDTO';

import { IProfilesRepo } from '@/app/repositories/users/IProfilesRepo';
import { IProfileAvatarsRepo } from '@/app/repositories/users/IProfileAvatarsRepo';
import logger from '@/shared/utils/logger';

type Response = Either<AppError.UnauthorizedError | AppError.UnexpectedError, Result<any>>;

export class GetNameAvatar implements UseCase<GetNameAvatarRequestDTO, Promise<Response>> {
  private userProfilesRepo: IProfilesRepo;
  private profilesAvatarRepo: IProfileAvatarsRepo;

  constructor(userProfilesRepo: IProfilesRepo, profilesAvatarRepo: IProfileAvatarsRepo) {
    this.userProfilesRepo = userProfilesRepo;
    this.profilesAvatarRepo = profilesAvatarRepo;
  }

  public async execute(req: GetNameAvatarRequestDTO): Promise<Response> {
    const { user_id } = req;

    if (!user_id) {
      return left(new AppError.UnauthorizedError(user_id));
    }

    try {
      const userProfile = await this.userProfilesRepo.getProfile(req.user);
      logger.debug(`[UseCase/GetNameAvatar] UseCase userProfile ${JSON.stringify(userProfile)}`);

      const nameAvatar = {
        id: req.user,
        name: userProfile.name.value,
        avatar: { data: userProfile.avatar.data, mimetype: userProfile.avatar.mimetype },
      };
      logger.debug(`[UseCase/GetNameAvatar] UseCase nameAvatar: ${JSON.stringify(nameAvatar)}`);
      return right(Result.ok<any>(nameAvatar));
    } catch (err) {
      logger.error(`[UseCase/GetNameAvatar] UseCase Error: ${JSON.stringify(err)}`);
      return left(new AppError.UnexpectedError(err));
    }
  }
}
