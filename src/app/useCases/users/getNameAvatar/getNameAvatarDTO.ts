export interface GetNameAvatarRequestDTO {
  user_id: string;
  user: string;
}

export interface GetNameAvatarResponseDTO {
  id: string;
  name: string;
  avatar: {
    mimetype: string;
    data: string;
  };
}
