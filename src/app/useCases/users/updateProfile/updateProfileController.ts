/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';
import { BaseController } from '@/shared/infra/BaseController';
import { UpdateProfile } from './updateProfile';
import { UpdateProfileRequestDTO } from './updateProfileDTO';
import logger from '@/shared/utils/logger';

export class UpdateProfileController extends BaseController {
  private useCase: UpdateProfile;

  constructor(useCase: UpdateProfile) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(req: Request | any, res: Response): Promise<any> {
    const dto: UpdateProfileRequestDTO = { ...req.body, user_id: req.user as Request };
    logger.debug('[UseCase/UpdateProfile] Controler - dto: %s', dto);

    try {
      const result = await this.useCase.execute(dto);
      if (result.isLeft()) {
        const error = result.value;
        const errorMessage = error.errorValue().message
          ? error.errorValue().message
          : error.errorValue();
        logger.error('[UseCase/UpdateProfile] Controler Error: %s', errorMessage);

        return this.badRequest(res, errorMessage);
      } else {
        return this.ok(res);
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
