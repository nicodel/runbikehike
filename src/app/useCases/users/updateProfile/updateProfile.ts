/* eslint-disable @typescript-eslint/no-explicit-any */
import { AppError } from '@/shared/core/AppError';
import { Either, Result, left, right } from '@/shared/core/Result';
import { UseCase } from '@/shared/core/UseCase';
import { UpdateProfileRequestDTO } from './updateProfileDTO';
import { IProfilesRepo } from '@/app/repositories/users/IProfilesRepo';
import { Guard } from '@/shared/core/Guard';
import { ShortText } from '@/app/domains/shortText';
import { Gender } from '@/app/domains/users/gender';
import { GenderType } from '@/app/domains/users/genderType';
import { Birthyear } from '@/app/domains/users/birthyear';
import { Weight } from '@/app/domains/users/weight';
import { Height } from '@/app/domains/users/height';
import { LongText } from '@/app/domains/longText';
import { UniqueId } from '@/app/domains/uniqueId';
import { UniqueEntityID } from '@/shared/domains/UniqueEntityID';
import { Profile } from '@/app/domains/users/profile';
import { IUsersRepo } from '@/app/repositories/auth/IUsersRepo';
import logger from '@/shared/utils/logger';
import { Image } from '@/app/domains/image';
import { IProfileAvatarsRepo } from '@/app/repositories/users/IProfileAvatarsRepo';

type Response = Either<AppError.UnexpectedError | Result<any>, Result<void>>;

export class UpdateProfile implements UseCase<UpdateProfileRequestDTO, Promise<Response>> {
  private profilesRepo: IProfilesRepo;
  private usersRepo: IUsersRepo;
  private avatarsRepo: IProfileAvatarsRepo;

  constructor(
    profilesRepo: IProfilesRepo,
    usersRepo: IUsersRepo,
    avatarsRepo: IProfileAvatarsRepo
  ) {
    this.profilesRepo = profilesRepo;
    this.usersRepo = usersRepo;
    this.avatarsRepo = avatarsRepo;
  }

  public async execute(dto: UpdateProfileRequestDTO): Promise<Response> {
    logger.debug('[UseCase/UpdateProfile] UseCase - dto: %s', dto);

    const dtoResult = Guard.againstNullOrUndefined(dto, 'dto');
    if (!dtoResult.succeeded) return left(new AppError.UnexpectedError(dtoResult.message));

    try {
      const existingProfile = await this.profilesRepo.getProfile(dto.user_id);
      logger.debug('[UseCase/updateProfile] UseCase - existingProfile: %s', existingProfile);
      const profileId = existingProfile ? existingProfile.id.id : null;
      logger.debug('[UseCase/updateProfile] UseCase - profileId: %s', profileId);

      const nameResult = ShortText.create({ value: dto.name, name: 'name' });
      if (nameResult.isFailure) {
        return left(nameResult);
      }

      const emailResult = ShortText.create({ value: dto.email, name: 'email' });
      if (emailResult.isFailure) {
        return left(emailResult);
      }

      const genderResult = Gender.create({ value: dto.gender as GenderType });
      if (genderResult.isFailure) {
        return left(genderResult);
      }

      const birthyearResult = Birthyear.create({ value: parseInt(dto.birthyear) });
      if (birthyearResult.isFailure) {
        return left(birthyearResult);
      }

      const weightResult = Weight.create({ value: parseInt(dto.weight) });
      if (weightResult.isFailure) {
        return left(weightResult);
      }

      const heightResult = Height.create({ value: parseInt(dto.height) });
      if (heightResult.isFailure) {
        return left(heightResult);
      }

      /* ??? */
      /* Is this really necessary ? */
      const userResult = UniqueId.create(new UniqueEntityID(dto.user_id));
      if (userResult.isFailure) {
        return left(userResult);
      }
      /* ??? */

      let avatarResult;
      if (dto.avatar) {
        if (!existingProfile.avatar) {
          avatarResult = Image.create(dto.avatar);
          if (avatarResult.isFailure) {
            return left(avatarResult);
          }
          logger.debug(
            '[UseCase/updateProfile] UseCase - avatarResult: %s',
            avatarResult.getValue()
          );
          const avatar = avatarResult.getValue();

          await this.avatarsRepo.save(avatar);
        } else {
          logger.debug(
            '[UseCase/updateProfile] UseCase - avatar is not new one: %s',
            existingProfile.avatar.props
          );
        }
      }

      let locationResult;
      if (dto.location) {
        locationResult = ShortText.create({ value: dto.location, name: 'location' });
        if (locationResult.isFailure) {
          return left(locationResult);
        }
      }

      let biographyResult;
      if (dto.biography) {
        biographyResult = LongText.create({ value: dto.biography, name: 'biography' });
        if (biographyResult.isFailure) {
          return left(biographyResult);
        }
      }

      const userDetails = await this.usersRepo.getUserById(dto.user_id);
      logger.debug(
        '[UseCase/updateProfile] UseCase - userDetails: %s',
        JSON.stringify(userDetails.props)
      );

      const profileProps = {
        name: nameResult.getValue(),
        email: emailResult.getValue(),
        gender: genderResult.getValue(),
        birthyear: birthyearResult.getValue(),
        height: heightResult.getValue(),
        weight: weightResult.getValue(),
        user: userDetails,
        avatar: avatarResult ? avatarResult.getValue() : null,
        location: locationResult ? locationResult.getValue() : null,
        biography: biographyResult ? biographyResult.getValue() : null,
      };
      logger.debug('[UseCase/updateProfile] UseCase - profileProps: %s', profileProps);

      const profileResult = Profile.create(profileProps, profileId);
      logger.debug('[UseCase/updateProfile] UseCase - profileResult: %s', profileResult.getValue());

      await this.profilesRepo.save(profileResult.getValue());

      return right(Result.ok<void>());
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
