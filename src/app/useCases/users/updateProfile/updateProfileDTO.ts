export interface UpdateProfileRequestDTO {
  name: string;
  email: string;
  gender: string;
  birthyear: string;
  height: string;
  weight: string;
  user_id: string;
  location?: string;
  biography?: string;
  avatar?: {
    mimetype: string;
    data: string;
  };
  is_admin: boolean;
  is_disabled: boolean;
}
