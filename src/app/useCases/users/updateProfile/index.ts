import { avatarsRepo, profilesRepo } from '@/app/repositories/users';
import { UpdateProfile } from './updateProfile';
import { usersRepo } from '@/app/repositories/auth';
import { UpdateProfileController } from './updateProfileController';

const updateProfile = new UpdateProfile(profilesRepo, usersRepo, avatarsRepo);

const updateProfileController = new UpdateProfileController(updateProfile);

export { updateProfileController };
