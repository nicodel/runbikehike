import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { fakeUsers } from '@/fake_data/users';
import { fakeInjectData, fakeDestroy } from '@/fake_data';

import { usersRepo } from '@/app/repositories/auth';
import { avatarsRepo, profilesRepo } from '@/app/repositories/users';

import { UpdateProfile } from './updateProfile';

const updateProfile = new UpdateProfile(profilesRepo, usersRepo, avatarsRepo);

describe('App / UseCases / Users / UpdateProfile', () => {
  describe('execute()', () => {
    before(async () => {
      await fakeInjectData('light');
    });

    after(async () => {
      await fakeDestroy();
    });

    it('should a Right result as a success', async () => {
      const result = await updateProfile.execute({
        email: fakeUsers.casquette_verte.email,
        name: 'Casquette Verte',
        gender: 'male',
        birthyear: '1981',
        height: '175',
        weight: '65',
        location: 'Paris',
        biography: null,
        user_id: fakeUsers.casquette_verte.id,
        is_admin: fakeUsers.casquette_verte.is_admin,
        is_disabled: fakeUsers.casquette_verte.is_disabled,
      });
      result.isRight().should.be.true;
    });

    it('should return Left result as a failure', async () => {
      const result = await updateProfile.execute({
        email: fakeUsers.casquette_verte.email,
        name: 'Casquette Verte',
        gender: 'male',
        birthyear: '1781', // pretty old!
        height: '175',
        weight: '65',
        location: 'Paris',
        biography: null,
        user_id: fakeUsers.casquette_verte.id,
        is_admin: fakeUsers.casquette_verte.is_admin,
        is_disabled: fakeUsers.casquette_verte.is_disabled,
      });
      result.isLeft().should.be.true;
    });
  });
});
