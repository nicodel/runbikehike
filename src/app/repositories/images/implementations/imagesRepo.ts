/* eslint-disable @typescript-eslint/no-explicit-any */
import { Image } from '@/app/domains/image';

import { ImageMap } from '@/app/mappers/imageMap';
import { IImagesRepo } from '../IImagesRepo';

import logger from '@/shared/utils/logger';

export class ImagesRepo implements IImagesRepo {
  private repo: any;

  constructor(repo: any) {
    this.repo = repo;
  }

  public async getByImageId(imageId: string): Promise<Image> {
    try {
      logger.debug(`[Repos/Images] getByImageId - imageId ${imageId}`);

      const rawImage = await this.repo.findOne({ where: { id: imageId } });
      logger.debug(`[Repos/Images] getByImageId - rawImage ${JSON.stringify(rawImage)}`);

      if (rawImage === null) {
        logger.error(`[Repos/Images] getByImageId - Image not found for id: ${imageId}`);
        throw new Error(`Image not found for id: ${imageId}`);
      }
      return ImageMap.toDomain(rawImage);
    } catch (err) {
      logger.error('[Repos/Images] getByImageId %s', err.toString());
      throw err.toString();
    }
  }

  public async getImagesBySessionId(sessionId: string): Promise<Image[]> {
    try {
      const rawImages = await this.repo.find({
        where: {
          session: { id: sessionId },
        },
      });
      if (!rawImages) {
        logger.error(
          `[Repos/Images] getByImageId - Image(s) not found for session_id: ${sessionId}`
        );
        throw new Error(`Image(s) not found for session_id: ${sessionId}`);
      }
      const images = rawImages.map((rawImage) => ImageMap.toDomain(rawImage));
      return images;
    } catch (err) {
      logger.error('[Repos/Images] getImagesBySessionId %s', err.toString());
      throw err.toString();
    }
  }

  public async removeImage(image: Image): Promise<void> {
    try {
      logger.debug(`[Repo/Images] remove - image ${JSON.stringify(image)}`);

      const rawImage = await ImageMap.toPersistence(image);
      logger.debug(`[Repo/Images] remove - rawSession ${JSON.stringify(rawImage)}`);

      await this.repo.remove(rawImage);
    } catch (err) {
      logger.error('[Repo/Images] remove - Error: %s', err.toString());
      throw new Error(err.toString());
    }
  }
}
