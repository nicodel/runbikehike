import { Image } from '@/app/domains/image';

export interface IImagesRepo {
  getByImageId(imageId: string): Promise<Image>;
  getImagesBySessionId(sessionId: string): Promise<Image[]>;
  removeImage(image: Image): Promise<void>;
}
