import { dataSource } from '@/shared/infra/database';
import { Images } from '@/shared/infra/database/models/images';

import { ImagesRepo } from './implementations/imagesRepo';

const imagesRepository = dataSource.getRepository(Images);
export const imagesRepo = new ImagesRepo(imagesRepository);
