/* eslint-disable @typescript-eslint/no-explicit-any */
import logger from '@/shared/utils/logger';
import { Profile } from '@/app/domains/users/profile';
import { ProfileMap } from '@/app/mappers/users/profileMap';
import { IProfilesRepo } from '../IProfilesRepo';

export class ProfilesRepo implements IProfilesRepo {
  private repo: any;

  constructor(repo: any) {
    this.repo = repo;
  }

  public async getProfile(user_id: string): Promise<Profile> {
    try {
      logger.debug('[Repos/Profiles] getProfile - id %s', user_id);
      const profile = await this.repo.findOne({ where: { user: { id: user_id } } });

      logger.debug('[Repos/Profiles] getProfile - profile %s', profile);
      logger.debug('[Repos/Profiles] getProfile - profile.user %s', profile.user);
      if (profile === null) {
        throw new Error(`Profile not found for id: ${user_id}`);
      }
      return ProfileMap.toDomain(profile);
    } catch (err) {
      throw new Error(err.toString());
    }
  }

  public async save(profileProps: Profile): Promise<void> {
    try {
      const rawProfile = await ProfileMap.toPersistence(profileProps);
      logger.debug('[Repos/Profiles] save rawProfile: %s', rawProfile);

      const profile = this.repo.create(rawProfile);
      logger.debug('[Repos/Profiles] save profile: %s', JSON.stringify(profile));

      await this.repo.save(profile);
    } catch (err) {
      logger.error('[Repos/Profiles] save Error: %s', err.toString());
      throw new Error(err.toString());
    }
  }

  public async searchByName(search_letters: string): Promise<any[]> {
    try {
      logger.debug('[Repos/Profiles] searchByName - search_letters %s', search_letters);

      const searchResults = await this.repo
        .createQueryBuilder('users_profiles')
        .leftJoinAndSelect('users_profiles.avatar', 'avatar')
        .leftJoinAndSelect('users_profiles.user', 'user')
        .where('users_profiles.name LIKE :searchTerm AND user.is_disabled = :is_disabled', {
          searchTerm: `%${search_letters}%`,
          is_disabled: false,
        })
        .select([
          'user.id',
          'users_profiles.name',
          'users_profiles.location',
          'avatar.mimetype',
          'avatar.data',
        ])
        .getMany();
      logger.debug('[Repos/Profiles] searchByName - results %s', searchResults);

      return searchResults;
    } catch (err) {
      throw new Error(err.toString());
    }
  }
}
