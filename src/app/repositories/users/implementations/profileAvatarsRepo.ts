/* eslint-disable @typescript-eslint/no-explicit-any */
import { ImageMap } from '@/app/mappers/imageMap';
import { IProfileAvatarsRepo } from '../IProfileAvatarsRepo';
import logger from '@/shared/utils/logger';
import { Image } from '@/app/domains/image';

export class ProfileAvatarsRepo implements IProfileAvatarsRepo {
  private repo: any;

  constructor(repo: any) {
    this.repo = repo;
  }

  public async save(avatarProps: Image): Promise<void> {
    const rawAvatar = ImageMap.toPersistence(avatarProps);

    try {
      const avatar = this.repo.create(rawAvatar);
      logger.debug('[Repos/Profiles] save avatar: %s', JSON.stringify(avatar));
      await this.repo.save(avatar);
    } catch (err) {
      logger.error('[Repos/ProfileAvatars] save Error: %s', err.toString());
      throw new Error(err.toString());
    }
  }
}
