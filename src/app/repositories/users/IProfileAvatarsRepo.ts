import { Image } from '@/app/domains/image';

export interface IProfileAvatarsRepo {
  save(avatar: Image): Promise<void>;
}
