import { dataSource } from '@/shared/infra/database';
import { UsersProfiles } from '@/shared/infra/database/models/usersProfiles';
import { ProfileAvatars } from '@/shared/infra/database/models/profileAvatars';

import { ProfilesRepo } from './implementations/profilesRepo';
import { ProfileAvatarsRepo } from './implementations/profileAvatarsRepo';

const profilesRepository = dataSource.getRepository(UsersProfiles);
const profileAvatarsRepository = dataSource.getRepository(ProfileAvatars);

export const profilesRepo = new ProfilesRepo(profilesRepository);
export const avatarsRepo = new ProfileAvatarsRepo(profileAvatarsRepository);
