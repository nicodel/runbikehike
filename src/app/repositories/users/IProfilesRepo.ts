/* eslint-disable @typescript-eslint/no-explicit-any */
// import { Image } from '@/app/domains/image';
import { Profile } from '@/app/domains/users/profile';

export interface IProfilesRepo {
  getProfile(user_id: string): Promise<Profile>;
  save(profile: Profile): Promise<void>;
  searchByName(search_letters: string): Promise<any[]>;
  // createProfile(profile: Profile): Promise<void>;
  // updateProfile(profile: Profile): Promise<void>;
  // saveAvatar(avatar: Image): Promise<void>;
}
