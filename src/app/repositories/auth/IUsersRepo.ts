import { User } from '@/app/domains/auth/user';
import { Email } from '@/app/domains/email';
import { Profile } from '@/app/domains/users/profile';

export interface IUsersRepo {
  exists(userEmail: Email): Promise<boolean>;
  getUserById(id: string): Promise<User>;
  getUserByEmail(email: Email | string): Promise<User>;
  save(user: User): Promise<void>;
  countAdminUsers(): Promise<number>;
  isAdmin(id: string): Promise<boolean>;
  getAllUsers(): Promise<{ user: User; profile: Profile }[]>;
}
