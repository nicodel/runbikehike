/* eslint-disable @typescript-eslint/no-explicit-any */
import { User } from '@/app/domains/auth/user';
import { Email } from '@/app/domains/email';

import { UsersMap } from '@/app/mappers/auth/usersMap';

import { IUsersRepo } from './IUsersRepo';
import { ProfileMap } from '@/app/mappers/users/profileMap';
import { Profile } from '@/app/domains/users/profile';

// import logger from '@/shared/utils/logger';

export class UsersRepo implements IUsersRepo {
  private repo: any;

  constructor(repo: any) {
    this.repo = repo;
  }

  async exists(userEmail: Email): Promise<boolean> {
    // logger.debug('[auth/repository/users] exist userEmail %s', userEmail.value);
    const user = await this.repo.findOneBy({
      email: userEmail.value,
    });
    // logger.debug('[auth/repository/users] exist user %s', user);
    return !!user === true;
  }

  async getUserByEmail(email: Email | string): Promise<User> {
    // logger.debug('[modules/auth/usersRepo] getUserByEmail');
    const rawUser = await this.repo.findOneBy({
      email: email instanceof Email ? (<Email>email).value : email,
    });
    // logger.debug('[modules/auth/usersRepo] getUserByEmail - rawUser %s', rawUser);

    if (rawUser === null) {
      throw new Error(`User "${email}" not found.`);
    }

    return UsersMap.toDomain(rawUser);
  }

  async getUserById(id: string): Promise<User> {
    // logger.debug('[Repo/Users] getUserById - id: %s', id);
    const rawUser = await this.repo.findOneBy({
      id: id,
    });
    // logger.debug('[Repo/Users] getUserById - rawUser: %s', rawUser);

    return rawUser ? UsersMap.toDomain(rawUser) : null;
  }

  public async save(user: User): Promise<void> {
    const rawUser = await UsersMap.toPersistence(user);
    // logger.debug('[auth/repository/users] save user %s', rawUser);

    try {
      const user = await this.repo.create(rawUser);
      await this.repo.save(user);
    } catch (err) {
      throw new Error(err.toString());
    }
  }

  public async countAdminUsers(): Promise<number> {
    try {
      const number_of_admins = this.repo.countBy({ is_admin: true });
      return number_of_admins;
    } catch (err) {
      throw new Error(err.toString());
    }
  }

  public async isAdmin(id: string): Promise<boolean> {
    try {
      const rawUser = await this.repo.findOneBy({
        id: id,
      });

      return rawUser ? rawUser.is_admin : null;
    } catch (err) {
      throw new Error(err.toString());
    }
  }

  public async getAllUsers(): Promise<{ user: User; profile: Profile }[]> {
    try {
      const rawUsersWithProfile = await this.repo.find({
        relations: {
          user_profile: true,
        },
      });

      // logger.debug(`[Repos/usersRepo] getAllUsers - rawUsers.length(): ${rawUsers.length}`);
      const users: { user: User; profile: Profile }[] = [];
      for (let index = 0; index < rawUsersWithProfile.length; index++) {
        const rawUserWithProfile = rawUsersWithProfile[index];
        // logger.debug(
        //   `[Repos/usersRepo] getAllUsers - rawUser[${index}]: ${JSON.stringify(rawUser.email)}`
        // );
        const user = UsersMap.toDomain(rawUserWithProfile);
        const profile = ProfileMap.toDomain(rawUserWithProfile.user_profile);
        users.push({ user, profile });
      }

      return users;
    } catch (err) {
      throw new Error(err.toString());
    }
  }
}
