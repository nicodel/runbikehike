import { Activity } from '@/app/domains/sessions/activity';

export interface IActivitiesRepo {
  save(activity: Activity): Promise<void>;
  getActivityByName(activityName: string): Promise<Activity>;
}
