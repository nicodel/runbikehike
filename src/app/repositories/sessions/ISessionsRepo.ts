import { Session } from '@/app/domains/sessions/session';

export interface ISessionsRepo {
  add(session: Session): Promise<void>;
  getBySessionId(sessionId: string): Promise<Session>;
  getSessionsForPeriod(period: number): Promise<Session[]>;
  removeSession(session: Session): Promise<void>;
  removeSessionById(sessionId: string): Promise<void>;
  save(session: Session): Promise<void>;
  itExists(sessionId: string): Promise<boolean>;
  getUserId(sessionId: string): Promise<string | null>;
}
