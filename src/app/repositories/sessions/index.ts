import { dataSource } from '@/shared/infra/database';
import { Activities } from '@/shared/infra/database/models/activities';
import { Sessions } from '@/shared/infra/database/models/sessions';

import { ActivitiesRepo } from './implementations/activitiesRepo';
import { SessionsRepo } from './implementations/sessionsRepo';
import { Comments } from '@/shared/infra/database/models/comments';
import { CommentsRepo } from './implementations/commentsRepo';

const commentsRepository = dataSource.getRepository(Comments);
const activitiesRepository = dataSource.getRepository(Activities);
const sessionsRepository = dataSource.getRepository(Sessions);

export const commentsRepo = new CommentsRepo(commentsRepository);
export const activitiesRepo = new ActivitiesRepo(activitiesRepository);
export const sessionsRepo = new SessionsRepo(sessionsRepository);
