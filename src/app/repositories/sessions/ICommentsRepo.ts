import { User } from '@/app/domains/auth/user';
import { Comment } from '@/app/domains/comment';
import { Session } from '@/app/domains/sessions/session';

export interface ICommentsRepo {
  getCommentById(commentId: string): Promise<Comment>;
  add(comment: Comment, user: User, session: Session): Promise<void>;
  getCommentsBySessionId(sessionId: string): Promise<Comment[]>;
  countCommentsBySessionId(sessionId: string): Promise<number>;
  deleteCommentById(commentId: string): Promise<void>;
}
