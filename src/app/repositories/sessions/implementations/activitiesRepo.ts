/* eslint-disable @typescript-eslint/no-explicit-any */
import logger from '@/shared/utils/logger';
import { IActivitiesRepo } from '../IActivitiesRepo';
import { ActivityMap } from '@/app/mappers/sessions/activityMap';
import { Activity } from '@/app/domains/sessions/activity';

export class ActivitiesRepo implements IActivitiesRepo {
  private repo: any;

  constructor(repo: any) {
    this.repo = repo;
  }

  public async save(activityProps: Activity): Promise<void> {
    try {
      const rawActivity = await ActivityMap.toPersistence(activityProps);
      // logger.debug('[Repos/Activities] save rawActivity: %s', rawActivity);

      const profile = this.repo.create(rawActivity);
      // logger.debug('[Repos/Activities] save activity: %s', JSON.stringify(profile));

      await this.repo.save(profile);
    } catch (err) {
      logger.error('[Repos/Activities] save Error: %s', err.toString());
      throw new Error(err.toString());
    }
  }

  public async getActivityByName(activityName: string): Promise<Activity> {
    try {
      const rawActivity = await this.repo.findOneBy({ name: activityName });
      // logger.debug('[Repos/Activities] getActivityByName rawActivity: %s', rawActivity);

      return rawActivity ? ActivityMap.toDomain(rawActivity) : null;
    } catch (err) {
      logger.error('[Repos/Activities] getActivityByName Error: %s', err.toString());
      throw new Error(err.toString());
    }
  }
}
