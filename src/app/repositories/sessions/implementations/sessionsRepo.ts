/* eslint-disable @typescript-eslint/no-explicit-any */
import { MoreThanOrEqual } from 'typeorm';
import { DateTime } from 'luxon';

import { Session } from '@/app/domains/sessions/session';
import { SessionMap } from '@/app/mappers/sessions/sessionMap';

import { ISessionsRepo } from '../ISessionsRepo';

import logger from '@/shared/utils/logger';

export class SessionsRepo implements ISessionsRepo {
  private repo: any;

  constructor(repo: any) {
    this.repo = repo;
  }

  public async add(sessionProps: Session): Promise<void> {
    const rawSession = await SessionMap.toPersistence(sessionProps);

    try {
      const session = this.repo.create(rawSession);
      await this.repo.save(session);
    } catch (err) {
      logger.error('[Repo/Sessions] add - Error: %s', err.toString());
      throw new Error(err.toString());
    }
  }

  public async getBySessionId(sessionId: string): Promise<Session> {
    try {
      logger.debug('[Repos/Sessions] getBySessionId - id %s', sessionId);

      const rawSession = await this.repo.findOne({
        where: { id: sessionId },
        relations: ['user', 'activity', 'images', 'comments'],
      });
      logger.debug(`[Repos/Sessions] getBySessionId - rawSession: ${JSON.stringify(rawSession)}`);

      if (rawSession === null) {
        throw new Error(`Session not found for id: ${sessionId}`);
      }
      return SessionMap.toDomain(rawSession);
    } catch (err) {
      logger.error('[Repos/Sessions] getBySessionId %s', err.toString());
      throw err.toString();
    }
  }

  public async getSessionsForPeriod(period: number): Promise<Session[]> {
    try {
      const currentDate = DateTime.now();
      const startPeriodDate = currentDate.minus({ days: period });

      const rawSessions = await this.repo.find({
        where: { start_date_time: MoreThanOrEqual(startPeriodDate.toISO()) },
        relations: ['user', 'activity'],
      });

      const sessions: Session[] = [];
      for (let index = 0; index < rawSessions.length; index++) {
        const rawSession = rawSessions[index];
        const session = SessionMap.toDomain(rawSession);
        sessions.push(session);
      }

      return sessions;
    } catch (err) {
      logger.error('[Repos/Sessions] getSessionsForPeriod %s - Error %s', period, err.toString());
      throw err.toString();
    }
  }

  public async removeSession(session: Session): Promise<void> {
    try {
      logger.debug(`[Repo/Sessions] remove - session ${JSON.stringify(session)}`);

      const rawSession = await SessionMap.toPersistence(session);
      logger.debug(`[Repo/Sessions] remove - rawSession ${JSON.stringify(rawSession)}`);

      await this.repo.remove(rawSession);
    } catch (err) {
      logger.error('[Repo/Sessions] remove - Error: %s', err.toString());
      throw new Error(err.toString());
    }
  }

  public async removeSessionById(sessionId: string): Promise<void> {
    try {
      logger.debug('[Repos/Sessions] removeSessionById - id %s', sessionId);
      const rawSession = await this.repo.findOne({
        where: { id: sessionId },
        relations: ['user', 'activity', 'images', 'comments'],
      });
      logger.debug(
        `[Repos/Sessions] removeSessionById - rawSession: ${JSON.stringify(rawSession)}`
      );

      if (rawSession) {
        // Remove the session, and the cascading delete will take care of associated images
        await this.repo.remove(rawSession);

        logger.debug(
          '[Repo/Sessions] removeSessionById - Session and associated Images deleted successfully.'
        );
      } else {
        logger.debug('[Repo/Sessions] removeSessionById - Session not found.');
      }
    } catch (err) {
      logger.error('[Repo/Sessions] removeSessionById - Error: %s', err.toString());
      throw new Error(err.toString());
    }
  }

  public async save(session: Session): Promise<void> {
    try {
      logger.debug(`[Repo/Sessions] save - session ${JSON.stringify(session)}`);
      logger.debug(
        `[Repo/Sessions] save - session.activity.name ${JSON.stringify(session.activity.name)}`
      );
      const rawSession = await SessionMap.toPersistence(session);
      logger.debug(
        `[Repo/Sessions] save - rawSession.activity.name ${JSON.stringify(
          rawSession.activity.name
        )}`
      );

      await this.repo.save(rawSession);
    } catch (err) {
      logger.error('[Repo/Sessions] save - Error: %s', err.toString());
      throw new Error(err.toString());
    }
  }

  public async itExists(sessionId: string): Promise<boolean> {
    try {
      const session = await this.repo.findOne({ where: { id: sessionId } });
      if (session === null) {
        return false;
      } else {
        return true;
      }
    } catch (err) {
      logger.error('[Repos/Sessions] itExists %s', err.toString());
      throw err.toString();
    }
  }

  public async getUserId(sessionId: string): Promise<string | null> {
    try {
      const session = await this.repo.findOne({ where: { id: sessionId }, relations: ['user'] });
      if (session === null) {
        return null;
      } else {
        logger.debug(
          `[Repos/Sessions] getUserId - session.user.id ${JSON.stringify(session.user.id)}`
        );
        return session.user.id;
      }
    } catch (err) {
      logger.error('[Repos/Sessions] getUserId %s', err.toString());
      throw err.toString();
    }
  }
}
