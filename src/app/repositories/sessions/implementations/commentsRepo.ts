/* eslint-disable @typescript-eslint/no-explicit-any */
import { Comment } from '@/app/domains/comment';
import { User } from '@/app/domains/auth/user';
import { Session } from '@/app/domains/sessions/session';
import { CommentMap } from '@/app/mappers/sessions/commentMap';

import { ICommentsRepo } from '../ICommentsRepo';

import logger from '@/shared/utils/logger';

export class CommentsRepo implements ICommentsRepo {
  private repo: any;

  constructor(repo: any) {
    this.repo = repo;
  }

  public async getCommentById(commentId: string): Promise<Comment> {
    try {
      // const rawComment = await this.repo.findOneBy({ id: commentId });
      const rawComment = await this.repo.findOne({
        where: { id: commentId },
        relations: ['session', 'user'],
      });

      return rawComment ? CommentMap.toDomain(rawComment) : null;
    } catch (err) {
      logger.error('[Repos/CommentsRepo] getCommentById Error: %s', err.toString());
      throw new Error(err.toString());
    }
  }

  public async add(
    commentDomain: Comment,
    userDomain: User,
    sessionDomain: Session
  ): Promise<void> {
    const rawComment = await CommentMap.toPersistence(commentDomain, userDomain, sessionDomain);
    logger.debug(`[Repos/CommentsRepo]  add - rawComment ${JSON.stringify(rawComment)}`);

    try {
      const comment = this.repo.create(rawComment);
      await this.repo.save(comment);
    } catch (err) {
      logger.error('[Repo/CommentsRepo] add - Error: %s', err.toString());
      throw new Error(err.toString());
    }
  }

  public async getCommentsBySessionId(sessionId: string): Promise<Comment[]> {
    try {
      const rawComments = await this.repo.find({
        where: {
          session: { id: sessionId },
        },
        relations: ['user', 'session'],
      });
      logger.debug(
        `[Repos/Comments] getCommentsBySessionId - rawComments ${JSON.stringify(rawComments)}`
      );
      if (!rawComments) {
        logger.error(
          `[Repos/Comments] getCommentsBySessionId - Comment(s) not found for session_id: ${sessionId}`
        );
        throw new Error(`Comment(s) not found for session_id: ${sessionId}`);
      }
      //const comments = rawComments.map((rawComment) => CommentMap.toDomain(rawComment));
      const comments = [];
      for (let index = 0; index < rawComments.length; index++) {
        const rawComment = rawComments[index];
        logger.debug(
          `[Repos/Comments] getCommentsBySessionId - rawComment ${JSON.stringify(rawComment)}`
        );
        const comment = CommentMap.toDomain(rawComment);
        comments.push(comment);
      }
      return comments;
    } catch (err) {
      logger.error('[Repos/Comments] getCommentsBySessionId %s', err.toString());
      throw err.toString();
    }
  }

  public async countCommentsBySessionId(sessionId: string): Promise<number> {
    try {
      const nb_comments = await this.repo.count({
        where: { session: { id: sessionId } },
      });
      logger.debug(
        `[Repos/Comments] countCommentsBySessionId - nb_comments ${JSON.stringify(nb_comments)}`
      );
      return nb_comments;
    } catch (err) {
      logger.error('[Repos/Comments] countCommentsBySessionId %s', err.toString());
      throw err.toString();
    }
  }

  public async deleteCommentById(commentId: string): Promise<void> {
    try {
      logger.debug(`[Repo/Comments] Delete by ID - commentId ${commentId}`);

      const rawComment = await this.repo.findOne({
        where: { id: commentId },
        relations: ['session', 'user'],
      });

      await this.repo.remove(rawComment);
    } catch (err) {
      logger.error('[Repo/Comments] Delete - Error: %s', err.toString());
      throw new Error(err.toString());
    }
  }
}
