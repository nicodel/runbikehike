import { getTimelineController } from '@/app/useCases/timeline/getTimeline';
import { passport } from '@/shared/infra/auth/passport';
import { Request, Response, Router } from 'express';

const timelineRouter = Router();

timelineRouter.get('/:period', passport.authenticate('session'), (req: Request, res: Response) =>
  getTimelineController.execute(req, res)
);

export { timelineRouter };
