/* eslint-disable @typescript-eslint/no-explicit-any */
import { Router, Request, Response } from 'express';
import { passport } from '@/shared/infra/auth/passport';
// import multer from 'multer';

import { getProfileController } from '@/app/useCases/users/getProfile';
import { updateProfileController } from '@/app/useCases/users/updateProfile';
import { getNameAvatarController } from '@/app/useCases/users/getNameAvatar';
// import { updateProfileController } from '@/app/useCases/users/updateProfile';

const usersRouter = Router();
// const upload = multer({ dest: 'uploads/' });
usersRouter.get('/profile', passport.authenticate('session'), (req: Request, res: Response) =>
  getProfileController.execute(req, res)
);

usersRouter.put('/profile', passport.authenticate('session'), (req: Request, res: Response) =>
  updateProfileController.execute(req, res)
);

usersRouter.get(
  '/name-avatar/:user',
  passport.authenticate('session'),
  (req: Request, res: Response) => getNameAvatarController.execute(req, res)
);

// usersRouter.put('/profile', [Auth.authenticate(), upload.single('avatar')], (req, res) =>
//   updateProfileController.execute(req, res)
// );

export { usersRouter };
