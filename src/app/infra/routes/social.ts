import express, { Request, Response } from 'express';
import { passport } from '@/shared/infra/auth/passport';
import logger from '@/shared/utils/logger';
import { searchUsersController } from '@/app/useCases/social/searchUsers';

const socialRouter = express.Router();

socialRouter.get('/users', passport.authenticate('session'), (req: Request, res: Response) => {
  logger.debug('[Router/Social] GET search_users - req.query: %s', req.query);
  searchUsersController.execute(req, res);
});

export { socialRouter };
