import { Request, Response, Router } from 'express';
import multer from 'multer';

import { passport } from '@/shared/infra/auth/passport';

import { addASessionController } from '@/app/useCases/sessions/addASession';
import { importGPSFileController } from '@/app/useCases/sessions/importGPSFile';
import { deleteSessionController } from '@/app/useCases/sessions/deleteSession';
import { editASessionController } from '@/app/useCases/sessions/editASession';
import { addACommentControler } from '@/app/useCases/sessions/addAComment';
import { getSessionChartsDataController } from '@/app/useCases/sessions/getSessionChartsData';
import { getSessionMapDataController } from '@/app/useCases/sessions/getSessionMapData';
import { getSessionMetadataController } from '@/app/useCases/sessions/getSessionMetadata';
import { getSessionImagescontroller } from '@/app/useCases/sessions/getSessionImages';
import { getSessionCommentsController } from '@/app/useCases/sessions/getSessionComments';
import { deleteACommentController } from '@/app/useCases/sessions/deleteAComment';

const sessionsRouter = Router();

const upload = multer({ limits: { fieldSize: 10 * 1024 * 1024 } });

sessionsRouter.post('/session', passport.authenticate('session'), (req: Request, res: Response) =>
  addASessionController.execute(req, res)
);

sessionsRouter.post(
  '/gps_file',
  [passport.authenticate('session'), upload.none()],
  (req: Request, res: Response) => importGPSFileController.execute(req, res)
);

sessionsRouter.get(
  '/session/:session_id',
  passport.authenticate('session'),
  (req: Request, res: Response) => getSessionMetadataController.execute(req, res)
);

sessionsRouter.get(
  '/session/:session_id/charts_data',
  passport.authenticate('session'),
  (req: Request, res: Response) => getSessionChartsDataController.execute(req, res)
);

sessionsRouter.get(
  '/session/:session_id/map_data',
  passport.authenticate('session'),
  (req: Request, res: Response) => getSessionMapDataController.execute(req, res)
);

sessionsRouter.get(
  '/session/:session_id/images',
  passport.authenticate('session'),
  (req: Request, res: Response) => getSessionImagescontroller.execute(req, res)
);

sessionsRouter.get(
  '/session/:session_id/comments',
  passport.authenticate('session'),
  (req: Request, res: Response) => getSessionCommentsController.execute(req, res)
);

sessionsRouter.delete(
  '/session/:session_id',
  passport.authenticate('session'),
  (req: Request, res: Response) => deleteSessionController.execute(req, res)
);

sessionsRouter.put('/session', passport.authenticate('session'), (req: Request, res: Response) =>
  editASessionController.execute(req, res)
);

sessionsRouter.post('/comment', passport.authenticate('session'), (req: Request, res: Response) =>
  addACommentControler.execute(req, res)
);

sessionsRouter.delete(
  '/comment/:comment_id',
  passport.authenticate('session'),
  (req: Request, res: Response) => deleteACommentController.execute(req, res)
);

export { sessionsRouter };
