import express, { Request, Response } from 'express';

import { passport } from '@/shared/infra/auth/passport';

import { getAllUsersController } from '@/app/useCases/admin/getAllUsers';
import { disableUserController } from '@/app/useCases/admin/disableUser';
import { enableUserController } from '@/app/useCases/admin/enableUser';

const adminRouter = express.Router();

adminRouter.get('/all_users', passport.authenticate('session'), (req: Request, res: Response) =>
  getAllUsersController.execute(req, res)
);

adminRouter.put(
  '/disable_user/:user',
  passport.authenticate('session'),
  (req: Request, res: Response) => disableUserController.execute(req, res)
);

adminRouter.put(
  '/enable_user/:user',
  passport.authenticate('session'),
  (req: Request, res: Response) => enableUserController.execute(req, res)
);

export { adminRouter };
