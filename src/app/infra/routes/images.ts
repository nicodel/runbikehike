import { Request, Response, Router } from 'express';

import { passport } from '@/shared/infra/auth/passport';

import { getImageController } from '@/app/useCases/images/getImage';

const imagesRouter = Router();

imagesRouter.get(
  '/image/:image_id',
  passport.authenticate('session'),
  (req: Request, res: Response) => getImageController.execute(req, res)
);

export { imagesRouter };
