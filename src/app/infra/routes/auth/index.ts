/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';
import express from 'express';

import { passport } from '@/shared/infra/auth/passport';

import { registerController } from '@/app/useCases/users/register';

import logger from '@/shared/utils/logger';

const authRouter = express.Router();

authRouter.post(
  '/signin',
  passport.authenticate('local', { failWithError: true }),
  async (req: Request | any, res: Response, next) => {
    logger.debug('[routes/auth] POST /signin - req: %s', req);
    return res.status(200).json({ message: 'Authentication successful' });
  },
  (err, req, res, next) => {
    logger.debug(`[routes/auth] POST /signin - err: ${JSON.stringify(err)}`);

    switch (err.statusCode) {
      case 401:
        return res.status(401).json({ message: 'Authentication failed' });

      case 403:
        return res.status(403).json({ message: 'User is disabled.' });

      default:
        return res.status(401).json({ message: 'Authentication failed' });
    }
  }
);

authRouter.post('/signout', (req: Request | any, res: Response, done) => {
  logger.debug('[routes/auth] POST /signout - req.session: %s', req.session);
  req.logout((err: Error) => {
    if (err) {
      logger.error('[routes/auth] POST /signout req.logout Error: %s', err);
      return done(err);
    }
  });
  res.clearCookie('connect.sid');
  res.sendStatus(200);
});

authRouter.post('/register', (req: Request, res: Response) => {
  logger.debug('[routes/auth] POST /register - req.body: %s', req.body);
  registerController.execute(req, res);
});

export { authRouter };
