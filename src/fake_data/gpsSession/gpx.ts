export const fakeSessionGPX = `<?xml version="1.0" encoding="UTF-8"?>
<gpx creator="Garmin Connect" version="1.1"
  xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/11.xsd"
  xmlns:ns3="http://www.garmin.com/xmlschemas/TrackPointExtension/v1"
  xmlns="http://www.topografix.com/GPX/1/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns2="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
  <metadata>
    <link href="connect.garmin.com">
      <text>Garmin Connect</text>
    </link>
    <time>2023-07-30T12:04:12.000Z</time>
  </metadata>
  <trk>
    <name>Carrera Solidaria 5 km , Las Tablas - &#x1f3e5;&#x1fa7a;&#x1f3c3;</name>
    <desc>Je pensais que ma première course serait Paris-Versailles, mais le hasard fait qu’il y a une course à but caritatif pour l’hôpital de Las Tablas, Panamà. Donc j’ai chaussé mes chaussures pour ce 5km sous le soleil des tropiques, avec la belle famille.

J-56: Paris-Versailles
J-71 : 20km de Paris
</desc>
    <type>running</type>
    <trkseg>
      <trkpt lat="7.78693682514131069183349609375" lon="-80.2727569453418254852294921875">
        <ele>38</ele>
        <time>2023-07-30T12:04:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>92</ns3:hr>
            <ns3:cad>0</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78692341409623622894287109375" lon="-80.27275417931377887725830078125">
        <ele>38.200000762939453125</ele>
        <time>2023-07-30T12:04:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>93</ns3:hr>
            <ns3:cad>0</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7869045548141002655029296875" lon="-80.27275367639958858489990234375">
        <ele>38.200000762939453125</ele>
        <time>2023-07-30T12:04:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>94</ns3:hr>
            <ns3:cad>0</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7868753857910633087158203125" lon="-80.27275585569441318511962890625">
        <ele>38.40000152587890625</ele>
        <time>2023-07-30T12:04:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>94</ns3:hr>
            <ns3:cad>0</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7868475578725337982177734375" lon="-80.2727584540843963623046875">
        <ele>38.59999847412109375</ele>
        <time>2023-07-30T12:04:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>94</ns3:hr>
            <ns3:cad>83</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786817215383052825927734375" lon="-80.27275971136987209320068359375">
        <ele>38.59999847412109375</ele>
        <time>2023-07-30T12:04:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>94</ns3:hr>
            <ns3:cad>83</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78678963892161846160888671875" lon="-80.27276222594082355499267578125">
        <ele>38.59999847412109375</ele>
        <time>2023-07-30T12:04:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>94</ns3:hr>
            <ns3:cad>83</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78675493784248828887939453125" lon="-80.272765494883060455322265625">
        <ele>38.799999237060546875</ele>
        <time>2023-07-30T12:04:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>97</ns3:hr>
            <ns3:cad>83</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78672434389591217041015625" lon="-80.27276859618723392486572265625">
        <ele>38.799999237060546875</ele>
        <time>2023-07-30T12:04:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>101</ns3:hr>
            <ns3:cad>84</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7866889722645282745361328125" lon="-80.27276960201561450958251953125">
        <ele>39</ele>
        <time>2023-07-30T12:04:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>104</ns3:hr>
            <ns3:cad>85</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78665770776569843292236328125" lon="-80.2727696858346462249755859375">
        <ele>39</ele>
        <time>2023-07-30T12:04:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>106</ns3:hr>
            <ns3:cad>86</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78662719763815402984619140625" lon="-80.272773206233978271484375">
        <ele>39.200000762939453125</ele>
        <time>2023-07-30T12:04:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>109</ns3:hr>
            <ns3:cad>86</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7865981124341487884521484375" lon="-80.2727760560810565948486328125">
        <ele>39.200000762939453125</ele>
        <time>2023-07-30T12:04:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>111</ns3:hr>
            <ns3:cad>87</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78656885959208011627197265625" lon="-80.272783935070037841796875">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:04:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>114</ns3:hr>
            <ns3:cad>87</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7865411154925823211669921875" lon="-80.27279692701995372772216796875">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:04:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>116</ns3:hr>
            <ns3:cad>88</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78652125038206577301025390625" lon="-80.27281637303531169891357421875">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:04:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>118</ns3:hr>
            <ns3:cad>88</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78651102446019649505615234375" lon="-80.27284353040158748626708984375">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:04:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>120</ns3:hr>
            <ns3:cad>88</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786502726376056671142578125" lon="-80.2728702686727046966552734375">
        <ele>39.799999237060546875</ele>
        <time>2023-07-30T12:04:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>121</ns3:hr>
            <ns3:cad>88</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7865136228501796722412109375" lon="-80.27290555648505687713623046875">
        <ele>40</ele>
        <time>2023-07-30T12:04:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>121</ns3:hr>
            <ns3:cad>87</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78651638887822628021240234375" lon="-80.27293405495584011077880859375">
        <ele>40</ele>
        <time>2023-07-30T12:04:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>122</ns3:hr>
            <ns3:cad>87</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786518149077892303466796875" lon="-80.27296842075884342193603515625">
        <ele>40</ele>
        <time>2023-07-30T12:04:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>123</ns3:hr>
            <ns3:cad>87</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78651974163949489593505859375" lon="-80.272999517619609832763671875">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:04:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>124</ns3:hr>
            <ns3:cad>88</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78652225621044635772705078125" lon="-80.27303145267069339752197265625">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:04:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>124</ns3:hr>
            <ns3:cad>88</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78652611188590526580810546875" lon="-80.273059196770191192626953125">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:04:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>125</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78653013519942760467529296875" lon="-80.27309163473546504974365234375">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:04:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>123</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786532230675220489501953125" lon="-80.27312147431075572967529296875">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:04:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>122</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7865357510745525360107421875" lon="-80.27315340936183929443359375">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:04:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>121</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78653834946453571319580078125" lon="-80.2731837518513202667236328125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:04:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>121</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78654086403548717498779296875" lon="-80.27321686036884784698486328125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:04:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>121</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78654396533966064453125" lon="-80.2732471190392971038818359375">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:04:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>122</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786547653377056121826171875" lon="-80.27328073047101497650146484375">
        <ele>41</ele>
        <time>2023-07-30T12:04:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>122</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7865494973957538604736328125" lon="-80.2733123302459716796875">
        <ele>41</ele>
        <time>2023-07-30T12:04:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>123</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78655276633799076080322265625" lon="-80.2733470313251018524169921875">
        <ele>41</ele>
        <time>2023-07-30T12:04:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>124</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78655662201344966888427734375" lon="-80.2733762003481388092041015625">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:04:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>125</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78656098060309886932373046875" lon="-80.2734088897705078125">
        <ele>41.40000152587890625</ele>
        <time>2023-07-30T12:04:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>125</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786570452153682708740234375" lon="-80.27343864552676677703857421875">
        <ele>41.40000152587890625</ele>
        <time>2023-07-30T12:04:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>125</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78659031726419925689697265625" lon="-80.27346597053110599517822265625">
        <ele>41.59999847412109375</ele>
        <time>2023-07-30T12:04:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>126</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78661303222179412841796875" lon="-80.27348466217517852783203125">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:04:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>125</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78664178214967250823974609375" lon="-80.273505784571170806884765625">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:04:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>126</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7866681851446628570556640625" lon="-80.273523218929767608642578125">
        <ele>42</ele>
        <time>2023-07-30T12:04:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>126</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7867017127573490142822265625" lon="-80.27353973127901554107666015625">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:04:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>126</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7867322228848934173583984375" lon="-80.2735572494566440582275390625">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:04:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>126</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78676181100308895111083984375" lon="-80.27357728220522403717041015625">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:04:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>127</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78679047711193561553955078125" lon="-80.27359723113477230072021484375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:04:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>128</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7868200652301311492919921875" lon="-80.27361500076949596405029296875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:04:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>128</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786851413547992706298828125" lon="-80.2736310102045536041259765625">
        <ele>43</ele>
        <time>2023-07-30T12:04:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>129</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78687999583780765533447265625" lon="-80.27364559471607208251953125">
        <ele>43</ele>
        <time>2023-07-30T12:04:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>130</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78691603802144527435302734375" lon="-80.27364718727767467498779296875">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:04:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>130</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786951325833797454833984375" lon="-80.2736507914960384368896484375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:05:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>130</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78698409907519817352294921875" lon="-80.273654647171497344970703125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:05:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>130</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78701628558337688446044921875" lon="-80.2736538089811801910400390625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:05:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>130</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78704889118671417236328125" lon="-80.27365305460989475250244140625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:05:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>131</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78708241879940032958984375" lon="-80.27364970184862613677978515625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:05:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>132</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.787115611135959625244140625" lon="-80.2736474387347698211669921875">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:05:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>132</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7871436066925525665283203125" lon="-80.2736420743167400360107421875">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:05:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>132</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7871788106858730316162109375" lon="-80.2736403979361057281494140625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:05:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>132</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78720890171825885772705078125" lon="-80.27363360859453678131103515625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>132</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.787242345511913299560546875" lon="-80.273627825081348419189453125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>132</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.787277214229106903076171875" lon="-80.27362204156816005706787109375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>132</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78731132857501506805419921875" lon="-80.273620784282684326171875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>132</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78733949176967144012451171875" lon="-80.273614414036273956298828125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>132</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7873746119439601898193359375" lon="-80.27360963635146617889404296875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>132</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7874044515192508697509765625" lon="-80.27360536158084869384765625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>133</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78744074515998363494873046875" lon="-80.27360443957149982452392578125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>133</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78747293166816234588623046875" lon="-80.27360326610505580902099609375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>134</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7875087223947048187255859375" lon="-80.27360158972442150115966796875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>134</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.787539064884185791015625" lon="-80.27359999716281890869140625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>135</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7875699102878570556640625" lon="-80.2735971473157405853271484375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>135</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78759815730154514312744140625" lon="-80.27359622530639171600341796875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>136</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78763017617166042327880859375" lon="-80.27359689585864543914794921875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>136</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.787662111222743988037109375" lon="-80.27359404601156711578369140625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>136</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78769396245479583740234375" lon="-80.27359136380255222320556640625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78772556222975254058837890625" lon="-80.2735899388790130615234375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78775758109986782073974609375" lon="-80.2735894359648227691650390625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.787786833941936492919921875" lon="-80.27358583174645900726318359375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78781868517398834228515625" lon="-80.2735834009945392608642578125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7878496982157230377197265625" lon="-80.2735827304422855377197265625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7878815494477748870849609375" lon="-80.27358465828001499176025390625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:05:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.787914238870143890380859375" lon="-80.273584239184856414794921875">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:05:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78794583864510059356689453125" lon="-80.2735818922519683837890625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:05:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78797827661037445068359375" lon="-80.273580215871334075927734375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:05:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78800887055695056915283203125" lon="-80.27357543818652629852294921875">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:05:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7880411408841609954833984375" lon="-80.27357258833944797515869140625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:05:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7880713157355785369873046875" lon="-80.2735686488449573516845703125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:05:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.788103334605693817138671875" lon="-80.27356772683560848236083984375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:05:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78813250362873077392578125" lon="-80.2735622785985469818115234375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:05:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78816577978432178497314453125" lon="-80.2735592611134052276611328125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:05:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.788196541368961334228515625" lon="-80.27355817146599292755126953125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:05:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78822680003941059112548828125" lon="-80.27355699799954891204833984375">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:05:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78825848363339900970458984375" lon="-80.27355481870472431182861328125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:05:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7882895804941654205322265625" lon="-80.273551382124423980712890625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:05:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.788321264088153839111328125" lon="-80.27354794554412364959716796875">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:05:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>140</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78835185803472995758056640625" lon="-80.2735443413257598876953125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:05:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>140</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78838220052421092987060546875" lon="-80.2735418267548084259033203125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:05:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>140</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7884129621088504791259765625" lon="-80.27354056946933269500732421875">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:05:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78844590298831462860107421875" lon="-80.27353939600288867950439453125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:05:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78847674839198589324951171875" lon="-80.2735378034412860870361328125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:05:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78850407339632511138916015625" lon="-80.2735357917845249176025390625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:05:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7885346673429012298583984375" lon="-80.2735351212322711944580078125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:05:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7885663509368896484375" lon="-80.273534953594207763671875">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:05:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78859518468379974365234375" lon="-80.2735334448516368865966796875">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:05:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78862762264907360076904296875" lon="-80.27352900244295597076416015625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:05:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78865762986242771148681640625" lon="-80.27352095581591129302978515625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:05:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78868763707578182220458984375" lon="-80.27351726777851581573486328125">
        <ele>44</ele>
        <time>2023-07-30T12:05:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7887177281081676483154296875" lon="-80.27351274155080318450927734375">
        <ele>44</ele>
        <time>2023-07-30T12:05:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.788747735321521759033203125" lon="-80.273509137332439422607421875">
        <ele>44</ele>
        <time>2023-07-30T12:05:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78877665288746356964111328125" lon="-80.2735039405524730682373046875">
        <ele>44</ele>
        <time>2023-07-30T12:05:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7888065762817859649658203125" lon="-80.2735012583434581756591796875">
        <ele>44</ele>
        <time>2023-07-30T12:05:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78883666731417179107666015625" lon="-80.27349983341991901397705078125">
        <ele>44</ele>
        <time>2023-07-30T12:06:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7888652496039867401123046875" lon="-80.27349798940122127532958984375">
        <ele>44</ele>
        <time>2023-07-30T12:06:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78889483772218227386474609375" lon="-80.27349983341991901397705078125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:06:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7889242582023143768310546875" lon="-80.27349715121090412139892578125">
        <ele>44</ele>
        <time>2023-07-30T12:06:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7889547683298587799072265625" lon="-80.27349664829671382904052734375">
        <ele>44</ele>
        <time>2023-07-30T12:06:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7889861166477203369140625" lon="-80.2734960615634918212890625">
        <ele>44</ele>
        <time>2023-07-30T12:06:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78901805169880390167236328125" lon="-80.27349312789738178253173828125">
        <ele>44</ele>
        <time>2023-07-30T12:06:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7890516631305217742919921875" lon="-80.27349128387868404388427734375">
        <ele>44</ele>
        <time>2023-07-30T12:06:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7890831790864467620849609375" lon="-80.27348826639354228973388671875">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:06:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7891167066991329193115234375" lon="-80.27348642237484455108642578125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:06:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7891448698937892913818359375" lon="-80.27348374016582965850830078125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:06:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78917378745973110198974609375" lon="-80.27348172850906848907470703125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:06:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>137</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78920262120664119720458984375" lon="-80.27348105795681476593017578125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:06:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78923061676323413848876953125" lon="-80.27348021976649761199951171875">
        <ele>44</ele>
        <time>2023-07-30T12:06:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78926146216690540313720703125" lon="-80.273477621376514434814453125">
        <ele>44</ele>
        <time>2023-07-30T12:06:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7892918884754180908203125" lon="-80.27347485534846782684326171875">
        <ele>44</ele>
        <time>2023-07-30T12:06:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78932206332683563232421875" lon="-80.273471586406230926513671875">
        <ele>44</ele>
        <time>2023-07-30T12:06:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78935399837791919708251953125" lon="-80.273469574749469757080078125">
        <ele>44</ele>
        <time>2023-07-30T12:06:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78938501141965389251708984375" lon="-80.27346781454980373382568359375">
        <ele>44</ele>
        <time>2023-07-30T12:06:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.789417617022991180419921875" lon="-80.27346764691174030303955078125">
        <ele>44</ele>
        <time>2023-07-30T12:06:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7894472889602184295654296875" lon="-80.27346588671207427978515625">
        <ele>44</ele>
        <time>2023-07-30T12:06:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78947922401130199432373046875" lon="-80.2734643779695034027099609375">
        <ele>44</ele>
        <time>2023-07-30T12:06:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78950839303433895111083984375" lon="-80.27345624752342700958251953125">
        <ele>44</ele>
        <time>2023-07-30T12:06:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>138</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78954259119927883148193359375" lon="-80.2734566666185855865478515625">
        <ele>44</ele>
        <time>2023-07-30T12:06:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7895716764032840728759765625" lon="-80.27345289476215839385986328125">
        <ele>44</ele>
        <time>2023-07-30T12:06:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78960411436855792999267578125" lon="-80.273450128734111785888671875">
        <ele>44</ele>
        <time>2023-07-30T12:06:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78963630087673664093017578125" lon="-80.27344577014446258544921875">
        <ele>44</ele>
        <time>2023-07-30T12:06:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78967100195586681365966796875" lon="-80.2734425850212574005126953125">
        <ele>44</ele>
        <time>2023-07-30T12:06:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.789701931178569793701171875" lon="-80.2734388969838619232177734375">
        <ele>44</ele>
        <time>2023-07-30T12:06:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.789733111858367919921875" lon="-80.2734358794987201690673828125">
        <ele>44</ele>
        <time>2023-07-30T12:06:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>139</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7897658012807369232177734375" lon="-80.27343395166099071502685546875">
        <ele>44</ele>
        <time>2023-07-30T12:06:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>140</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78979890979826450347900390625" lon="-80.27343613095581531524658203125">
        <ele>44</ele>
        <time>2023-07-30T12:06:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>140</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7898283302783966064453125" lon="-80.27343462221324443817138671875">
        <ele>44</ele>
        <time>2023-07-30T12:06:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>140</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7898606844246387481689453125" lon="-80.27343478985130786895751953125">
        <ele>44</ele>
        <time>2023-07-30T12:06:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>140</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78988574631512165069580078125" lon="-80.2734328620135784149169921875">
        <ele>44</ele>
        <time>2023-07-30T12:06:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>140</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7899163402616977691650390625" lon="-80.2734318561851978302001953125">
        <ele>44</ele>
        <time>2023-07-30T12:06:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7899461798369884490966796875" lon="-80.27343009598553180694580078125">
        <ele>44</ele>
        <time>2023-07-30T12:06:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.789974175393581390380859375" lon="-80.27342640794813632965087890625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:06:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79000326059758663177490234375" lon="-80.27342322282493114471435546875">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:06:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79003460891544818878173828125" lon="-80.27342339046299457550048828125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:06:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79006495140492916107177734375" lon="-80.27342271991074085235595703125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:06:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79009814374148845672607421875" lon="-80.273422636091709136962890625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:06:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79012697748839855194091796875" lon="-80.27342305518686771392822265625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:06:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79016025364398956298828125" lon="-80.2734263241291046142578125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:06:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79019352979958057403564453125" lon="-80.27342431247234344482421875">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:06:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>142</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.790226638317108154296875" lon="-80.27342624031007289886474609375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:06:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>143</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79025815427303314208984375" lon="-80.27342556975781917572021484375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:06:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>143</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79028908349573612213134765625" lon="-80.27342565357685089111328125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:06:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>143</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79032093472778797149658203125" lon="-80.27342565357685089111328125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:06:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79034876264631748199462890625" lon="-80.27342422865331172943115234375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:06:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79037726111710071563720703125" lon="-80.27342204935848712921142578125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:06:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79040710069239139556884765625" lon="-80.2734182775020599365234375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:06:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7904390357434749603271484375" lon="-80.27341769076883792877197265625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:06:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79046870768070220947265625" lon="-80.27341852895915508270263671875">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:06:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79049661941826343536376953125" lon="-80.27341601438820362091064453125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:06:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79052629135549068450927734375" lon="-80.2734164334833621978759765625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:06:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79055546037852764129638671875" lon="-80.27341383509337902069091796875">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:06:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79058898799121379852294921875" lon="-80.273407883942127227783203125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:06:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.790615558624267578125" lon="-80.27340729720890522003173828125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:06:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79064380563795566558837890625" lon="-80.27340562082827091217041015625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:06:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7906737290322780609130859375" lon="-80.2734033577144145965576171875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:07:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79070231132209300994873046875" lon="-80.27340260334312915802001953125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:07:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79073290526866912841796875" lon="-80.27340126223862171173095703125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:07:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>145</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79076400212943553924560546875" lon="-80.2733996696770191192626953125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:07:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>145</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.790790908038616180419921875" lon="-80.27339757420122623443603515625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:07:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>145</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79081831686198711395263671875" lon="-80.2733976580202579498291015625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:07:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79084564186632633209228515625" lon="-80.27339480817317962646484375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:07:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>143</ns3:hr>
            <ns3:cad>88</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.790874727070331573486328125" lon="-80.27339288033545017242431640625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:07:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>142</ns3:hr>
            <ns3:cad>88</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.790904231369495391845703125" lon="-80.2733892761170864105224609375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:07:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>142</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79093717224895954132080078125" lon="-80.27338919229805469512939453125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:07:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79096357524394989013671875" lon="-80.2733889408409595489501953125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:07:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79099634848535060882568359375" lon="-80.27338768355548381805419921875">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:07:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>88</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7910247631371021270751953125" lon="-80.2733842469751834869384765625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:07:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>88</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7910562790930271148681640625" lon="-80.2733802236616611480712890625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:07:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7910851128399372100830078125" lon="-80.27337611652910709381103515625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:07:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79111838899552822113037109375" lon="-80.27337661944329738616943359375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:07:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>89</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79114856384694576263427734375" lon="-80.27337678708136081695556640625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:07:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79118125326931476593017578125" lon="-80.27337712235748767852783203125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:07:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7912115119397640228271484375" lon="-80.27337393723428249359130859375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:07:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79124428518116474151611328125" lon="-80.27337133884429931640625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:07:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79127395711839199066162109375" lon="-80.2733688242733478546142578125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:07:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7913063950836658477783203125" lon="-80.27336907573044300079345703125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:07:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79133765958249568939208984375" lon="-80.27336471714079380035400390625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:07:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79136951081454753875732421875" lon="-80.27335993945598602294921875">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:07:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79139867983758449554443359375" lon="-80.27335465885698795318603515625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:07:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7914287708699703216552734375" lon="-80.2733493782579898834228515625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:07:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>141</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79146070592105388641357421875" lon="-80.27334426529705524444580078125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:07:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>142</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79149205423891544342041015625" lon="-80.27334040962159633636474609375">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:07:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>142</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79152357019484043121337890625" lon="-80.27333672158420085906982421875">
        <ele>44</ele>
        <time>2023-07-30T12:07:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>142</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79155357740819454193115234375" lon="-80.27333219535648822784423828125">
        <ele>44</ele>
        <time>2023-07-30T12:07:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>142</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7915856800973415374755859375" lon="-80.27332783676683902740478515625">
        <ele>44</ele>
        <time>2023-07-30T12:07:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>142</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7916171960532665252685546875" lon="-80.273324064910411834716796875">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:07:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>142</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.791651897132396697998046875" lon="-80.27332196943461894989013671875">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:07:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>142</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79168198816478252410888671875" lon="-80.2733188681304454803466796875">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:07:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>142</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7917147614061832427978515625" lon="-80.27331694029271602630615234375">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:07:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>142</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79174334369599819183349609375" lon="-80.27331626974046230316162109375">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:07:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>143</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7917747758328914642333984375" lon="-80.2733168564736843109130859375">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:07:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>143</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79180604033172130584716796875" lon="-80.27331568300724029541015625">
        <ele>45</ele>
        <time>2023-07-30T12:07:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>143</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7918384782969951629638671875" lon="-80.273313336074352264404296875">
        <ele>45</ele>
        <time>2023-07-30T12:07:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>143</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7918694913387298583984375" lon="-80.2733114920556545257568359375">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:07:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>143</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7919015102088451385498046875" lon="-80.27330939657986164093017578125">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:07:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>143</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7919387258589267730712890625" lon="-80.27330872602760791778564453125">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:07:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79197233729064464569091796875" lon="-80.2733029425144195556640625">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:07:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79200754128396511077880859375" lon="-80.27329690754413604736328125">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:07:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>145</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79204190708696842193603515625" lon="-80.273290537297725677490234375">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:07:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>145</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7920771948993206024169921875" lon="-80.27328626252710819244384765625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:07:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7921087108552455902099609375" lon="-80.27328073047101497650146484375">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:07:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7921457588672637939453125" lon="-80.27327922172844409942626953125">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:07:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7921781130135059356689453125" lon="-80.273277796804904937744140625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:07:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7922143228352069854736328125" lon="-80.273279808461666107177734375">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:07:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79224826954305171966552734375" lon="-80.27328081429004669189453125">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:07:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79228112660348415374755859375" lon="-80.27328358031809329986572265625">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:07:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79231431894004344940185546875" lon="-80.273283831775188446044921875">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:07:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.792345918715000152587890625" lon="-80.2732813172042369842529296875">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:07:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7923762612044811248779296875" lon="-80.273275785148143768310546875">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:07:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79240551404654979705810546875" lon="-80.27326849289238452911376953125">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:07:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.792437113821506500244140625" lon="-80.27326279319822788238525390625">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:07:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>147</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79246787540614604949951171875" lon="-80.27325608767569065093994140625">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:07:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7925019897520542144775390625" lon="-80.27325055561959743499755859375">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:07:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7925341762602329254150390625" lon="-80.273245275020599365234375">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:07:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7925690449774265289306640625" lon="-80.2732417546212673187255859375">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7926002256572246551513671875" lon="-80.2732372283935546875">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79263769276440143585205078125" lon="-80.273234210908412933349609375">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>145</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7926687896251678466796875" lon="-80.27323110960423946380615234375">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>145</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7927058376371860504150390625" lon="-80.27323245070874691009521484375">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>145</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79273794032633304595947265625" lon="-80.2732293494045734405517578125">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7927749045193195343017578125" lon="-80.273227505385875701904296875">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79280717484652996063232421875" lon="-80.27322406880557537078857421875">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7928422950208187103271484375" lon="-80.27322021313011646270751953125">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79287707991898059844970703125" lon="-80.273217447102069854736328125">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.792910523712635040283203125" lon="-80.27321602217853069305419921875">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>143</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79294296167790889739990234375" lon="-80.27321367524564266204833984375">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>143</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79297523200511932373046875" lon="-80.27321082539856433868408203125">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>143</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79301001690328121185302734375" lon="-80.27320881374180316925048828125">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79304253868758678436279296875" lon="-80.2732062153518199920654296875">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:08:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79307807795703411102294921875" lon="-80.27320629917085170745849609375">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:08:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.793110348284244537353515625" lon="-80.27320546098053455352783203125">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:08:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>144</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79314664192497730255126953125" lon="-80.27320361696183681488037109375">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:08:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>145</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79317949898540973663330078125" lon="-80.273200683295726776123046875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:08:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>146</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7932144515216350555419921875" lon="-80.27319808490574359893798828125">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:08:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>147</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79324806295335292816162109375" lon="-80.2731958217918872833251953125">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:08:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>147</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79328360222280025482177734375" lon="-80.27319322340190410614013671875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:08:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>148</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79331645928323268890380859375" lon="-80.27319288812577724456787109375">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:08:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>148</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.793348729610443115234375" lon="-80.2731897868216037750244140625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:08:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>148</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79338250868022441864013671875" lon="-80.2731876075267791748046875">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:08:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>148</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79341368936002254486083984375" lon="-80.27318517677485942840576171875">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:08:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>149</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79344738461077213287353515625" lon="-80.2731837518513202667236328125">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:08:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>149</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79348409734666347503662109375" lon="-80.273181907832622528076171875">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:08:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.793518044054508209228515625" lon="-80.27317897416651248931884765625">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:08:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79355433769524097442626953125" lon="-80.2731770463287830352783203125">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:08:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79358736239373683929443359375" lon="-80.27317486703395843505859375">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:08:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7936194650828838348388671875" lon="-80.2731726877391338348388671875">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:08:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.793651483952999114990234375" lon="-80.27316950261592864990234375">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:08:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.793685682117938995361328125" lon="-80.27316958643496036529541015625">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:08:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.793719209730625152587890625" lon="-80.27316623367369174957275390625">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:08:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79375575482845306396484375" lon="-80.27316623367369174957275390625">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:08:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79378919862210750579833984375" lon="-80.27316388674080371856689453125">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:08:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79382423497736454010009765625" lon="-80.27316221036016941070556640625">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:08:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79385508038103580474853515625" lon="-80.273161791265010833740234375">
        <ele>45</ele>
        <time>2023-07-30T12:08:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79388911090791225433349609375" lon="-80.2731607854366302490234375">
        <ele>45</ele>
        <time>2023-07-30T12:08:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79391962103545665740966796875" lon="-80.273159779608249664306640625">
        <ele>45</ele>
        <time>2023-07-30T12:08:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.793952561914920806884765625" lon="-80.2731621265411376953125">
        <ele>45</ele>
        <time>2023-07-30T12:08:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79398298822343349456787109375" lon="-80.27316137216985225677490234375">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:08:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7940144203603267669677734375" lon="-80.2731582708656787872314453125">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:08:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7940506301820278167724609375" lon="-80.273157767951488494873046875">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:08:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79408256523311138153076171875" lon="-80.273157097399234771728515625">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:08:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7941180206835269927978515625" lon="-80.2731602825224399566650390625">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:08:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79415364377200603485107421875" lon="-80.27316053397953510284423828125">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:08:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79419303871691226959228515625" lon="-80.27316288091242313385009765625">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:08:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79422472231090068817138671875" lon="-80.2731621265411376953125">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:08:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7942588366568088531494140625" lon="-80.27316388674080371856689453125">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:08:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7942923642694950103759765625" lon="-80.2731643058359622955322265625">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:08:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7943272329866886138916015625" lon="-80.2731639705598354339599609375">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:08:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.794361598789691925048828125" lon="-80.27316422201693058013916015625">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:08:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79439277946949005126953125" lon="-80.2731626294553279876708984375">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:08:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79442857019603252410888671875" lon="-80.2731639705598354339599609375">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:08:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79446025379002094268798828125" lon="-80.27316170744597911834716796875">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:08:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79449612833559513092041015625" lon="-80.27316413819789886474609375">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:08:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79452630318701267242431640625" lon="-80.27316438965499401092529296875">
        <ele>44</ele>
        <time>2023-07-30T12:08:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7945636026561260223388671875" lon="-80.2731693349778652191162109375">
        <ele>44</ele>
        <time>2023-07-30T12:08:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7945967949926853179931640625" lon="-80.273170173168182373046875">
        <ele>44</ele>
        <time>2023-07-30T12:09:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7946316637098789215087890625" lon="-80.273172855377197265625">
        <ele>44</ele>
        <time>2023-07-30T12:09:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.794666029512882232666015625" lon="-80.27317461557686328887939453125">
        <ele>44</ele>
        <time>2023-07-30T12:09:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79469964094460010528564453125" lon="-80.27317729778587818145751953125">
        <ele>44</ele>
        <time>2023-07-30T12:09:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79473618604242801666259765625" lon="-80.27317947708070278167724609375">
        <ele>44</ele>
        <time>2023-07-30T12:09:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79477088712155818939208984375" lon="-80.27318291366100311279296875">
        <ele>44</ele>
        <time>2023-07-30T12:09:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7948053367435932159423828125" lon="-80.273187942802906036376953125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:09:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79483693651854991912841796875" lon="-80.27319171465933322906494140625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:09:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79487155377864837646484375" lon="-80.27319925837218761444091796875">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:09:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7949030697345733642578125" lon="-80.2732037007808685302734375">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:09:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.794936262071132659912109375" lon="-80.2732139267027378082275390625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:09:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7949655987322330474853515625" lon="-80.27322289533913135528564453125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:09:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79499636031687259674072265625" lon="-80.27323245070874691009521484375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:09:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.795027457177639007568359375" lon="-80.2732417546212673187255859375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:09:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7950641699135303497314453125" lon="-80.27325122617185115814208984375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:09:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79509711079299449920654296875" lon="-80.27325960807502269744873046875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:09:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79512963257730007171630859375" lon="-80.27326798997819423675537109375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:09:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7951605618000030517578125" lon="-80.2732789702713489532470703125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7951925806701183319091796875" lon="-80.2732868492603302001953125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7952249348163604736328125" lon="-80.27329665608704090118408203125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.795253098011016845703125" lon="-80.27330419979989528656005859375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79528436250984668731689453125" lon="-80.27331559918820858001708984375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7953162975609302520751953125" lon="-80.27332414872944355010986328125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79535074718296527862548828125" lon="-80.27333370409905910491943359375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79537983238697052001953125" lon="-80.2733409963548183441162109375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.795415706932544708251953125" lon="-80.2733534015715122222900390625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7954470552504062652587890625" lon="-80.273362286388874053955078125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79547966085374355316162109375" lon="-80.273373015224933624267578125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79551335610449314117431640625" lon="-80.27338399551808834075927734375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79554252512753009796142578125" lon="-80.27339304797351360321044921875">
        <ele>43</ele>
        <time>2023-07-30T12:09:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79557672329246997833251953125" lon="-80.27340427972376346588134765625">
        <ele>43</ele>
        <time>2023-07-30T12:09:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79560689814388751983642578125" lon="-80.27341316454112529754638671875">
        <ele>43</ele>
        <time>2023-07-30T12:09:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7956408448517322540283203125" lon="-80.27342121116816997528076171875">
        <ele>43</ele>
        <time>2023-07-30T12:09:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79567085206508636474609375" lon="-80.273433364927768707275390625">
        <ele>43</ele>
        <time>2023-07-30T12:09:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7957022003829479217529296875" lon="-80.2734425850212574005126953125">
        <ele>43</ele>
        <time>2023-07-30T12:09:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7957327105104923248291015625" lon="-80.273451469838619232177734375">
        <ele>43</ele>
        <time>2023-07-30T12:09:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79576213099062442779541015625" lon="-80.273460857570171356201171875">
        <ele>43</ele>
        <time>2023-07-30T12:09:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79579398222267627716064453125" lon="-80.273472256958484649658203125">
        <ele>43</ele>
        <time>2023-07-30T12:09:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7958239056169986724853515625" lon="-80.27348072268068790435791015625">
        <ele>43</ele>
        <time>2023-07-30T12:09:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7958587743341922760009765625" lon="-80.273492373526096343994140625">
        <ele>43</ele>
        <time>2023-07-30T12:09:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7958902902901172637939453125" lon="-80.27350050397217273712158203125">
        <ele>43</ele>
        <time>2023-07-30T12:09:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7959238179028034210205078125" lon="-80.273510478436946868896484375">
        <ele>43</ele>
        <time>2023-07-30T12:09:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79595256783068180084228515625" lon="-80.2735183574259281158447265625">
        <ele>43</ele>
        <time>2023-07-30T12:09:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79598458670079708099365234375" lon="-80.27352699078619480133056640625">
        <ele>43</ele>
        <time>2023-07-30T12:09:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7960143424570560455322265625" lon="-80.27353503741323947906494140625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79604409821331501007080078125" lon="-80.2735441736876964569091796875">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7960750274360179901123046875" lon="-80.273554734885692596435546875">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79610612429678440093994140625" lon="-80.27356378734111785888671875">
        <ele>43</ele>
        <time>2023-07-30T12:09:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79613981954753398895263671875" lon="-80.27357459999620914459228515625">
        <ele>43</ele>
        <time>2023-07-30T12:09:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.796168737113475799560546875" lon="-80.273581556975841522216796875">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79620134271681308746337890625" lon="-80.273591615259647369384765625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79623210430145263671875" lon="-80.27360058389604091644287109375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7962656319141387939453125" lon="-80.27361097745597362518310546875">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79629580676555633544921875" lon="-80.2736189402639865875244140625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7963288314640522003173828125" lon="-80.27362950146198272705078125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:09:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.796359173953533172607421875" lon="-80.27363930828869342803955078125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:09:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.796389348804950714111328125" lon="-80.27364752255380153656005859375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:09:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79642078094184398651123046875" lon="-80.27365841902792453765869140625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:09:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79645145870745182037353515625" lon="-80.273668058216571807861328125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:09:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7964837290346622467041015625" lon="-80.2736784517765045166015625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:09:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79651449061930179595947265625" lon="-80.27368892915546894073486328125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:10:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79655086807906627655029296875" lon="-80.2737007476389408111572265625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:10:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79658020474016666412353515625" lon="-80.27371105737984180450439453125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7966131456196308135986328125" lon="-80.27372161857783794403076171875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.796644158661365509033203125" lon="-80.27373251505196094512939453125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79667642898857593536376953125" lon="-80.2737406454980373382568359375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7967090345919132232666015625" lon="-80.27375279925763607025146484375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>150</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7967412211000919342041015625" lon="-80.27376218698918819427490234375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7967739105224609375" lon="-80.27377291582524776458740234375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79680198989808559417724609375" lon="-80.27378506958484649658203125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79683350585401058197021484375" lon="-80.27379722334444522857666015625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>151</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79685948975384235382080078125" lon="-80.2738080359995365142822265625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.796892262995243072509765625" lon="-80.27382035739719867706298828125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.796924114227294921875" lon="-80.27383225969970226287841796875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79695286415517330169677734375" lon="-80.27384198270738124847412109375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:10:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7969859726727008819580078125" lon="-80.27385321445763111114501953125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:10:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79701572842895984649658203125" lon="-80.27386193163692951202392578125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:10:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79704825021326541900634765625" lon="-80.2738757617771625518798828125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:10:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79707725159823894500732421875" lon="-80.27388540096580982208251953125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:10:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.797109521925449371337890625" lon="-80.27389646507799625396728515625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:10:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79714162461459636688232421875" lon="-80.2739076130092144012451171875">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:10:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79717481695115566253662109375" lon="-80.27391674928367137908935546875">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:10:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79720800928771495819091796875" lon="-80.2739260531961917877197265625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:10:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79723902232944965362548828125" lon="-80.27393259108066558837890625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:10:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7972741425037384033203125" lon="-80.273942984640598297119140625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:10:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7973054908215999603271484375" lon="-80.27395111508667469024658203125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:10:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79733809642493724822998046875" lon="-80.2739682979881763458251953125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79736877419054508209228515625" lon="-80.273980200290679931640625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79740129597485065460205078125" lon="-80.2739944495260715484619140625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79742904007434844970703125" lon="-80.27400542981922626495361328125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79746013693511486053466796875" lon="-80.27401850558817386627197265625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7974915690720081329345703125" lon="-80.2740303240716457366943359375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:10:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79752383939921855926513671875" lon="-80.2740417234599590301513671875">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:10:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79755652882158756256103515625" lon="-80.2740547992289066314697265625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:10:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79758787713944911956787109375" lon="-80.27406628243625164031982421875">
        <ele>43</ele>
        <time>2023-07-30T12:10:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.797621823847293853759765625" lon="-80.2740789391100406646728515625">
        <ele>43</ele>
        <time>2023-07-30T12:10:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79765074141323566436767578125" lon="-80.27409008704125881195068359375">
        <ele>43</ele>
        <time>2023-07-30T12:10:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7976820059120655059814453125" lon="-80.2741049230098724365234375">
        <ele>43</ele>
        <time>2023-07-30T12:10:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79771176166832447052001953125" lon="-80.27411573566496372222900390625">
        <ele>43</ele>
        <time>2023-07-30T12:10:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79774160124361515045166015625" lon="-80.27412814088165760040283203125">
        <ele>43</ele>
        <time>2023-07-30T12:10:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7977725304663181304931640625" lon="-80.27414180338382720947265625">
        <ele>43</ele>
        <time>2023-07-30T12:10:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7978040464222431182861328125" lon="-80.2741537056863307952880859375">
        <ele>43</ele>
        <time>2023-07-30T12:10:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7978360652923583984375" lon="-80.2741674520075321197509765625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:10:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79786515049636363983154296875" lon="-80.27417977340519428253173828125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:10:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7978982590138912200927734375" lon="-80.2741939388215541839599609375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:10:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79792734421789646148681640625" lon="-80.274205505847930908203125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:10:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79795718379318714141845703125" lon="-80.2742200903594493865966796875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:10:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7979844249784946441650390625" lon="-80.27423157356679439544677734375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:10:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7980149351060390472412109375" lon="-80.27424112893640995025634765625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:10:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79804821126163005828857421875" lon="-80.27425386942923069000244140625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:10:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7980783022940158843994140625" lon="-80.27426426298916339874267578125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:10:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79811208136379718780517578125" lon="-80.2742784284055233001708984375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:10:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79814384877681732177734375" lon="-80.2742908336222171783447265625">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:10:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79817737638950347900390625" lon="-80.27430969290435314178466796875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:10:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79820830561220645904541015625" lon="-80.27432578615844249725341796875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:10:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.798240743577480316162109375" lon="-80.27434053830802440643310546875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:10:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79827066697180271148681640625" lon="-80.2743511833250522613525390625">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:10:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79830335639417171478271484375" lon="-80.27436601929366588592529296875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:10:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79833353124558925628662109375" lon="-80.2743783406913280487060546875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:10:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.798366136848926544189453125" lon="-80.274388901889324188232421875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:10:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79839807190001010894775390625" lon="-80.274402312934398651123046875">
        <ele>42</ele>
        <time>2023-07-30T12:11:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7984293363988399505615234375" lon="-80.27441463433206081390380859375">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:11:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79846244491636753082275390625" lon="-80.27442821301519870758056640625">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:11:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79849119484424591064453125" lon="-80.27443978004157543182373046875">
        <ele>42</ele>
        <time>2023-07-30T12:11:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79852262698113918304443359375" lon="-80.27445428073406219482421875">
        <ele>42</ele>
        <time>2023-07-30T12:11:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79855112545192241668701171875" lon="-80.2744658477604389190673828125">
        <ele>42</ele>
        <time>2023-07-30T12:11:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.798580713570117950439453125" lon="-80.2744795940816402435302734375">
        <ele>42</ele>
        <time>2023-07-30T12:11:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79861147515475749969482421875" lon="-80.27449241839349269866943359375">
        <ele>42</ele>
        <time>2023-07-30T12:11:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79864399693906307220458984375" lon="-80.2745054103434085845947265625">
        <ele>42</ele>
        <time>2023-07-30T12:11:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79867584817111492156982421875" lon="-80.27451924048364162445068359375">
        <ele>42</ele>
        <time>2023-07-30T12:11:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7987045980989933013916015625" lon="-80.2745318971574306488037109375">
        <ele>42</ele>
        <time>2023-07-30T12:11:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79873519204556941986083984375" lon="-80.2745473198592662811279296875">
        <ele>42</ele>
        <time>2023-07-30T12:11:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7987636066973209381103515625" lon="-80.274559557437896728515625">
        <ele>42</ele>
        <time>2023-07-30T12:11:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7987944521009922027587890625" lon="-80.27457422576844692230224609375">
        <ele>42</ele>
        <time>2023-07-30T12:11:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.798824794590473175048828125" lon="-80.27458730153739452362060546875">
        <ele>42</ele>
        <time>2023-07-30T12:11:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.798856310546398162841796875" lon="-80.27460171841084957122802734375">
        <ele>42</ele>
        <time>2023-07-30T12:11:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7988883294165134429931640625" lon="-80.27461663819849491119384765625">
        <ele>42</ele>
        <time>2023-07-30T12:11:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79891825281083583831787109375" lon="-80.2746297977864742279052734375">
        <ele>42</ele>
        <time>2023-07-30T12:11:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79895144514739513397216796875" lon="-80.274644382297992706298828125">
        <ele>42</ele>
        <time>2023-07-30T12:11:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79898061417043209075927734375" lon="-80.27465687133371829986572265625">
        <ele>42</ele>
        <time>2023-07-30T12:11:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79901179485023021697998046875" lon="-80.2746710367500782012939453125">
        <ele>42</ele>
        <time>2023-07-30T12:11:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7990392036736011505126953125" lon="-80.27468134649097919464111328125">
        <ele>42</ele>
        <time>2023-07-30T12:11:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79907164163887500762939453125" lon="-80.2746958471834659576416015625">
        <ele>42</ele>
        <time>2023-07-30T12:11:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79910005629062652587890625" lon="-80.27470867149531841278076171875">
        <ele>42</ele>
        <time>2023-07-30T12:11:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7991290576756000518798828125" lon="-80.27472384274005889892578125">
        <ele>42</ele>
        <time>2023-07-30T12:11:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79915906488895416259765625" lon="-80.27473716996610164642333984375">
        <ele>42</ele>
        <time>2023-07-30T12:11:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7991873957216739654541015625" lon="-80.2747498266398906707763671875">
        <ele>42</ele>
        <time>2023-07-30T12:11:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79921916313469409942626953125" lon="-80.2747642435133457183837890625">
        <ele>42</ele>
        <time>2023-07-30T12:11:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79924950562417507171630859375" lon="-80.27477840892970561981201171875">
        <ele>42</ele>
        <time>2023-07-30T12:11:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79928110539913177490234375" lon="-80.27479450218379497528076171875">
        <ele>42</ele>
        <time>2023-07-30T12:11:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.799308933317661285400390625" lon="-80.27480657212436199188232421875">
        <ele>42</ele>
        <time>2023-07-30T12:11:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.799339108169078826904296875" lon="-80.27481763623654842376708984375">
        <ele>42</ele>
        <time>2023-07-30T12:11:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7993681095540523529052734375" lon="-80.2748306281864643096923828125">
        <ele>42</ele>
        <time>2023-07-30T12:11:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79939652420580387115478515625" lon="-80.27484110556542873382568359375">
        <ele>42</ele>
        <time>2023-07-30T12:11:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7994252741336822509765625" lon="-80.27485082857310771942138671875">
        <ele>42</ele>
        <time>2023-07-30T12:11:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79945167712867259979248046875" lon="-80.2748628146946430206298828125">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:11:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7994837798178195953369140625" lon="-80.2748772315680980682373046875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:11:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7995122782886028289794921875" lon="-80.2748893015086650848388671875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:11:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79954354278743267059326171875" lon="-80.27490246109664440155029296875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:11:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.799571454524993896484375" lon="-80.274913273751735687255859375">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:11:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7996007911860942840576171875" lon="-80.27492794208228588104248046875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:11:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7996302954852581024169921875" lon="-80.27494135312736034393310546875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:11:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.799658961594104766845703125" lon="-80.274952836334705352783203125">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:11:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7996903099119663238525390625" lon="-80.27496767230331897735595703125">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:11:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.799719981849193572998046875" lon="-80.27498024515807628631591796875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:11:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79975074343383312225341796875" lon="-80.27499365620315074920654296875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:11:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.799781002104282379150390625" lon="-80.27500404976308345794677734375">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:11:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79981377534568309783935546875" lon="-80.2750160358846187591552734375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:11:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79984495602548122406005859375" lon="-80.27503087185323238372802734375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:11:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79987613670527935028076171875" lon="-80.27504327706992626190185546875">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:11:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79990631155669689178466796875" lon="-80.27505836449563503265380859375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:11:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79993430711328983306884765625" lon="-80.27507110498845577239990234375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:11:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.799963057041168212890625" lon="-80.27508468367159366607666015625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:11:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79998845420777797698974609375" lon="-80.2750971727073192596435546875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:11:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8000205568969249725341796875" lon="-80.2751132659614086151123046875">
        <ele>43</ele>
        <time>2023-07-30T12:11:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80004863627254962921142578125" lon="-80.2751268446445465087890625">
        <ele>43</ele>
        <time>2023-07-30T12:11:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80007797293365001678466796875" lon="-80.27513883076608180999755859375">
        <ele>43</ele>
        <time>2023-07-30T12:11:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80010630376636981964111328125" lon="-80.2751524932682514190673828125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:11:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80013463459908962249755859375" lon="-80.27516481466591358184814453125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:11:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80016212724149227142333984375" lon="-80.2751783095300197601318359375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:11:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80019280500710010528564453125" lon="-80.275192558765411376953125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:12:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80022297985851764678955078125" lon="-80.27520705945789813995361328125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:12:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80025281943380832672119140625" lon="-80.27521979995071887969970703125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:12:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8002830781042575836181640625" lon="-80.2752336300909519195556640625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:12:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80030998401343822479248046875" lon="-80.2752448618412017822265625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:12:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80034066177904605865478515625" lon="-80.2752597816288471221923828125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:12:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8003662265837192535400390625" lon="-80.275271348655223846435546875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:12:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.800396569073200225830078125" lon="-80.275284759700298309326171875">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:12:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80042515136301517486572265625" lon="-80.27529724873602390289306640625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:12:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80045566149055957794189453125" lon="-80.275310911238193511962890625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:12:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8004855848848819732666015625" lon="-80.27532465755939483642578125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:12:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80051316134631633758544921875" lon="-80.2753355540335178375244140625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:12:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80054417438805103302001953125" lon="-80.27534837834537029266357421875">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:12:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8005732595920562744140625" lon="-80.27535961009562015533447265625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:12:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80060485936701297760009765625" lon="-80.27537318877875804901123046875">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:12:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80063319019973278045654296875" lon="-80.27538475580513477325439453125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:12:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8006646223366260528564453125" lon="-80.2753985859453678131103515625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:12:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80069328844547271728515625" lon="-80.27541057206690311431884765625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:12:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.800722457468509674072265625" lon="-80.27542549185454845428466796875">
        <ele>44</ele>
        <time>2023-07-30T12:12:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.800751961767673492431640625" lon="-80.27543990872800350189208984375">
        <ele>44</ele>
        <time>2023-07-30T12:12:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>152</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80077911913394927978515625" lon="-80.275452397763729095458984375">
        <ele>44</ele>
        <time>2023-07-30T12:12:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80080988071858882904052734375" lon="-80.275466479361057281494140625">
        <ele>44</ele>
        <time>2023-07-30T12:12:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8008362837135791778564453125" lon="-80.27547930367290973663330078125">
        <ele>44</ele>
        <time>2023-07-30T12:12:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80086972750723361968994140625" lon="-80.27549388818442821502685546875">
        <ele>44</ele>
        <time>2023-07-30T12:12:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8008999861776828765869140625" lon="-80.27550394646823406219482421875">
        <ele>44</ele>
        <time>2023-07-30T12:12:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80093493871390819549560546875" lon="-80.27551853097975254058837890625">
        <ele>44</ele>
        <time>2023-07-30T12:12:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.800965197384357452392578125" lon="-80.27552875690162181854248046875">
        <ele>44</ele>
        <time>2023-07-30T12:12:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8009975515305995941162109375" lon="-80.2755430899560451507568359375">
        <ele>44</ele>
        <time>2023-07-30T12:12:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80102931894361972808837890625" lon="-80.27555725537240505218505859375">
        <ele>44</ele>
        <time>2023-07-30T12:12:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8010575659573078155517578125" lon="-80.27556915767490863800048828125">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:12:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80108665116131305694580078125" lon="-80.2755826525390148162841796875">
        <ele>44</ele>
        <time>2023-07-30T12:12:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80111498199403285980224609375" lon="-80.275593213737010955810546875">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:12:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80114565975964069366455078125" lon="-80.2756071276962757110595703125">
        <ele>44</ele>
        <time>2023-07-30T12:12:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8011740744113922119140625" lon="-80.2756228856742382049560546875">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:12:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80120651237666606903076171875" lon="-80.27563688345253467559814453125">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80123517848551273345947265625" lon="-80.2756490372121334075927734375">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80126803554594516754150390625" lon="-80.27566052041947841644287109375">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:12:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8013006411492824554443359375" lon="-80.2756765298545360565185546875">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:12:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8013298101723194122314453125" lon="-80.27568985708057880401611328125">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80136140994727611541748046875" lon="-80.2757050283253192901611328125">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80138789676129817962646484375" lon="-80.27571667917072772979736328125">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8014216758310794830322265625" lon="-80.27573260478675365447998046875">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8014521859586238861083984375" lon="-80.2757464349269866943359375">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:12:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80148277990520000457763671875" lon="-80.27575758285820484161376953125">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8015125356614589691162109375" lon="-80.27577183209359645843505859375">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80154329724609851837158203125" lon="-80.27578289620578289031982421875">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.801573388278484344482421875" lon="-80.2757947146892547607421875">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80160213820636272430419921875" lon="-80.27580468915402889251708984375">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80163281597197055816650390625" lon="-80.2758200280368328094482421875">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:12:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.801659889519214630126953125" lon="-80.2758322656154632568359375">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:12:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.801690064370632171630859375" lon="-80.2758461795747280120849609375">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80172015540301799774169921875" lon="-80.275859422981739044189453125">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80175116844475269317626953125" lon="-80.27587006799876689910888671875">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8017787449061870574951171875" lon="-80.275878198444843292236328125">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8018075786530971527099609375" lon="-80.2758907712996006011962890625">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8018377535045146942138671875" lon="-80.27590242214500904083251953125">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.801867090165615081787109375" lon="-80.2759145759046077728271484375">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80190028250217437744140625" lon="-80.27592773549258708953857421875">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.801928110420703887939453125" lon="-80.27593812905251979827880859375">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80196256004273891448974609375" lon="-80.2759507857263088226318359375">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:12:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80198904685676097869873046875" lon="-80.2759601734578609466552734375">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:13:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8020228259265422821044921875" lon="-80.275971405208110809326171875">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:13:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.802055180072784423828125" lon="-80.27598221786320209503173828125">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:13:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80208778567612171173095703125" lon="-80.2759926952421665191650390625">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:13:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80211594887077808380126953125" lon="-80.2760066092014312744140625">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:13:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8021441958844661712646484375" lon="-80.2760235406458377838134765625">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:13:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.802175544202327728271484375" lon="-80.27603938244283199310302734375">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:13:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80220412649214267730712890625" lon="-80.27605044655501842498779296875">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:13:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80223329551517963409423828125" lon="-80.27606234885752201080322265625">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:13:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8022588603198528289794921875" lon="-80.2760709822177886962890625">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80229330994188785552978515625" lon="-80.2760815434157848358154296875">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8023220598697662353515625" lon="-80.276089422404766082763671875">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80235215090215206146240234375" lon="-80.2760979719460010528564453125">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8023829124867916107177734375" lon="-80.27610719203948974609375">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80241317115724086761474609375" lon="-80.27611683122813701629638671875">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80244502238929271697998046875" lon="-80.276127643883228302001953125">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8024777956306934356689453125" lon="-80.276137031614780426025390625">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8025086410343647003173828125" lon="-80.276145748794078826904296875">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80253940261900424957275390625" lon="-80.2761516161262989044189453125">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:13:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80257544480264186859130859375" lon="-80.27616175822913646697998046875">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:13:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80260293744504451751708984375" lon="-80.276168547570705413818359375">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:13:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80263663269579410552978515625" lon="-80.27617701329290866851806640625">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:13:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80266764573752880096435546875" lon="-80.27618472464382648468017578125">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:13:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80269958078861236572265625" lon="-80.27619344182312488555908203125">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:13:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80272950418293476104736328125" lon="-80.27620132081210613250732421875">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8027627803385257720947265625" lon="-80.2762089483439922332763671875">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80279823578894138336181640625" lon="-80.276218168437480926513671875">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8028284944593906402587890625" lon="-80.27622579596936702728271484375">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80286210589110851287841796875" lon="-80.27623501606285572052001953125">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8028933703899383544921875" lon="-80.2762399613857269287109375">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8029260598123073577880859375" lon="-80.2762464992702007293701171875">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8029562346637248992919921875" lon="-80.27625127695500850677490234375">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80299068428575992584228515625" lon="-80.27625848539173603057861328125">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8030279837548732757568359375" lon="-80.2762672863900661468505859375">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80305673368275165557861328125" lon="-80.2762763388454914093017578125">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80308707617223262786865234375" lon="-80.27628195472061634063720703125">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80311808921396732330322265625" lon="-80.2762874029576778411865234375">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8031508624553680419921875" lon="-80.27629402466118335723876953125">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80318078584969043731689453125" lon="-80.27629888616502285003662109375">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80321297235786914825439453125" lon="-80.2763043344020843505859375">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80324180610477924346923828125" lon="-80.27631036937236785888671875">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8032742440700531005859375" lon="-80.27631732635200023651123046875">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:13:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80330450274050235748291015625" lon="-80.27632336132228374481201171875">
        <ele>45</ele>
        <time>2023-07-30T12:13:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8033344261348247528076171875" lon="-80.2763283066451549530029296875">
        <ele>45</ele>
        <time>2023-07-30T12:13:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8033672831952571868896484375" lon="-80.27633375488221645355224609375">
        <ele>45</ele>
        <time>2023-07-30T12:13:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80339636839926242828369140625" lon="-80.2763373591005802154541015625">
        <ele>45</ele>
        <time>2023-07-30T12:13:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8034286387264728546142578125" lon="-80.27634465135633945465087890625">
        <ele>45</ele>
        <time>2023-07-30T12:13:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.803455628454685211181640625" lon="-80.27634984813630580902099609375">
        <ele>45</ele>
        <time>2023-07-30T12:13:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80348974280059337615966796875" lon="-80.27635739184916019439697265625">
        <ele>45</ele>
        <time>2023-07-30T12:13:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80352109111845493316650390625" lon="-80.27636141516268253326416015625">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:13:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.803556211292743682861328125" lon="-80.27636828832328319549560546875">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:13:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.803587727248668670654296875" lon="-80.27637231163680553436279296875">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:13:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80362016521394252777099609375" lon="-80.27637817896902561187744140625">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:13:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80365386046469211578369140625" lon="-80.2763846330344676971435546875">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:13:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80368654988706111907958984375" lon="-80.27638924308121204376220703125">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:13:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80372058041393756866455078125" lon="-80.2763967029750347137451171875">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:13:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80375058762729167938232421875" lon="-80.27640215121209621429443359375">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:13:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8037826903164386749267578125" lon="-80.27641204185783863067626953125">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:13:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8038142062723636627197265625" lon="-80.276417993009090423583984375">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:13:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80384781770408153533935546875" lon="-80.27642662636935710906982421875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:13:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80388000421226024627685546875" lon="-80.27643333189189434051513671875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:14:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80391403473913669586181640625" lon="-80.27644154615700244903564453125">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:14:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.803948819637298583984375" lon="-80.2764493413269519805908203125">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:14:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8039794974029064178466796875" lon="-80.27645428664982318878173828125">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:14:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.804012857377529144287109375" lon="-80.2764610759913921356201171875">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:14:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80404311604797840118408203125" lon="-80.2764654345810413360595703125">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:14:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8040750510990619659423828125" lon="-80.2764728106558322906494140625">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:14:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80410363338887691497802734375" lon="-80.276477672159671783447265625">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:14:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80413665808737277984619140625" lon="-80.27648731134831905364990234375">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:14:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80416767112910747528076171875" lon="-80.27649284340441226959228515625">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:14:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.804198600351810455322265625" lon="-80.2765001356601715087890625">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:14:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80422936193645000457763671875" lon="-80.2765039913356304168701171875">
        <ele>46</ele>
        <time>2023-07-30T12:14:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80426238663494586944580078125" lon="-80.2765106968581676483154296875">
        <ele>46</ele>
        <time>2023-07-30T12:14:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8042939864099025726318359375" lon="-80.27651648037135601043701171875">
        <ele>46</ele>
        <time>2023-07-30T12:14:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80432541854679584503173828125" lon="-80.27652251534163951873779296875">
        <ele>46</ele>
        <time>2023-07-30T12:14:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80435928143560886383056640625" lon="-80.27652855031192302703857421875">
        <ele>46</ele>
        <time>2023-07-30T12:14:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8043898753821849822998046875" lon="-80.27653475292026996612548828125">
        <ele>46</ele>
        <time>2023-07-30T12:14:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8044250793755054473876953125" lon="-80.27654447592794895172119140625">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80445374548435211181640625" lon="-80.27654908597469329833984375">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80448727309703826904296875" lon="-80.276556126773357391357421875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.804517447948455810546875" lon="-80.27656308375298976898193359375">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.804550640285015106201171875" lon="-80.27656853199005126953125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8045806474983692169189453125" lon="-80.27657347731292247772216796875">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:14:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8046135045588016510009765625" lon="-80.27658018283545970916748046875">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:14:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.804645188152790069580078125" lon="-80.27658621780574321746826171875">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:14:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80467544682323932647705078125" lon="-80.27659459970891475677490234375">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:14:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8047087229788303375244140625" lon="-80.27660264633595943450927734375">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:14:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80474015511572360992431640625" lon="-80.27660801075398921966552734375">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80477368272840976715087890625" lon="-80.27661480009555816650390625">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80480469577014446258544921875" lon="-80.27662192471325397491455078125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8048384748399257659912109375" lon="-80.27662913314998149871826171875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.804867140948772430419921875" lon="-80.2766344137489795684814453125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80489957891404628753662109375" lon="-80.2766422927379608154296875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80493193306028842926025390625" lon="-80.27664857916533946990966796875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80496311374008655548095703125" lon="-80.27665612287819385528564453125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8049957193434238433837890625" lon="-80.276662409305572509765625">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8050265647470951080322265625" lon="-80.276668779551982879638671875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80505883507430553436279296875" lon="-80.27667590416967868804931640625">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80508850701153278350830078125" lon="-80.27668143622577190399169921875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80512102879583835601806640625" lon="-80.276689566671848297119140625">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80514835380017757415771484375" lon="-80.27669585309922695159912109375">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80518137849867343902587890625" lon="-80.2767041511833667755126953125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80521188862621784210205078125" lon="-80.27671026997268199920654296875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80524658970534801483154296875" lon="-80.2767175622284412384033203125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.805278860032558441162109375" lon="-80.27672418393194675445556640625">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8053092025220394134521484375" lon="-80.27673038654029369354248046875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80534063465893268585205078125" lon="-80.27673642151057720184326171875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8053739108145236968994140625" lon="-80.276742540299892425537109375">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.805404253304004669189453125" lon="-80.27675000019371509552001953125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.805434763431549072265625" lon="-80.27675536461174488067626953125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8054687939584255218505859375" lon="-80.27676391415297985076904296875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.805499136447906494140625" lon="-80.2767712064087390899658203125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80553123913705348968505859375" lon="-80.27677849866449832916259765625">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80556007288396358489990234375" lon="-80.27678436599671840667724609375">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80559242703020572662353515625" lon="-80.2767919935286045074462890625">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80562411062419414520263671875" lon="-80.27679853141307830810546875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80565663240849971771240234375" lon="-80.27680481784045696258544921875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8056900762021541595458984375" lon="-80.2768131159245967864990234375">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.805720083415508270263671875" lon="-80.276817977428436279296875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80575486831367015838623046875" lon="-80.27682560496032238006591796875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:14:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80578386969864368438720703125" lon="-80.27683231048285961151123046875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:15:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80581639148294925689697265625" lon="-80.27684052474796772003173828125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:15:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80584405176341533660888671875" lon="-80.2768449671566486358642578125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:15:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80587489716708660125732421875" lon="-80.27685242705047130584716796875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:15:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8059069998562335968017578125" lon="-80.2768590487539768218994140625">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:15:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80594195239245891571044921875" lon="-80.27686986140906810760498046875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:15:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80597388744354248046875" lon="-80.27687891386449337005615234375">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:15:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80600515194237232208251953125" lon="-80.2768878825008869171142578125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:15:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80603985302150249481201171875" lon="-80.27689701877534389495849609375">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:15:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80607053078711032867431640625" lon="-80.27690405957400798797607421875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:15:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.806105650961399078369140625" lon="-80.2769131958484649658203125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:15:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80613691546022891998291015625" lon="-80.276920907199382781982421875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:15:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.806169688701629638671875" lon="-80.27692987583577632904052734375">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.806199528276920318603515625" lon="-80.27693691663444042205810546875">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80623481608927249908447265625" lon="-80.276946388185024261474609375">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80626599676907062530517578125" lon="-80.2769529260694980621337890625">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8062965907156467437744140625" lon="-80.27696055360138416290283203125">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80632936395704746246337890625" lon="-80.27696927078068256378173828125">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80635987408459186553955078125" lon="-80.2769743837416172027587890625">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80639491043984889984130859375" lon="-80.27698234654963016510009765625">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80642692930996417999267578125" lon="-80.276987962424755096435546875">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.806461714208126068115234375" lon="-80.27699542231857776641845703125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:15:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80649297870695590972900390625" lon="-80.277001373469829559326171875">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8065269254148006439208984375" lon="-80.2770112641155719757080078125">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80655525624752044677734375" lon="-80.2770156227052211761474609375">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80658668838441371917724609375" lon="-80.2770231664180755615234375">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.806619293987751007080078125" lon="-80.27702978812158107757568359375">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8066509775817394256591796875" lon="-80.27703699655830860137939453125">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8066811524331569671630859375" lon="-80.27704286389052867889404296875">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.806713841855525970458984375" lon="-80.277047641575336456298828125">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8067477047443389892578125" lon="-80.27705560438334941864013671875">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.806778214871883392333984375" lon="-80.2770598791539669036865234375">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80681199394166469573974609375" lon="-80.277067087590694427490234375">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80684292316436767578125" lon="-80.27707278728485107421875">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:15:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80687619931995868682861328125" lon="-80.27707974426448345184326171875">
        <ele>46.59999847412109375</ele>
        <time>2023-07-30T12:15:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.806904613971710205078125" lon="-80.27708393521606922149658203125">
        <ele>46.59999847412109375</ele>
        <time>2023-07-30T12:15:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8069386444985866546630859375" lon="-80.27709147892892360687255859375">
        <ele>46.59999847412109375</ele>
        <time>2023-07-30T12:15:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8069674782454967498779296875" lon="-80.277096927165985107421875">
        <ele>46.59999847412109375</ele>
        <time>2023-07-30T12:15:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8069986589252948760986328125" lon="-80.27710430324077606201171875">
        <ele>46.59999847412109375</ele>
        <time>2023-07-30T12:15:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8070291690528392791748046875" lon="-80.2771101705729961395263671875">
        <ele>46.799999237060546875</ele>
        <time>2023-07-30T12:15:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8070593439042568206787109375" lon="-80.2771145291626453399658203125">
        <ele>46.799999237060546875</ele>
        <time>2023-07-30T12:15:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8070911951363086700439453125" lon="-80.27712081559002399444580078125">
        <ele>46.799999237060546875</ele>
        <time>2023-07-30T12:15:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80712245963513851165771484375" lon="-80.27712441980838775634765625">
        <ele>47</ele>
        <time>2023-07-30T12:15:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80715615488588809967041015625" lon="-80.27713104151189327239990234375">
        <ele>47</ele>
        <time>2023-07-30T12:15:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.807187251746654510498046875" lon="-80.27713489718735218048095703125">
        <ele>47</ele>
        <time>2023-07-30T12:15:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80722220428287982940673828125" lon="-80.27714009396731853485107421875">
        <ele>47</ele>
        <time>2023-07-30T12:15:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80725397169589996337890625" lon="-80.27714361436665058135986328125">
        <ele>47.200000762939453125</ele>
        <time>2023-07-30T12:15:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.807289175689220428466796875" lon="-80.27714545838534832000732421875">
        <ele>47.200000762939453125</ele>
        <time>2023-07-30T12:15:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.807318679988384246826171875" lon="-80.27714747004210948944091796875">
        <ele>47.200000762939453125</ele>
        <time>2023-07-30T12:15:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8073527105152606964111328125" lon="-80.27715350501239299774169921875">
        <ele>47.200000762939453125</ele>
        <time>2023-07-30T12:15:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80737995170056819915771484375" lon="-80.27716473676264286041259765625">
        <ele>47.200000762939453125</ele>
        <time>2023-07-30T12:15:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80739168636500835418701171875" lon="-80.27718485333025455474853515625">
        <ele>47.200000762939453125</ele>
        <time>2023-07-30T12:15:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80739076435565948486328125" lon="-80.27721167542040348052978515625">
        <ele>47.200000762939453125</ele>
        <time>2023-07-30T12:15:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80737601220607757568359375" lon="-80.27724855579435825347900390625">
        <ele>47.200000762939453125</ele>
        <time>2023-07-30T12:15:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8073634393513202667236328125" lon="-80.27728208340704441070556640625">
        <ele>47.200000762939453125</ele>
        <time>2023-07-30T12:15:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>88</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80734240077435970306396484375" lon="-80.277319215238094329833984375">
        <ele>47.200000762939453125</ele>
        <time>2023-07-30T12:15:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>86</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80732773244380950927734375" lon="-80.27735098265111446380615234375">
        <ele>47.200000762939453125</ele>
        <time>2023-07-30T12:15:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>86</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8073151595890522003173828125" lon="-80.27738392353057861328125">
        <ele>47.200000762939453125</ele>
        <time>2023-07-30T12:15:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>88</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80730518512427806854248046875" lon="-80.2774119190871715545654296875">
        <ele>47.200000762939453125</ele>
        <time>2023-07-30T12:15:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80728347599506378173828125" lon="-80.2774407528340816497802734375">
        <ele>47</ele>
        <time>2023-07-30T12:15:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80725724063813686370849609375" lon="-80.2774625457823276519775390625">
        <ele>47</ele>
        <time>2023-07-30T12:16:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.807225473225116729736328125" lon="-80.27746648527681827545166015625">
        <ele>47</ele>
        <time>2023-07-30T12:16:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80718817375600337982177734375" lon="-80.27745659463107585906982421875">
        <ele>47</ele>
        <time>2023-07-30T12:16:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80715783126652240753173828125" lon="-80.27742105536162853240966796875">
        <ele>47</ele>
        <time>2023-07-30T12:16:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80712916515767574310302734375" lon="-80.2773924730718135833740234375">
        <ele>47</ele>
        <time>2023-07-30T12:16:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.807102091610431671142578125" lon="-80.27737093158066272735595703125">
        <ele>46.799999237060546875</ele>
        <time>2023-07-30T12:16:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8070730902254581451416015625" lon="-80.2773478813469409942626953125">
        <ele>46.799999237060546875</ele>
        <time>2023-07-30T12:16:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8070462681353092193603515625" lon="-80.27732692658901214599609375">
        <ele>46.799999237060546875</ele>
        <time>2023-07-30T12:16:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8070132434368133544921875" lon="-80.2773019485175609588623046875">
        <ele>46.799999237060546875</ele>
        <time>2023-07-30T12:16:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80698843300342559814453125" lon="-80.277284346520900726318359375">
        <ele>46.799999237060546875</ele>
        <time>2023-07-30T12:16:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80696035362780094146728515625" lon="-80.27726464904844760894775390625">
        <ele>46.799999237060546875</ele>
        <time>2023-07-30T12:16:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80693185515701770782470703125" lon="-80.2772466279566287994384765625">
        <ele>46.59999847412109375</ele>
        <time>2023-07-30T12:16:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.806902267038822174072265625" lon="-80.27723061852157115936279296875">
        <ele>46.59999847412109375</ele>
        <time>2023-07-30T12:16:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8068722598254680633544921875" lon="-80.27721553109586238861083984375">
        <ele>46.59999847412109375</ele>
        <time>2023-07-30T12:16:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80684015713632106781005859375" lon="-80.277204550802707672119140625">
        <ele>46.59999847412109375</ele>
        <time>2023-07-30T12:16:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8068068809807300567626953125" lon="-80.27718736790120601654052734375">
        <ele>46.59999847412109375</ele>
        <time>2023-07-30T12:16:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8067783825099468231201171875" lon="-80.27717311866581439971923828125">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:16:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80674510635435581207275390625" lon="-80.27715786360204219818115234375">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:16:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80671493150293827056884765625" lon="-80.27714721858501434326171875">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:16:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8066821582615375518798828125" lon="-80.27713925577700138092041015625">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:16:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80665240250527858734130859375" lon="-80.27712936513125896453857421875">
        <ele>46.40000152587890625</ele>
        <time>2023-07-30T12:16:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80662088654935359954833984375" lon="-80.27711930684745311737060546875">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:16:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80658886767923831939697265625" lon="-80.27711109258234500885009765625">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:16:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8065560944378376007080078125" lon="-80.2771041356027126312255859375">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:16:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80652399174869060516357421875" lon="-80.277095921337604522705078125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:16:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80649247579276561737060546875" lon="-80.27708879671990871429443359375">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:16:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8064558468759059906005859375" lon="-80.27707882225513458251953125">
        <ele>46</ele>
        <time>2023-07-30T12:16:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8064236603677272796630859375" lon="-80.27707186527550220489501953125">
        <ele>46</ele>
        <time>2023-07-30T12:16:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80638870783150196075439453125" lon="-80.27706272900104522705078125">
        <ele>46</ele>
        <time>2023-07-30T12:16:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8063582815229892730712890625" lon="-80.27705426327884197235107421875">
        <ele>46</ele>
        <time>2023-07-30T12:16:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8063227422535419464111328125" lon="-80.2770454622805118560791015625">
        <ele>46</ele>
        <time>2023-07-30T12:16:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8062898851931095123291015625" lon="-80.27703833766281604766845703125">
        <ele>46</ele>
        <time>2023-07-30T12:16:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.806256525218486785888671875" lon="-80.2770317159593105316162109375">
        <ele>46</ele>
        <time>2023-07-30T12:16:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80622400343418121337890625" lon="-80.2770233340561389923095703125">
        <ele>46</ele>
        <time>2023-07-30T12:16:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80619106255471706390380859375" lon="-80.277017466723918914794921875">
        <ele>46</ele>
        <time>2023-07-30T12:16:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80615736730396747589111328125" lon="-80.2770092524588108062744140625">
        <ele>46</ele>
        <time>2023-07-30T12:16:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8061269409954547882080078125" lon="-80.27700464241206645965576171875">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:16:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80609550885856151580810546875" lon="-80.27699944563210010528564453125">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:16:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80606449581682682037353515625" lon="-80.2769955061376094818115234375">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:16:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80603180639445781707763671875" lon="-80.276989638805389404296875">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:16:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80600096099078655242919921875" lon="-80.2769898064434528350830078125">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:16:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.805977575480937957763671875" lon="-80.276976563036441802978515625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.805950753390789031982421875" lon="-80.27696759440004825592041015625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80592761933803558349609375" lon="-80.2769562788307666778564453125">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80590054579079151153564453125" lon="-80.2769438736140727996826171875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80587489716708660125732421875" lon="-80.2769328095018863677978515625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>88</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80584690161049365997314453125" lon="-80.27692534960806369781494140625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>87</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80581991188228130340576171875" lon="-80.27692367322742938995361328125">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>87</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.805791832506656646728515625" lon="-80.27691361494362354278564453125">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>86</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80576492659747600555419921875" lon="-80.27690607123076915740966796875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>86</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80573475174605846405029296875" lon="-80.27690070681273937225341796875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>86</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8057054989039897918701171875" lon="-80.27689450420439243316650390625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>86</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8056786768138408660888671875" lon="-80.27688997797667980194091796875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>86</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.805648334324359893798828125" lon="-80.2768831886351108551025390625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>86</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8056236915290355682373046875" lon="-80.2768805064260959625244140625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>86</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80559946782886981964111328125" lon="-80.27687522582709789276123046875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:16:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>86</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80558831989765167236328125" lon="-80.27687153778970241546630859375">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:16:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>85</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80558823607861995697021484375" lon="-80.27686734683811664581298828125">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:16:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>85</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8055898286402225494384765625" lon="-80.2768691070377826690673828125">
        <ele>46.200000762939453125</ele>
        <time>2023-07-30T12:16:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>0</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8055817820131778717041015625" lon="-80.27686835266649723052978515625">
        <ele>46</ele>
        <time>2023-07-30T12:16:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>0</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.805557139217853546142578125" lon="-80.27686466462910175323486328125">
        <ele>46</ele>
        <time>2023-07-30T12:17:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>0</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8055231086909770965576171875" lon="-80.2768556959927082061767578125">
        <ele>46</ele>
        <time>2023-07-30T12:17:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>0</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80548597685992717742919921875" lon="-80.27684488333761692047119140625">
        <ele>46</ele>
        <time>2023-07-30T12:17:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>80</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80545026995241641998291015625" lon="-80.27683935128152370452880859375">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:17:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.805412299931049346923828125" lon="-80.27683298103511333465576171875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80537868849933147430419921875" lon="-80.2768265269696712493896484375">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8053420595824718475341796875" lon="-80.2768193185329437255859375">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80530937016010284423828125" lon="-80.27681353501975536346435546875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>97</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8052786923944950103759765625" lon="-80.27680800296366214752197265625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>97</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.805243991315364837646484375" lon="-80.27680062688887119293212890625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80521146953105926513671875" lon="-80.27679442428052425384521484375">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8051747567951679229736328125" lon="-80.2767898142337799072265625">
        <ele>45.799999237060546875</ele>
        <time>2023-07-30T12:17:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80514030717313289642333984375" lon="-80.27678470127284526824951171875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8051073662936687469482421875" lon="-80.276780426502227783203125">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80507274903357028961181640625" lon="-80.27677983976900577545166015625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80504274182021617889404296875" lon="-80.276776738464832305908203125">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>153</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80500720255076885223388671875" lon="-80.27677263133227825164794921875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>154</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8049749322235584259033203125" lon="-80.2767678536474704742431640625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8049407340586185455322265625" lon="-80.2767611481249332427978515625">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8049115650355815887451171875" lon="-80.27675704099237918853759765625">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.804879210889339447021484375" lon="-80.27675268240272998809814453125">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80484794639050960540771484375" lon="-80.27674748562276363372802734375">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80481542460620403289794921875" lon="-80.27673893608152866363525390625">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80478374101221561431884765625" lon="-80.27673290111124515533447265625">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8047489561140537261962890625" lon="-80.27672451920807361602783203125">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8047199547290802001953125" lon="-80.27672099880874156951904296875">
        <ele>45.59999847412109375</ele>
        <time>2023-07-30T12:17:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8046850860118865966796875" lon="-80.276715718209743499755859375">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8046530671417713165283203125" lon="-80.276710689067840576171875">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80461912043392658233642578125" lon="-80.27670088224112987518310546875">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80458894558250904083251953125" lon="-80.2766947634518146514892578125">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80455675907433032989501953125" lon="-80.276687555015087127685546875">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80452641658484935760498046875" lon="-80.27668093331158161163330078125">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.804490961134433746337890625" lon="-80.27667104266583919525146484375">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80445818789303302764892578125" lon="-80.27666165493428707122802734375">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8044260852038860321044921875" lon="-80.2766538597643375396728515625">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8043888695538043975830078125" lon="-80.276644639670848846435546875">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80435475520789623260498046875" lon="-80.2766391076147556304931640625">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80431787483394145965576171875" lon="-80.27662829495966434478759765625">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80428300611674785614013671875" lon="-80.27662225998938083648681640625">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80423942022025585174560546875" lon="-80.27661144733428955078125">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80421108938753604888916015625" lon="-80.276603735983371734619140625">
        <ele>45.40000152587890625</ele>
        <time>2023-07-30T12:17:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8041793219745159149169921875" lon="-80.2765935100615024566650390625">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:17:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80414822511374950408935546875" lon="-80.27658454142510890960693359375">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:17:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80411536805331707000732421875" lon="-80.2765790931880474090576171875">
        <ele>45.200000762939453125</ele>
        <time>2023-07-30T12:17:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>155</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8040800802409648895263671875" lon="-80.27656945399940013885498046875">
        <ele>45</ele>
        <time>2023-07-30T12:17:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>156</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.804045379161834716796875" lon="-80.27656258083879947662353515625">
        <ele>45</ele>
        <time>2023-07-30T12:17:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8040086664259433746337890625" lon="-80.2765516005456447601318359375">
        <ele>45</ele>
        <time>2023-07-30T12:17:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80398125760257244110107421875" lon="-80.27654481120407581329345703125">
        <ele>45</ele>
        <time>2023-07-30T12:17:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80394823290407657623291015625" lon="-80.27653659693896770477294921875">
        <ele>45</ele>
        <time>2023-07-30T12:17:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80391738750040531158447265625" lon="-80.27653089724481105804443359375">
        <ele>45</ele>
        <time>2023-07-30T12:17:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80388285405933856964111328125" lon="-80.276519246399402618408203125">
        <ele>45</ele>
        <time>2023-07-30T12:17:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80385368503630161285400390625" lon="-80.27651111595332622528076171875">
        <ele>45</ele>
        <time>2023-07-30T12:17:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80381672084331512451171875" lon="-80.276506841182708740234375">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:17:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80378176830708980560302734375" lon="-80.276501812040805816650390625">
        <ele>44.799999237060546875</ele>
        <time>2023-07-30T12:17:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8037454746663570404052734375" lon="-80.276496447622776031494140625">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:17:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80370817519724369049072265625" lon="-80.2764905802905559539794921875">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:17:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8036767430603504180908203125" lon="-80.2764867246150970458984375">
        <ele>44.59999847412109375</ele>
        <time>2023-07-30T12:17:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80364162288606166839599609375" lon="-80.2764788456261157989501953125">
        <ele>44.40000152587890625</ele>
        <time>2023-07-30T12:17:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80360658653080463409423828125" lon="-80.27647130191326141357421875">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:17:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80357205308973789215087890625" lon="-80.2764640934765338897705078125">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:17:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80353651382029056549072265625" lon="-80.27645277790725231170654296875">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:18:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8035023994743824005126953125" lon="-80.27644406072795391082763671875">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:18:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.803467027842998504638671875" lon="-80.2764355950057506561279296875">
        <ele>44.200000762939453125</ele>
        <time>2023-07-30T12:18:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80343341641128063201904296875" lon="-80.27642779983580112457275390625">
        <ele>44</ele>
        <time>2023-07-30T12:18:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8033991344273090362548828125" lon="-80.27642008848488330841064453125">
        <ele>44</ele>
        <time>2023-07-30T12:18:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8033635951578617095947265625" lon="-80.27641187421977519989013671875">
        <ele>44</ele>
        <time>2023-07-30T12:18:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80332813970744609832763671875" lon="-80.27640583924949169158935546875">
        <ele>44</ele>
        <time>2023-07-30T12:18:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8032930195331573486328125" lon="-80.2763963676989078521728515625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:18:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80325739644467830657958984375" lon="-80.276388823986053466796875">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:18:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80321883969008922576904296875" lon="-80.2763809449970722198486328125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:18:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8031828813254833221435546875" lon="-80.2763739041984081268310546875">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:18:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80314960516989231109619140625" lon="-80.276367366313934326171875">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:18:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8031154908239841461181640625" lon="-80.27635999023914337158203125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:18:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80308523215353488922119140625" lon="-80.27635328471660614013671875">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:18:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>97</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80305019579827785491943359375" lon="-80.2763460762798786163330078125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:18:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>97</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80301616527140140533447265625" lon="-80.27633694000542163848876953125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:18:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>97</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80297844670712947845458984375" lon="-80.27632914483547210693359375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:18:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80294424854218959808349609375" lon="-80.27632101438939571380615234375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:18:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80291281640529632568359375" lon="-80.276316069066524505615234375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:18:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8028747625648975372314453125" lon="-80.27630894444882869720458984375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:18:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8028398938477039337158203125" lon="-80.2763018198311328887939453125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:18:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.802802510559558868408203125" lon="-80.276297964155673980712890625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:18:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80276596546173095703125" lon="-80.2762900851666927337646484375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:18:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8027332760393619537353515625" lon="-80.27628220617771148681640625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80269555747509002685546875" lon="-80.27627357281744480133056640625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8026640415191650390625" lon="-80.2762669511139392852783203125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8026296757161617279052734375" lon="-80.276260413229465484619140625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:18:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8025924600660800933837890625" lon="-80.27625194750726222991943359375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8025554120540618896484375" lon="-80.27624289505183696746826171875">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8025193698704242706298828125" lon="-80.27623149566352367401123046875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:18:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80248894356191158294677734375" lon="-80.276220180094242095947265625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:18:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80245591886341571807861328125" lon="-80.27620844542980194091796875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:18:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80242197215557098388671875" lon="-80.2761970460414886474609375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80238366685807704925537109375" lon="-80.2761871553957462310791015625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80234578065574169158935546875" lon="-80.27617349289357662200927734375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80231510289013385772705078125" lon="-80.27616460807621479034423828125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80228140763938426971435546875" lon="-80.27615346014499664306640625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80224871821701526641845703125" lon="-80.27614315040409564971923828125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8022186271846294403076171875" lon="-80.2761345170438289642333984375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8021847642958164215087890625" lon="-80.27612387202680110931396484375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.802152745425701141357421875" lon="-80.27611364610493183135986328125">
        <ele>43</ele>
        <time>2023-07-30T12:18:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8021187148988246917724609375" lon="-80.27610157616436481475830078125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80208409763872623443603515625" lon="-80.27609118260443210601806640625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8020513243973255157470703125" lon="-80.2760817110538482666015625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80201536603271961212158203125" lon="-80.27606897056102752685546875">
        <ele>43</ele>
        <time>2023-07-30T12:18:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80198150314390659332275390625" lon="-80.27605916373431682586669921875">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:18:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80194646678864955902099609375" lon="-80.276045501232147216796875">
        <ele>43</ele>
        <time>2023-07-30T12:18:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.801910676062107086181640625" lon="-80.27603410184383392333984375">
        <ele>43</ele>
        <time>2023-07-30T12:18:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80187672935426235198974609375" lon="-80.2760235406458377838134765625">
        <ele>43</ele>
        <time>2023-07-30T12:18:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.801840938627719879150390625" lon="-80.276008285582065582275390625">
        <ele>43</ele>
        <time>2023-07-30T12:18:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80180917121469974517822265625" lon="-80.27599629946053028106689453125">
        <ele>43</ele>
        <time>2023-07-30T12:18:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80177480541169643402099609375" lon="-80.27598137967288494110107421875">
        <ele>43</ele>
        <time>2023-07-30T12:18:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80174261890351772308349609375" lon="-80.27596746571362018585205078125">
        <ele>43</ele>
        <time>2023-07-30T12:18:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.801711522042751312255859375" lon="-80.27595388703048229217529296875">
        <ele>43</ele>
        <time>2023-07-30T12:18:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80167908407747745513916015625" lon="-80.275938548147678375244140625">
        <ele>43</ele>
        <time>2023-07-30T12:18:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8016466461122035980224609375" lon="-80.27592522092163562774658203125">
        <ele>43</ele>
        <time>2023-07-30T12:18:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80161236412823200225830078125" lon="-80.2759088762104511260986328125">
        <ele>43</ele>
        <time>2023-07-30T12:18:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80158118344843387603759765625" lon="-80.27589621953666210174560546875">
        <ele>43</ele>
        <time>2023-07-30T12:18:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80154732055962085723876953125" lon="-80.27588163502514362335205078125">
        <ele>43</ele>
        <time>2023-07-30T12:18:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80151278711855411529541015625" lon="-80.27586688287556171417236328125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:18:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80147976242005825042724609375" lon="-80.27585632167756557464599609375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80144355259835720062255859375" lon="-80.2758391387760639190673828125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8014101088047027587890625" lon="-80.27582581155002117156982421875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80137649737298488616943359375" lon="-80.27581013739109039306640625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.801344394683837890625" lon="-80.27579563669860363006591796875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8013127110898494720458984375" lon="-80.27578121982514858245849609375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80128178186714649200439453125" lon="-80.27576462365686893463134765625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80125160701572895050048828125" lon="-80.27575020678341388702392578125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8012188337743282318115234375" lon="-80.27573193423449993133544921875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8011913411319255828857421875" lon="-80.2757174335420131683349609375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80115890316665172576904296875" lon="-80.27570209465920925140380859375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8011283092200756072998046875" lon="-80.27568700723350048065185546875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80109620653092861175537109375" lon="-80.27567409910261631011962890625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8010656125843524932861328125" lon="-80.27565859258174896240234375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8010320849716663360595703125" lon="-80.27565532363951206207275390625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8009997308254241943359375" lon="-80.27564157731831073760986328125">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:19:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80096729286015033721923828125" lon="-80.275628753006458282470703125">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:19:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80093158595263957977294921875" lon="-80.27561659924685955047607421875">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:19:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8008979745209217071533203125" lon="-80.27560519985854625701904296875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:19:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8008664585649967193603515625" lon="-80.27558877132833003997802734375">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:19:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80083569698035717010498046875" lon="-80.27557083405554294586181640625">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:19:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80080535449087619781494140625" lon="-80.27555742301046848297119140625">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:19:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8007797896862030029296875" lon="-80.27554099448025226593017578125">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:19:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8007502853870391845703125" lon="-80.27553009800612926483154296875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8007199428975582122802734375" lon="-80.27551643550395965576171875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80069001950323581695556640625" lon="-80.27550302445888519287109375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80066017992794513702392578125" lon="-80.2754901163280010223388671875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8006280772387981414794921875" lon="-80.27547678910195827484130859375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8005955554544925689697265625" lon="-80.2754632942378520965576171875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:19:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8005687333643436431884765625" lon="-80.27545147575438022613525390625">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:19:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8005405701696872711181640625" lon="-80.27543839998543262481689453125">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:19:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8005103953182697296142578125" lon="-80.27542515657842159271240234375">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:19:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.800478376448154449462890625" lon="-80.2754079736769199371337890625">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:19:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.800447531044483184814453125" lon="-80.27539439499378204345703125">
        <ele>42</ele>
        <time>2023-07-30T12:19:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.800416015088558197021484375" lon="-80.275379307568073272705078125">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:19:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.800381146371364593505859375" lon="-80.27536480687558650970458984375">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:19:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8003437630832195281982421875" lon="-80.27535089291632175445556640625">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:19:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80031266622245311737060546875" lon="-80.2753385715186595916748046875">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:19:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80028332956135272979736328125" lon="-80.27532189153134822845458984375">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:19:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.800253741443157196044921875" lon="-80.27530831284821033477783203125">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:19:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80022415332496166229248046875" lon="-80.2752996794879436492919921875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:19:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80018878169357776641845703125" lon="-80.27528467588126659393310546875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:19:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.8001586906611919403076171875" lon="-80.27526740916073322296142578125">
        <ele>41.40000152587890625</ele>
        <time>2023-07-30T12:19:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80012524686753749847412109375" lon="-80.27524846605956554412841796875">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:19:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80009113252162933349609375" lon="-80.27523270808160305023193359375">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:19:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80005492269992828369140625" lon="-80.27522005140781402587890625">
        <ele>41</ele>
        <time>2023-07-30T12:19:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.80002583749592304229736328125" lon="-80.2752101607620716094970703125">
        <ele>41</ele>
        <time>2023-07-30T12:19:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79999851249158382415771484375" lon="-80.27519993484020233154296875">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:19:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7999659068882465362548828125" lon="-80.27518476359546184539794921875">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:19:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79993430711328983306884765625" lon="-80.2751702629029750823974609375">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:19:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79989960603415966033935546875" lon="-80.2751545049250125885009765625">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:19:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7998651564121246337890625" lon="-80.2751394174993038177490234375">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:19:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79983137734234333038330078125" lon="-80.27512424625456333160400390625">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:19:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>157</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.799797095358371734619140625" lon="-80.2751080691814422607421875">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:19:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.799763567745685577392578125" lon="-80.27509373612701892852783203125">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:19:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>158</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.799729369580745697021484375" lon="-80.2750804089009761810302734375">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:19:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>159</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79969626106321811676025390625" lon="-80.27506842277944087982177734375">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:19:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79966155998408794403076171875" lon="-80.275054089725017547607421875">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:19:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7996276132762432098388671875" lon="-80.27503900229930877685546875">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:19:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>160</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79959316365420818328857421875" lon="-80.2750244177877902984619140625">
        <ele>40</ele>
        <time>2023-07-30T12:19:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79955946840345859527587890625" lon="-80.27500815689563751220703125">
        <ele>40</ele>
        <time>2023-07-30T12:20:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79952594079077243804931640625" lon="-80.27499449439346790313720703125">
        <ele>40</ele>
        <time>2023-07-30T12:20:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79949157498776912689208984375" lon="-80.274978317320346832275390625">
        <ele>40</ele>
        <time>2023-07-30T12:20:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>161</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79946022666990756988525390625" lon="-80.27496448718011379241943359375">
        <ele>40</ele>
        <time>2023-07-30T12:20:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7994279563426971435546875" lon="-80.2749479748308658599853515625">
        <ele>40</ele>
        <time>2023-07-30T12:20:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7993956021964550018310546875" lon="-80.27493447996675968170166015625">
        <ele>39.799999237060546875</ele>
        <time>2023-07-30T12:20:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>162</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79936425387859344482421875" lon="-80.274921990931034088134765625">
        <ele>39.799999237060546875</ele>
        <time>2023-07-30T12:20:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7993308939039707183837890625" lon="-80.274905897676944732666015625">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79930180869996547698974609375" lon="-80.274893827736377716064453125">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79926710762083530426025390625" lon="-80.2748765610158443450927734375">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79923676513135433197021484375" lon="-80.274862982332706451416015625">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7992061711847782135009765625" lon="-80.27485082857310771942138671875">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7991736494004726409912109375" lon="-80.2748369984328746795654296875">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79914506711065769195556640625" lon="-80.27482585050165653228759765625">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7991099469363689422607421875" lon="-80.27481101453304290771484375">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7990792691707611083984375" lon="-80.27480087243020534515380859375">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79904549010097980499267578125" lon="-80.27478662319481372833251953125">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79901464469730854034423828125" lon="-80.2747749723494052886962890625">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79898329637944698333740234375" lon="-80.27476231567561626434326171875">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79895060695707798004150390625" lon="-80.27474605478346347808837890625">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79891942627727985382080078125" lon="-80.2747344039380550384521484375">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.798887826502323150634765625" lon="-80.2747203223407268524169921875">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.798857986927032470703125" lon="-80.27470800094306468963623046875">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79882538132369518280029296875" lon="-80.27469157241284847259521484375">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7987957932054996490478515625" lon="-80.2746780775487422943115234375">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79876486398279666900634765625" lon="-80.27466257102787494659423828125">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79873636551201343536376953125" lon="-80.27464999817311763763427734375">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79870820231735706329345703125" lon="-80.27463734149932861328125">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7986769378185272216796875" lon="-80.27462233789265155792236328125">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79864609241485595703125" lon="-80.27461211197078227996826171875">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.798614241182804107666015625" lon="-80.27459585107862949371337890625">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79858515597879886627197265625" lon="-80.27458361349999904632568359375">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79854944907128810882568359375" lon="-80.27456760406494140625">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7985175140202045440673828125" lon="-80.27455243282020092010498046875">
        <ele>39.40000152587890625</ele>
        <time>2023-07-30T12:20:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79848457314074039459228515625" lon="-80.27453751303255558013916015625">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79845230281352996826171875" lon="-80.2745235152542591094970703125">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79842036776244640350341796875" lon="-80.2745147980749607086181640625">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79838583432137966156005859375" lon="-80.27449962683022022247314453125">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7983562462031841278076171875" lon="-80.2744876407086849212646484375">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79832280240952968597412109375" lon="-80.274472720921039581298828125">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79829346574842929840087890625" lon="-80.27446157298982143402099609375">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79826211743056774139404296875" lon="-80.27444849722087383270263671875">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7982311882078647613525390625" lon="-80.274436511099338531494140625">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7982006780803203582763671875" lon="-80.27442276477813720703125">
        <ele>39.59999847412109375</ele>
        <time>2023-07-30T12:20:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.798170335590839385986328125" lon="-80.27440491132438182830810546875">
        <ele>39.799999237060546875</ele>
        <time>2023-07-30T12:20:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79814141802489757537841796875" lon="-80.27439267374575138092041015625">
        <ele>39.799999237060546875</ele>
        <time>2023-07-30T12:20:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79811208136379718780517578125" lon="-80.27438060380518436431884765625">
        <ele>39.799999237060546875</ele>
        <time>2023-07-30T12:20:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79808257706463336944580078125" lon="-80.2743682824075222015380859375">
        <ele>39.799999237060546875</ele>
        <time>2023-07-30T12:20:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7980514802038669586181640625" lon="-80.274354033172130584716796875">
        <ele>40</ele>
        <time>2023-07-30T12:20:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79802289791405200958251953125" lon="-80.2743409574031829833984375">
        <ele>40</ele>
        <time>2023-07-30T12:20:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79798987321555614471435546875" lon="-80.27432712726294994354248046875">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:20:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.797959111630916595458984375" lon="-80.274314470589160919189453125">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:20:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7979312837123870849609375" lon="-80.274303741753101348876953125">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:20:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79790068976581096649169921875" lon="-80.27428932487964630126953125">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:20:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.797871939837932586669921875" lon="-80.27427867986261844635009765625">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:20:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7978409267961978912353515625" lon="-80.2742640115320682525634765625">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:20:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79781217686831951141357421875" lon="-80.2742516063153743743896484375">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:20:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79778250493109226226806640625" lon="-80.27423727326095104217529296875">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:20:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79775031842291355133056640625" lon="-80.274223275482654571533203125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:20:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.797721065580844879150390625" lon="-80.27421229518949985504150390625">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:20:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79768829233944416046142578125" lon="-80.27419787831604480743408203125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79765677638351917266845703125" lon="-80.2741862274706363677978515625">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79762081801891326904296875" lon="-80.27417105622589588165283203125">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:21:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79758905060589313507080078125" lon="-80.27415865100920200347900390625">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:21:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>163</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.797556780278682708740234375" lon="-80.274144150316715240478515625">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:21:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7975267730653285980224609375" lon="-80.274132080376148223876953125">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:21:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79749668203294277191162109375" lon="-80.2741205133497714996337890625">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:21:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.797463238239288330078125" lon="-80.27410701848566532135009765625">
        <ele>41</ele>
        <time>2023-07-30T12:21:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79743314720690250396728515625" lon="-80.27409763075411319732666015625">
        <ele>41</ele>
        <time>2023-07-30T12:21:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7974002063274383544921875" lon="-80.27408623136579990386962890625">
        <ele>41</ele>
        <time>2023-07-30T12:21:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7973728813230991363525390625" lon="-80.274078436195850372314453125">
        <ele>41</ele>
        <time>2023-07-30T12:21:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79733910225331783294677734375" lon="-80.27406544424593448638916015625">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7973091788589954376220703125" lon="-80.27405203320086002349853515625">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79727975837886333465576171875" lon="-80.27403996326029300689697265625">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7972473204135894775390625" lon="-80.2740299887955188751220703125">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7972139604389667510986328125" lon="-80.274019427597522735595703125">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7971797622740268707275390625" lon="-80.274007022380828857421875">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>164</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7971472404897212982177734375" lon="-80.27399788610637187957763671875">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7971132099628448486328125" lon="-80.27398548088967800140380859375">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79708177782595157623291015625" lon="-80.2739750035107135772705078125">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79704774729907512664794921875" lon="-80.27396268211305141448974609375">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79701656661927700042724609375" lon="-80.27395312674343585968017578125">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79698287136852741241455078125" lon="-80.27394407428801059722900390625">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79695001430809497833251953125" lon="-80.27393301017582416534423828125">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7969199232757091522216796875" lon="-80.27392412535846233367919921875">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79688547365367412567138671875" lon="-80.27391121722757816314697265625">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7968528680503368377685546875" lon="-80.2739015780389308929443359375">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7968190051615238189697265625" lon="-80.27389059774577617645263671875">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.796789668500423431396484375" lon="-80.27388204820454120635986328125">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79675773344933986663818359375" lon="-80.2738707326352596282958984375">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79672755859792232513427734375" lon="-80.27386142872273921966552734375">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:21:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79669419862329959869384765625" lon="-80.27385036461055278778076171875">
        <ele>41</ele>
        <time>2023-07-30T12:21:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7966634370386600494384765625" lon="-80.273840390145778656005859375">
        <ele>41</ele>
        <time>2023-07-30T12:21:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7966319210827350616455078125" lon="-80.27382924221456050872802734375">
        <ele>41</ele>
        <time>2023-07-30T12:21:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7966000698506832122802734375" lon="-80.27382320724427700042724609375">
        <ele>41</ele>
        <time>2023-07-30T12:21:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79656997881829738616943359375" lon="-80.27381197549402713775634765625">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:21:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.796537876129150390625" lon="-80.2738006599247455596923828125">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:21:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.796508036553859710693359375" lon="-80.27379102073609828948974609375">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:21:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.796475179493427276611328125" lon="-80.27377903461456298828125">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:21:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7964475192129611968994140625" lon="-80.27377241291105747222900390625">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:21:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79641449451446533203125" lon="-80.273762606084346771240234375">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:21:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79638524167239665985107421875" lon="-80.2737537212669849395751953125">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:21:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7963536418974399566650390625" lon="-80.27374139986932277679443359375">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:21:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79632271267473697662353515625" lon="-80.2737315930426120758056640625">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79629119671881198883056640625" lon="-80.27372128330171108245849609375">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79625800438225269317626953125" lon="-80.27370988391339778900146484375">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79622699134051799774169921875" lon="-80.27370099909603595733642578125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7961900271475315093994140625" lon="-80.273688174784183502197265625">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7961581759154796600341796875" lon="-80.27367912232875823974609375">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7961256541311740875244140625" lon="-80.2736655436456203460693359375">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79609623365104198455810546875" lon="-80.27365439571440219879150390625">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79606538824737071990966796875" lon="-80.27364148758351802825927734375">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7960336208343505859375" lon="-80.2736303396522998809814453125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7960022725164890289306640625" lon="-80.27361935935914516448974609375">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79596832580864429473876953125" lon="-80.27360737323760986328125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.795937061309814453125" lon="-80.2735981531441211700439453125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7959033660590648651123046875" lon="-80.273586250841617584228515625">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79587210156023502349853515625" lon="-80.2735780365765094757080078125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7958366461098194122314453125" lon="-80.27356755919754505157470703125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:21:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79580571688711643218994140625" lon="-80.2735595963895320892333984375">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:21:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79577436856925487518310546875" lon="-80.27354828082025051116943359375">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:22:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79574587009847164154052734375" lon="-80.273540318012237548828125">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:22:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>165</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7957142703235149383544921875" lon="-80.27353025972843170166015625">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:22:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79568292200565338134765625" lon="-80.27352028526365756988525390625">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.795651741325855255126953125" lon="-80.27351173572242259979248046875">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7956219017505645751953125" lon="-80.27350117452442646026611328125">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79559281654655933380126953125" lon="-80.27349178679287433624267578125">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7955617196857929229736328125" lon="-80.27347954921424388885498046875">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79552936553955078125" lon="-80.273473262786865234375">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79549575410783290863037109375" lon="-80.27346127666532993316650390625">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79546641744673252105712890625" lon="-80.27345423586666584014892578125">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7954349853098392486572265625" lon="-80.27344174683094024658203125">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.795404307544231414794921875" lon="-80.27343294583261013031005859375">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7953712828457355499267578125" lon="-80.27342305518686771392822265625">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79533951543271541595458984375" lon="-80.273413248360157012939453125">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7953082509338855743408203125" lon="-80.27340411208570003509521484375">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79527447186410427093505859375" lon="-80.27339338324964046478271484375">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:22:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79524597339332103729248046875" lon="-80.2733869291841983795166015625">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:22:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.795213200151920318603515625" lon="-80.27337645180523395538330078125">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:22:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79518344439566135406494140625" lon="-80.27336706407368183135986328125">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:22:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79514991678297519683837890625" lon="-80.273355580866336822509765625">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:22:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.795119993388652801513671875" lon="-80.2733463607728481292724609375">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:22:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7950886450707912445068359375" lon="-80.27333588339388370513916015625">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:22:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.795057632029056549072265625" lon="-80.2733259089291095733642578125">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:22:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79502552933990955352783203125" lon="-80.27331878431141376495361328125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:22:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79499300755560398101806640625" lon="-80.27331031858921051025390625">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:22:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79495897702872753143310546875" lon="-80.27330805547535419464111328125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:22:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79492176137864589691162109375" lon="-80.2732997573912143707275390625">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:22:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7948868088424205780029296875" lon="-80.27329430915415287017822265625">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:22:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79485336504876613616943359375" lon="-80.2732883580029010772705078125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:22:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79481832869350910186767578125" lon="-80.27328425087034702301025390625">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:22:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.794781364500522613525390625" lon="-80.2732850052416324615478515625">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:22:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.794747836887836456298828125" lon="-80.273283161222934722900390625">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:22:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79471305198967456817626953125" lon="-80.27327938936650753021240234375">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:22:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79468136839568614959716796875" lon="-80.27326966635882854461669921875">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:22:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79464951716363430023193359375" lon="-80.27326446957886219024658203125">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:22:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7946152351796627044677734375" lon="-80.2732595242559909820556640625">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:22:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7945803664624691009521484375" lon="-80.27325265109539031982421875">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:22:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79454658739268779754638671875" lon="-80.27325105853378772735595703125">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:22:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79451347887516021728515625" lon="-80.2732457779347896575927734375">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:22:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.794480621814727783203125" lon="-80.27324385009706020355224609375">
        <ele>41</ele>
        <time>2023-07-30T12:22:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79444499872624874114990234375" lon="-80.2732404135167598724365234375">
        <ele>41</ele>
        <time>2023-07-30T12:22:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.794410884380340576171875" lon="-80.2732417546212673187255859375">
        <ele>41</ele>
        <time>2023-07-30T12:22:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79437677003443241119384765625" lon="-80.27324116788804531097412109375">
        <ele>41</ele>
        <time>2023-07-30T12:22:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79434324242174625396728515625" lon="-80.273240916430950164794921875">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:22:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.794310636818408966064453125" lon="-80.27324133552610874176025390625">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:22:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79427618719637393951416015625" lon="-80.27323848567903041839599609375">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:22:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7942450903356075286865234375" lon="-80.27324066497385501861572265625">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:22:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79420879669487476348876953125" lon="-80.27323965914547443389892578125">
        <ele>41.40000152587890625</ele>
        <time>2023-07-30T12:22:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79417845420539379119873046875" lon="-80.27324100024998188018798828125">
        <ele>41.40000152587890625</ele>
        <time>2023-07-30T12:22:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7941448427736759185791015625" lon="-80.2732394076883792877197265625">
        <ele>41.40000152587890625</ele>
        <time>2023-07-30T12:22:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79411324299871921539306640625" lon="-80.27323915623128414154052734375">
        <ele>41.59999847412109375</ele>
        <time>2023-07-30T12:22:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79408105649054050445556640625" lon="-80.2732407487928867340087890625">
        <ele>41.59999847412109375</ele>
        <time>2023-07-30T12:22:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.794047780334949493408203125" lon="-80.27324100024998188018798828125">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:22:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7940171025693416595458984375" lon="-80.27324267663061618804931640625">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:22:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7939822338521480560302734375" lon="-80.27324317954480648040771484375">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:22:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79395096935331821441650390625" lon="-80.27324602939188480377197265625">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:22:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7939160168170928955078125" lon="-80.27324728667736053466796875">
        <ele>42</ele>
        <time>2023-07-30T12:22:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79388424940407276153564453125" lon="-80.2732531540095806121826171875">
        <ele>42</ele>
        <time>2023-07-30T12:22:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.793847955763339996337890625" lon="-80.2732541598379611968994140625">
        <ele>42</ele>
        <time>2023-07-30T12:22:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.793817780911922454833984375" lon="-80.27325659058988094329833984375">
        <ele>42</ele>
        <time>2023-07-30T12:23:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79378752224147319793701171875" lon="-80.2732595242559909820556640625">
        <ele>42</ele>
        <time>2023-07-30T12:23:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79375466518104076385498046875" lon="-80.27326011098921298980712890625">
        <ele>42</ele>
        <time>2023-07-30T12:23:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79372499324381351470947265625" lon="-80.27326262556016445159912109375">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:23:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7936918847262859344482421875" lon="-80.27326396666467189788818359375">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:23:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.793660871684551239013671875" lon="-80.2732694149017333984375">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:23:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79362608678638935089111328125" lon="-80.27327033691108226776123046875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:23:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79359490610659122467041015625" lon="-80.27327276766300201416015625">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:23:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79356204904615879058837890625" lon="-80.2732722647488117218017578125">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:23:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7935302816331386566162109375" lon="-80.2732734382152557373046875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:23:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79349734075367450714111328125" lon="-80.27327419258654117584228515625">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:23:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79346347786486148834228515625" lon="-80.2732752822339534759521484375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:23:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79343179427087306976318359375" lon="-80.27327804826200008392333984375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:23:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7933988533914089202880859375" lon="-80.2732796408236026763916015625">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:23:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79336557723581790924072265625" lon="-80.2732833288609981536865234375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:23:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7933307923376560211181640625" lon="-80.273283161222934722900390625">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:23:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79329843819141387939453125" lon="-80.2732853405177593231201171875">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:23:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7932640723884105682373046875" lon="-80.2732870168983936309814453125">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:23:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79323272407054901123046875" lon="-80.27329095639288425445556640625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:23:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7931976877152919769287109375" lon="-80.2732917107641696929931640625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:23:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.793164663016796112060546875" lon="-80.27329447679221630096435546875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:23:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7931296266615390777587890625" lon="-80.2732957340776920318603515625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:23:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79309836216270923614501953125" lon="-80.2732990868389606475830078125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:23:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79306483455002307891845703125" lon="-80.2733007632195949554443359375">
        <ele>43</ele>
        <time>2023-07-30T12:23:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79303289949893951416015625" lon="-80.27330227196216583251953125">
        <ele>43</ele>
        <time>2023-07-30T12:23:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79300079680979251861572265625" lon="-80.27330386452376842498779296875">
        <ele>43</ele>
        <time>2023-07-30T12:23:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7929656766355037689208984375" lon="-80.27330436743795871734619140625">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:23:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.792935334146022796630859375" lon="-80.273308642208576202392578125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:23:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7928999625146389007568359375" lon="-80.2733109891414642333984375">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:23:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79286802746355533599853515625" lon="-80.2733161859214305877685546875">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:23:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79283165000379085540771484375" lon="-80.2733205445110797882080078125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79279979877173900604248046875" lon="-80.27332632802426815032958984375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79276442714035511016845703125" lon="-80.2733299322426319122314453125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7927319891750812530517578125" lon="-80.27333370409905910491943359375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79269821010529994964599609375" lon="-80.27333638630807399749755859375">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79266317375004291534423828125" lon="-80.27334091253578662872314453125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79262981377542018890380859375" lon="-80.27334694750607013702392578125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79259544797241687774658203125" lon="-80.273350887000560760498046875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7925616689026355743408203125" lon="-80.2733547426760196685791015625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79252369888126850128173828125" lon="-80.27335683815181255340576171875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79249377548694610595703125" lon="-80.2733621187508106231689453125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>168</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7924597449600696563720703125" lon="-80.273364968597888946533203125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:23:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>168</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7924292348325252532958984375" lon="-80.2733674831688404083251953125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:23:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>168</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7923926897346973419189453125" lon="-80.273369662463665008544921875">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:23:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>168</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79235706664621829986572265625" lon="-80.27337469160556793212890625">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:23:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7923195995390415191650390625" lon="-80.27337846346199512481689453125">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:23:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79228213243186473846435546875" lon="-80.27338198386132717132568359375">
        <ele>43.799999237060546875</ele>
        <time>2023-07-30T12:23:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.792248688638210296630859375" lon="-80.27338785119354724884033203125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:23:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.792212478816509246826171875" lon="-80.27338944375514984130859375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:23:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.792179621756076812744140625" lon="-80.27339355088770389556884765625">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:23:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7921419031918048858642578125" lon="-80.27339505963027477264404296875">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:23:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79210887849330902099609375" lon="-80.273396484553813934326171875">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:23:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7920721657574176788330078125" lon="-80.27339338324964046478271484375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:23:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79203469865024089813232421875" lon="-80.27339170686900615692138671875">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:23:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7920004166662693023681640625" lon="-80.2733919583261013031005859375">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:23:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.791964709758758544921875" lon="-80.27339254505932331085205078125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:23:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79193344525992870330810546875" lon="-80.2733941376209259033203125">
        <ele>43.59999847412109375</ele>
        <time>2023-07-30T12:23:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7918971516191959381103515625" lon="-80.2733929641544818878173828125">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79186320491135120391845703125" lon="-80.27339623309671878814697265625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79182464815676212310791015625" lon="-80.27339749038219451904296875">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:23:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79179128818213939666748046875" lon="-80.2734016813337802886962890625">
        <ele>43.40000152587890625</ele>
        <time>2023-07-30T12:24:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79175457544624805450439453125" lon="-80.27340377680957317352294921875">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:24:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.791720293462276458740234375" lon="-80.2734088897705078125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:24:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79168433509767055511474609375" lon="-80.27341132052242755889892578125">
        <ele>43.200000762939453125</ele>
        <time>2023-07-30T12:24:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79164829291403293609619140625" lon="-80.27341366745531558990478515625">
        <ele>43</ele>
        <time>2023-07-30T12:24:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7916108258068561553955078125" lon="-80.27341567911207675933837890625">
        <ele>43</ele>
        <time>2023-07-30T12:24:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7915732748806476593017578125" lon="-80.27341718785464763641357421875">
        <ele>43</ele>
        <time>2023-07-30T12:24:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7915407530963420867919921875" lon="-80.2734217979013919830322265625">
        <ele>43</ele>
        <time>2023-07-30T12:24:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79150563292205333709716796875" lon="-80.27342355810105800628662109375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:24:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79147428460419178009033203125" lon="-80.27342833578586578369140625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:24:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79143841005861759185791015625" lon="-80.27342967689037322998046875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:24:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7914066426455974578857421875" lon="-80.2734345383942127227783203125">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:24:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79137034900486469268798828125" lon="-80.27343663387000560760498046875">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:24:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79133975505828857421875" lon="-80.2734419144690036773681640625">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:24:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79130287468433380126953125" lon="-80.27344468049705028533935546875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:24:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7912695147097110748291015625" lon="-80.273448787629604339599609375">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:24:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79123238287866115570068359375" lon="-80.27345071546733379364013671875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:24:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79119583778083324432373046875" lon="-80.2734539844095706939697265625">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:24:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.791161723434925079345703125" lon="-80.2734553255140781402587890625">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:24:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79112878255546092987060546875" lon="-80.2734573371708393096923828125">
        <ele>42</ele>
        <time>2023-07-30T12:24:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.791092656552791595458984375" lon="-80.273456834256649017333984375">
        <ele>42</ele>
        <time>2023-07-30T12:24:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7910572849214076995849609375" lon="-80.273458845913410186767578125">
        <ele>42</ele>
        <time>2023-07-30T12:24:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79102149419486522674560546875" lon="-80.27346010319888591766357421875">
        <ele>42</ele>
        <time>2023-07-30T12:24:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79098905622959136962890625" lon="-80.27346194721758365631103515625">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79095360077917575836181640625" lon="-80.27346731163561344146728515625">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79091889970004558563232421875" lon="-80.2734717540442943572998046875">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79088553972542285919189453125" lon="-80.273476280272006988525390625">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79085226356983184814453125" lon="-80.27347988449037075042724609375">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7908207476139068603515625" lon="-80.27348357252776622772216796875">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7907893992960453033447265625" lon="-80.27348625473678112030029296875">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79075796715915203094482421875" lon="-80.2734898589551448822021484375">
        <ele>41.59999847412109375</ele>
        <time>2023-07-30T12:24:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7907260321080684661865234375" lon="-80.2734906971454620361328125">
        <ele>41.59999847412109375</ele>
        <time>2023-07-30T12:24:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.790696360170841217041015625" lon="-80.2734928764402866363525390625">
        <ele>41.59999847412109375</ele>
        <time>2023-07-30T12:24:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79066325165331363677978515625" lon="-80.27349539101123809814453125">
        <ele>41.59999847412109375</ele>
        <time>2023-07-30T12:24:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79063257388770580291748046875" lon="-80.27349815703928470611572265625">
        <ele>41.59999847412109375</ele>
        <time>2023-07-30T12:24:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79060122556984424591064453125" lon="-80.27350050397217273712158203125">
        <ele>41.59999847412109375</ele>
        <time>2023-07-30T12:24:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7905694581568241119384765625" lon="-80.27350335381925106048583984375">
        <ele>41.59999847412109375</ele>
        <time>2023-07-30T12:24:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79053534381091594696044921875" lon="-80.273505114018917083740234375">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>168</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79050282202661037445068359375" lon="-80.27350821532309055328369140625">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>168</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.790467031300067901611328125" lon="-80.2735089696943759918212890625">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>168</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79043652117252349853515625" lon="-80.27351190336048603057861328125">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>168</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7904001437127590179443359375" lon="-80.27351148426532745361328125">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>168</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79036703519523143768310546875" lon="-80.27351290918886661529541015625">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>168</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.790332920849323272705078125" lon="-80.2735109813511371612548828125">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>168</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79030517674982547760009765625" lon="-80.273513160645961761474609375">
        <ele>41.59999847412109375</ele>
        <time>2023-07-30T12:24:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>90</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79026863165199756622314453125" lon="-80.27351374737918376922607421875">
        <ele>41.59999847412109375</ele>
        <time>2023-07-30T12:24:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79023577459156513214111328125" lon="-80.273516513407230377197265625">
        <ele>41.59999847412109375</ele>
        <time>2023-07-30T12:24:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7902016602456569671630859375" lon="-80.2735183574259281158447265625">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7901716530323028564453125" lon="-80.27352279983460903167724609375">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7901367843151092529296875" lon="-80.2735237218439579010009765625">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79010619036853313446044921875" lon="-80.2735274098813533782958984375">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79007308185100555419921875" lon="-80.27352732606232166290283203125">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.79004248790442943572998046875" lon="-80.27352984063327312469482421875">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:24:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7900068648159503936767578125" lon="-80.273529924452304840087890625">
        <ele>42</ele>
        <time>2023-07-30T12:24:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78997375629842281341552734375" lon="-80.2735336124897003173828125">
        <ele>42</ele>
        <time>2023-07-30T12:24:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.789937295019626617431640625" lon="-80.2735347859561443328857421875">
        <ele>42</ele>
        <time>2023-07-30T12:24:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78990410268306732177734375" lon="-80.2735391445457935333251953125">
        <ele>42</ele>
        <time>2023-07-30T12:24:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.789869569242000579833984375" lon="-80.27354090474545955657958984375">
        <ele>42</ele>
        <time>2023-07-30T12:24:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78983478434383869171142578125" lon="-80.27354501187801361083984375">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:24:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78980092145502567291259765625" lon="-80.2735481970012187957763671875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:24:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>166</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7897664718329906463623046875" lon="-80.273551382124423980712890625">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:25:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.789732106029987335205078125" lon="-80.27355532161891460418701171875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:25:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78969623148441314697265625" lon="-80.2735595963895320892333984375">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:25:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78966127894818782806396484375" lon="-80.273562781512737274169921875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:25:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78962573967874050140380859375" lon="-80.2735632844269275665283203125">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:25:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7895931340754032135009765625" lon="-80.27356521226465702056884765625">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:25:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>167</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7895572595298290252685546875" lon="-80.27356588281691074371337890625">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:25:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>168</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7895247377455234527587890625" lon="-80.27356873266398906707763671875">
        <ele>42.200000762939453125</ele>
        <time>2023-07-30T12:25:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>168</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78948643244802951812744140625" lon="-80.27356957085430622100830078125">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:25:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78945424593985080718994140625" lon="-80.2735750190913677215576171875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:25:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78941719792783260345458984375" lon="-80.2735766954720020294189453125">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:25:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7893852628767490386962890625" lon="-80.27357979677617549896240234375">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:25:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78935047797858715057373046875" lon="-80.2735803835093975067138671875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:25:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.789319045841693878173828125" lon="-80.27358180843293666839599609375">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:25:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78928241692483425140380859375" lon="-80.2735823951661586761474609375">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:25:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78924637474119663238525390625" lon="-80.27358398772776126861572265625">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:25:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.789213769137859344482421875" lon="-80.27358516119420528411865234375">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:25:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78917948715388774871826171875" lon="-80.273585580289363861083984375">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:25:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78914579190313816070556640625" lon="-80.27358633466064929962158203125">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:25:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78911276720464229583740234375" lon="-80.27358650229871273040771484375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:25:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78908074833452701568603515625" lon="-80.2735874243080615997314453125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7890439517796039581298828125" lon="-80.2735874243080615997314453125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78901117853820323944091796875" lon="-80.27358935214579105377197265625">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:25:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78897144831717014312744140625" lon="-80.27358968742191791534423828125">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:25:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78893850743770599365234375" lon="-80.27359320782124996185302734375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:25:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7889021299779415130615234375" lon="-80.27359287254512310028076171875">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:25:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>171</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78886902146041393280029296875" lon="-80.2735931240022182464599609375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:25:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>171</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78883314691483974456787109375" lon="-80.27358985505998134613037109375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:25:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>171</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7888012118637561798095703125" lon="-80.27359019033610820770263671875">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:25:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>171</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7887686900794506072998046875" lon="-80.27359069325029850006103515625">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:25:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>171</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78873692266643047332763671875" lon="-80.27359253726899623870849609375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:25:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7887044847011566162109375" lon="-80.2735921181738376617431640625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7886714600026607513427734375" lon="-80.273592956364154815673828125">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:25:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78864120133221149444580078125" lon="-80.27359521947801113128662109375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78860683552920818328857421875" lon="-80.27359521947801113128662109375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78857238590717315673828125" lon="-80.27359806932508945465087890625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7885350026190280914306640625" lon="-80.27359924279153347015380859375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.788502313196659088134765625" lon="-80.27360242791473865509033203125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7884679473936557769775390625" lon="-80.27360460720956325531005859375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78843693435192108154296875" lon="-80.2736095525324344635009765625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78840265236794948577880859375" lon="-80.2736118994653224945068359375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7883683703839778900146484375" lon="-80.27361500076949596405029296875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.788330651819705963134765625" lon="-80.273615084588527679443359375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.788295783102512359619140625" lon="-80.273617766797542572021484375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7882604114711284637451171875" lon="-80.273620449006557464599609375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7882238663733005523681640625" lon="-80.27362271212041378021240234375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78818908147513866424560546875" lon="-80.2736259810626506805419921875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78815622441470623016357421875" lon="-80.2736259810626506805419921875">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78812219388782978057861328125" lon="-80.27362925000488758087158203125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7880864031612873077392578125" lon="-80.27363084256649017333984375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78805346228182315826416015625" lon="-80.273634530603885650634765625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78801809065043926239013671875" lon="-80.273633189499378204345703125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78798556886613368988037109375" lon="-80.2736341953277587890625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7879481017589569091796875" lon="-80.27363411150872707366943359375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>169</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78791348449885845184326171875" lon="-80.2736355364322662353515625">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7878751792013645172119140625" lon="-80.27363880537450313568115234375">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7878409810364246368408203125" lon="-80.273641236126422882080078125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78780401684343814849853515625" lon="-80.27364467270672321319580078125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78777015395462512969970703125" lon="-80.273646600544452667236328125">
        <ele>42.799999237060546875</ele>
        <time>2023-07-30T12:25:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7877377159893512725830078125" lon="-80.2736497856676578521728515625">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:25:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7877011708915233612060546875" lon="-80.27364986948668956756591796875">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:26:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78766823001205921173095703125" lon="-80.2736528031527996063232421875">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:26:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78763084672391414642333984375" lon="-80.27365355752408504486083984375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:26:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.787597067654132843017578125" lon="-80.273655988276004791259765625">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:26:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78756094165146350860595703125" lon="-80.27365590445697307586669921875">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:26:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.787526659667491912841796875" lon="-80.2736591733992099761962890625">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:26:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78749153949320316314697265625" lon="-80.2736618556082248687744140625">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:26:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78745532967150211334228515625" lon="-80.27366428636014461517333984375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:26:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78742171823978424072265625" lon="-80.2736675553023815155029296875">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:26:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7873856760561466217041015625" lon="-80.2736699022352695465087890625">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:26:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78735239990055561065673828125" lon="-80.27367384172976016998291015625">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:26:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78731417842209339141845703125" lon="-80.27367752976715564727783203125">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:26:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7872810699045658111572265625" lon="-80.27368440292775630950927734375">
        <ele>42.59999847412109375</ele>
        <time>2023-07-30T12:26:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7872426807880401611328125" lon="-80.27368851006031036376953125">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>94</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78720772825181484222412109375" lon="-80.27369278483092784881591796875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78717093169689178466796875" lon="-80.27369479648768901824951171875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7871362306177616119384765625" lon="-80.2736927010118961334228515625">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78710228390991687774658203125" lon="-80.27368574403226375579833984375">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7870715223252773284912109375" lon="-80.2736730873584747314453125">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7870390005409717559814453125" lon="-80.273662023246288299560546875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78700270690023899078369140625" lon="-80.273648612201213836669921875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78696842491626739501953125" lon="-80.27364064939320087432861328125">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7869300357997417449951171875" lon="-80.27362874709069728851318359375">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78689189814031124114990234375" lon="-80.27361609041690826416015625">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>91</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78684915043413639068603515625" lon="-80.27360728941857814788818359375">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786807827651500701904296875" lon="-80.27359471656382083892822265625">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78677036054432392120361328125" lon="-80.273573510348796844482421875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>96</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786728031933307647705078125" lon="-80.27355129830539226531982421875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>97</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7866869606077671051025390625" lon="-80.27353185229003429412841796875">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>101</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78664597310125827789306640625" lon="-80.27350804768502712249755859375">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>170</ns3:hr>
            <ns3:cad>102</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78660573996603488922119140625" lon="-80.27347804047167301177978515625">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>171</ns3:hr>
            <ns3:cad>103</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78657439164817333221435546875" lon="-80.27344434522092342376708984375">
        <ele>42.40000152587890625</ele>
        <time>2023-07-30T12:26:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>171</ns3:hr>
            <ns3:cad>104</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78655041940510272979736328125" lon="-80.27340142987668514251708984375">
        <ele>42</ele>
        <time>2023-07-30T12:26:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>171</ns3:hr>
            <ns3:cad>105</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786537595093250274658203125" lon="-80.2733507193624973297119140625">
        <ele>42</ele>
        <time>2023-07-30T12:26:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>171</ns3:hr>
            <ns3:cad>105</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78652988374233245849609375" lon="-80.27329514734447002410888671875">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:26:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>172</ns3:hr>
            <ns3:cad>106</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78652778826653957366943359375" lon="-80.27324586175382137298583984375">
        <ele>41.799999237060546875</ele>
        <time>2023-07-30T12:26:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>172</ns3:hr>
            <ns3:cad>106</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78652393259108066558837890625" lon="-80.27319607324898242950439453125">
        <ele>41.40000152587890625</ele>
        <time>2023-07-30T12:26:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>173</ns3:hr>
            <ns3:cad>103</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78651957400143146514892578125" lon="-80.2731438539922237396240234375">
        <ele>41.200000762939453125</ele>
        <time>2023-07-30T12:26:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>174</ns3:hr>
            <ns3:cad>103</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78651605360209941864013671875" lon="-80.2730925567448139190673828125">
        <ele>41</ele>
        <time>2023-07-30T12:26:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>174</ns3:hr>
            <ns3:cad>104</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78651286847889423370361328125" lon="-80.27304687537252902984619140625">
        <ele>41</ele>
        <time>2023-07-30T12:26:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>174</ns3:hr>
            <ns3:cad>107</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78650716878473758697509765625" lon="-80.2730043791234493255615234375">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:26:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>175</ns3:hr>
            <ns3:cad>107</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786501385271549224853515625" lon="-80.2729597873985767364501953125">
        <ele>40.799999237060546875</ele>
        <time>2023-07-30T12:26:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>175</ns3:hr>
            <ns3:cad>107</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78649996034801006317138671875" lon="-80.27291092090308666229248046875">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:26:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>175</ns3:hr>
            <ns3:cad>106</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7865018881857395172119140625" lon="-80.2728692628443241119384765625">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:26:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>176</ns3:hr>
            <ns3:cad>106</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78651135973632335662841796875" lon="-80.2728256769478321075439453125">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:26:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>178</ns3:hr>
            <ns3:cad>105</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78653332032263278961181640625" lon="-80.27278862893581390380859375">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:26:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>179</ns3:hr>
            <ns3:cad>103</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78656953014433383941650390625" lon="-80.2727663330733776092529296875">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:26:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>179</ns3:hr>
            <ns3:cad>103</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786614038050174713134765625" lon="-80.272751748561859130859375">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:26:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>179</ns3:hr>
            <ns3:cad>103</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786658294498920440673828125" lon="-80.272746048867702484130859375">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:26:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>180</ns3:hr>
            <ns3:cad>103</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78670690953731536865234375" lon="-80.27274227701127529144287109375">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:26:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>180</ns3:hr>
            <ns3:cad>97</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7867442928254604339599609375" lon="-80.27273925952613353729248046875">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:26:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>180</ns3:hr>
            <ns3:cad>95</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78678645379841327667236328125" lon="-80.27273439802229404449462890625">
        <ele>40.200000762939453125</ele>
        <time>2023-07-30T12:26:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>180</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.786823920905590057373046875" lon="-80.27272987179458141326904296875">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:26:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>180</ns3:hr>
            <ns3:cad>92</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7868646569550037384033203125" lon="-80.27272836305201053619384765625">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:26:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>181</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.78689491562545299530029296875" lon="-80.2727294526994228363037109375">
        <ele>40.40000152587890625</ele>
        <time>2023-07-30T12:26:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>182</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="7.7869213186204433441162109375" lon="-80.27272685430943965911865234375">
        <ele>40.59999847412109375</ele>
        <time>2023-07-30T12:26:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:hr>182</ns3:hr>
            <ns3:cad>93</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
    </trkseg>
  </trk>
</gpx>
`;
