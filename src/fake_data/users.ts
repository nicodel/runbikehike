import { fakeAvatarsData } from './profilesAvatars';
import { fakeProfiles } from './usersProfiles';

const fakeUsers = {
  forrest_gump: {
    id: 'a9202021-d1fe-470c-b00f-ceb85084cbea',
    email: 'forrest@gump.rbh',
    password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
    is_admin: false,
    is_disabled: false,
  },
  chloe_mccardell: {
    id: '59cb9131-cd96-4318-8899-d7f564d63f64',
    email: 'chloe@mccardell.rbh',
    password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
    is_admin: false,
    is_disabled: false,
  },
  casquette_verte: {
    id: '4ee99abe-41b0-49fd-91a3-129a334295ce',
    email: 'casquette@verte.rbh',
    password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
    is_admin: false,
    is_disabled: true,
  },
  antoine_albeau: {
    id: '2cf6e163-2a23-4da4-bf21-42a09c0175bd',
    email: 'antoine@albeau.rbh',
    password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
    is_admin: true,
    is_disabled: false,
  },
};

const fakeUsersNameAndAvatar = {
  forrest_gump: {
    id: fakeUsers.forrest_gump.id,
    name: fakeProfiles.forrest_gump.name,
    avatar: fakeAvatarsData.forrest_gump,
  },
  chloe_mccardell: {
    id: fakeUsers.chloe_mccardell.id,
    name: fakeProfiles.chloe_mccardell.name,
    avatar: fakeAvatarsData.chloe_mccardell,
  },
  casquette_verte: {
    id: fakeUsers.casquette_verte.id,
    name: fakeProfiles.casquette_verte.name,
    avatar: fakeAvatarsData.casquette_verte,
  },
};

export { fakeUsers, fakeUsersNameAndAvatar };
