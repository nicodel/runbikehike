import { fakeAvatars, fakeAvatarsData } from './profilesAvatars';

const fakeProfiles = {
  forrest_gump: {
    id: 'a5a7b009-c5be-41e5-ba57-8640681c0cdc',
    name: 'Forrest Gump',
    gender: 'male',
    birthyear: 1944,
    height: 183,
    weight: 80,
    location: 'Greenbow, Alabama',
    biography:
      'Forrest Alexander Gump is a fictional character and the protagonist of the 1986 novel by Winston Groom, Robert Zemeckis 1994 film of the same name, and Gump and Co., the written sequel to Groom novel.',
    user: {
      id: 'a9202021-d1fe-470c-b00f-ceb85084cbea',
      email: 'forrest@gump.rbh',
      password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
      is_admin: false,
      is_disabled: false,
    },
    avatar: {
      id: 'd81f5d0d-3410-486c-a595-cf86e62f8906',
      mimetype: fakeAvatarsData.forrest_gump.type,
      data: fakeAvatarsData.forrest_gump.data,
    },
  },
  chloe_mccardell: {
    id: '04a724b1-e7f2-445c-ab82-d5b419bc4462',
    name: 'Chloë McCardel',
    gender: 'female',
    birthyear: 1985,
    height: 165,
    weight: 65,
    location: 'Melbourne',
    biography:
      'McCardel past swims include forty-four solo crossings of the English Channel, including eight crossings in one season and three crossings in one week, three double-crossings in 2010, 2012 and 2017 and, in 2015, the fourth person to do a non-stop triple-crossing. She also won the 28.5-mile (46-kilometer) Manhattan Island Marathon Swim in 2010. As of 2021, she holds the world record for the longest unassisted ocean swim, at 124.4 km.',
    user: {
      id: '59cb9131-cd96-4318-8899-d7f564d63f64',
      email: 'chloe@mccardell.rbh',
      password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
      is_admin: false,
      is_disabled: false,
    },
    avatar: {
      id: '8b5604e0-5ff4-430f-9568-f4bb681dd43e',
      mimetype: fakeAvatarsData.chloe_mccardell.type,
      data: fakeAvatarsData.chloe_mccardell.data,
    },
  },
  casquette_verte: {
    id: '2024e2f2-c550-48d4-b6e4-ab79d384b203',
    name: 'Alex Boucheix',
    gender: 'male',
    birthyear: 1984,
    height: 178,
    weight: 80,
    location: 'Paris, FR',
    biography: '',
    user: {
      id: '4ee99abe-41b0-49fd-91a3-129a334295ce',
      email: 'casquette@verte.rbh',
      password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
      is_admin: false,
      is_disabled: true,
    },
    avatar: {
      id: '89e4cb41-435c-4017-bf12-ec6a93e0771d',
      mimetype: fakeAvatarsData.casquette_verte.type,
      data: fakeAvatarsData.casquette_verte.data,
    },
  },

  antoine_albeau: {
    id: '9659321a-4890-435d-b780-272d20e0b624',
    name: 'Antoine Albeau',
    gender: 'male',
    birthyear: 1972,
    height: 185,
    weight: 100,
    location: 'La Couarde-sur-Mer, FR',
    biography: '',
    user: {
      id: '2cf6e163-2a23-4da4-bf21-42a09c0175bd',
      email: 'antoine@albeau.rbh',
      password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
      is_admin: true,
      is_disabled: false,
    },
    avatar: fakeAvatars.antoine_albeau,
  },
};

export { fakeProfiles };
