// import path from 'path';
// import { readFileSync } from 'fs';

import { DateTime } from 'luxon';
import { fakeActivities } from './activities';
// import { fakeSessionJSON } from './gpsSession/json';
// import { fakeProfiles } from './usersProfiles';
// import { fakeAvatarsData } from './profilesAvatars';
import { fakeUsers } from './users';
import { fakeSessionJSON } from './gpsSession/json';

const fakeSessions = [
  {
    id: '4010fa35-756c-419e-b69f-08d82d52b666',
    name: 'Amet purus gravida quis blandit',
    start_date_time: DateTime.now().toISO(),
    duration: 1 * 60 * 60 + 32 * 60 + 27, // 1:32:27
    distance: 20.12 * 1000, // 20.12 km
    /*  */
    burned_calories: 1660,
    average_heart_rate: 159,
    elevation_gain: 76,
    /*  */
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Proin fermentum leo vel orci porta non. Sit amet volutpat consequat mauris nunc congue nisi vitae suscipit. Cursus metus aliquam eleifend mi in. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida.',
    average_speed: (20.12 * 1000) / (1 * 60 * 60 + 32 * 60 + 27), // 4:36 /km
    gps_track: null,
    location: 'Congue, Malesuada',
    nb_comments: 2,
    nb_likes: 3,
    is_favorite: true,
    user: fakeUsers.casquette_verte,
    activity: fakeActivities.running,
    images: null,
  },

  {
    id: 'ebe9ac13-c3aa-49fb-9b45-c1d2ac27f0eb',
    name: 'A pellentesque sit amet porttitor eget',
    start_date_time: DateTime.now()
      .minus({ hours: Math.floor(Math.random() * 12), minutes: Math.floor(Math.random() * 59) })
      .toISO(),
    duration: 4 * 60 * 60 + 19 * 60 + 42, // 4:19:42
    distance: 16.8 * 1000, // 16.80 km
    /*  */
    burned_calories: 1725,
    average_heart_rate: 108,
    elevation_gain: 929,
    /*  */
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Accumsan lacus vel facilisis volutpat est velit egestas dui id. Libero justo laoreet sit amet cursus sit. Velit dignissim sodales ut eu sem integer.',
    average_speed: (16.8 * 1000) / (4 * 60 * 60 + 19 * 60 + 42), // 15:28 /km
    gps_track: null,
    location: 'Egestas, Lacus',
    nb_comments: 2,
    nb_likes: 0,
    is_favorite: false,
    user: fakeUsers.forrest_gump,
    activity: fakeActivities.hiking,
  },

  {
    id: 'f5d764cf-d1a7-4bc4-baab-9187267b38f1',
    name: 'Egestas purus viverra accumsan 🏥🩺🏃',
    start_date_time: DateTime.now()
      .minus({ day: 1 })
      .plus({ hours: Math.floor(Math.random() * 12), minutes: Math.floor(Math.random() * 59) })
      .toISO(),
    duration: 20 * 60 + 44, // 20:44
    distance: 5040,
    /*  */
    burned_calories: 416,
    average_heart_rate: 156,
    elevation_gain: 15,
    /*  */
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    average_speed: 5040 / (20 * 60 + 44), // 4:31 /km
    gps_track: fakeSessionJSON,
    location: 'Tellus, Elementum',
    nb_comments: 0,
    nb_likes: 3,
    is_favorite: false,
    user: fakeUsers.forrest_gump,
    activity: fakeActivities.running,
  },

  {
    id: 'ecf8ff2d-c57d-4885-bf49-6466330c5d95',
    name: 'Vivamus at augue',
    start_date_time: DateTime.now()
      .minus({ day: 2 })
      .plus({ hours: Math.floor(Math.random() * 12), minutes: Math.floor(Math.random() * 59) })
      .toISO(),
    duration: 46 * 60 + 37, // 46:37
    distance: 2500,
    /*  */
    burned_calories: 772,
    average_heart_rate: null,
    elevation_gain: null,
    /*  */
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eget est lorem ipsum dolor. Leo a diam sollicitudin tempor id eu nisl. Id venenatis a condimentum vitae sapien pellentesque habitant morbi. Feugiat pretium nibh ipsum consequat nisl vel pretium. Morbi non arcu risus quis varius quam quisque id diam. Sed arcu non odio euismod lacinia at quis.',
    average_speed: 2500 / (46 * 60 + 37), // 1:52 /100 mètres
    gps_track: null,
    location: 'Quisque, Lobortis',
    nb_comments: 0,
    nb_likes: 1,
    is_favorite: false,
    user: fakeUsers.chloe_mccardell,
    activity: fakeActivities.swimming,
  },

  {
    id: '4bfe2d2d-e286-4d88-b43c-b9ee29ca88df',
    name: 'Adipiscing elit ut aliquam',
    start_date_time: DateTime.now()
      .minus({ day: 3 })
      .plus({ hours: Math.floor(Math.random() * 12), minutes: Math.floor(Math.random() * 59) })
      .toISO(),
    duration: 3 * 60 * 60 + 37 * 60 + 49, // 3:37:49
    distance: 17.12 * 1000,
    /*  */
    burned_calories: 2013,
    average_heart_rate: 124,
    elevation_gain: 891,
    /*  */
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Interdum velit laoreet id donec ultrices tincidunt arcu non sodales. Vitae ultricies leo integer malesuada nunc. Mauris ultrices eros in cursus turpis massa tincidunt dui ut. Sit amet dictum sit amet justo donec enim diam. Nunc sed velit dignissim sodales ut eu sem integer. Integer feugiat scelerisque varius morbi enim nunc faucibus a. Ullamcorper malesuada proin libero nunc consequat interdum varius sit amet. Orci ac auctor augue mauris augue neque gravida in. In est ante in nibh mauris cursus. Id nibh tortor id aliquet lectus proin nibh nisl. Suscipit adipiscing bibendum est ultricies integer. Elementum nibh tellus molestie nunc. Aliquet sagittis id consectetur purus. In est ante in nibh mauris cursus. Dui ut ornare lectus sit amet. Elit duis tristique sollicitudin nibh sit amet commodo nulla.',
    average_speed: (17.12 * 1000) / (3 * 60 * 60 + 37 * 60 + 49), // 12:43 /km
    gps_track: null,
    location: 'Tincidunt, Proin',
    nb_comments: 0,
    nb_likes: 1,
    is_favorite: false,
    user: fakeUsers.forrest_gump,
    activity: fakeActivities.walking,
  },

  {
    id: '26ced29d-c3ec-4466-805d-4baec4d42695',
    name: 'In eu mi bibendum',
    start_date_time: DateTime.now()
      .minus({ day: 6 })
      .plus({ hours: Math.floor(Math.random() * 12), minutes: Math.floor(Math.random() * 59) })
      .toISO(),
    duration: 30 * 60 + 33, // 30:33
    distance: 2000,
    /*  */
    burned_calories: 560,
    average_heart_rate: 159,
    elevation_gain: null,
    /*  */
    description: null,
    average_speed: 2000 / (30 * 60 + 33), // 1:21 min/100m
    gps_track: null,
    location: null,
    nb_comments: 0,
    nb_likes: 0,
    is_favorite: false,
    user: fakeUsers.chloe_mccardell,
    activity: fakeActivities.swimming,
  },

  {
    id: 'cf3bb74e-d181-4bb5-a069-97dd125a4f8e',
    name: 'Elit ullamcorper dignissim',
    start_date_time: DateTime.now()
      .minus({ day: 7 })
      .plus({ hours: Math.floor(Math.random() * 12), minutes: Math.floor(Math.random() * 59) })
      .toISO(),
    duration: 2 * 45 * 60,
    distance: null,
    /*  */
    burned_calories: 360,
    average_heart_rate: null,
    elevation_gain: null,
    /*  */
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque ornare aenean euismod elementum. Fusce id velit ut tortor pretium viverra suspendisse potenti nullam.',
    average_speed: null,
    gps_track: null,
    location: 'Neque, Convallis',
    nb_comments: 0,
    nb_likes: 0,
    is_favorite: false,
    user: fakeUsers.casquette_verte,
    activity: fakeActivities.football,
  },

  {
    id: 'e09ade35-f953-4f98-aa1d-035be98eec23',
    name: 'Five times across the US',
    start_date_time: '1976-07-06T10:00:00.000Z',
    start_date: '1976-07-06',
    start_time: '10:00:00',
    duration: 37987200, // 3 years, 2 months, 14 days and 16 hours
    distance: 25000000,
    average_speed: 0.658116418161907,
    burned_calories: 5318208,
    average_heart_rate: null,
    elevation_gain: null,
    nb_comments: 0,
    location: 'Greenbow, Alabama, USA',
    nb_likes: 0,
    is_favorite: false,
    description:
      'Heartbroken, Forrest, "for no particular reason", starts running and embarks on a cross-country marathon, becoming famous for another feat. Forrest starts to garner many followers, some of whom are struggling businessmen, whom he unwittingly gives inspiration. After a total of about three years and two-and-a half months running, Forrest decides to end the run, and returns to Greenbow, much to the surprise of his followers.',
    user: fakeUsers.forrest_gump,
    activity: fakeActivities.running,
  },

  {
    id: 'c57b28b7-1f79-4ad5-952a-384bd65f2a41',
    name: 'World record: from South Eleuthera Island to Nassau, Bahamas',
    start_date_time: '2014-10-22T10:00:00.000Z',
    start_date: '2014-10-22',
    start_time: '10:00:00',
    duration: 148860, // 41 hours and 16 minutes
    distance: 124400,
    average_speed: 0.835684535805455,
    burned_calories: 28221,
    average_heart_rate: null,
    elevation_gain: null,
    location: '',
    nb_comments: 0,
    nb_likes: 0,
    is_favorite: false,
    description:
      'On 22 October 2014 McCardel completed an unprecedented swim from South Eleuthera Island to Nassau, Bahamas. 124.4 kilometers (77.3 miles) in 41 hours, 21 minutes. She set a new world record, longest unassisted ocean swim, conducted under the ‘Rules of Marathon Swimming’.',
    user: fakeUsers.chloe_mccardell,
    activity: fakeActivities.swimming,
  },
];

/*const fakeSessions = {
  forrest_gump: {
    id: 'e09ade35-f953-4f98-aa1d-035be98eec23',
    name: 'Five times across the US',
    start_date_time: '1976-07-06T10:00:00.000Z',
    start_date: '1976-07-06',
    start_time: '10:00:00',
    duration: 37987200, // 3 years, 2 months, 14 days and 16 hours
    distance: 25000000,
    average_speed: 0.658116418161907,
    burned_calories: 5318208,
    average_heart_rate: null,
    elevation_gain: null,
    nb_comments: 0,
    location: 'Greenbow, Alabama, USA',
    nb_likes: 0,
    is_favorite: false,
    description:
      'Heartbroken, Forrest, "for no particular reason", starts running and embarks on a cross-country marathon, becoming famous for another feat. Forrest starts to garner many followers, some of whom are struggling businessmen, whom he unwittingly gives inspiration. After a total of about three years and two-and-a half months running, Forrest decides to end the run, and returns to Greenbow, much to the surprise of his followers.',
    user: {
      id: 'a9202021-d1fe-470c-b00f-ceb85084cbea',
      email: 'forrest@gump.rbh',
      password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
    },
    activity: fakeActivities.running,
  },
  chloe_mccardell: {
    id: 'c57b28b7-1f79-4ad5-952a-384bd65f2a41',
    name: 'World record: from South Eleuthera Island to Nassau, Bahamas',
    start_date_time: '2014-10-22T10:00:00.000Z',
    start_date: '2014-10-22',
    start_time: '10:00:00',
    duration: 148860, // 41 hours and 16 minutes
    distance: 124400,
    average_speed: 0.835684535805455,
    burned_calories: 28221,
    average_heart_rate: null,
    elevation_gain: null,
    location: '',
    nb_comments: 0,
    nb_likes: 0,
    is_favorite: false,
    description:
      'On 22 October 2014 McCardel completed an unprecedented swim from South Eleuthera Island to Nassau, Bahamas. 124.4 kilometers (77.3 miles) in 41 hours, 21 minutes. She set a new world record, longest unassisted ocean swim, conducted under the ‘Rules of Marathon Swimming’.',
    user: {
      id: '59cb9131-cd96-4318-8899-d7f564d63f64',
      email: 'chloe@mccardell.rbh',
      password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
    },
    activity: fakeActivities.swimming,
  },
  casquette_verte: {
    id: 'c244ca7c-6611-4411-86e4-439362a0d0c4',
    name: 'Carrera Solidaria 5 km , Las Tablas - &#x1f3e5;&#x1fa7a;&#x1f3c3;',
    start_date_time: '2023-07-30T12:04:12.000Z',
    start_date: '2023-07-30',
    start_time: '12:04:12',
    duration: 1363,
    distance: 5067,
    average_speed: 3.7177691750561705,
    average_heart_rate: 156,
    elevation_gain: 15,
    burned_calories: 450,
    gps_track: fakeSessionJSON,
    description: `Je pensais que ma première course serait Paris-Versailles, mais le hasard fait qu’il y a une course à but caritatif pour l’hôpital de Las Tablas, Panamà. Donc j’ai chaussé mes chaussures pour ce 5km sous le soleil des tropiques, avec la belle famille.

    J-56: Paris-Versailles
    J-71 : 20km de Paris`,
    location: 'Las Tablas, Panamà',
    nb_comments: 0,
    nb_likes: 3,
    is_favorite: true,
    images: [
      {
        id: '3294cd56-7d2d-4205-9a5c-978d8e0a1611',
        filename: '4d9d1e4ddc4d4d2c85e5cc65f2d3e7c6.jpg',
        mimetype: 'image/jpeg',
        size: 46790,
      },
      {
        id: '845ad7f9-585c-495d-a5e6-847cb3f8f240',
        filename: 'ddcc60adceaa4ee5ae479c0649413239.jpg',
        mimetype: 'image/jpeg',
        size: 46790,
      },
    ],
    user: {
      id: '4ee99abe-41b0-49fd-91a3-129a334295ce',
      email: 'casquette@verte.rbh',
      password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
    },
    activity: fakeActivities.running,
  },
};*/

export { fakeSessions };
