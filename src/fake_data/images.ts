import { image1Data } from './image1';
import { image2Data } from './image2';
import { fakeSessions } from './sessions';
import { fakeUsers } from './users';

const fakeImages = [
  {
    id: '205b1894-7978-4663-90d1-35007f8b07a9',
    mimetype: 'image/jpeg',
    data: image1Data,
    user: fakeUsers.forrest_gump,
    session: fakeSessions[2],
  },
  {
    id: '5c512f83-72dd-430c-8a7e-98af597e78a8',
    mimetype: 'image/jpeg',
    data: image2Data,
    user: fakeUsers.forrest_gump,
    session: fakeSessions[2],
  },
];

export { fakeImages };
