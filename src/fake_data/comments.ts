import { DateTime } from 'luxon';
import { fakeSessions } from './sessions';
import { fakeUsers } from './users';

const fakeComments = [
  {
    id: '85abc1b4-3300-43e1-bec3-c63c2385da94',
    date_time: DateTime.fromISO(fakeSessions[0].start_date_time)
      .plus({ hours: 1, minutes: 59 })
      .toISO(),
    text: 'Sem integer vitae justo eget.',
    user: fakeUsers.casquette_verte,
    session: fakeSessions[0],
  },
  {
    id: '9bff5c80-32c6-46ab-b13d-11c7ed4ab71e',
    date_time: DateTime.fromISO(fakeSessions[1].start_date_time)
      .plus({ hours: 2, minutes: 30 })
      .toISO(),
    text: 'Aliquet sagittis id consectetur purus.',
    user: fakeUsers.casquette_verte,
    session: fakeSessions[1],
  },
  {
    id: 'c53d7500-08f9-4ee1-8a4d-6a4917c470fa',
    date_time: DateTime.fromISO(fakeSessions[1].start_date_time)
      .plus({ hours: 2, minutes: 59 })
      .toISO(),
    text: 'Fermentum iaculis eu non diam phasellus vestibulum. Suspendisse in est ante in nibh mauris cursus mattis molestie. Nulla pellentesque dignissim enim sit amet venenatis urna.',
    user: fakeUsers.forrest_gump,
    session: fakeSessions[1],
  },
  {
    id: '7d558d8e-1751-40b5-a01f-1e846e3b2902',
    date_time: DateTime.fromISO(fakeSessions[0].start_date_time)
      .plus({ hours: 1, minutes: 59 })
      .toISO(),
    text: 'Morbi enim',
    user: fakeUsers.casquette_verte,
    session: fakeSessions[0],
  },
  // {
  //   id: 'eacaf098-a442-4eb9-a246-baef4f861972',
  //   date_time: DateTime.fromISO(fakeSessions[2].start_date_time)
  //     .plus({ hours: 1, minutes: 59 })
  //     .toISO(),
  //   text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quam viverra orci sagittis eu volutpat odio facilisis mauris sit. Ornare quam viverra orci sagittis eu. Porttitor lacus luctus accumsan tortor posuere ac. Nec feugiat nisl pretium fusce.',
  //   user: fakeUsers.chloe_mccardell,
  //   session: fakeSessions[2],
  // },
  // {
  //   id: 'a2245435-5721-48ff-9076-67de61e29ffb',
  //   date_time: DateTime.fromISO(fakeSessions[2].start_date_time)
  //     .plus({ hours: 0, minutes: 39 })
  //     .toISO(),
  //   text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  //   user: fakeUsers.forrest_gump,
  //   session: fakeSessions[2],
  // },
];

export { fakeComments };
