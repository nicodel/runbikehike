import path from 'path';

import { CONFIG } from '@/config';

import { dataSource } from '@/shared/infra/database';
import { ProfileAvatars } from '@/shared/infra/database/models/profileAvatars';
import { Users } from '@/shared/infra/database/models/users';
import { UsersProfiles } from '@/shared/infra/database/models/usersProfiles';
import logger from '@/shared/utils/logger';

import { fakeAvatars } from './profilesAvatars';
import { fakeUsers } from './users';
import { fakeProfiles } from './usersProfiles';
import { fakeActivities } from './activities';
import { fakeSessions } from './sessions';
import { fakeImages } from './images';
import { fakeComments } from './comments';
import { randomUserProfiles } from './randomUsers';

import { Activities } from '@/shared/infra/database/models/activities';
import { Sessions } from '@/shared/infra/database/models/sessions';
import { Images } from '@/shared/infra/database/models/images';
import { Comments } from '@/shared/infra/database/models/comments';

async function fakeInjectData(level?: string) {
  try {
    logger.debug(
      '[FakeData] injectFakeData - dataSource.isInitialized: %s',
      dataSource.isInitialized
    );

    if (!dataSource.isInitialized) {
      await dataSource
        .initialize()
        .then(() => {
          logger.info(
            '[FakeData] fakeInitializeDatasource - Data Source has been initialized successfully.'
          );
        })
        .catch((err) => {
          logger.error(
            `[FakeData] fakeInitializeDatasource - Error during Data Source initialization: ${err}`
          );
        });
    }

    logger.debug('[FakeData] injectFakeData - loading avatar pictures from filesystem');

    const dirName =
      CONFIG.ENV === 'qualification' ? path.join(__dirname, '../../src/fake_data') : __dirname;
    logger.debug(`[FakeData] injectFakeData - dirName: ${dirName}`);

    logger.debug('[FakeData] injectFakeData - Loading fake data in database.');
    const usersRepo = dataSource.getRepository(Users);
    const usersProfilesRepo = dataSource.getRepository(UsersProfiles);
    const profilesAvatars = dataSource.getRepository(ProfileAvatars);
    const activitiesRepo = dataSource.getRepository(Activities);
    const imagesRepo = dataSource.getRepository(Images);
    const commentsRepo = dataSource.getRepository(Comments);
    const sessionsRepo = dataSource.getRepository(Sessions);

    await imagesRepo.delete({});
    await commentsRepo.delete({});
    await sessionsRepo.delete({});
    await usersProfilesRepo.delete({});
    await usersRepo.delete({});
    await profilesAvatars.delete({});
    await activitiesRepo.delete({});

    const rAvatars = randomUserProfiles.map((profile) => profile.avatar);
    await profilesAvatars.save([
      fakeAvatars.forrest_gump,
      fakeAvatars.chloe_mccardell,
      fakeAvatars.casquette_verte,
      fakeAvatars.antoine_albeau,
      ...rAvatars,
    ]);

    const rUsers = randomUserProfiles.map((profile) => profile.user);
    await usersRepo.save([
      fakeUsers.forrest_gump,
      fakeUsers.chloe_mccardell,
      fakeUsers.casquette_verte,
      fakeUsers.antoine_albeau,
      ...rUsers,
    ]);

    await usersProfilesRepo.save([
      fakeProfiles.forrest_gump,
      fakeProfiles.chloe_mccardell,
      fakeProfiles.casquette_verte,
      fakeProfiles.antoine_albeau,
      ...randomUserProfiles,
    ]);
    await activitiesRepo.save([
      fakeActivities.running,
      fakeActivities.biking,
      fakeActivities.swimming,
      fakeActivities.training,
      fakeActivities.football,
      fakeActivities.skiing,
      fakeActivities.walking,
      fakeActivities.hiking,
    ]);

    let nb_sessions = 0;
    if (level === 'light') {
      nb_sessions = 2;
    } else {
      nb_sessions = fakeSessions.length;
    }
    for (let i = 0; i < nb_sessions; i++) {
      await sessionsRepo.save(fakeSessions[i]);
    }
    await commentsRepo.save(fakeComments);

    if (level !== 'light') {
      await imagesRepo.save(fakeImages);
    }

    logger.debug('[FakeData] injectFakeData - Fake data added to the database.');
  } catch (err) {
    logger.error('[FakeData] injectFakeData - Error: %s', err);
  }
}

async function fakeDropDatabase() {
  await dataSource.dropDatabase();
}

async function fakeDestroy() {
  await dataSource.destroy();
}
export { fakeInjectData, fakeDropDatabase, fakeDestroy };
