const xml = `<?xml version="1.0" encoding="UTF-8"?>
<gpx creator="Garmin Connect" version="1.1"
  xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/11.xsd"
  xmlns:ns3="http://www.garmin.com/xmlschemas/TrackPointExtension/v1"
  xmlns="http://www.topografix.com/GPX/1/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns2="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
  <metadata>
    <link href="connect.garmin.com">
      <text>Garmin Connect</text>
    </link>
  </metadata>
  <trk>
    <name>Antony Course à pied</name>
    <type>running</type>
    <trkseg>
      <trkpt lat="48.74080105684697628021240234375" lon="2.31284310109913349151611328125">
        <ele>83.8000030517578125</ele>
        <time>2019-10-26T13:37:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>99</ns3:hr>
            <ns3:cad>0</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74079317785799503326416015625" lon="2.3128160275518894195556640625">
        <ele>84.8000030517578125</ele>
        <time>2019-10-26T13:37:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>100</ns3:hr>
            <ns3:cad>0</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7407781742513179779052734375" lon="2.31277571059763431549072265625">
        <ele>85.40000152587890625</ele>
        <time>2019-10-26T13:37:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>100</ns3:hr>
            <ns3:cad>56</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
    </trkseg>
    </trk>
</gpx>`;

export default xml;
