/* eslint-disable @typescript-eslint/no-loss-of-precision */
/* eslint-disable quotes */
import { DateTime } from '@/app/domains/dateTime';
import { Coordinate } from '@/app/domains/sessions/coordinate';
import { Distance } from '@/app/domains/sessions/distance';
import { HeartRate } from '@/app/domains/sessions/heartRate';
import { Speed } from '@/app/domains/sessions/speed';
import { TrackPoint } from '@/app/domains/sessions/trackPoint';

const segments = [
  [
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:37:36.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74080105684697628021240234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31284310109913349151611328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 83.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 0 }).getValue(),
      heart_rate: HeartRate.create({ value: 99 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:37:37.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74079317785799503326416015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3128160275518894195556640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 2.170003474878529 }).getValue(),
      speed: Speed.create({ value: 0.46082875515025495 }).getValue(),
      heart_rate: HeartRate.create({ value: 100 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:37:40.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7407781742513179779052734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31277571059763431549072265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 3.394658268647673 }).getValue(),
      speed: Speed.create({ value: 0.8837413850187362 }).getValue(),
      heart_rate: HeartRate.create({ value: 100 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:37:44.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.740767277777194976806640625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31270144693553447723388671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 5.578869617393904 }).getValue(),
      speed: Speed.create({ value: 0.7169911244257663 }).getValue(),
      heart_rate: HeartRate.create({ value: 105 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:37:45.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74076233245432376861572265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3126752115786075592041015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.0008691943379096 }).getValue(),
      speed: Speed.create({ value: 0.49978279581184787 }).getValue(),
      heart_rate: HeartRate.create({ value: 108 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:37:46.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74076350592076778411865234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3126564361155033111572265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86 }).getValue(),
      distance: Distance.create({ value: 1.3829625582349148 }).getValue(),
      speed: Speed.create({ value: 0.7230853749766786 }).getValue(),
      heart_rate: HeartRate.create({ value: 108 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:37:48.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7407627515494823455810546875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31261813081800937652587890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86 }).getValue(),
      distance: Distance.create({ value: 2.8101561613351618 }).getValue(),
      speed: Speed.create({ value: 0.7117042203981148 }).getValue(),
      heart_rate: HeartRate.create({ value: 112 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:37:51.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.740745820105075836181640625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3125397600233554840087890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 6.047411988104356 }).getValue(),
      speed: Speed.create({ value: 0.4960799770052364 }).getValue(),
      heart_rate: HeartRate.create({ value: 115 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:37:52.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74074087478220462799072265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.312512435019016265869140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 2.07781243907782 }).getValue(),
      speed: Speed.create({ value: 0.48127539386751506 }).getValue(),
      heart_rate: HeartRate.create({ value: 116 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:37:55.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.740741126239299774169921875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31245920993387699127197265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87 }).getValue(),
      distance: Distance.create({ value: 3.9030646952578913 }).getValue(),
      speed: Speed.create({ value: 0.7686267674847695 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:37:56.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74074288643896579742431640625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3124334774911403656005859375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87 }).getValue(),
      distance: Distance.create({ value: 1.8970687667961021 }).getValue(),
      speed: Speed.create({ value: 0.5271290200454186 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:37:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74074020422995090484619140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3123753070831298828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 4.276016084398143 }).getValue(),
      speed: Speed.create({ value: 0.467725088148611 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:01.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74073182232677936553955078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3123100958764553070068359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 4.871882761620598 }).getValue(),
      speed: Speed.create({ value: 0.6157783647080356 }).getValue(),
      heart_rate: HeartRate.create({ value: 131 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:02.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7407267093658447265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3122893087565898895263671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 1.6268824713087404 }).getValue(),
      speed: Speed.create({ value: 0.6146725517274478 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:07.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7406958639621734619140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31215201318264007568359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 10.636009435563151 }).getValue(),
      speed: Speed.create({ value: 0.47010112489010425 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:08.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7406958639621734619140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31212527491152286529541015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 1.9607036327253782 }).getValue(),
      speed: Speed.create({ value: 0.5100209859916462 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:09.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7407012283802032470703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31210130266845226287841796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 1.856319471881483 }).getValue(),
      speed: Speed.create({ value: 0.5387003773582381 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:15.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74076618812978267669677734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31197071261703968048095703125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 11.99483700549687 }).getValue(),
      speed: Speed.create({ value: 0.5002152173681378 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:16.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74077490530908107757568359375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3119513504207134246826171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 1.7191383942254574 }).getValue(),
      speed: Speed.create({ value: 0.5816867352616722 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:19.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74078764580190181732177734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3118762485682964324951171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 5.686466979275452 }).getValue(),
      speed: Speed.create({ value: 0.5275683497211213 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:20.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74078513123095035552978515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31184917502105236053466796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87 }).getValue(),
      distance: Distance.create({ value: 2.0048789524866715 }).getValue(),
      speed: Speed.create({ value: 0.49878323015945175 }).getValue(),
      heart_rate: HeartRate.create({ value: 121 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:21.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.740783035755157470703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31182537041604518890380859375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87 }).getValue(),
      distance: Distance.create({ value: 1.761058972538089 }).getValue(),
      speed: Speed.create({ value: 0.5678401550396528 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:22.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74078295193612575531005859375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31180198490619659423828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 1.7148697329875198 }).getValue(),
      speed: Speed.create({ value: 0.5831346724266184 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:25.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.740775994956493377685546875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31173115782439708709716796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 5.250999552063949 }).getValue(),
      speed: Speed.create({ value: 0.571319797355691 }).getValue(),
      heart_rate: HeartRate.create({ value: 119 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:27.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7407369352877140045166015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31174054555594921112060546875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 4.397453544063033 }).getValue(),
      speed: Speed.create({ value: 0.4548086705089094 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:29.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74071036465466022491455078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.311706431210041046142578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 3.8713200064455267 }).getValue(),
      speed: Speed.create({ value: 0.5166196534179851 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:31.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74068354256451129913330078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31166921555995941162109375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 4.0426048373387875 }).getValue(),
      speed: Speed.create({ value: 0.4947305216496458 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:33.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7406618334352970123291015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3116251267492771148681640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88 }).getValue(),
      distance: Distance.create({ value: 4.034785285978873 }).getValue(),
      speed: Speed.create({ value: 0.49568932625736567 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:34.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7406464107334613800048828125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.311605513095855712890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88 }).getValue(),
      distance: Distance.create({ value: 2.2382056471647376 }).getValue(),
      speed: Speed.create({ value: 0.4467864698968823 }).getValue(),
      heart_rate: HeartRate.create({ value: 119 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:39.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74057675711810588836669921875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3114940337836742401123046875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 11.261141005594478 }).getValue(),
      speed: Speed.create({ value: 0.44400474139485735 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:43.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7405215203762054443359375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.311414238065481185913085,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 8.483133678356165 }).getValue(),
      speed: Speed.create({ value: 0.4715238674365801 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:46.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.740482628345489501953125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3113574087619781494140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 6.005693470971272 }).getValue(),
      speed: Speed.create({ value: 0.4995259938757455 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2019-10-26T13:38:47.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74047315679490566253662109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.311334945261478424072265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 1.9551518386370066 }).getValue(),
      speed: Speed.create({ value: 0.5114692272172218 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
  ],
];

const long_segments = [
  [
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:14.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74078546650707721710205078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31281460262835025787353515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 83.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 0 }).getValue(),
      heart_rate: HeartRate.create({ value: 102 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:15.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74078680761158466339111328125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31280555017292499542236328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 83.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 0.6803547696651192 }).getValue(),
      speed: Speed.create({ value: 1.4698213999325895 }).getValue(),
      heart_rate: HeartRate.create({ value: 102 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:22.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74074422754347324371337890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31266791932284832000732421875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 83.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 11.147802778358223 }).getValue(),
      speed: Speed.create({ value: 0.6279264299140136 }).getValue(),
      heart_rate: HeartRate.create({ value: 105 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:26.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7407104484736919403076171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3125897161662578582763671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84 }).getValue(),
      distance: Distance.create({ value: 6.855184816483209 }).getValue(),
      speed: Speed.create({ value: 0.5834999503415353 }).getValue(),
      heart_rate: HeartRate.create({ value: 108 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:28.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74068421311676502227783203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3125350661575794219970703125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84 }).getValue(),
      distance: Distance.create({ value: 4.95681261397049 }).getValue(),
      speed: Speed.create({ value: 0.40348509329626775 }).getValue(),
      heart_rate: HeartRate.create({ value: 111 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:29.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74067482538521289825439453125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31250061653554439544677734375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84 }).getValue(),
      distance: Distance.create({ value: 2.7333523239355473 }).getValue(),
      speed: Speed.create({ value: 0.3658511166830391 }).getValue(),
      heart_rate: HeartRate.create({ value: 113 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:31.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.740663342177867889404296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3124210722744464874267578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 5.971066019703769 }).getValue(),
      speed: Speed.create({ value: 0.33494856586751026 }).getValue(),
      heart_rate: HeartRate.create({ value: 118 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:32.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7406591512262821197509765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.312343120574951171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 5.735126340629149 }).getValue(),
      speed: Speed.create({ value: 0.17436407510602442 }).getValue(),
      heart_rate: HeartRate.create({ value: 118 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:33.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7406433932483196258544921875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3123084194958209991455078125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 3.0895457619347715 }).getValue(),
      speed: Speed.create({ value: 0.32367217612396465 }).getValue(),
      heart_rate: HeartRate.create({ value: 121 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:34.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74063744209706783294677734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31225687079131603240966796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 3.8375291873634136 }).getValue(),
      speed: Speed.create({ value: 0.2605843372586967 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:36.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.740615732967853546142578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.312198616564273834228515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 4.906633236991923 }).getValue(),
      speed: Speed.create({ value: 0.40761147275522197 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:38.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74059637077152729034423828125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31215008534491062164306640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 4.159350716963017 }).getValue(),
      speed: Speed.create({ value: 0.4808442798159411 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:41.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74060475267469882965087890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3120730556547641754150390625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 5.724934947874271 }).getValue(),
      speed: Speed.create({ value: 0.5240234216310059 }).getValue(),
      heart_rate: HeartRate.create({ value: 131 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:47.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74069225974380970001220703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3119024001061916351318359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 15.85188114256155 }).getValue(),
      speed: Speed.create({ value: 0.3785039735057238 }).getValue(),
      heart_rate: HeartRate.create({ value: 134 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:48.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74070785008370876312255859375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.311881445348262786865234375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 2.3165486333725784 }).getValue(),
      speed: Speed.create({ value: 0.43167667002273835 }).getValue(),
      heart_rate: HeartRate.create({ value: 134 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:49.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74071304686367511749267578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31185386888682842254638671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 2.1031112371312255 }).getValue(),
      speed: Speed.create({ value: 0.4754860239176232 }).getValue(),
      heart_rate: HeartRate.create({ value: 134 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:53.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74072561971843242645263671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3117756657302379608154296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 5.902548857316769 }).getValue(),
      speed: Speed.create({ value: 0.6776733402285389 }).getValue(),
      heart_rate: HeartRate.create({ value: 137 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:54.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74072478152811527252197265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3117629252374172210693359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 0.9388907248328369 }).getValue(),
      speed: Speed.create({ value: 1.0650866746798924 }).getValue(),
      heart_rate: HeartRate.create({ value: 138 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:57.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74070231802761554718017578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31173534877598285675048828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 3.2137673356557226 }).getValue(),
      speed: Speed.create({ value: 0.933483879407186 }).getValue(),
      heart_rate: HeartRate.create({ value: 138 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:21:59.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7406705506145954132080078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.311711795628070831298828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 3.932008218618845 }).getValue(),
      speed: Speed.create({ value: 0.5086459358171227 }).getValue(),
      heart_rate: HeartRate.create({ value: 140 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:02.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74063308350741863250732421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31166728772222995758056640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85 }).getValue(),
      distance: Distance.create({ value: 5.292339136545648 }).getValue(),
      speed: Speed.create({ value: 0.5668570971357145 }).getValue(),
      heart_rate: HeartRate.create({ value: 140 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:04.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74060424976050853729248046875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3116283118724822998046875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 4.2951301795986625 }).getValue(),
      speed: Speed.create({ value: 0.4656436281022989 }).getValue(),
      heart_rate: HeartRate.create({ value: 141 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:07.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.740566112101078033447265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31157978065311908721923828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 5.536113430994678 }).getValue(),
      speed: Speed.create({ value: 0.5418964111544564 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:11.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7405109591782093048095703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.311483807861804962158203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86 }).getValue(),
      distance: Distance.create({ value: 9.334824002153036 }).getValue(),
      speed: Speed.create({ value: 0.4285029904235385 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:19.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7403875775635242462158203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31131558306515216827392578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 18.449839613529775 }).getValue(),
      speed: Speed.create({ value: 0.43360810541319716 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:27.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74026092700660228729248046875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31109564192593097686767578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86 }).getValue(),
      distance: Distance.create({ value: 21.411440382694202 }).getValue(),
      speed: Speed.create({ value: 0.3736320330166111 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:28.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74024726450443267822265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31108315289020538330078125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86 }).getValue(),
      distance: Distance.create({ value: 1.773894723145079 }).getValue(),
      speed: Speed.create({ value: 0.5637313122094532 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:30.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7402202747762203216552734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31103361584246158599853515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 4.711922676624884 }).getValue(),
      speed: Speed.create({ value: 0.42445518257795045 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:34.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74016545712947845458984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31094636023044586181640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 8.83713175452659 }).getValue(),
      speed: Speed.create({ value: 0.4526355508902653 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:35.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.74015078879892826080322265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31092330999672412872314453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 2.348904053535568 }).getValue(),
      speed: Speed.create({ value: 0.42573045863444314 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:41.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7400623597204685211181640625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3107289336621761322021484375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 17.316256087398717 }).getValue(),
      speed: Speed.create({ value: 0.34649522216100076 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:48.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73991274274885654449462890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.310546375811100006103515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 21.353957113163453 }).getValue(),
      speed: Speed.create({ value: 0.32780809490737967 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:51.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73986161313951015472412109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31042911298573017120361328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 10.308509515110607 }).getValue(),
      speed: Speed.create({ value: 0.2910217035355582 }).getValue(),
      heart_rate: HeartRate.create({ value: 140 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:54.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7398273311555385589599609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31036172248423099517822265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 6.241197338747911 }).getValue(),
      speed: Speed.create({ value: 0.48067699788544904 }).getValue(),
      heart_rate: HeartRate.create({ value: 140 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:22:57.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73978449963033199310302734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31028293259441852569580078125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 7.487650952501056 }).getValue(),
      speed: Speed.create({ value: 0.40065970209227336 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:00.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7397364713251590728759765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31020229868590831756591796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87 }).getValue(),
      distance: Distance.create({ value: 7.967684378116005 }).getValue(),
      speed: Speed.create({ value: 0.3765209385351385 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:05.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73962775804102420806884765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3100325651466846466064453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 17.35077867992855 }).getValue(),
      speed: Speed.create({ value: 0.28817150470508973 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:07.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7396013550460338592529296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30996576137840747833251953125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 5.711176925035449 }).getValue(),
      speed: Speed.create({ value: 0.35019051699008363 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:14.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73951359651982784271240234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30981639586389064788818359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 14.669554984566679 }).getValue(),
      speed: Speed.create({ value: 0.4771787560948136 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:15.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73949582688510417938232421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30978772975504398345947265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.884972423883925 }).getValue(),
      speed: Speed.create({ value: 0.346623763791038 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:20.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73941670171916484832763671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.309652529656887054443359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 13.2554015296815 }).getValue(),
      speed: Speed.create({ value: 0.3772047183032516 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:26.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73932785354554653167724609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3094562254846096038818359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 17.459308172206253 }).getValue(),
      speed: Speed.create({ value: 0.3436562285756256 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:34.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.739202208817005157470703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.309237457811832427978515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 21.273333127434586 }).getValue(),
      speed: Speed.create({ value: 0.37605766581462563 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:40.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7390766479074954986572265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30907342396676540374755859375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 18.428897519776314 }).getValue(),
      speed: Speed.create({ value: 0.3255756343297972 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:41.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.739059381186962127685546875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30905163101851940155029296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 2.498053378151717 }).getValue(),
      speed: Speed.create({ value: 0.4003117022022521 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:46.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73900565318763256072998046875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3089392296969890594482421875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 10.180007297364739 }).getValue(),
      speed: Speed.create({ value: 0.49115878348086567 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:52.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73891764320433139801025390625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30876505374908447265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 16.090746854282504 }).getValue(),
      speed: Speed.create({ value: 0.3728851155472077 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.738828040659427642822265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30859163217246532440185546875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 16.155493310277414 }).getValue(),
      speed: Speed.create({ value: 0.37139070189723417 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:23:59.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73880700208246707916259765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3085281811654567718505859375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 5.207989484060873 }).getValue(),
      speed: Speed.create({ value: 0.19201267649647039 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:24:06.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7386712990701198577880859375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30832207016646862030029296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 21.357522925656447 }).getValue(),
      speed: Speed.create({ value: 0.3277533646746558 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:24:13.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73857758939266204833984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30818242765963077545166015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 14.609644400129916 }).getValue(),
      speed: Speed.create({ value: 0.4791355496604526 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:24:19.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.738445155322551727294921875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30807161889970302581787109375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 16.81919345037612 }).getValue(),
      speed: Speed.create({ value: 0.3567352987349001 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:24:26.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7382746674120426177978515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3079820163547992706298828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 20.063854459923547 }).getValue(),
      speed: Speed.create({ value: 0.34888610331489983 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:24:27.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73826452530920505523681640625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30799190700054168701171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 1.3408567830770766 }).getValue(),
      speed: Speed.create({ value: 0.745791804628934 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:24:31.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73822043649852275848388671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30804295279085636138916015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 6.168197626771401 }).getValue(),
      speed: Speed.create({ value: 0.6484876526392535 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:24:35.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73817835934460163116455078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30810011737048625946044921875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 6.2820535209400745 }).getValue(),
      speed: Speed.create({ value: 0.636734466948862 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:24:36.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73817400075495243072509765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3081178031861782073974609375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 1.3845524227696067 }).getValue(),
      speed: Speed.create({ value: 0.7222550649253406 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:24:40.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7381361983716487884521484375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30817505158483982086181640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 5.94085846620142 }).getValue(),
      speed: Speed.create({ value: 0.6733033656258094 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:24:41.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7381164170801639556884765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30819567106664180755615234375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.6691899405601487 }).getValue(),
      speed: Speed.create({ value: 0.3746455000464084 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:24:48.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.737965710461139678955078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30834419839084148406982421875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 19.98649715161698 }).getValue(),
      speed: Speed.create({ value: 0.35023645949053533 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:24:55.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73783285729587078094482421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30847076512873172760009765625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 17.446419790177135 }).getValue(),
      speed: Speed.create({ value: 0.40122845169306387 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:01.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7377516366541385650634765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.308578304946422576904296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 11.98993110934925 }).getValue(),
      speed: Speed.create({ value: 0.5004198894288433 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:09.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73767259530723094940185546875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30871467851102352142333984375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 13.31399266254504 }).getValue(),
      speed: Speed.create({ value: 0.6008715944771114 }).getValue(),
      heart_rate: HeartRate.create({ value: 146 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:14.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7376238964498043060302734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30880201794207096099853515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 8.387266718419918 }).getValue(),
      speed: Speed.create({ value: 0.5961417667831068 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:15.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73761090449988842010498046875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.308823727071285247802734375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 2.149767179741451 }).getValue(),
      speed: Speed.create({ value: 0.46516665126512374 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:22.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7375176139175891876220703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.308974601328372955322265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 15.166582574878717 }).getValue(),
      speed: Speed.create({ value: 0.46154102055887675 }).getValue(),
      heart_rate: HeartRate.create({ value: 146 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:27.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73739339411258697509765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30913762934505939483642578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 18.268086278451598 }).getValue(),
      speed: Speed.create({ value: 0.27370135676980173 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:32.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.737333714962005615234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30926050804555416107177734375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 11.191022967728006 }).getValue(),
      speed: Speed.create({ value: 0.4467866802184838 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:38.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73721527867019176483154296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.309450693428516387939453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 19.182256969685273 }).getValue(),
      speed: Speed.create({ value: 0.31278905341963226 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:43.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73713280074298381805419921875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30957147665321826934814453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 12.750157266773646 }).getValue(),
      speed: Speed.create({ value: 0.3921520256875405 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:45.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.737091980874538421630859375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30961866676807403564453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 5.707752582235323 }).getValue(),
      speed: Speed.create({ value: 0.3504006123574371 }).getValue(),
      heart_rate: HeartRate.create({ value: 142 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:48.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7370371632277965545654296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30969334952533245086669921875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 8.194526048037725 }).getValue(),
      speed: Speed.create({ value: 0.36609804916275607 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:50.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7369935773313045501708984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3097214289009571075439453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 5.265847546709926 }).getValue(),
      speed: Speed.create({ value: 0.37980590631599076 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:55.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7368608079850673675537109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3098183237016201019287109375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 16.384335715792837 }).getValue(),
      speed: Speed.create({ value: 0.30516952818419774 }).getValue(),
      heart_rate: HeartRate.create({ value: 146 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:25:59.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73675234615802764892578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30990624986588954925537109375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 13.675931097245773 }).getValue(),
      speed: Speed.create({ value: 0.29248465582029504 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:03.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.736668191850185394287109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3100055754184722900390625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 11.858375478062083 }).getValue(),
      speed: Speed.create({ value: 0.33731433174805214 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:05.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.736615888774394989013671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31005092151463031768798828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 6.6994577179323125 }).getValue(),
      speed: Speed.create({ value: 0.2985316251264095 }).getValue(),
      heart_rate: HeartRate.create({ value: 146 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:10.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73652167618274688720703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31015334837138652801513671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 12.890647632795535 }).getValue(),
      speed: Speed.create({ value: 0.3878781068593738 }).getValue(),
      heart_rate: HeartRate.create({ value: 147 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:16.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7364021502435207366943359375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31031201779842376708984375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 17.664698748081754 }).getValue(),
      speed: Speed.create({ value: 0.3396604768395245 }).getValue(),
      heart_rate: HeartRate.create({ value: 150 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:17.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7363813631236553192138671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31033842079341411590576171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 3.015272430690664 }).getValue(),
      speed: Speed.create({ value: 0.3316449916172068 }).getValue(),
      heart_rate: HeartRate.create({ value: 150 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:23.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73625270090997219085693359375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31050220318138599395751953125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 18.680082794491163 }).getValue(),
      speed: Speed.create({ value: 0.3211977198393053 }).getValue(),
      heart_rate: HeartRate.create({ value: 149 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:26.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73619536869227886199951171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.310549728572368621826171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 7.26558719702511 }).getValue(),
      speed: Speed.create({ value: 0.4129053741490224 }).getValue(),
      heart_rate: HeartRate.create({ value: 150 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:28.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73615832068026065826416015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31055743992328643798828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 4.15818631016717 }).getValue(),
      speed: Speed.create({ value: 0.48097892946975596 }).getValue(),
      heart_rate: HeartRate.create({ value: 148 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:34.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7360256351530551910400390625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31055031530559062957763671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 14.763206262565305 }).getValue(),
      speed: Speed.create({ value: 0.40641578077887125 }).getValue(),
      heart_rate: HeartRate.create({ value: 150 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:36.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73598230071365833282470703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31052131392061710357666015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 5.267079616470601 }).getValue(),
      speed: Speed.create({ value: 0.3797170625152185 }).getValue(),
      heart_rate: HeartRate.create({ value: 146 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:38.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73593016527593135833740234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31048778630793094635009765625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 6.297074975016558 }).getValue(),
      speed: Speed.create({ value: 0.31760777947458707 }).getValue(),
      heart_rate: HeartRate.create({ value: 149 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:39.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7359139882028102874755859375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31046347878873348236083984375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.532483239819121 }).getValue(),
      speed: Speed.create({ value: 0.39486934573806837 }).getValue(),
      heart_rate: HeartRate.create({ value: 148 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:44.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.735810220241546630859375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3103742115199565887451171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 13.266260061328792 }).getValue(),
      speed: Speed.create({ value: 0.376895973460902 }).getValue(),
      heart_rate: HeartRate.create({ value: 147 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:45.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7357993237674236297607421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3103651590645313262939453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 1.3815880852103446 }).getValue(),
      speed: Speed.create({ value: 0.7238047365237313 }).getValue(),
      heart_rate: HeartRate.create({ value: 147 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:48.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73576604761183261871337890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31032182462513446807861328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 4.877574683862417 }).getValue(),
      speed: Speed.create({ value: 0.6150597775418135 }).getValue(),
      heart_rate: HeartRate.create({ value: 149 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:51.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7357181869447231292724609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.31026784516870975494384765625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 6.632747831399883 }).getValue(),
      speed: Speed.create({ value: 0.45230122963484215 }).getValue(),
      heart_rate: HeartRate.create({ value: 147 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:57.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7356169335544109344482421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3101686872541904449462890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 13.403093388459858 }).getValue(),
      speed: Speed.create({ value: 0.4476578522661071 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:26:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73559757135808467864990234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3101485706865787506103515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 2.6099397701575824 }).getValue(),
      speed: Speed.create({ value: 0.3831506042530714 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:01.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.735558427870273590087890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3100821860134601593017578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 6.530432991795786 }).getValue(),
      speed: Speed.create({ value: 0.4593876093314049 }).getValue(),
      heart_rate: HeartRate.create({ value: 142 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:06.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73549011535942554473876953125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.309989817440509796142578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 10.177762995836819 }).getValue(),
      speed: Speed.create({ value: 0.4912670890494536 }).getValue(),
      heart_rate: HeartRate.create({ value: 139 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:11.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73544284142553806304931640625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30989099480211734771728515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 8.953010634416216 }).getValue(),
      speed: Speed.create({ value: 0.5584713571968215 }).getValue(),
      heart_rate: HeartRate.create({ value: 138 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:12.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73543228022754192352294921875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3098699562251567840576171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 1.9389871436142108 }).getValue(),
      speed: Speed.create({ value: 0.5157331771349611 }).getValue(),
      heart_rate: HeartRate.create({ value: 138 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:15.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7353859283030033111572265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30980667285621166229248046875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 6.935693688674481 }).getValue(),
      speed: Speed.create({ value: 0.4325450538420976 }).getValue(),
      heart_rate: HeartRate.create({ value: 135 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:18.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7353537417948246002197265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30976216495037078857421875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 4.843897197660601 }).getValue(),
      speed: Speed.create({ value: 0.6193360175870112 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:25.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7352893687784671783447265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30962445028126239776611328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 12.378962869878784 }).getValue(),
      speed: Speed.create({ value: 0.5654754823631315 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:27.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73527185060083866119384765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.309592850506305694580078125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 3.0273736999311436 }).getValue(),
      speed: Speed.create({ value: 0.660638625500872 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:33.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73522792942821979522705078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.309498973190784454345703125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 8.441030642898552 }).getValue(),
      speed: Speed.create({ value: 0.7108136735705144 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:35.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7352119199931621551513671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3094622604548931121826171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 3.2277111840040407 }).getValue(),
      speed: Speed.create({ value: 0.6196341264706837 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:41.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7351757101714611053466796875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30936276726424694061279296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 8.333762944815218 }).getValue(),
      speed: Speed.create({ value: 0.7199628834814471 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:48.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.735129274427890777587890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30919043533504009246826171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 13.652478933545686 }).getValue(),
      speed: Speed.create({ value: 0.5127273980112291 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:51.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73510756529867649078369140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30912589468061923980712890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 5.313272283063945 }).getValue(),
      speed: Speed.create({ value: 0.5646238024658551 }).getValue(),
      heart_rate: HeartRate.create({ value: 121 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:54.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73505308292806148529052734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30907945893704891204833984375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 6.949726642929089 }).getValue(),
      speed: Speed.create({ value: 0.43167165474807734 }).getValue(),
      heart_rate: HeartRate.create({ value: 118 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:27:59.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73497781343758106231689453125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.309026233851909637451171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 9.235072478447357 }).getValue(),
      speed: Speed.create({ value: 0.5414142673670302 }).getValue(),
      heart_rate: HeartRate.create({ value: 121 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:00.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7349581159651279449462890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30902145616710186004638671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.218108250891256 }).getValue(),
      speed: Speed.create({ value: 0.45083462432376364 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:02.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73491662554442882537841796875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30899077840149402618408203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 5.1328741829057805 }).getValue(),
      speed: Speed.create({ value: 0.3896452413855538 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:05.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73487203381955623626708984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.308944761753082275390625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 5.9978761118702275 }).getValue(),
      speed: Speed.create({ value: 0.5001770533510661 }).getValue(),
      heart_rate: HeartRate.create({ value: 131 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:06.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73486029915511608123779296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.308934368193149566650390625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 1.5111614181849113 }).getValue(),
      speed: Speed.create({ value: 0.6617426755118733 }).getValue(),
      heart_rate: HeartRate.create({ value: 131 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:07.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73484655283391475677490234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30891802348196506500244140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 1.9424806905745526 }).getValue(),
      speed: Speed.create({ value: 0.5148056322270144 }).getValue(),
      heart_rate: HeartRate.create({ value: 134 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:11.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7347807548940181732177734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30887837707996368408203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 7.872975843077687 }).getValue(),
      speed: Speed.create({ value: 0.5080670993696748 }).getValue(),
      heart_rate: HeartRate.create({ value: 137 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:13.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73474513180553913116455078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30885180644690990447998046875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 4.414472483615656 }).getValue(),
      speed: Speed.create({ value: 0.4530552647961933 }).getValue(),
      heart_rate: HeartRate.create({ value: 138 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:17.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73467891477048397064208984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30877871625125408172607421875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 9.107502838810236 }).getValue(),
      speed: Speed.create({ value: 0.43919832590714214 }).getValue(),
      heart_rate: HeartRate.create({ value: 141 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:20.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7346241809427738189697265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3087072186172008514404296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 8.033390018200304 }).getValue(),
      speed: Speed.create({ value: 0.37344134832284426 }).getValue(),
      heart_rate: HeartRate.create({ value: 143 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:22.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73459987342357635498046875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30865030549466609954833984375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 4.972633937135136 }).getValue(),
      speed: Speed.create({ value: 0.40220133339480285 }).getValue(),
      heart_rate: HeartRate.create({ value: 142 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:27.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.734516389667987823486328125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30855860747396945953369140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 11.462941113981076 }).getValue(),
      speed: Speed.create({ value: 0.43618823042732197 }).getValue(),
      heart_rate: HeartRate.create({ value: 141 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:32.000Z' }).getValue(),
      latitude: Coordinate.create({ value: 48.73445034027099609375, name: 'latitude' }).getValue(),
      longitude: Coordinate.create({
        value: 2.30848417617380619049072265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 9.15078231908633 }).getValue(),
      speed: Speed.create({ value: 0.5464013704676597 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:34.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73442259617149829864501953125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30844427831470966339111328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 4.251944821699945 }).getValue(),
      speed: Speed.create({ value: 0.47037299021213347 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:39.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73432318679988384246826171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30834486894309520721435546875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 13.24156632369631 }).getValue(),
      speed: Speed.create({ value: 0.37759883368573255 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:46.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73420852236449718475341796875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30821243487298488616943359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 16.028070526087248 }).getValue(),
      speed: Speed.create({ value: 0.4367337907957678 }).getValue(),
      heart_rate: HeartRate.create({ value: 147 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:50.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7341574765741825103759765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30813431553542613983154296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 8.06480015828365 }).getValue(),
      speed: Speed.create({ value: 0.49598253168015016 }).getValue(),
      heart_rate: HeartRate.create({ value: 146 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:51.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.734149597585201263427734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3081137798726558685302734375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91 }).getValue(),
      distance: Distance.create({ value: 1.7423521400614765 }).getValue(),
      speed: Speed.create({ value: 0.5739367932619616 }).getValue(),
      heart_rate: HeartRate.create({ value: 146 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:57.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7340533733367919921875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.307927869260311126708984375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 17.331547775359564 }).getValue(),
      speed: Speed.create({ value: 0.34618950815981137 }).getValue(),
      heart_rate: HeartRate.create({ value: 150 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.734040297567844390869140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3079008795320987701416015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 2.4560211764175537 }).getValue(),
      speed: Speed.create({ value: 0.40716261309222024 }).getValue(),
      heart_rate: HeartRate.create({ value: 153 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:28:59.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.734026215970516204833984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3078707046806812286376953125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 2.7109230199755956 }).getValue(),
      speed: Speed.create({ value: 0.3688780509927583 }).getValue(),
      heart_rate: HeartRate.create({ value: 153 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:01.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7339915148913860321044921875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30779945850372314453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 6.495441682353047 }).getValue(),
      speed: Speed.create({ value: 0.3079082374696154 }).getValue(),
      heart_rate: HeartRate.create({ value: 156 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:04.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73389654792845249176025390625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30771035887300968170166015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 12.418136120758989 }).getValue(),
      speed: Speed.create({ value: 0.24158214814419685 }).getValue(),
      heart_rate: HeartRate.create({ value: 157 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:10.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73378875665366649627685546875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3075969517230987548828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 14.588920638961696 }).getValue(),
      speed: Speed.create({ value: 0.41127100136360906 }).getValue(),
      heart_rate: HeartRate.create({ value: 154 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:17.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.733656071126461029052734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3073711432516574859619140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 22.179643205510256 }).getValue(),
      speed: Speed.create({ value: 0.3156047162319066 }).getValue(),
      heart_rate: HeartRate.create({ value: 155 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:18.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7336388044059276580810546875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3073453269898891448974609375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 2.696497407163028 }).getValue(),
      speed: Speed.create({ value: 0.37085145987664614 }).getValue(),
      heart_rate: HeartRate.create({ value: 154 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:25.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73352841474115848541259765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30720727704465389251708984375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 15.911529905143484 }).getValue(),
      speed: Speed.create({ value: 0.4399325546776752 }).getValue(),
      heart_rate: HeartRate.create({ value: 154 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:31.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7334473617374897003173828125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.307052798569202423095703125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 14.477047976549164 }).getValue(),
      speed: Speed.create({ value: 0.41444913422399227 }).getValue(),
      heart_rate: HeartRate.create({ value: 155 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:33.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.733425401151180267333984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3069986514747142791748046875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 4.661861394340515 }).getValue(),
      speed: Speed.create({ value: 0.429013183967244 }).getValue(),
      heart_rate: HeartRate.create({ value: 151 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:35.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7333987466990947723388671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30696755461394786834716796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 3.7397457448349805 }).getValue(),
      speed: Speed.create({ value: 0.5347957151264174 }).getValue(),
      heart_rate: HeartRate.create({ value: 149 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:37.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.733373768627643585205078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30693151243031024932861328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 3.8342385999612434 }).getValue(),
      speed: Speed.create({ value: 0.5216159474322271 }).getValue(),
      heart_rate: HeartRate.create({ value: 149 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:39.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73334459960460662841796875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.306907959282398223876953125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92 }).getValue(),
      distance: Distance.create({ value: 3.6747564502881764 }).getValue(),
      speed: Speed.create({ value: 0.5442537558763 }).getValue(),
      heart_rate: HeartRate.create({ value: 145 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:42.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7332968227565288543701171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3068581707775592803955078125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92 }).getValue(),
      distance: Distance.create({ value: 6.446438890187473 }).getValue(),
      speed: Speed.create({ value: 0.46537321629876727 }).getValue(),
      heart_rate: HeartRate.create({ value: 142 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:44.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73325834982097148895263671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3068206198513507843017578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 5.087803028826124 }).getValue(),
      speed: Speed.create({ value: 0.3930969789255869 }).getValue(),
      heart_rate: HeartRate.create({ value: 144 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:46.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7332076393067836761474609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30675163678824901580810546875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 7.5757156387796245 }).getValue(),
      speed: Speed.create({ value: 0.26400146142789765 }).getValue(),
      heart_rate: HeartRate.create({ value: 140 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:49.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73314435593783855438232421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30662322603166103363037109375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 11.75625543158911 }).getValue(),
      speed: Speed.create({ value: 0.25518329517909133 }).getValue(),
      heart_rate: HeartRate.create({ value: 137 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:51.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73310361988842487335205078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30654477141797542572021484375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92 }).getValue(),
      distance: Distance.create({ value: 7.322913102086957 }).getValue(),
      speed: Speed.create({ value: 0.27311535342813503 }).getValue(),
      heart_rate: HeartRate.create({ value: 137 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:53.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7330698408186435699462890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30649163015186786651611328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 5.412745350802969 }).getValue(),
      speed: Speed.create({ value: 0.3694982620424411 }).getValue(),
      heart_rate: HeartRate.create({ value: 134 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:55.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7330381572246551513671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30643262155354022979736328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 5.580425147339265 }).getValue(),
      speed: Speed.create({ value: 0.3583956324463192 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:57.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7329984270036220550537109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3063787259161472320556640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 5.927995010693314 }).getValue(),
      speed: Speed.create({ value: 0.33738220028732585 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:29:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7329762987792491912841796875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.306353412568569183349609375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 3.082349424655205 }).getValue(),
      speed: Speed.create({ value: 0.324427851041535 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:30:03.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73290471732616424560546875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30626858770847320556640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 10.102273108154186 }).getValue(),
      speed: Speed.create({ value: 0.49493811407297855 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:30:11.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73279072344303131103515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.306124754250049591064453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 16.490852693999095 }).getValue(),
      speed: Speed.create({ value: 0.48511742530519014 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:30:18.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73267538845539093017578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.305997349321842193603515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 15.86766242412002 }).getValue(),
      speed: Speed.create({ value: 0.441148785051003 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:30:25.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73256114311516284942626953125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3058556951582431793212890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 16.410736852637964 }).getValue(),
      speed: Speed.create({ value: 0.4265500119133759 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:30:26.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7325473129749298095703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3058385960757732391357421875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 1.9843507230519097 }).getValue(),
      speed: Speed.create({ value: 0.5039431731412938 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:30:31.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7324582971632480621337890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.305735163390636444091796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 12.470702462119409 }).getValue(),
      speed: Speed.create({ value: 0.4009397237395274 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:30:38.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73235293664038181304931640625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.305537350475788116455078125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 18.647620101018045 }).getValue(),
      speed: Speed.create({ value: 0.3753830227170835 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:30:41.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.732307255268096923828125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30547121725976467132568359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 7.0233407507483445 }).getValue(),
      speed: Speed.create({ value: 0.42714715211280996 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:30:43.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.732290156185626983642578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3054429702460765838623046875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 2.8119279898986558 }).getValue(),
      speed: Speed.create({ value: 0.7112557672830312 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:30:45.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.732272721827030181884765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30541388504207134246826171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 2.8824623303032477 }).getValue(),
      speed: Speed.create({ value: 0.6938512184440555 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:30:51.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73218362219631671905517578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30528555810451507568359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 13.665205169206041 }).getValue(),
      speed: Speed.create({ value: 0.4390713440234871 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:30:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73212779872119426727294921875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30515723116695880889892578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 11.274368917628577 }).getValue(),
      speed: Speed.create({ value: 0.6208773237014461 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:03.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.732047416269779205322265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3050426505506038665771484375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 12.268255757162558 }).getValue(),
      speed: Speed.create({ value: 0.4075558986517588 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:04.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73201464302837848663330078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.305003590881824493408203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 4.635400909544277 }).getValue(),
      speed: Speed.create({ value: 0.2157310704109763 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:06.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73197734355926513671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.304943911731243133544921875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 6.029923019207813 }).getValue(),
      speed: Speed.create({ value: 0.33167919285688524 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:11.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73185236938297748565673828125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3047606833279132843017578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 96 }).getValue(),
      distance: Distance.create({ value: 19.331397263223973 }).getValue(),
      speed: Speed.create({ value: 0.2586465909275991 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:12.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73184080235660076141357421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.304728329181671142578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 96 }).getValue(),
      distance: Distance.create({ value: 2.699091695028242 }).getValue(),
      speed: Speed.create({ value: 0.3704950083178024 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:17.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73177391476929187774658203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30460704304277896881103515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 96 }).getValue(),
      distance: Distance.create({ value: 11.595078651165474 }).getValue(),
      speed: Speed.create({ value: 0.4312174285680613 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:23.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73168137855827808380126953125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30448491871356964111328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 96.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 13.641893438462505 }).getValue(),
      speed: Speed.create({ value: 0.43982164404563945 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:31.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73155799694359302520751953125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30432608164846897125244140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 96.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 17.998168093552643 }).getValue(),
      speed: Speed.create({ value: 0.44448968130627603 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:35.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73151776380836963653564453125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30424754321575164794921875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 96.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 7.293452418375208 }).getValue(),
      speed: Speed.create({ value: 0.5484371146265867 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:38.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7314963899552822113037109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30418140999972820281982421875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97 }).getValue(),
      distance: Distance.create({ value: 5.401381571555528 }).getValue(),
      speed: Speed.create({ value: 0.5554134549202083 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:41.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73147434554994106292724609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30409071780741214752197265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 7.0889134682649315 }).getValue(),
      speed: Speed.create({ value: 0.42319602481115826 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:45.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73138633556663990020751953125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30396063067018985748291015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 13.66752628340253 }).getValue(),
      speed: Speed.create({ value: 0.29266451858647535 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:50.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7312305159866809844970703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30378377251327037811279296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 21.643889163332005 }).getValue(),
      speed: Speed.create({ value: 0.2310120867034724 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:51.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7312083877623081207275390625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3037278652191162109375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 4.78202684593928 }).getValue(),
      speed: Speed.create({ value: 0.20911635007845325 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:31:56.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73112062923610210418701171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30367765761911869049072265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 10.429981396099897 }).getValue(),
      speed: Speed.create({ value: 0.4793872405054969 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:32:01.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73098048381507396697998046875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3037414439022541046142578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 16.270551001939143 }).getValue(),
      speed: Speed.create({ value: 0.3073036677985948 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:32:03.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73093195259571075439453125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3037256859242916107177734375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97 }).getValue(),
      distance: Distance.create({ value: 5.518800680660729 }).getValue(),
      speed: Speed.create({ value: 0.3623975779753208 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:32:06.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73089046217501163482666015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30365955270826816558837890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 6.694143332430532 }).getValue(),
      speed: Speed.create({ value: 0.44815293772784365 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:32:12.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73074478469789028167724609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30366072617471218109130859375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 16.198825025965448 }).getValue(),
      speed: Speed.create({ value: 0.37039723500824717 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:32:14.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73069214634597301483154296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30363868176937103271484375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 98 }).getValue(),
      distance: Distance.create({ value: 6.072323444472552 }).getValue(),
      speed: Speed.create({ value: 0.32936321957957265 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:32:17.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73064386658370494842529296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.303562574088573455810546875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 7.744656112204837 }).getValue(),
      speed: Speed.create({ value: 0.3873638747203103 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:32:23.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.730614446103572845458984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.303342632949352264404296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 16.459774711310356 }).getValue(),
      speed: Speed.create({ value: 0.3645250378716965 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:32:29.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73055267147719860076904296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3031687922775745391845703125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 98 }).getValue(),
      distance: Distance.create({ value: 14.482796427535408 }).getValue(),
      speed: Speed.create({ value: 0.4142846328069974 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:32:36.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.730471618473529815673828125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30298497714102268218994140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 98 }).getValue(),
      distance: Distance.create({ value: 16.216885644591184 }).getValue(),
      speed: Speed.create({ value: 0.4316488475908264 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:32:44.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73038017190992832183837890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30278414674103260040283203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 17.898692617426413 }).getValue(),
      speed: Speed.create({ value: 0.44696001942684294 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:32:50.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73029015026986598968505859375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30258633382618427276611328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 17.626568570813916 }).getValue(),
      speed: Speed.create({ value: 0.34039523778523767 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:32:57.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.730209432542324066162109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30233269743621349334716796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 20.654911791549043 }).getValue(),
      speed: Speed.create({ value: 0.3389024397995275 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:32:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73020297847688198089599609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3023024387657642364501953125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.332465904514106 }).getValue(),
      speed: Speed.create({ value: 0.4287308114835307 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:04.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73016752302646636962890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.302092723548412322998046875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 15.878738234472554 }).getValue(),
      speed: Speed.create({ value: 0.3778637767939313 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:09.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73013173229992389678955078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30190991424024105072021484375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 97 }).getValue(),
      distance: Distance.create({ value: 13.986286048298117 }).getValue(),
      speed: Speed.create({ value: 0.3574930458832144 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:12.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73011446557939052581787109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3018001951277256011962890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 96.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 8.273206342728006 }).getValue(),
      speed: Speed.create({ value: 0.3626163636831014 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:14.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73010507784783840179443359375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30172081850469112396240234375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 96.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 5.9147156823034495 }).getValue(),
      speed: Speed.create({ value: 0.3381396684854871 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:19.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7300791777670383453369140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30153004638850688934326171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 96.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 14.285483167493107 }).getValue(),
      speed: Speed.create({ value: 0.35000566248802817 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:24.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73006702400743961334228515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30131278745830059051513671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 96 }).getValue(),
      distance: Distance.create({ value: 15.992057073622698 }).getValue(),
      speed: Speed.create({ value: 0.3126552123333152 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:27.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73006685636937618255615234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3011963628232479095458984375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 8.539183993565372 }).getValue(),
      speed: Speed.create({ value: 0.3513216253755188 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:29.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7300654314458370208740234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30110122822225093841552734375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 6.97944512948917 }).getValue(),
      speed: Speed.create({ value: 0.28655573084881913 }).getValue(),
      heart_rate: HeartRate.create({ value: 71 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:33.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73005998320877552032470703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3009194247424602508544921875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 13.348130291529438 }).getValue(),
      speed: Speed.create({ value: 0.2996674375090833 }).getValue(),
      heart_rate: HeartRate.create({ value: 76 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:34.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7300587259232997894287109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30088011361658573150634765625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 2.886661955430414 }).getValue(),
      speed: Speed.create({ value: 0.3464208887080772 }).getValue(),
      heart_rate: HeartRate.create({ value: 80 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:36.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.730048500001430511474609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.300803922116756439208984375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95 }).getValue(),
      distance: Distance.create({ value: 5.702775573451272 }).getValue(),
      speed: Speed.create({ value: 0.35070641904808764 }).getValue(),
      heart_rate: HeartRate.create({ value: 85 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:37.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73004338704049587249755859375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3007678799331188201904296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 2.7039604863652555 }).getValue(),
      speed: Speed.create({ value: 0.3698278895133671 }).getValue(),
      heart_rate: HeartRate.create({ value: 89 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:39.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.73001975007355213165283203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30071180500090122222900390625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 4.8809109142231675 }).getValue(),
      speed: Speed.create({ value: 0.4097595787237011 }).getValue(),
      heart_rate: HeartRate.create({ value: 94 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:40.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.729994855821132659912109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30070040561258792877197265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 2.8916263784265404 }).getValue(),
      speed: Speed.create({ value: 0.3458261438824415 }).getValue(),
      heart_rate: HeartRate.create({ value: 96 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:42.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.729939870536327362060546875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30069638229906558990478515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 6.121201698181469 }).getValue(),
      speed: Speed.create({ value: 0.3267332296196308 }).getValue(),
      heart_rate: HeartRate.create({ value: 101 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:44.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72988664545118808746337890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30071180500090122222900390625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 6.025491657727107 }).getValue(),
      speed: Speed.create({ value: 0.3319231215655563 }).getValue(),
      heart_rate: HeartRate.create({ value: 106 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:46.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72983635403215885162353515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3007048480212688446044921875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 5.615382077468668 }).getValue(),
      speed: Speed.create({ value: 0.35616454453292884 }).getValue(),
      heart_rate: HeartRate.create({ value: 112 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:47.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7298109568655490875244140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30069839395582675933837890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.863435746807305 }).getValue(),
      speed: Speed.create({ value: 0.3492308151544827 }).getValue(),
      heart_rate: HeartRate.create({ value: 116 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:48.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.729787655174732208251953125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30069336481392383575439453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.6171542413396915 }).getValue(),
      speed: Speed.create({ value: 0.3820944078130112 }).getValue(),
      heart_rate: HeartRate.create({ value: 121 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:50.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7297355197370052337646484375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30067777447402477264404296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 5.908894192433019 }).getValue(),
      speed: Speed.create({ value: 0.3384728063943364 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:33:54.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7296372838318347930908203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30064558796584606170654296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 11.175522834737235 }).getValue(),
      speed: Speed.create({ value: 0.3579250885306835 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:34:02.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72943318448960781097412109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3005958832800388336181640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94 }).getValue(),
      distance: Distance.create({ value: 22.985757959357635 }).getValue(),
      speed: Speed.create({ value: 0.34804160098375847 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:34:04.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72938842512667179107666015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30057970620691776275634765625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94 }).getValue(),
      distance: Distance.create({ value: 5.116493462641024 }).getValue(),
      speed: Speed.create({ value: 0.39089271091683225 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:34:12.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72920435853302478790283203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.300538383424282073974609375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 20.690465036063966 }).getValue(),
      speed: Speed.create({ value: 0.3866515318073234 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:34:13.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72918105684220790863037109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30053544975817203521728515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 2.5999490464268376 }).getValue(),
      speed: Speed.create({ value: 0.3846229222739269 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:34:15.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72913478873670101165771484375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30052773840725421905517578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 5.175775231414408 }).getValue(),
      speed: Speed.create({ value: 0.38641554367758946 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:34:21.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72898475266993045806884765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30050896294414997100830078125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94 }).getValue(),
      distance: Distance.create({ value: 16.73998966733481 }).getValue(),
      speed: Speed.create({ value: 0.35842316030265897 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:34:22.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72896052896976470947265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30050493963062763214111328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 2.709669212642279 }).getValue(),
      speed: Speed.create({ value: 0.3690487367736191 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:34:28.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72881300747394561767578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30046520940959453582763671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 16.660472265668158 }).getValue(),
      speed: Speed.create({ value: 0.3601338488083593 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:34:36.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72861779294908046722412109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30043654330074787139892578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 21.80845678003384 }).getValue(),
      speed: Speed.create({ value: 0.36683017421591196 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:34:37.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7285925634205341339111328125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.300432436168193817138671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 2.8215232941919695 }).getValue(),
      speed: Speed.create({ value: 0.3544184809880795 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:34:44.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72842081822454929351806640625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30041558854281902313232421875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 19.137132973283574 }).getValue(),
      speed: Speed.create({ value: 0.36578101901535415 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:34:49.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72830498032271862030029296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.300404272973537445068359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 12.907299058374555 }).getValue(),
      speed: Speed.create({ value: 0.38737771375614666 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:34:53.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7282093428075313568115234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3003844916820526123046875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 10.732928005318563 }).getValue(),
      speed: Speed.create({ value: 0.3726848813313433 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:01.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7280212529003620147705078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30035859160125255584716796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93 }).getValue(),
      distance: Distance.create({ value: 21.00074347413812 }).getValue(),
      speed: Speed.create({ value: 0.38093889437065864 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:02.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72799602337181568145751953125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30035272426903247833251953125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93 }).getValue(),
      distance: Distance.create({ value: 2.838212733031779 }).getValue(),
      speed: Speed.create({ value: 0.3523344069180466 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:08.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72785621322691440582275390625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30036647059023380279541015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 15.578840689694214 }).getValue(),
      speed: Speed.create({ value: 0.3851377724126255 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:11.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72778781689703464508056640625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30037770234048366546630859375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 7.649814471185084 }).getValue(),
      speed: Speed.create({ value: 0.3921663736160192 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:19.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72758866287767887115478515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3003896884620189666748046875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 22.162361304367654 }).getValue(),
      speed: Speed.create({ value: 0.36097236617216405 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:24.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7274781055748462677001953125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30042078532278537750244140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 12.50322064049728 }).getValue(),
      speed: Speed.create({ value: 0.39989696605091174 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:26.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72743024490773677825927734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30042950250208377838134765625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 5.3601357472645 }).getValue(),
      speed: Speed.create({ value: 0.3731248786041815 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:33.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7272682227194309234619140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30048398487269878387451171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 18.453934624374902 }).getValue(),
      speed: Speed.create({ value: 0.3793229001014256 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:34.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7272445857524871826171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30048214085400104522705078125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.6317887780413787 }).getValue(),
      speed: Speed.create({ value: 0.3799697028665867 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:38.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72715146280825138092041015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30041827075183391571044921875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 11.365271348154359 }).getValue(),
      speed: Speed.create({ value: 0.35194936200529625 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:45.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7269825674593448638916015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3004113137722015380859375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91 }).getValue(),
      distance: Distance.create({ value: 18.787237338994654 }).getValue(),
      speed: Speed.create({ value: 0.37259336610768473 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:47.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7269254028797149658203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3004144988954067230224609375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 6.36070322732415 }).getValue(),
      speed: Speed.create({ value: 0.31443064210391863 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:53.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.726774193346500396728515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30042732320725917816162109375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 16.84002540320159 }).getValue(),
      speed: Speed.create({ value: 0.356293999346301 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:35:56.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7267022766172885894775390625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30042933486402034759521484375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 7.998136636361189 }).getValue(),
      speed: Speed.create({ value: 0.3750873655197859 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:03.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72653287835419178009033203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30043218471109867095947265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 18.837387300764625 }).getValue(),
      speed: Speed.create({ value: 0.37160142689829734 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:08.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72640597634017467498779296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3004175163805484771728515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 14.15181930405869 }).getValue(),
      speed: Speed.create({ value: 0.3533114642416341 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:10.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.726354427635669708251953125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3004185222089290618896484375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 5.732429205792815 }).getValue(),
      speed: Speed.create({ value: 0.3488922284428619 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:18.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72615636326372623443603515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3004118166863918304443359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 22.02924487945244 }).getValue(),
      speed: Speed.create({ value: 0.3631536189178196 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:19.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72613658197224140167236328125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3004158399999141693115234375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 2.219288291631898 }).getValue(),
      speed: Speed.create({ value: 0.4505949063808538 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:25.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72600817121565341949462890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.300445176661014556884765625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 14.43986351086784 }).getValue(),
      speed: Speed.create({ value: 0.41551639289971365 }).getValue(),
      heart_rate: HeartRate.create({ value: 121 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:30.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72590255923569202423095703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3004406504333019256591796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87 }).getValue(),
      distance: Distance.create({ value: 11.748208492214642 }).getValue(),
      speed: Speed.create({ value: 0.42559680510551234 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:31.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72588294558227062225341796875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30045598931610584259033203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 2.454057240807622 }).getValue(),
      speed: Speed.create({ value: 0.4074884576330841 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:34.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72582041658461093902587890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.300511896610260009765625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 8.072174596603995 }).getValue(),
      speed: Speed.create({ value: 0.37164706537221276 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:37.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7257521040737628936767578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3005427420139312744140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 7.925806688814711 }).getValue(),
      speed: Speed.create({ value: 0.3785103671824028 }).getValue(),
      heart_rate: HeartRate.create({ value: 121 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:39.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.725702650845050811767578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.300549782812595367431640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 5.523147035270251 }).getValue(),
      speed: Speed.create({ value: 0.3621123948408769 }).getValue(),
      heart_rate: HeartRate.create({ value: 118 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:40.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72567725367844104766845703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3005506210029125213623046875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.824705269232233 }).getValue(),
      speed: Speed.create({ value: 0.35401923552604986 }).getValue(),
      heart_rate: HeartRate.create({ value: 119 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:46.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72552084736526012420654296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30059294961392879486083984375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 17.66656595888074 }).getValue(),
      speed: Speed.create({ value: 0.3396245775192028 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:52.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.725387491285800933837890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30060049332678318023681640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 14.838840239761394 }).getValue(),
      speed: Speed.create({ value: 0.404344268356142 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:57.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72525866143405437469482421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30061323381960391998291015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 14.355676979444418 }).getValue(),
      speed: Speed.create({ value: 0.3482942676377709 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:36:59.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72522127814590930938720703125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30057283304631710052490234375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 5.105042448938734 }).getValue(),
      speed: Speed.create({ value: 0.3917695141625652 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:00.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7251925282180309295654296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30048029683530330657958984375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 7.502867070713246 }).getValue(),
      speed: Speed.create({ value: 0.1332823826645428 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:01.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72517534531652927398681640625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30047442950308322906494140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 1.9585241972826228 }).getValue(),
      speed: Speed.create({ value: 0.5105885346667974 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:02.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72515606693923473358154296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30047593824565410614013671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 2.1465125863138486 }).getValue(),
      speed: Speed.create({ value: 0.46587194800347037 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:09.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72502245940268039703369140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30043218471109867095947265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 15.19919020219316 }).getValue(),
      speed: Speed.create({ value: 0.4605508521756599 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:15.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72486907057464122772216796875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30040838010609149932861328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 17.145206974058137 }).getValue(),
      speed: Speed.create({ value: 0.3499520308549443 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:22.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72467461042106151580810546875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30040854774415493011474609375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 21.622986008855772 }).getValue(),
      speed: Speed.create({ value: 0.3237295717221074 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:27.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72456581331789493560791015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30047375895082950592041015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 13.009044504551293 }).getValue(),
      speed: Speed.create({ value: 0.3843479817638198 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:32.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7244619615375995635986328125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30053813196718692779541015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 12.475912060186362 }).getValue(),
      speed: Speed.create({ value: 0.4007723023277956 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:36.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72436054050922393798828125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.300545759499073028564453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 11.291374455974593 }).getValue(),
      speed: Speed.create({ value: 0.3542527099420996 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:42.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.724200613796710968017578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30044308118522167205810546875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 19.312291087824615 }).getValue(),
      speed: Speed.create({ value: 0.3106829724507769 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:49.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7240393459796905517578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30033126659691333770751953125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 19.71891062780365 }).getValue(),
      speed: Speed.create({ value: 0.35498918434824717 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:52.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72397027909755706787109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.300286591053009033203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88 }).getValue(),
      distance: Distance.create({ value: 8.349861883187424 }).getValue(),
      speed: Speed.create({ value: 0.3592873800751778 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:55.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72390146367251873016357421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30024007149040699005126953125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88 }).getValue(),
      distance: Distance.create({ value: 8.378329104913686 }).getValue(),
      speed: Speed.create({ value: 0.3580666219282999 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:37:59.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72380976565182209014892578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3001895286142826080322265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 10.849487902534163 }).getValue(),
      speed: Speed.create({ value: 0.3686809954473245 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:38:07.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.723627962172031402587890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30007679201662540435791015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 21.841693057832828 }).getValue(),
      speed: Speed.create({ value: 0.36627197254431954 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:38:14.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7234621681272983551025390625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.300002612173557281494140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 19.22173698683493 }).getValue(),
      speed: Speed.create({ value: 0.36417104264793226 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:38:15.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.723437525331974029541015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29999565519392490386962890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87 }).getValue(),
      distance: Distance.create({ value: 2.787270409272041 }).getValue(),
      speed: Speed.create({ value: 0.3587739448147669 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:38:23.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72325203381478786468505859375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29992994107306003570556640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 21.18152128512581 }).getValue(),
      speed: Speed.create({ value: 0.37768769732407265 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:38:31.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72305623255670070648193359375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.299867831170558929443359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 22.243705962043745 }).getValue(),
      speed: Speed.create({ value: 0.3596522995606512 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:38:32.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72303552925586700439453125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2998529113829135894775390625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 2.5490159620550736 }).getValue(),
      speed: Speed.create({ value: 0.3923082534146933 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:38:38.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72290544211864471435546875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29980940930545330047607421875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 14.81284121238691 }).getValue(),
      speed: Speed.create({ value: 0.40505396054489756 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:38:45.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72275389730930328369140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29976900853216648101806640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 17.109638204973294 }).getValue(),
      speed: Speed.create({ value: 0.4091261262301441 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:38:53.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7225534021854400634765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.299719639122486114501953125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 22.586273369112117 }).getValue(),
      speed: Speed.create({ value: 0.3541974308581782 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:38:54.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7225324474275112152099609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2997080720961093902587890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 2.479750464519563 }).getValue(),
      speed: Speed.create({ value: 0.4032663827703906 }).getValue(),
      heart_rate: HeartRate.create({ value: 122 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:39:01.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72237428091466426849365234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2996222414076328277587890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 18.680358470654014 }).getValue(),
      speed: Speed.create({ value: 0.37472514304244636 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:39:05.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72227839194238185882568359375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29960422031581401824951171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85 }).getValue(),
      distance: Distance.create({ value: 10.744005493612994 }).getValue(),
      speed: Speed.create({ value: 0.3723006286973593 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:39:09.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.722189627587795257568359375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2995789907872676849365234375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 10.04216347613093 }).getValue(),
      speed: Speed.create({ value: 0.3983205421329319 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:39:17.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72200715355575084686279296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29954563081264495849609375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 20.437229184056502 }).getValue(),
      speed: Speed.create({ value: 0.3914424958467933 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:39:21.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72190229594707489013671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29952417314052581787109375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 11.765404931997871 }).getValue(),
      speed: Speed.create({ value: 0.33997979866560907 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:39:25.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72180582024157047271728515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2995092533528804779052734375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 10.783295425296222 }).getValue(),
      speed: Speed.create({ value: 0.37094411701051194 }).getValue(),
      heart_rate: HeartRate.create({ value: 119 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:39:32.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72165150940418243408203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2994740493595600128173828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85 }).getValue(),
      distance: Distance.create({ value: 17.35183184597334 }).getValue(),
      speed: Speed.create({ value: 0.4034156198686548 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:39:40.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72146375477313995361328125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29942367412149906158447265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 21.201891836127167 }).getValue(),
      speed: Speed.create({ value: 0.37732481902243853 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:39:48.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7212748266756534576416015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29941864497959613800048828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 21.011085112673538 }).getValue(),
      speed: Speed.create({ value: 0.38075139656516516 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:39:56.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72109419666230678558349609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29939919896423816680908203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 20.135735668267586 }).getValue(),
      speed: Speed.create({ value: 0.3973035866083305 }).getValue(),
      heart_rate: HeartRate.create({ value: 121 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:39:57.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7210728228092193603515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29939517565071582794189453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.3949198419193185 }).getValue(),
      speed: Speed.create({ value: 0.41755050941437255 }).getValue(),
      heart_rate: HeartRate.create({ value: 121 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:40:04.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7209145724773406982421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2993687726557254791259765625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85 }).getValue(),
      distance: Distance.create({ value: 17.702909972046108 }).getValue(),
      speed: Speed.create({ value: 0.39541521767061993 }).getValue(),
      heart_rate: HeartRate.create({ value: 120 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:40:07.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72084273956716060638427734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29936198331415653228759765625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85 }).getValue(),
      distance: Distance.create({ value: 8.002968148333316 }).getValue(),
      speed: Speed.create({ value: 0.37486091964826507 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:40:11.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7207538075745105743408203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29934706352651119232177734375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 84.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 9.9491714319312 }).getValue(),
      speed: Speed.create({ value: 0.40204352969155477 }).getValue(),
      heart_rate: HeartRate.create({ value: 121 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:40:13.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7207088805735111236572265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29933440685272216796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85 }).getValue(),
      distance: Distance.create({ value: 5.081203820866574 }).getValue(),
      speed: Speed.create({ value: 0.39360751320125353 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:40:20.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72054736129939556121826171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29930615983903408050537109375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85 }).getValue(),
      distance: Distance.create({ value: 18.079267666937636 }).getValue(),
      speed: Speed.create({ value: 0.3871838245307476 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:40:28.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72037134133279323577880859375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29928436689078807830810546875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85 }).getValue(),
      distance: Distance.create({ value: 19.637711022852432 }).getValue(),
      speed: Speed.create({ value: 0.40737945429028816 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:40:34.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72023253701627254486083984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29926290921866893768310546875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85 }).getValue(),
      distance: Distance.create({ value: 15.514398794855499 }).getValue(),
      speed: Speed.create({ value: 0.38673751263823203 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:40:38.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72014435939490795135498046875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29924715124070644378662109375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 9.872814767079502 }).getValue(),
      speed: Speed.create({ value: 0.4051529471957518 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:40:45.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71999801136553287506103515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29924120008945465087890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 16.27901350697991 }).getValue(),
      speed: Speed.create({ value: 0.43000148608505223 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:40:51.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71987077407538890838623046875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2992497496306896209716796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 14.162036156790172 }).getValue(),
      speed: Speed.create({ value: 0.4236678916487035 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:40:59.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71970548294484615325927734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29931722395122051239013671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 19.034415398216822 }).getValue(),
      speed: Speed.create({ value: 0.42029134242543925 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:41:06.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7195654213428497314453125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29945552535355091094970703125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 18.58740081830905 }).getValue(),
      speed: Speed.create({ value: 0.3765991850299385 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:41:12.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7194227613508701324462890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29954102076590061187744140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 17.05797820706024 }).getValue(),
      speed: Speed.create({ value: 0.3517415679143393 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:41:19.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71925973333418369293212890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29962936602532863616943359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 19.25161108610606 }).getValue(),
      speed: Speed.create({ value: 0.36360593244333295 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:41:24.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71915462426841259002685546875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29958661831915378570556640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 12.101008821442074 }).getValue(),
      speed: Speed.create({ value: 0.4131886914370625 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:41:32.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71899050660431385040283203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2994381748139858245849609375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 85.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 21.251336017993715 }).getValue(),
      speed: Speed.create({ value: 0.376446920477203 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:41:37.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71886611916124820709228515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29937992058694362640380859375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86 }).getValue(),
      distance: Distance.create({ value: 14.476436835242076 }).getValue(),
      speed: Speed.create({ value: 0.3453888589371509 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:41:39.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71881750412285327911376953125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29934823699295520782470703125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 5.8842753313128116 }).getValue(),
      speed: Speed.create({ value: 0.33988892215106287 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:41:46.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71864383108913898468017578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.299252264201641082763671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 20.554998409429093 }).getValue(),
      speed: Speed.create({ value: 0.3405497709398472 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:41:49.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71857442893087863922119140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29920607991516590118408203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 86.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 8.428184490535385 }).getValue(),
      speed: Speed.create({ value: 0.35594854424092354 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:41:52.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.718503601849079132080078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2991712950170040130615234375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 8.278730163594506 }).getValue(),
      speed: Speed.create({ value: 0.36237441500296985 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:41:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71836404316127300262451171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.299101389944553375244140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87 }).getValue(),
      distance: Distance.create({ value: 16.343662388305606 }).getValue(),
      speed: Speed.create({ value: 0.36711477864919584 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:42:06.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71818073093891143798828125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.299031317234039306640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 21.021636987600417 }).getValue(),
      speed: Speed.create({ value: 0.3805602772381042 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:42:07.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7181580998003482818603515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2990234382450580596923828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 2.581998940507098 }).getValue(),
      speed: Speed.create({ value: 0.38729682817127825 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:42:15.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71797160245478153228759765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29896300472319126129150390625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 21.206195787638556 }).getValue(),
      speed: Speed.create({ value: 0.3772482382089169 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:42:23.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71779273264110088348388671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2989248670637607574462890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 20.085243743346673 }).getValue(),
      speed: Speed.create({ value: 0.3983023607891259 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:42:28.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71767387725412845611572265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.298891842365264892578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 13.436354998594256 }).getValue(),
      speed: Speed.create({ value: 0.37212473178351657 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:42:30.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71762760914862155914306640625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2988873161375522613525390625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 5.155483430260513 }).getValue(),
      speed: Speed.create({ value: 0.3879364616440902 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:42:31.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7176042236387729644775390625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29888421483337879180908203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 2.6102847350382574 }).getValue(),
      speed: Speed.create({ value: 0.3830999685884244 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:42:39.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.717424012720584869384765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29885085485875606536865234375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 20.187442490117338 }).getValue(),
      speed: Speed.create({ value: 0.3962859586555533 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:42:41.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71737548150122165679931640625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.298840545117855072021484375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 5.449172850454906 }).getValue(),
      speed: Speed.create({ value: 0.3670281811363419 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:42:48.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71721773408353328704833984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29882277548313140869140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 17.589089962769208 }).getValue(),
      speed: Speed.create({ value: 0.39797397220759495 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:42:54.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71708580292761325836181640625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.298809699714183807373046875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 14.701406083652575 }).getValue(),
      speed: Speed.create({ value: 0.40812422742827165 }).getValue(),
      heart_rate: HeartRate.create({ value: 131 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:42:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7169946916401386260986328125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29879369027912616729736328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 10.198967448908485 }).getValue(),
      speed: Speed.create({ value: 0.3921965649991449 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:43:06.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7168107926845550537109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2987418062984943389892578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 20.799889065376522 }).getValue(),
      speed: Speed.create({ value: 0.3846174359322326 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:43:11.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71671314351260662078857421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2987458296120166778564453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 10.862103715297309 }).getValue(),
      speed: Speed.create({ value: 0.4603159876809503 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:43:12.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71669269166886806488037109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29874641634523868560791015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.2745486136598605 }).getValue(),
      speed: Speed.create({ value: 0.43964767074859346 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:43:19.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7165326811373233795166015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.298736609518527984619140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 17.806900150850577 }).getValue(),
      speed: Speed.create({ value: 0.39310603983285847 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:43:27.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.716350458562374114990234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29870936833322048187255859375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 20.360549647282724 }).getValue(),
      speed: Speed.create({ value: 0.39291670110033905 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:43:30.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7162768654525279998779296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29869361035525798797607421875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90 }).getValue(),
      distance: Distance.create({ value: 8.264440356782089 }).getValue(),
      speed: Speed.create({ value: 0.3630009862117397 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:43:34.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71617804281413555145263671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2986754216253757476806640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 11.06930341408659 }).getValue(),
      speed: Speed.create({ value: 0.3613596854622012 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:43:37.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7161159329116344451904296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2986680455505847930908203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 6.927474599488039 }).getValue(),
      speed: Speed.create({ value: 0.43305824610626636 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:43:43.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71597193181514739990234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29864382185041904449462890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 16.11051363924414 }).getValue(),
      speed: Speed.create({ value: 0.3724276043803097 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:43:44.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71594787575304508209228515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2986356914043426513671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 2.740613169143533 }).getValue(),
      speed: Speed.create({ value: 0.3648818487990077 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:43:52.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71576934121549129486083984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29858732782304286956787109375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91 }).getValue(),
      distance: Distance.create({ value: 20.16673455479274 }).getValue(),
      speed: Speed.create({ value: 0.39669287946762577 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:43:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7156279385089874267578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29859235696494579315185546875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 15.727592145847181 }).getValue(),
      speed: Speed.create({ value: 0.3814951420637062 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:44:04.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71548921801149845123291015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2986656986176967620849609375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 16.336586427670117 }).getValue(),
      speed: Speed.create({ value: 0.3672737892071193 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:44:11.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7153464742004871368408203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29880458675324916839599609375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 18.861680896799584 }).getValue(),
      speed: Speed.create({ value: 0.371122809165314 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:44:12.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71532878838479518890380859375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2988283075392246246337890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 2.6260406548408617 }).getValue(),
      speed: Speed.create({ value: 0.38080141606208306 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:44:18.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71523222886025905609130859375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.298963926732540130615234375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 14.638388575600342 }).getValue(),
      speed: Speed.create({ value: 0.40988118118417494 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:44:26.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7150899879634380340576171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.299155704677104949951171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 21.169055101900597 }).getValue(),
      speed: Speed.create({ value: 0.3779101127325114 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:44:27.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7150799296796321868896484375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29917808435857295989990234375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 1.9866576642212699 }).getValue(),
      speed: Speed.create({ value: 0.5033579856305943 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:44:34.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71496417559683322906494140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29935343377292156219482421875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 18.19818228154554 }).getValue(),
      speed: Speed.create({ value: 0.38465380177549807 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:44:42.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.714833669364452362060546875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.299567423760890960693359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93 }).getValue(),
      distance: Distance.create({ value: 21.37924259038104 }).getValue(),
      speed: Speed.create({ value: 0.37419473426992983 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:44:47.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71474557556211948394775390625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29970262385904788970947265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 13.940775744745084 }).getValue(),
      speed: Speed.create({ value: 0.35866009837255497 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:44:49.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7147154845297336578369140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.29975358583033084869384765625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 5.017492120210058 }).getValue(),
      speed: Speed.create({ value: 0.3986055089043707 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:44:56.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.714600987732410430908203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.2999474592506885528564453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 19.089587587802832 }).getValue(),
      speed: Speed.create({ value: 0.3666920496738549 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:45:00.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7145381234586238861083984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3000674881041049957275390625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 11.243326202857315 }).getValue(),
      speed: Speed.create({ value: 0.3557666057028091 }).getValue(),
      heart_rate: HeartRate.create({ value: 133 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:45:03.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.714485652744770050048828125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.300155162811279296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 8.684356382950867 }).getValue(),
      speed: Speed.create({ value: 0.3454487434313039 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:45:09.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71440191753208637237548828125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30030863545835018157958984375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 14.610949115644413 }).getValue(),
      speed: Speed.create({ value: 0.4106509407780777 }).getValue(),
      heart_rate: HeartRate.create({ value: 133 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:45:17.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71429697610437870025634765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30050695128738880157470703125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94 }).getValue(),
      distance: Distance.create({ value: 18.651207361611938 }).getValue(),
      speed: Speed.create({ value: 0.42892665578667377 }).getValue(),
      heart_rate: HeartRate.create({ value: 133 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:45:24.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7141669727861881256103515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30069939978420734405517578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 20.207177118928787 }).getValue(),
      speed: Speed.create({ value: 0.3464115724230897 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:45:25.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71415205299854278564453125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3007239587604999542236328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 2.4492757122723052 }).getValue(),
      speed: Speed.create({ value: 0.40828396533285927 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:45:33.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7140266597270965576171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30094247497618198394775390625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 21.247120350076916 }).getValue(),
      speed: Speed.create({ value: 0.37652161178496074 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:45:40.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71392012573778629302978515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3011355102062225341796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 18.463765995655926 }).getValue(),
      speed: Speed.create({ value: 0.3791209226572157 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:45:41.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.713902942836284637451171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30115990154445171356201171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 2.6178433684927858 }).getValue(),
      speed: Speed.create({ value: 0.38199382439589824 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:45:49.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7137622945010662078857421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30137933976948261260986328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 22.44541620075439 }).getValue(),
      speed: Speed.create({ value: 0.35642021196876356 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:45:51.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.713728599250316619873046875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30143985711038112640380859375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95 }).getValue(),
      distance: Distance.create({ value: 5.809682994308519 }).getValue(),
      speed: Speed.create({ value: 0.3442528623264486 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:45:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.713605217635631561279296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30163850821554660797119140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 20.016179110394415 }).getValue(),
      speed: Speed.create({ value: 0.3497170944261233 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:45:59.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71358342468738555908203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30166952125728130340576171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 3.3241003293546076 }).getValue(),
      speed: Speed.create({ value: 0.3008332784570782 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:46:06.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71349398978054523468017578125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30190857313573360443115234375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95 }).getValue(),
      distance: Distance.create({ value: 20.162203325507587 }).getValue(),
      speed: Speed.create({ value: 0.34718427777901467 }).getValue(),
      heart_rate: HeartRate.create({ value: 131 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:46:07.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71348678134381771087646484375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30194092728197574615478515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95 }).getValue(),
      distance: Distance.create({ value: 2.5054699852067697 }).getValue(),
      speed: Speed.create({ value: 0.3991267131134571 }).getValue(),
      heart_rate: HeartRate.create({ value: 131 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:46:14.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7134229950606822967529296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.302181236445903778076171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95 }).getValue(),
      distance: Distance.create({ value: 19.00445022687909 }).getValue(),
      speed: Speed.create({ value: 0.3683347803505253 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:46:18.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71338762342929840087890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30232180096209049224853515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 11.037665339212033 }).getValue(),
      speed: Speed.create({ value: 0.36239547740134287 }).getValue(),
      heart_rate: HeartRate.create({ value: 131 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:46:22.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.713370859622955322265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30246697552502155303955078125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 10.813239867992326 }).getValue(),
      speed: Speed.create({ value: 0.36991688419306956 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:46:28.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7133579514920711517333984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3026665486395359039306640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 96.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 14.712729193163238 }).getValue(),
      speed: Speed.create({ value: 0.40781012966568436 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:46:33.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.713414110243320465087890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30279051698744297027587890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 95.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 11.032781367865434 }).getValue(),
      speed: Speed.create({ value: 0.4531948774552191 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:46:39.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71353941969573497772216796875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3028060235083103179931640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 13.980145177438175 }).getValue(),
      speed: Speed.create({ value: 0.4291800924702189 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:46:40.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71356355957686901092529296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3028003238141536712646484375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 2.7166116215252956 }).getValue(),
      speed: Speed.create({ value: 0.3681056180708415 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:46:46.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71370554901659488677978515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3028333485126495361328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 15.973345605943795 }).getValue(),
      speed: Speed.create({ value: 0.3756257548053901 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:46:54.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71388249099254608154296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30291238985955715179443359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 20.511896112523832 }).getValue(),
      speed: Speed.create({ value: 0.39001757595269243 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:46:55.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71390453539788722991943359375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3029317520558834075927734375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.8331162284696543 }).getValue(),
      speed: Speed.create({ value: 0.352968222747488 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:46:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7139751948416233062744140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30297894217073917388916015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 8.58599478825052 }).getValue(),
      speed: Speed.create({ value: 0.34940622187487713 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:47:04.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7141089700162410736083984375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30303526856005191802978515625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 94.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 15.438503554148268 }).getValue(),
      speed: Speed.create({ value: 0.3886387031590133 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:47:11.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71426931582391262054443359375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30310517363250255584716796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 18.552647286394166 }).getValue(),
      speed: Speed.create({ value: 0.37730464509685063 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:47:19.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71444231830537319183349609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30322034098207950592041015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93 }).getValue(),
      distance: Distance.create({ value: 21.010892949982445 }).getValue(),
      speed: Speed.create({ value: 0.3807548788642362 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:47:20.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71446453034877777099609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30323475785553455352783203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93 }).getValue(),
      distance: Distance.create({ value: 2.686827379853082 }).getValue(),
      speed: Speed.create({ value: 0.3721861729928779 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:47:24.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.714556731283664703369140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30327465571463108062744140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 10.661976587412852 }).getValue(),
      speed: Speed.create({ value: 0.3751649581300204 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:47:27.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71463057585060596466064453125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30330080725252628326416015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 8.432327764859105 }).getValue(),
      speed: Speed.create({ value: 0.35577364680986484 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:47:34.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71480961330235004425048828125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30335428379476070404052734375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 93 }).getValue(),
      distance: Distance.create({ value: 20.29098410450346 }).getValue(),
      speed: Speed.create({ value: 0.3449808034912606 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:47:39.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71492846868932247161865234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3034018091857433319091796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 92.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 13.66833993350077 }).getValue(),
      speed: Speed.create({ value: 0.36580887103525433 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:47:43.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71502184309065341949462890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30343273840844631195068359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 10.62783593029614 }).getValue(),
      speed: Speed.create({ value: 0.37637013087466265 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:47:47.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71511991135776042938232421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30346526019275188446044921875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 11.16268083865414 }).getValue(),
      speed: Speed.create({ value: 0.3583368599188823 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:47:49.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7151567079126834869384765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30350566096603870391845703125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 5.05240982081959 }).getValue(),
      speed: Speed.create({ value: 0.3958507070741868 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:47:52.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71523004956543445587158203125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30354924686253070831298828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 8.759750580778894 }).getValue(),
      speed: Speed.create({ value: 0.3424755045631959 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:47:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71537421829998493194580078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30355863459408283233642578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 16.04562059258534 }).getValue(),
      speed: Speed.create({ value: 0.37393380738246995 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:48:06.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71556021273136138916015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3036085069179534912109375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 21.00280954506675 }).getValue(),
      speed: Speed.create({ value: 0.3809014209662765 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:48:14.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71575073339045047760009765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30363977141678333282470703125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 21.30874433641533 }).getValue(),
      speed: Speed.create({ value: 0.37543272722684523 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:48:21.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7158916331827640533447265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30368562042713165283203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 16.024369158517054 }).getValue(),
      speed: Speed.create({ value: 0.43683466916883 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:48:26.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7160341255366802215576171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30373113416135311126708984375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 16.192458799971092 }).getValue(),
      speed: Speed.create({ value: 0.30878571696652557 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:48:31.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71613261289894580841064453125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30380271561443805694580078125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 91 }).getValue(),
      distance: Distance.create({ value: 12.145376656065372 }).getValue(),
      speed: Speed.create({ value: 0.4116792868258237 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:48:39.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71632556430995464324951171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.303841523826122283935546875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 21.643307570757994 }).getValue(),
      speed: Speed.create({ value: 0.3696292710273499 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:48:46.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71648917905986309051513671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3038755543529987335205078125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 18.363638737283303 }).getValue(),
      speed: Speed.create({ value: 0.38118806954027307 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:48:50.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71657467447221279144287109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30388586409389972686767578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 9.536697957491855 }).getValue(),
      speed: Speed.create({ value: 0.41943238821542767 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:48:54.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7166694737970829010009765625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30394219048321247100830078125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 90 }).getValue(),
      distance: Distance.create({ value: 11.322252646133544 }).getValue(),
      speed: Speed.create({ value: 0.3532865874853947 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:49:01.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7168552167713642120361328125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30400455184280872344970703125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 21.154335722371663 }).getValue(),
      speed: Speed.create({ value: 0.3309014327780184 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:49:10.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7170560471713542938232421875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30403162539005279541015625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 22.41947860624915 }).getValue(),
      speed: Speed.create({ value: 0.40143663276323305 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:49:13.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7171244435012340545654296875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3040457069873809814453125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 7.675169261775575 }).getValue(),
      speed: Speed.create({ value: 0.39087085869764643 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:49:15.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71717750094830989837646484375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3040698468685150146484375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 6.159797363513451 }).getValue(),
      speed: Speed.create({ value: 0.3246860053947021 }).getValue(),
      heart_rate: HeartRate.create({ value: 123 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:49:21.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71732368133962154388427734375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30413832701742649078369140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 17.013215002596112 }).getValue(),
      speed: Speed.create({ value: 0.35266702966396635 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:49:27.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71745410375297069549560546875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30414704419672489166259765625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 14.516404664258957 }).getValue(),
      speed: Speed.create({ value: 0.41332548511634454 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:49:33.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71757731772959232330322265625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3041950725018978118896484375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 14.146597824009504 }).getValue(),
      speed: Speed.create({ value: 0.424130244928349 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:49:39.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71768611483275890350341796875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.304213345050811767578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 12.171730710768857 }).getValue(),
      speed: Speed.create({ value: 0.4929455097697438 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:49:45.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.717852495610713958740234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30420689098536968231201171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 18.506756421042294 }).getValue(),
      speed: Speed.create({ value: 0.32420592044848895 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:49:52.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.718018122017383575439453125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30427704751491546630859375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 19.122484173273868 }).getValue(),
      speed: Speed.create({ value: 0.3660612259668334 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:49:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71810956858098506927490234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30432038195431232452392578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 10.653781301460723 }).getValue(),
      speed: Speed.create({ value: 0.5631803235136195 }).getValue(),
      heart_rate: HeartRate.create({ value: 125 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:01.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.718188442289829254150390625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30430646799504756927490234375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88 }).getValue(),
      distance: Distance.create({ value: 8.82955843902492 }).getValue(),
      speed: Speed.create({ value: 0.33976784011537736 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:03.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71823797933757305145263671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30432691983878612518310546875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 5.708958082431398 }).getValue(),
      speed: Speed.create({ value: 0.35032662197236114 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:09.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.718418441712856292724609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30449221096932888031005859375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 23.445829021047246 }).getValue(),
      speed: Speed.create({ value: 0.25590905719792717 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:10.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.718445934355258941650390625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30451752431690692901611328125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88 }).getValue(),
      distance: Distance.create({ value: 3.5768832158391612 }).getValue(),
      speed: Speed.create({ value: 0.2795730080232416 }).getValue(),
      heart_rate: HeartRate.create({ value: 130 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:17.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71862404979765415191650390625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30459178797900676727294921875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 20.541205757580794 }).getValue(),
      speed: Speed.create({ value: 0.3407784373814876 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:18.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7186482734978199005126953125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3045990802347660064697265625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 87.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 2.746164680267827 }).getValue(),
      speed: Speed.create({ value: 0.36414422164313626 }).getValue(),
      heart_rate: HeartRate.create({ value: 131 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:26.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71883963234722614288330078125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3046572506427764892578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 21.70184881894537 }).getValue(),
      speed: Speed.create({ value: 0.36863218736535147 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:28.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71888615190982818603515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30466169305145740509033203125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 5.182995646240396 }).getValue(),
      speed: Speed.create({ value: 0.385877229407041 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:36.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71906192041933536529541015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3047097213566303253173828125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88 }).getValue(),
      distance: Distance.create({ value: 19.85962026195314 }).getValue(),
      speed: Speed.create({ value: 0.40282744052897723 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:44.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71925034560263156890869140625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30478457175195217132568359375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 21.65952990537003 }).getValue(),
      speed: Speed.create({ value: 0.36935242985197786 }).getValue(),
      heart_rate: HeartRate.create({ value: 134 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:51.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71941530145704746246337890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3048629425466060638427734375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 19.222203471652776 }).getValue(),
      speed: Speed.create({ value: 0.36416220493779433 }).getValue(),
      heart_rate: HeartRate.create({ value: 133 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:53.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71945343911647796630859375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.304911054670810699462890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 5.517357601835703 }).getValue(),
      speed: Speed.create({ value: 0.3624923639777439 }).getValue(),
      heart_rate: HeartRate.create({ value: 131 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:55.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71947783045470714569091796875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.304975427687168121337890625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 5.445858073907833 }).getValue(),
      speed: Speed.create({ value: 0.36725158328719393 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:50:59.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71955402195453643798828125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30501960031688213348388671875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 9.070699713507283 }).getValue(),
      speed: Speed.create({ value: 0.4409803131332365 }).getValue(),
      heart_rate: HeartRate.create({ value: 132 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:02.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71961361728608608245849609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.305045165121555328369140625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 6.886974157749125 }).getValue(),
      speed: Speed.create({ value: 0.4356049451157651 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:03.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71963490732014179229736328125,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30505061335861682891845703125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 2.4008464650200905 }).getValue(),
      speed: Speed.create({ value: 0.4165197627461079 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:06.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71968955732882022857666015625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3050698079168796539306640625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 6.237814830816438 }).getValue(),
      speed: Speed.create({ value: 0.4809376490592851 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:13.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.71985233388841152191162109375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3051251284778118133544921875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.59999847412109375 }).getValue(),
      distance: Distance.create({ value: 18.54932111695729 }).getValue(),
      speed: Speed.create({ value: 0.37737230143699374 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:16.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.719920478761196136474609375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3051559738814830780029296875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 88.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 7.90801957469289 }).getValue(),
      speed: Speed.create({ value: 0.3793617316781244 }).getValue(),
      heart_rate: HeartRate.create({ value: 124 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:17.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.719943277537822723388671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30517013929784297943115234375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 2.7398269871405927 }).getValue(),
      speed: Speed.create({ value: 0.36498655013382625 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:21.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72003572992980480194091796875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30521255172789096832275390625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 10.740756449591956 }).getValue(),
      speed: Speed.create({ value: 0.37241324843111595 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:25.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7201214767992496490478515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30527038685977458953857421875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 10.435991981811513 }).getValue(),
      speed: Speed.create({ value: 0.3832889108166665 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:28.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.720186688005924224853515625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3053102008998394012451171875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.1999969482421875 }).getValue(),
      distance: Distance.create({ value: 7.817285501069526 }).getValue(),
      speed: Speed.create({ value: 0.38376492704399157 }).getValue(),
      heart_rate: HeartRate.create({ value: 128 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:35.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.720340244472026824951171875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.30537348426878452301025390625,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 17.694560466633668 }).getValue(),
      speed: Speed.create({ value: 0.3956018016497093 }).getValue(),
      heart_rate: HeartRate.create({ value: 129 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:43.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7205274961888790130615234375,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.305406592786312103271484375,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89 }).getValue(),
      distance: Distance.create({ value: 20.96262186419238 }).getValue(),
      speed: Speed.create({ value: 0.3816316514140496 }).getValue(),
      heart_rate: HeartRate.create({ value: 127 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:50.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.7206887640058994293212890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3054258711636066436767578125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.8000030517578125 }).getValue(),
      distance: Distance.create({ value: 17.987844311836664 }).getValue(),
      speed: Speed.create({ value: 0.3891516892546008 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:57.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72085489332675933837890625,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3054510168731212615966796875,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 18.5646109517885 }).getValue(),
      speed: Speed.create({ value: 0.37706149717754395 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
    TrackPoint.create({
      date_time: DateTime.create({ value: '2022-03-19T07:51:58.000Z' }).getValue(),
      latitude: Coordinate.create({
        value: 48.72087727300822734832763671875,
        name: 'latitude',
      }).getValue(),
      longitude: Coordinate.create({
        value: 2.3054567165672779083251953125,
        name: 'longitude',
      }).getValue(),
      altitude: Distance.create({ value: 89.40000152587890625 }).getValue(),
      distance: Distance.create({ value: 2.5233889979548367 }).getValue(),
      speed: Speed.create({ value: 0.39629244670975533 }).getValue(),
      heart_rate: HeartRate.create({ value: 126 }).getValue(),
    }).getValue(),
  ],
];

const tracks = {
  forrest_gump: {
    Segments: segments,
    TrackId: 'b2dbb4ef-d07f-4ed4-a854-54b8e47d47b6',
    median_track: [
      {
        altitude: 86.5999984741211,
        climb: 1031.0000076293945,
        date_time: '2019-10-26T13:38:01.000Z',
        distance: 40.410776010083005,
        latitude: 48.74073182232678,
        longitude: 2.3123100958764553,
        speed: 0.5134559079286333,
      },
    ],
  },
  chloe_mccardell: {
    Segments: long_segments,
    TrackId: '35de8cc3-f4e1-4db3-b108-c11d6b4bbfae',
    interval_track: [
      {
        altitude: 83.19999694824219,
        climb: 83.19999694824219,
        date_time: '2022-03-19T07:21:14.000Z',
        distance: 0,
        latitude: 48.74078546650708,
        longitude: 2.3128146026283503,
        speed: null,
        time: 0,
      },
      {
        altitude: 90.5999984741211,
        climb: 7.400001525878906,
        date_time: '2022-03-19T07:28:17.000Z',
        distance: 1004.9852407191719,
        latitude: 48.734678914770484,
        longitude: 2.308778716251254,
        speed: 2.3758516329058437,
        time: 423,
      },
      {
        altitude: 93.19999694824219,
        climb: 2.5999984741210938,
        date_time: '2022-03-19T07:34:44.000Z',
        distance: 2018.6479016117485,
        latitude: 48.72842081822455,
        longitude: 2.300415588542819,
        speed: 2.619283361479526,
        time: 387,
      },
      {
        altitude: 85.80000305175781,
        climb: -7.399993896484375,
        date_time: '2022-03-19T07:41:06.000Z',
        distance: 3031.967990907064,
        latitude: 48.71956542134285,
        longitude: 2.299455525353551,
        speed: 2.652670390825434,
        time: 382,
      },
      {
        altitude: 92.80000305175781,
        climb: 7,
        date_time: '2022-03-19T07:47:27.000Z',
        distance: 4036.597067440476,
        latitude: 48.714630575850606,
        longitude: 2.3033008072525263,
        speed: 2.6368217231848083,
        time: 381,
      },
    ],
  },
};

export { tracks };
