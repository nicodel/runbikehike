const xml = `<?xml version="1.0" encoding="UTF-8"?>
<gpx creator="Garmin Connect" version="1.1"
  xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/11.xsd"
  xmlns:ns3="http://www.garmin.com/xmlschemas/TrackPointExtension/v1"
  xmlns="http://www.topografix.com/GPX/1/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns2="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
  <metadata>
    <link href="connect.garmin.com">
      <text>Garmin Connect</text>
    </link>
    <time>2019-10-26T13:37:36.000Z</time>
  </metadata>
  <trk>
    <name>Antony Course à pied</name>
    <type>running</type>
    <trkseg>
      <trkpt lat="48.74080105684697628021240234375" lon="2.31284310109913349151611328125">
        <ele>83.8000030517578125</ele>
        <time>2019-10-26T13:37:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>99</ns3:hr>
            <ns3:cad>0</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74079317785799503326416015625" lon="2.3128160275518894195556640625">
        <ele>84.8000030517578125</ele>
        <time>2019-10-26T13:37:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>100</ns3:hr>
            <ns3:cad>0</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7407781742513179779052734375" lon="2.31277571059763431549072265625">
        <ele>85.40000152587890625</ele>
        <time>2019-10-26T13:37:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>100</ns3:hr>
            <ns3:cad>56</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.740767277777194976806640625" lon="2.31270144693553447723388671875">
        <ele>85.1999969482421875</ele>
        <time>2019-10-26T13:37:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>105</ns3:hr>
            <ns3:cad>78</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74076233245432376861572265625" lon="2.3126752115786075592041015625">
        <ele>85.59999847412109375</ele>
        <time>2019-10-26T13:37:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>108</ns3:hr>
            <ns3:cad>79</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74076350592076778411865234375" lon="2.3126564361155033111572265625">
        <ele>86</ele>
        <time>2019-10-26T13:37:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>108</ns3:hr>
            <ns3:cad>79</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7407627515494823455810546875" lon="2.31261813081800937652587890625">
        <ele>86</ele>
        <time>2019-10-26T13:37:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
            <ns3:cad>80</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.740745820105075836181640625" lon="2.3125397600233554840087890625">
        <ele>86.59999847412109375</ele>
        <time>2019-10-26T13:37:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
            <ns3:cad>78</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74074087478220462799072265625" lon="2.312512435019016265869140625">
        <ele>86.8000030517578125</ele>
        <time>2019-10-26T13:37:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
            <ns3:cad>78</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.740741126239299774169921875" lon="2.31245920993387699127197265625">
        <ele>87</ele>
        <time>2019-10-26T13:37:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
            <ns3:cad>80</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74074288643896579742431640625" lon="2.3124334774911403656005859375">
        <ele>87</ele>
        <time>2019-10-26T13:37:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
            <ns3:cad>79</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74074020422995090484619140625" lon="2.3123753070831298828125">
        <ele>86.8000030517578125</ele>
        <time>2019-10-26T13:37:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
            <ns3:cad>77</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74073182232677936553955078125" lon="2.3123100958764553070068359375">
        <ele>86.59999847412109375</ele>
        <time>2019-10-26T13:38:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
            <ns3:cad>74</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7407267093658447265625" lon="2.3122893087565898895263671875">
        <ele>86.59999847412109375</ele>
        <time>2019-10-26T13:38:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
            <ns3:cad>74</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7406958639621734619140625" lon="2.31215201318264007568359375">
        <ele>86.8000030517578125</ele>
        <time>2019-10-26T13:38:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
            <ns3:cad>77</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7406958639621734619140625" lon="2.31212527491152286529541015625">
        <ele>86.8000030517578125</ele>
        <time>2019-10-26T13:38:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
            <ns3:cad>77</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7407012283802032470703125" lon="2.31210130266845226287841796875">
        <ele>86.8000030517578125</ele>
        <time>2019-10-26T13:38:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
            <ns3:cad>77</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74076618812978267669677734375" lon="2.31197071261703968048095703125">
        <ele>86.59999847412109375</ele>
        <time>2019-10-26T13:38:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
            <ns3:cad>59</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74077490530908107757568359375" lon="2.3119513504207134246826171875">
        <ele>86.59999847412109375</ele>
        <time>2019-10-26T13:38:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
            <ns3:cad>59</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74078764580190181732177734375" lon="2.3118762485682964324951171875">
        <ele>86.8000030517578125</ele>
        <time>2019-10-26T13:38:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
            <ns3:cad>59</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74078513123095035552978515625" lon="2.31184917502105236053466796875">
        <ele>87</ele>
        <time>2019-10-26T13:38:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
            <ns3:cad>59</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.740783035755157470703125" lon="2.31182537041604518890380859375">
        <ele>87</ele>
        <time>2019-10-26T13:38:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
            <ns3:cad>59</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74078295193612575531005859375" lon="2.31180198490619659423828125">
        <ele>87.1999969482421875</ele>
        <time>2019-10-26T13:38:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
            <ns3:cad>0</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.740775994956493377685546875" lon="2.31173115782439708709716796875">
        <ele>87.59999847412109375</ele>
        <time>2019-10-26T13:38:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
            <ns3:cad>74</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7407369352877140045166015625" lon="2.31174054555594921112060546875">
        <ele>87.59999847412109375</ele>
        <time>2019-10-26T13:38:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
            <ns3:cad>77</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74071036465466022491455078125" lon="2.311706431210041046142578125">
        <ele>87.8000030517578125</ele>
        <time>2019-10-26T13:38:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
            <ns3:cad>79</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74068354256451129913330078125" lon="2.31166921555995941162109375">
        <ele>87.8000030517578125</ele>
        <time>2019-10-26T13:38:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
            <ns3:cad>79</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7406618334352970123291015625" lon="2.3116251267492771148681640625">
        <ele>88</ele>
        <time>2019-10-26T13:38:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
            <ns3:cad>80</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7406464107334613800048828125" lon="2.311605513095855712890625">
        <ele>88</ele>
        <time>2019-10-26T13:38:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
            <ns3:cad>81</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74057675711810588836669921875" lon="2.3114940337836742401123046875">
        <ele>88.40000152587890625</ele>
        <time>2019-10-26T13:38:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
            <ns3:cad>79</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7405215203762054443359375" lon="2.3114142380654811859130859375">
        <ele>88.59999847412109375</ele>
        <time>2019-10-26T13:38:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
            <ns3:cad>80</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.740482628345489501953125" lon="2.3113574087619781494140625">
        <ele>89</ele>
        <time>2019-10-26T13:38:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
            <ns3:cad>79</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.74047315679490566253662109375" lon="2.311334945261478424072265625">
        <ele>89</ele>
        <time>2019-10-26T13:38:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
            <ns3:cad>79</ns3:cad>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      </trkseg>
    </trk>
  </gpx>`;

export default xml;
