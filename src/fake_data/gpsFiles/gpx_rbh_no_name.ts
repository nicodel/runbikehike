const xml = ` <?xml version="1.0" encoding="UTF-8"?>
<gpx version="1.1"
creator="Run, Bike, Hike - https://github.com/nicodel/Run-Bike-Hike"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns="http://www.topografix.com/GPX/1/1"
xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
<metadata>
<author><name>Nicolas Delebecque</name><link href="https://github.com/nicodel/"></link></author><time>2015-08-21T07:46:25.150Z</time></metadata><trk>
<trkseg>
<trkpt lat="46.184447" lon="-1.316761">
	<time>2015-08-21T07:52:57.213Z</time>
	<ele>60</ele>
	<speed>0.4301162660121918</speed>
	<hdop>3</hdop>
	<vdop>3</vdop>
  < /trkpt>
  </trkseg>
  </trk>
  </gpx>`;

export default xml;
