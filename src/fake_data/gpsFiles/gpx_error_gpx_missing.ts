const xml = `<?xml version="1.0" encoding="UTF-8"?>
<gpX creator="Garmin Connect" version="1.1"
  xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/11.xsd"
  xmlns:ns3="http://www.garmin.com/xmlschemas/TrackPointExtension/v1"
  xmlns="http://www.topografix.com/GPX/1/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns2="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
  <metadata>
    <link href="connect.garmin.com">
      <text>Garmin Connect</text>
    </link>
    <time>2022-04-16T06:29:33.000Z</time>
  </metadata>
  <trk>
    <name>Course d'enduran</name>
    <desc>Course d'endurance en 6:19 min/km. Pas trop mal pour mon âge !</desc>
    <type>running</type>
    <trkseg/>
  </trk>
</gpX>`;
export default xml;
