const xml = `<?xml version="1.0" encoding="UTF-8"?>
<gpx creator="Garmin Connect" version="1.1"
  xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/11.xsd"
  xmlns:ns3="http://www.garmin.com/xmlschemas/TrackPointExtension/v1"
  xmlns="http://www.topografix.com/GPX/1/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns2="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
  <metadata>
    <link href="connect.garmin.com">
      <text>Garmin Connect</text>
    </link>
    <time>2019-10-26T13:37:36.000Z</time>
  </metadata>
  <trk>
    <name>Antony Course à pied</name>
    <type>running</type>
    <trkseg>
    <trkpt lat="2.31284310109913349151611328125">
    <ele>83.8000030517578125</ele>
    <time>2019-10-26T13:37:36.000Z</time>
    <extensions>
      <ns3:TrackPointExtension>
        <ns3:atemp>29.0</ns3:atemp>
        <ns3:hr>99</ns3:hr>
        <ns3:cad>0</ns3:cad>
      </ns3:TrackPointExtension>
    </extensions>
  </trkpt>
      </trkseg>
    </trk>
  </gpx>`;

export default xml;
