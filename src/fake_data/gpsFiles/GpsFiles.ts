import gpx_error_gpx_missing from './gpx_error_gpx_missing';
import gpx_error_trk_missing from './gpx_error_trk_missing';
import gpx_error_trkseg_missing from './gpx_error_trkseg_missing';
import gpx_garmin_no_metadata_time from './gpx_garmin_no_metadata_time';
import gpx_garmin_metadata from './gpx_garmin_metadata_time';
import gpx_garmin_no_time from './gpx_garmin_no_time';
import gpx_rbh_metadata_name from './gpx_rbh_metadata_name';
import gpx_rbh_no_name from './gpx_rbh_no_name';
import gpx_garmin_gps from './gpx_garmin_gps';
import gpx_garmin_gps_no_time from './gpx_garmin_gps_no_time';
import gpx_garmin_gps_no_lat from './gpx_garmin_gps_no_lat';
import gpx_garmin_gps_no_lon from './gpx_garmin_gps_no_lon';
import gpx_garmin_gps_no_ele from './gpx_garmin_gps_no_ele';
import gpx_no_hr from './gpx_no_hr';
import garmin_no_gps from './gpx_garmin_no_gps';

// import logger from '@/shared/utils/logger';

const parseIt = (xml_file): string => {
  // let json_file = '';
  // logger.debug(`XML file to be parsed, ${xml_file}`);
  // const parser = new XMLParser();
  // json_file = parser.parse(xml_file);
  /*parseString(xml_file, (err, res) => {
    if (err) {
      logger.error(`error while parsing XML file, ${JSON.stringify(err)}`);
    }
    // logger.debug(`parsed XML file, ${JSON.stringify(res)}`);
    json_file = JSON.stringify(res);
  });*/
  // logger.debug(`[FAKE_DATA / GPSFiles] parseIt() - json_file, ${json_file}`);
  return xml_file;
};

const GPS_ALL_MANDATORY = parseIt(garmin_no_gps);
const GPX_ERROR_GPX_MISSING = parseIt(gpx_error_gpx_missing);
const GPX_ERROR_TRK_MISSING = parseIt(gpx_error_trk_missing);
const GPX_ERROR_TRKSEG_MISSING = parseIt(gpx_error_trkseg_missing);
const GPX_NO_METADATA_TIME = parseIt(gpx_garmin_no_metadata_time);
const GPX_METADATA_TIME = parseIt(gpx_garmin_metadata);
const GPX_NO_TIME = parseIt(gpx_garmin_no_time);
const GPX_METADATA_NAME = parseIt(gpx_rbh_metadata_name);
const GPX_TRACK_NAME = parseIt(gpx_garmin_metadata);
const GPX_NO_NAME = parseIt(gpx_rbh_no_name);
const GPX_TYPE = parseIt(gpx_garmin_metadata);
const GPX_NO_TYPE = parseIt(gpx_rbh_no_name);
const GPX_NO_DURATION = parseIt(garmin_no_gps);
const GPX_DURATION = parseIt(gpx_garmin_gps);
const GPX_NO_DISTANCE = parseIt(garmin_no_gps);
const GPX_TRK_DESC = parseIt(garmin_no_gps);
const GPX_NO_DESC = parseIt(gpx_rbh_no_name);
const GPX_GPS_NO_TIME = parseIt(gpx_garmin_gps_no_time);
const GPX_GPS_NO_LAT = parseIt(gpx_garmin_gps_no_lat);
const GPX_GPS_NO_LON = parseIt(gpx_garmin_gps_no_lon);
const GPX_GPS_NO_ELE = parseIt(gpx_garmin_gps_no_ele);
const GPX_GPS_NO_HR = parseIt(gpx_no_hr);

export {
  GPS_ALL_MANDATORY,
  GPX_ERROR_GPX_MISSING,
  GPX_ERROR_TRK_MISSING,
  GPX_ERROR_TRKSEG_MISSING,
  GPX_NO_METADATA_TIME,
  GPX_METADATA_TIME,
  GPX_NO_TIME,
  GPX_METADATA_NAME,
  GPX_TRACK_NAME,
  GPX_NO_NAME,
  GPX_TYPE,
  GPX_NO_TYPE,
  GPX_NO_DURATION,
  GPX_DURATION,
  GPX_NO_DISTANCE,
  GPX_TRK_DESC,
  GPX_NO_DESC,
  GPX_GPS_NO_TIME,
  GPX_GPS_NO_LAT,
  GPX_GPS_NO_LON,
  GPX_GPS_NO_ELE,
  GPX_GPS_NO_HR,
};
