const xml = `<?xml version="1.0" encoding="UTF-8"?>
<gpx creator="Garmin Connect" version="1.1"
  xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/11.xsd"
  xmlns:ns3="http://www.garmin.com/xmlschemas/TrackPointExtension/v1"
  xmlns="http://www.topografix.com/GPX/1/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns2="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
  <metadata>
    <link href="connect.garmin.com">
      <text>Garmin Connect</text>
    </link>
    <time>2018-08-10T16:03:23.000Z</time>
  </metadata>
  <trk>
    <name>Montreuil Cyclisme</name>
    <type>cycling</type>
    <trkseg>
      <trkpt lat="48.8518860004842281341552734375" lon="2.41946526803076267242431640625">
        <ele>54.200000762939453125</ele>
        <time>2018-08-10T16:03:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>96</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85191299021244049072265625" lon="2.4193889088928699493408203125">
        <ele>55</ele>
        <time>2018-08-10T16:03:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>94</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85193620808422565460205078125" lon="2.41930869407951831817626953125">
        <ele>55.40000152587890625</ele>
        <time>2018-08-10T16:03:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>95</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85194517672061920166015625" lon="2.41928748786449432373046875">
        <ele>55.40000152587890625</ele>
        <time>2018-08-10T16:03:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>94</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8519999943673610687255859375" lon="2.4191216938197612762451171875">
        <ele>55.59999847412109375</ele>
        <time>2018-08-10T16:03:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>92</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85198733769357204437255859375" lon="2.4188806302845478057861328125">
        <ele>56</ele>
        <time>2018-08-10T16:03:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>94</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85194693692028522491455078125" lon="2.41859170608222484588623046875">
        <ele>56</ele>
        <time>2018-08-10T16:03:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>95</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85191072709858417510986328125" lon="2.41841635666787624359130859375">
        <ele>55.799999237060546875</ele>
        <time>2018-08-10T16:03:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>91</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8519037701189517974853515625" lon="2.41836053319275379180908203125">
        <ele>55.799999237060546875</ele>
        <time>2018-08-10T16:03:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>94</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85189480148255825042724609375" lon="2.418308816850185394287109375">
        <ele>56</ele>
        <time>2018-08-10T16:03:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>93</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85183964855968952178955078125" lon="2.41803933866322040557861328125">
        <ele>56.200000762939453125</ele>
        <time>2018-08-10T16:03:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>91</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85182606987655162811279296875" lon="2.41799206472933292388916015625">
        <ele>56.200000762939453125</ele>
        <time>2018-08-10T16:03:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>97</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.851811401546001434326171875" lon="2.41794504225254058837890625">
        <ele>56</ele>
        <time>2018-08-10T16:03:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>97</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85180092416703701019287109375" lon="2.41790237836539745330810546875">
        <ele>55.799999237060546875</ele>
        <time>2018-08-10T16:03:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>100</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8517921231687068939208984375" lon="2.4178610555827617645263671875">
        <ele>55.799999237060546875</ele>
        <time>2018-08-10T16:04:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>102</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8517867587506771087646484375" lon="2.41781629621982574462890625">
        <ele>56</ele>
        <time>2018-08-10T16:04:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>106</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.851764462888240814208984375" lon="2.417650334537029266357421875">
        <ele>56.200000762939453125</ele>
        <time>2018-08-10T16:04:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>110</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.851743005216121673583984375" lon="2.4175961874425411224365234375">
        <ele>56.200000762939453125</ele>
        <time>2018-08-10T16:04:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>113</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.851713836193084716796875" lon="2.41752929985523223876953125">
        <ele>56.40000152587890625</ele>
        <time>2018-08-10T16:04:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85164476931095123291015625" lon="2.41734716109931468963623046875">
        <ele>56</ele>
        <time>2018-08-10T16:04:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>109</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85159003548324108123779296875" lon="2.417231909930706024169921875">
        <ele>55.40000152587890625</ele>
        <time>2018-08-10T16:04:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>30.0</ns3:atemp>
            <ns3:hr>111</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8515413366258144378662109375" lon="2.41703300736844539642333984375">
        <ele>55</ele>
        <time>2018-08-10T16:04:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>108</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85151208378374576568603515625" lon="2.4168605916202068328857421875">
        <ele>55.200000762939453125</ele>
        <time>2018-08-10T16:04:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>107</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85149029083549976348876953125" lon="2.4164731800556182861328125">
        <ele>54.799999237060546875</ele>
        <time>2018-08-10T16:04:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85148609988391399383544921875" lon="2.41641978733241558074951171875">
        <ele>54.799999237060546875</ele>
        <time>2018-08-10T16:04:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>109</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85148609988391399383544921875" lon="2.41635222919285297393798828125">
        <ele>54.59999847412109375</ele>
        <time>2018-08-10T16:04:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85148056782782077789306640625" lon="2.416174113750457763671875">
        <ele>54.59999847412109375</ele>
        <time>2018-08-10T16:04:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85141275823116302490234375" lon="2.41597403772175312042236328125">
        <ele>54.799999237060546875</ele>
        <time>2018-08-10T16:04:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.851243443787097930908203125" lon="2.41590396501123905181884765625">
        <ele>55</ele>
        <time>2018-08-10T16:04:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.851010762155055999755859375" lon="2.41594704799354076385498046875">
        <ele>54.200000762939453125</ele>
        <time>2018-08-10T16:04:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>117</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.850901462137699127197265625" lon="2.4159704335033893585205078125">
        <ele>53.59999847412109375</ele>
        <time>2018-08-10T16:04:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.85084773413836956024169921875" lon="2.41598543711006641387939453125">
        <ele>53.40000152587890625</ele>
        <time>2018-08-10T16:04:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>118</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.850674144923686981201171875" lon="2.4160156957805156707763671875">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:04:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.850485049188137054443359375" lon="2.41603271104395389556884765625">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:04:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.850422017276287078857421875" lon="2.416035644710063934326171875">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:04:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>117</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84999780915677547454833984375" lon="2.41606657393276691436767578125">
        <ele>54</ele>
        <time>2018-08-10T16:04:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84994366206228733062744140625" lon="2.4160726927220821380615234375">
        <ele>54.200000762939453125</ele>
        <time>2018-08-10T16:04:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>117</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84967208839952945709228515625" lon="2.41611577570438385009765625">
        <ele>54.200000762939453125</ele>
        <time>2018-08-10T16:05:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>113</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84956412948668003082275390625" lon="2.416122816503047943115234375">
        <ele>53.40000152587890625</ele>
        <time>2018-08-10T16:05:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84951174259185791015625" lon="2.41612910293042659759521484375">
        <ele>53</ele>
        <time>2018-08-10T16:05:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84932935237884521484375" lon="2.4161622114479541778564453125">
        <ele>52</ele>
        <time>2018-08-10T16:05:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>113</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8492976687848567962646484375" lon="2.41615743376314640045166015625">
        <ele>52</ele>
        <time>2018-08-10T16:05:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84921879507601261138916015625" lon="2.4160297773778438568115234375">
        <ele>51.799999237060546875</ele>
        <time>2018-08-10T16:05:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>117</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8492312841117382049560546875" lon="2.4157776497304439544677734375">
        <ele>51.799999237060546875</ele>
        <time>2018-08-10T16:05:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>117</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84926120750606060028076171875" lon="2.4152482487261295318603515625">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:05:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>117</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84929020889103412628173828125" lon="2.41492143832147121429443359375">
        <ele>53.40000152587890625</ele>
        <time>2018-08-10T16:05:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84929875843226909637451171875" lon="2.414456494152545928955078125">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:05:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8492128439247608184814453125" lon="2.4138172902166843414306640625">
        <ele>53</ele>
        <time>2018-08-10T16:05:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84916263632476329803466796875" lon="2.41342141292989253997802734375">
        <ele>52.799999237060546875</ele>
        <time>2018-08-10T16:05:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.849139250814914703369140625" lon="2.41318278014659881591796875">
        <ele>53</ele>
        <time>2018-08-10T16:05:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8491246663033962249755859375" lon="2.4127420596778392791748046875">
        <ele>53</ele>
        <time>2018-08-10T16:05:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84912558831274509429931640625" lon="2.41267274133861064910888671875">
        <ele>53</ele>
        <time>2018-08-10T16:05:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8491313718259334564208984375" lon="2.41225515492260456085205078125">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:06:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84912659414112567901611328125" lon="2.41218374110758304595947265625">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:06:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8491199724376201629638671875" lon="2.4121105670928955078125">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:06:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>113</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84911217726767063140869140625" lon="2.412039153277873992919921875">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:06:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84909960441291332244873046875" lon="2.4119102396070957183837890625">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:06:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84909139014780521392822265625" lon="2.4117995984852313995361328125">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:06:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>110</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84910882450640201568603515625" lon="2.41167361848056316375732421875">
        <ele>54</ele>
        <time>2018-08-10T16:06:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>110</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84912072680890560150146484375" lon="2.411616034805774688720703125">
        <ele>54.40000152587890625</ele>
        <time>2018-08-10T16:06:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>111</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.849107064306735992431640625" lon="2.41149835288524627685546875">
        <ele>54.59999847412109375</ele>
        <time>2018-08-10T16:06:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>113</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.849040009081363677978515625" lon="2.4113617278635501861572265625">
        <ele>54.40000152587890625</ele>
        <time>2018-08-10T16:06:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>110</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84900120086967945098876953125" lon="2.41131738759577274322509765625">
        <ele>54.200000762939453125</ele>
        <time>2018-08-10T16:06:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>111</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84895493276417255401611328125" lon="2.4112857878208160400390625">
        <ele>54.200000762939453125</ele>
        <time>2018-08-10T16:06:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>110</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8487947545945644378662109375" lon="2.41124630905687808990478515625">
        <ele>53.799999237060546875</ele>
        <time>2018-08-10T16:06:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.848280273377895355224609375" lon="2.41115771234035491943359375">
        <ele>53.59999847412109375</ele>
        <time>2018-08-10T16:06:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84803183376789093017578125" lon="2.411146648228168487548828125">
        <ele>53.40000152587890625</ele>
        <time>2018-08-10T16:06:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>117</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.847918845713138580322265625" lon="2.41114622913300991058349609375">
        <ele>53.40000152587890625</ele>
        <time>2018-08-10T16:06:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8478663749992847442626953125" lon="2.4111417867243289947509765625">
        <ele>53.40000152587890625</ele>
        <time>2018-08-10T16:06:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>114</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.847536630928516387939453125" lon="2.41110431961715221405029296875">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:06:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>111</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84748256765305995941162109375" lon="2.4110935069620609283447265625">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:06:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>111</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84735650382936000823974609375" lon="2.41094338707625865936279296875">
        <ele>53</ele>
        <time>2018-08-10T16:06:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>111</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84723563678562641143798828125" lon="2.41080642677843570709228515625">
        <ele>52.40000152587890625</ele>
        <time>2018-08-10T16:07:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.847204707562923431396484375" lon="2.4107895791530609130859375">
        <ele>52.40000152587890625</ele>
        <time>2018-08-10T16:07:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84704939089715480804443359375" lon="2.41075026802718639373779296875">
        <ele>52.59999847412109375</ele>
        <time>2018-08-10T16:07:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>109</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84701100178062915802001953125" lon="2.4107458256185054779052734375">
        <ele>52.799999237060546875</ele>
        <time>2018-08-10T16:07:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>110</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.846971690654754638671875" lon="2.41073970682919025421142578125">
        <ele>52.799999237060546875</ele>
        <time>2018-08-10T16:07:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>107</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84675317443907260894775390625" lon="2.41071380674839019775390625">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:07:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>105</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84671009145677089691162109375" lon="2.41073333658277988433837890625">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:07:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>105</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.846621327102184295654296875" lon="2.410785220563411712646484375">
        <ele>53</ele>
        <time>2018-08-10T16:07:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>106</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.846521079540252685546875" lon="2.4108014814555644989013671875">
        <ele>52.799999237060546875</ele>
        <time>2018-08-10T16:07:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>105</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8464169763028621673583984375" lon="2.41077625192701816558837890625">
        <ele>52.40000152587890625</ele>
        <time>2018-08-10T16:07:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>105</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84636626578867435455322265625" lon="2.4107547104358673095703125">
        <ele>52.200000762939453125</ele>
        <time>2018-08-10T16:07:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>108</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84621690027415752410888671875" lon="2.410688661038875579833984375">
        <ele>52</ele>
        <time>2018-08-10T16:07:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>107</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84592521004378795623779296875" lon="2.410593442618846893310546875">
        <ele>52.40000152587890625</ele>
        <time>2018-08-10T16:07:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>103</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.845830745995044708251953125" lon="2.4105704762041568756103515625">
        <ele>52.40000152587890625</ele>
        <time>2018-08-10T16:07:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>104</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8456900976598262786865234375" lon="2.410521693527698516845703125">
        <ele>53.40000152587890625</ele>
        <time>2018-08-10T16:07:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>103</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84558892808854579925537109375" lon="2.41049026139080524444580078125">
        <ele>53.59999847412109375</ele>
        <time>2018-08-10T16:07:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>106</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8455378822982311248779296875" lon="2.41047332994639873504638671875">
        <ele>53.59999847412109375</ele>
        <time>2018-08-10T16:07:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>103</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84548599831759929656982421875" lon="2.41045773960649967193603515625">
        <ele>53.799999237060546875</ele>
        <time>2018-08-10T16:07:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>104</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84543604217469692230224609375" lon="2.4104435741901397705078125">
        <ele>53.59999847412109375</ele>
        <time>2018-08-10T16:07:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>107</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8453333638608455657958984375" lon="2.41041004657745361328125">
        <ele>53.59999847412109375</ele>
        <time>2018-08-10T16:07:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>111</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8451801426708698272705078125" lon="2.41035849787294864654541015625">
        <ele>53</ele>
        <time>2018-08-10T16:07:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>114</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8451271690428256988525390625" lon="2.41034206934273242950439453125">
        <ele>52.799999237060546875</ele>
        <time>2018-08-10T16:07:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>111</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84507201611995697021484375" lon="2.410326898097991943359375">
        <ele>52.59999847412109375</ele>
        <time>2018-08-10T16:07:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84476381354033946990966796875" lon="2.41024844348430633544921875">
        <ele>52.799999237060546875</ele>
        <time>2018-08-10T16:07:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84466138668358325958251953125" lon="2.41024098359048366546630859375">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:07:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8445280306041240692138671875" lon="2.41026445291936397552490234375">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:08:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>117</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8443442992866039276123046875" lon="2.4101768620312213897705078125">
        <ele>52.40000152587890625</ele>
        <time>2018-08-10T16:08:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.844297863543033599853515625" lon="2.4101775325834751129150390625">
        <ele>52.40000152587890625</ele>
        <time>2018-08-10T16:08:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8442360050976276397705078125" lon="2.41016856394708156585693359375">
        <ele>52.40000152587890625</ele>
        <time>2018-08-10T16:08:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.844203986227512359619140625" lon="2.41016353480517864227294921875">
        <ele>52.40000152587890625</ele>
        <time>2018-08-10T16:08:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8441686145961284637451171875" lon="2.41015431471168994903564453125">
        <ele>52.40000152587890625</ele>
        <time>2018-08-10T16:08:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84396267123520374298095703125" lon="2.410053312778472900390625">
        <ele>52.59999847412109375</ele>
        <time>2018-08-10T16:08:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84373703040182590484619140625" lon="2.4099435098469257354736328125">
        <ele>52.40000152587890625</ele>
        <time>2018-08-10T16:08:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8436043448746204376220703125" lon="2.40989129059016704559326171875">
        <ele>52.59999847412109375</ele>
        <time>2018-08-10T16:08:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.843306116759777069091796875" lon="2.409814260900020599365234375">
        <ele>52.59999847412109375</ele>
        <time>2018-08-10T16:08:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>114</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84314996190369129180908203125" lon="2.40976941771805286407470703125">
        <ele>52.59999847412109375</ele>
        <time>2018-08-10T16:08:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>110</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8429052941501140594482421875" lon="2.409721724689006805419921875">
        <ele>52.59999847412109375</ele>
        <time>2018-08-10T16:08:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>109</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84265375323593616485595703125" lon="2.4096303619444370269775390625">
        <ele>52</ele>
        <time>2018-08-10T16:08:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>104</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84260438382625579833984375" lon="2.40961787290871143341064453125">
        <ele>52</ele>
        <time>2018-08-10T16:08:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>100</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84255400858819484710693359375" lon="2.4096021987497806549072265625">
        <ele>51.799999237060546875</ele>
        <time>2018-08-10T16:08:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>94</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8425053097307682037353515625" lon="2.4095867760479450225830078125">
        <ele>51.799999237060546875</ele>
        <time>2018-08-10T16:08:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>88</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8424577005207538604736328125" lon="2.409568168222904205322265625">
        <ele>51.59999847412109375</ele>
        <time>2018-08-10T16:08:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>83</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84240925312042236328125" lon="2.4095477163791656494140625">
        <ele>51.59999847412109375</ele>
        <time>2018-08-10T16:08:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>86</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84230825118720531463623046875" lon="2.40950404666364192962646484375">
        <ele>51.799999237060546875</ele>
        <time>2018-08-10T16:08:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>89</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84225569665431976318359375" lon="2.4094801582396030426025390625">
        <ele>51.59999847412109375</ele>
        <time>2018-08-10T16:08:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>90</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8422018848359584808349609375" lon="2.40945425815880298614501953125">
        <ele>51.40000152587890625</ele>
        <time>2018-08-10T16:08:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>94</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84214547462761402130126953125" lon="2.4094365723431110382080078125">
        <ele>51.40000152587890625</ele>
        <time>2018-08-10T16:08:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>98</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84208939969539642333984375" lon="2.4094245024025440216064453125">
        <ele>51</ele>
        <time>2018-08-10T16:08:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>105</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84203567169606685638427734375" lon="2.40941100753843784332275390625">
        <ele>50.59999847412109375</ele>
        <time>2018-08-10T16:08:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>110</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84198303334414958953857421875" lon="2.40940245799720287322998046875">
        <ele>50.40000152587890625</ele>
        <time>2018-08-10T16:08:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>114</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84193374775350093841552734375" lon="2.40939013659954071044921875">
        <ele>50.200000762939453125</ele>
        <time>2018-08-10T16:08:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84187809191644191741943359375" lon="2.40937764756381511688232421875">
        <ele>50</ele>
        <time>2018-08-10T16:08:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84182411246001720428466796875" lon="2.409359626471996307373046875">
        <ele>50</ele>
        <time>2018-08-10T16:08:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8414864055812358856201171875" lon="2.40923113189637660980224609375">
        <ele>49.59999847412109375</ele>
        <time>2018-08-10T16:09:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8411189429461956024169921875" lon="2.409091405570507049560546875">
        <ele>50</ele>
        <time>2018-08-10T16:09:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8409787975251674652099609375" lon="2.409024350345134735107421875">
        <ele>50</ele>
        <time>2018-08-10T16:09:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8408708386123180389404296875" lon="2.409036420285701751708984375">
        <ele>49.799999237060546875</ele>
        <time>2018-08-10T16:09:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.840826414525508880615234375" lon="2.409038431942462921142578125">
        <ele>49.59999847412109375</ele>
        <time>2018-08-10T16:09:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84069205261766910552978515625" lon="2.40902527235448360443115234375">
        <ele>50</ele>
        <time>2018-08-10T16:09:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84066472761332988739013671875" lon="2.40904773585498332977294921875">
        <ele>50.200000762939453125</ele>
        <time>2018-08-10T16:09:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8406438566744327545166015625" lon="2.4091237597167491912841796875">
        <ele>50.40000152587890625</ele>
        <time>2018-08-10T16:09:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84061552584171295166015625" lon="2.409279830753803253173828125">
        <ele>50.59999847412109375</ele>
        <time>2018-08-10T16:09:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84058342315256595611572265625" lon="2.4093071557581424713134765625">
        <ele>50.59999847412109375</ele>
        <time>2018-08-10T16:09:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.840510584414005279541015625" lon="2.40930556319653987884521484375">
        <ele>50.40000152587890625</ele>
        <time>2018-08-10T16:09:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84046674706041812896728515625" lon="2.409290559589862823486328125">
        <ele>50.40000152587890625</ele>
        <time>2018-08-10T16:09:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.84013851173222064971923828125" lon="2.409187294542789459228515625">
        <ele>49.59999847412109375</ele>
        <time>2018-08-10T16:09:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83997573517262935638427734375" lon="2.4091452173888683319091796875">
        <ele>49</ele>
        <time>2018-08-10T16:09:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83986174128949642181396484375" lon="2.4091105163097381591796875">
        <ele>48.799999237060546875</ele>
        <time>2018-08-10T16:09:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83962713181972503662109375" lon="2.4090446345508098602294921875">
        <ele>48</ele>
        <time>2018-08-10T16:09:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83944549597799777984619140625" lon="2.40899291820824146270751953125">
        <ele>47</ele>
        <time>2018-08-10T16:09:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.839382715523242950439453125" lon="2.40897447802126407623291015625">
        <ele>46.799999237060546875</ele>
        <time>2018-08-10T16:09:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.839182890951633453369140625" lon="2.408924102783203125">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:09:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.839055486023426055908203125" lon="2.40888110361993312835693359375">
        <ele>44.799999237060546875</ele>
        <time>2018-08-10T16:09:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83899052627384662628173828125" lon="2.40885646082460880279541015625">
        <ele>44.59999847412109375</ele>
        <time>2018-08-10T16:09:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83854796178638935089111328125" lon="2.40871983580291271209716796875">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:10:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83820547722280025482177734375" lon="2.40858069621026515960693359375">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:10:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83814898319542407989501953125" lon="2.4085584841668605804443359375">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:10:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83775905705988407135009765625" lon="2.40840048529207706451416015625">
        <ele>47</ele>
        <time>2018-08-10T16:10:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83760415948927402496337890625" lon="2.408340387046337127685546875">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:10:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>117</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.837339878082275390625" lon="2.40825690329074859619140625">
        <ele>47.59999847412109375</ele>
        <time>2018-08-10T16:10:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83718053810298442840576171875" lon="2.408192865550518035888671875">
        <ele>47.59999847412109375</ele>
        <time>2018-08-10T16:10:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83713242597877979278564453125" lon="2.40817870013415813446044921875">
        <ele>47.59999847412109375</ele>
        <time>2018-08-10T16:10:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83698473684489727020263671875" lon="2.408132515847682952880859375">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:10:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>118</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83693293668329715728759765625" lon="2.40811633877456188201904296875">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:10:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83666773326694965362548828125" lon="2.40804961882531642913818359375">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:10:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>118</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.836617358028888702392578125" lon="2.4080404825508594512939453125">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:10:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83656681515276432037353515625" lon="2.4080274067819118499755859375">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:10:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8365166075527667999267578125" lon="2.40800452418625354766845703125">
        <ele>45</ele>
        <time>2018-08-10T16:10:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8364710099995136260986328125" lon="2.407972253859043121337890625">
        <ele>45</ele>
        <time>2018-08-10T16:10:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83642314933240413665771484375" lon="2.40790042094886302947998046875">
        <ele>45</ele>
        <time>2018-08-10T16:10:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83617915213108062744140625" lon="2.40755106322467327117919921875">
        <ele>44.59999847412109375</ele>
        <time>2018-08-10T16:10:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8361166231334209442138671875" lon="2.407439835369586944580078125">
        <ele>44.200000762939453125</ele>
        <time>2018-08-10T16:10:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.835887797176837921142578125" lon="2.40719600580632686614990234375">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:10:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83583574555814266204833984375" lon="2.40717220120131969451904296875">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:10:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83578159846365451812744140625" lon="2.40717153064906597137451171875">
        <ele>43</ele>
        <time>2018-08-10T16:10:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8357250206172466278076171875" lon="2.40717370994389057159423828125">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:10:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.835671879351139068603515625" lon="2.40716801024973392486572265625">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:11:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83557565510272979736328125" lon="2.40711453370749950408935546875">
        <ele>43.59999847412109375</ele>
        <time>2018-08-10T16:11:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8355292193591594696044921875" lon="2.407076060771942138671875">
        <ele>43.799999237060546875</ele>
        <time>2018-08-10T16:11:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.835408687591552734375" lon="2.40697455592453479766845703125">
        <ele>43.59999847412109375</ele>
        <time>2018-08-10T16:11:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83531975559890270233154296875" lon="2.4069260247051715850830078125">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:11:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83527767844498157501220703125" lon="2.40689325146377086639404296875">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:11:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.835202157497406005859375" lon="2.40678914822638034820556640625">
        <ele>42.59999847412109375</ele>
        <time>2018-08-10T16:11:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83518011309206485748291015625" lon="2.4066014774143695831298828125">
        <ele>42.59999847412109375</ele>
        <time>2018-08-10T16:11:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8351238705217838287353515625" lon="2.4063510261476039886474609375">
        <ele>42.200000762939453125</ele>
        <time>2018-08-10T16:11:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83507684804499149322509765625" lon="2.40624063648283481597900390625">
        <ele>42</ele>
        <time>2018-08-10T16:11:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83502588607370853424072265625" lon="2.4061421491205692291259765625">
        <ele>42.200000762939453125</ele>
        <time>2018-08-10T16:11:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83493150584399700164794921875" lon="2.40596185438334941864013671875">
        <ele>42.40000152587890625</ele>
        <time>2018-08-10T16:11:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83485053665935993194580078125" lon="2.40577627904713153839111328125">
        <ele>42.40000152587890625</ele>
        <time>2018-08-10T16:11:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83470930159091949462890625" lon="2.4054675735533237457275390625">
        <ele>42.59999847412109375</ele>
        <time>2018-08-10T16:11:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8346816413104534149169921875" lon="2.405398003756999969482421875">
        <ele>42.59999847412109375</ele>
        <time>2018-08-10T16:11:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83459798991680145263671875" lon="2.405185438692569732666015625">
        <ele>43</ele>
        <time>2018-08-10T16:11:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83446413092315196990966796875" lon="2.40498754195868968963623046875">
        <ele>43</ele>
        <time>2018-08-10T16:11:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.834420628845691680908203125" lon="2.40492283366620540618896484375">
        <ele>43</ele>
        <time>2018-08-10T16:11:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>111</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83437679149210453033447265625" lon="2.4048687703907489776611328125">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:11:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>106</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83433722890913486480712890625" lon="2.40481068380177021026611328125">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:11:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>101</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83429984562098979949951171875" lon="2.4047504179179668426513671875">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:11:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>97</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8342652283608913421630859375" lon="2.40468679927289485931396484375">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:11:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>96</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83423765189945697784423828125" lon="2.404617480933666229248046875">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:11:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>95</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83411393500864505767822265625" lon="2.40427155978977680206298828125">
        <ele>43</ele>
        <time>2018-08-10T16:11:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>98</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8340421020984649658203125" lon="2.4040654487907886505126953125">
        <ele>43</ele>
        <time>2018-08-10T16:11:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>100</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8339960016310214996337890625" lon="2.40386654622852802276611328125">
        <ele>43</ele>
        <time>2018-08-10T16:12:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>103</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83393607102334499359130859375" lon="2.4037124030292034149169921875">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:12:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>102</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83387052454054355621337890625" lon="2.4036178551614284515380859375">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:12:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>109</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83385543711483478546142578125" lon="2.4035776220262050628662109375">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:12:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>112</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8338105939328670501708984375" lon="2.4034233950078487396240234375">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:12:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83377111516892910003662109375" lon="2.403297163546085357666015625">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:12:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8336694426834583282470703125" lon="2.4030250869691371917724609375">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:12:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>107</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83364253677427768707275390625" lon="2.40295610390603542327880859375">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:12:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>102</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.833613954484462738037109375" lon="2.40288686938583850860595703125">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:12:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>97</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83358570747077465057373046875" lon="2.4028193950653076171875">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:12:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>94</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.833556286990642547607421875" lon="2.40275527350604534149169921875">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:12:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>89</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8335225917398929595947265625" lon="2.402694337069988250732421875">
        <ele>43.59999847412109375</ele>
        <time>2018-08-10T16:12:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>83</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8334870524704456329345703125" lon="2.402642033994197845458984375">
        <ele>43.59999847412109375</ele>
        <time>2018-08-10T16:12:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>79</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83344388566911220550537109375" lon="2.4025942571461200714111328125">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:12:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>73</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83341454900801181793212890625" lon="2.4025483243167400360107421875">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:12:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>72</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83338403888046741485595703125" lon="2.4024782516062259674072265625">
        <ele>43</ele>
        <time>2018-08-10T16:12:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>75</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83337180130183696746826171875" lon="2.40244681946933269500732421875">
        <ele>43</ele>
        <time>2018-08-10T16:12:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>76</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83337666280567646026611328125" lon="2.4024386890232563018798828125">
        <ele>43</ele>
        <time>2018-08-10T16:12:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>79</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83336844854056835174560546875" lon="2.40244053304195404052734375">
        <ele>43</ele>
        <time>2018-08-10T16:12:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>77</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.833365850150585174560546875" lon="2.40243877284228801727294921875">
        <ele>43</ele>
        <time>2018-08-10T16:12:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>80</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8333608210086822509765625" lon="2.4024242721498012542724609375">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:12:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>77</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83334145881235599517822265625" lon="2.40236786194145679473876953125">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:12:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>83</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.833324611186981201171875" lon="2.40232964046299457550048828125">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:12:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>86</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83331052958965301513671875" lon="2.402277253568172454833984375">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:12:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>89</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8332875631749629974365234375" lon="2.402157895267009735107421875">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:12:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>89</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83325529284775257110595703125" lon="2.40202001295983791351318359375">
        <ele>43.59999847412109375</ele>
        <time>2018-08-10T16:12:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>93</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.833233416080474853515625" lon="2.401946671307086944580078125">
        <ele>43.59999847412109375</ele>
        <time>2018-08-10T16:12:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>99</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83321120403707027435302734375" lon="2.401870898902416229248046875">
        <ele>43.799999237060546875</ele>
        <time>2018-08-10T16:12:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>105</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8331878185272216796875" lon="2.4017966352403163909912109375">
        <ele>44</ele>
        <time>2018-08-10T16:12:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>108</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.833164684474468231201171875" lon="2.40172295831143856048583984375">
        <ele>44</ele>
        <time>2018-08-10T16:12:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>109</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.833140544593334197998046875" lon="2.401649951934814453125">
        <ele>44.40000152587890625</ele>
        <time>2018-08-10T16:12:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>109</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83309687487781047821044921875" lon="2.401502095162868499755859375">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:12:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>110</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8330602459609508514404296875" lon="2.40135264582931995391845703125">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:12:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>113</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8329888321459293365478515625" lon="2.4011187069118022918701171875">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:13:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>117</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8329048454761505126953125" lon="2.40087680518627166748046875">
        <ele>46</ele>
        <time>2018-08-10T16:13:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>118</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83281842805445194244384765625" lon="2.40063733421266078948974609375">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:13:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83278942666947841644287109375" lon="2.400561310350894927978515625">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:13:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8326213695108890533447265625" lon="2.400109358131885528564453125">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:13:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83259312249720096588134765625" lon="2.40003752522170543670654296875">
        <ele>47.59999847412109375</ele>
        <time>2018-08-10T16:13:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.832372091710567474365234375" lon="2.39947409369051456451416015625">
        <ele>48.59999847412109375</ele>
        <time>2018-08-10T16:13:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>114</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8323120772838592529296875" lon="2.39933445118367671966552734375">
        <ele>48.799999237060546875</ele>
        <time>2018-08-10T16:13:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>118</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8321627117693424224853515625" lon="2.39899196662008762359619140625">
        <ele>49</ele>
        <time>2018-08-10T16:13:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.832133375108242034912109375" lon="2.398925833404064178466796875">
        <ele>49</ele>
        <time>2018-08-10T16:13:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>118</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83210177533328533172607421875" lon="2.3988653160631656646728515625">
        <ele>49.200000762939453125</ele>
        <time>2018-08-10T16:13:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83206925354897975921630859375" lon="2.39880706183612346649169921875">
        <ele>49.40000152587890625</ele>
        <time>2018-08-10T16:13:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83200890384614467620849609375" lon="2.39868468604981899261474609375">
        <ele>49.59999847412109375</ele>
        <time>2018-08-10T16:13:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>118</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8319441117346286773681640625" lon="2.398572452366352081298828125">
        <ele>49.200000762939453125</ele>
        <time>2018-08-10T16:13:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83187546394765377044677734375" lon="2.39847145043313503265380859375">
        <ele>49</ele>
        <time>2018-08-10T16:13:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8316621445119380950927734375" lon="2.398156709969043731689453125">
        <ele>49.59999847412109375</ele>
        <time>2018-08-10T16:13:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8315504975616931915283203125" lon="2.3978631757199764251708984375">
        <ele>50.200000762939453125</ele>
        <time>2018-08-10T16:13:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8315263576805591583251953125" lon="2.3977344296872615814208984375">
        <ele>50.200000762939453125</ele>
        <time>2018-08-10T16:13:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83150565437972545623779296875" lon="2.39760040305554866790771484375">
        <ele>50</ele>
        <time>2018-08-10T16:13:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83145854808390140533447265625" lon="2.39747936837375164031982421875">
        <ele>49.40000152587890625</ele>
        <time>2018-08-10T16:13:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8314311392605304718017578125" lon="2.3974214494228363037109375">
        <ele>49</ele>
        <time>2018-08-10T16:13:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8313786685466766357421875" lon="2.3972985707223415374755859375">
        <ele>48.799999237060546875</ele>
        <time>2018-08-10T16:13:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8313786685466766357421875" lon="2.3972985707223415374755859375">
        <ele>48.200000762939453125</ele>
        <time>2018-08-10T16:13:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.831266351044178009033203125" lon="2.39704283885657787322998046875">
        <ele>47.200000762939453125</ele>
        <time>2018-08-10T16:14:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.831159733235836029052734375" lon="2.3967750370502471923828125">
        <ele>46</ele>
        <time>2018-08-10T16:14:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83105345070362091064453125" lon="2.3964959196746349334716796875">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:14:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.830923698842525482177734375" lon="2.39612602628767490386962890625">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:14:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8307052664458751678466796875" lon="2.395470477640628814697265625">
        <ele>45</ele>
        <time>2018-08-10T16:14:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83066528476774692535400390625" lon="2.39532102830708026885986328125">
        <ele>44</ele>
        <time>2018-08-10T16:14:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8306489400565624237060546875" lon="2.39524441771209239959716796875">
        <ele>43.59999847412109375</ele>
        <time>2018-08-10T16:14:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83063234388828277587890625" lon="2.3951685614883899688720703125">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:14:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8306008279323577880859375" lon="2.39501492120325565338134765625">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:14:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8305847346782684326171875" lon="2.39493713714182376861572265625">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:14:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83054994978010654449462890625" lon="2.39478190429508686065673828125">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:14:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8305329345166683197021484375" lon="2.3947058804333209991455078125">
        <ele>42.40000152587890625</ele>
        <time>2018-08-10T16:14:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8304222933948040008544921875" lon="2.39419332705438137054443359375">
        <ele>42.40000152587890625</ele>
        <time>2018-08-10T16:14:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83028566837310791015625" lon="2.39358882419764995574951171875">
        <ele>41.40000152587890625</ele>
        <time>2018-08-10T16:14:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83026965893805027008056640625" lon="2.39351456053555011749267578125">
        <ele>41.200000762939453125</ele>
        <time>2018-08-10T16:14:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.830236382782459259033203125" lon="2.39336796104907989501953125">
        <ele>40.799999237060546875</ele>
        <time>2018-08-10T16:14:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83011710830032825469970703125" lon="2.3928393982350826263427734375">
        <ele>40</ele>
        <time>2018-08-10T16:15:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8300742767751216888427734375" lon="2.3926248215138912200927734375">
        <ele>38.799999237060546875</ele>
        <time>2018-08-10T16:15:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.830073773860931396484375" lon="2.39256120286881923675537109375">
        <ele>38.799999237060546875</ele>
        <time>2018-08-10T16:15:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8301153481006622314453125" lon="2.3924593627452850341796875">
        <ele>39.40000152587890625</ele>
        <time>2018-08-10T16:15:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.830148875713348388671875" lon="2.3924191296100616455078125">
        <ele>39.799999237060546875</ele>
        <time>2018-08-10T16:15:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8302107341587543487548828125" lon="2.39233170635998249053955078125">
        <ele>40.40000152587890625</ele>
        <time>2018-08-10T16:15:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83021433837711811065673828125" lon="2.392217628657817840576171875">
        <ele>40.40000152587890625</ele>
        <time>2018-08-10T16:15:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83012582547962665557861328125" lon="2.3919712007045745849609375">
        <ele>39.59999847412109375</ele>
        <time>2018-08-10T16:15:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83010788820683956146240234375" lon="2.39189811050891876220703125">
        <ele>39.40000152587890625</ele>
        <time>2018-08-10T16:15:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83004988543689250946044921875" lon="2.3916706256568431854248046875">
        <ele>39</ele>
        <time>2018-08-10T16:15:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.83000043220818042755126953125" lon="2.3915204219520092010498046875">
        <ele>38.799999237060546875</ele>
        <time>2018-08-10T16:15:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.829924575984477996826171875" lon="2.39129553548991680145263671875">
        <ele>38.200000762939453125</ele>
        <time>2018-08-10T16:15:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82975350134074687957763671875" lon="2.39086168818175792694091796875">
        <ele>37.799999237060546875</ele>
        <time>2018-08-10T16:15:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82972575724124908447265625" lon="2.39078977145254611968994140625">
        <ele>37.799999237060546875</ele>
        <time>2018-08-10T16:15:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.829666413366794586181640625" lon="2.39064359106123447418212890625">
        <ele>37.59999847412109375</ele>
        <time>2018-08-10T16:15:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82936340756714344024658203125" lon="2.3898951709270477294921875">
        <ele>37.40000152587890625</ele>
        <time>2018-08-10T16:15:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82912133820354938507080078125" lon="2.3893129639327526092529296875">
        <ele>38.799999237060546875</ele>
        <time>2018-08-10T16:15:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82906056940555572509765625" lon="2.3891664482653141021728515625">
        <ele>39.200000762939453125</ele>
        <time>2018-08-10T16:15:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82894112728536128997802734375" lon="2.38887735642492771148681640625">
        <ele>39.799999237060546875</ele>
        <time>2018-08-10T16:15:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82884724996984004974365234375" lon="2.3886642046272754669189453125">
        <ele>40.40000152587890625</ele>
        <time>2018-08-10T16:16:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82874867878854274749755859375" lon="2.388461194932460784912109375">
        <ele>40.799999237060546875</ele>
        <time>2018-08-10T16:16:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82871347479522228240966796875" lon="2.3883969895541667938232421875">
        <ele>41</ele>
        <time>2018-08-10T16:16:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8284817151725292205810546875" lon="2.3879054747521877288818359375">
        <ele>41.200000762939453125</ele>
        <time>2018-08-10T16:16:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82844810374081134796142578125" lon="2.38783364184200763702392578125">
        <ele>41</ele>
        <time>2018-08-10T16:16:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8281722553074359893798828125" lon="2.387251518666744232177734375">
        <ele>40.799999237060546875</ele>
        <time>2018-08-10T16:16:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8281385600566864013671875" lon="2.3871807754039764404296875">
        <ele>40.59999847412109375</ele>
        <time>2018-08-10T16:16:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82806857116520404815673828125" lon="2.3870391212403774261474609375">
        <ele>40</ele>
        <time>2018-08-10T16:16:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82803470827639102935791015625" lon="2.38696795888245105743408203125">
        <ele>39.799999237060546875</ele>
        <time>2018-08-10T16:16:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8277280144393444061279296875" lon="2.3863407410681247711181640625">
        <ele>40.200000762939453125</ele>
        <time>2018-08-10T16:16:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>149</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8276959955692291259765625" lon="2.386269830167293548583984375">
        <ele>40.200000762939453125</ele>
        <time>2018-08-10T16:16:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>149</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82736633531749248504638671875" lon="2.38556834869086742401123046875">
        <ele>40</ele>
        <time>2018-08-10T16:16:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>148</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8272683508694171905517578125" lon="2.38535963930189609527587890625">
        <ele>40.40000152587890625</ele>
        <time>2018-08-10T16:16:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>148</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82714672945439815521240234375" lon="2.385087311267852783203125">
        <ele>41</ele>
        <time>2018-08-10T16:16:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>147</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82708101533353328704833984375" lon="2.38495077006518840789794921875">
        <ele>41.200000762939453125</ele>
        <time>2018-08-10T16:16:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>147</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8269005529582500457763671875" lon="2.38457609899342060089111328125">
        <ele>41.200000762939453125</ele>
        <time>2018-08-10T16:16:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>148</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.826747834682464599609375" lon="2.384238727390766143798828125">
        <ele>42</ele>
        <time>2018-08-10T16:17:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>149</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82671070285141468048095703125" lon="2.38416756503283977508544921875">
        <ele>42.200000762939453125</ele>
        <time>2018-08-10T16:17:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>147</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8264569826424121856689453125" lon="2.3836095817387104034423828125">
        <ele>42.40000152587890625</ele>
        <time>2018-08-10T16:17:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>148</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8263600878417491912841796875" lon="2.38331504166126251220703125">
        <ele>42.59999847412109375</ele>
        <time>2018-08-10T16:17:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>148</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8263124786317348480224609375" lon="2.38319509662687778472900390625">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:17:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>148</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8261173479259014129638671875" lon="2.3827046714723110198974609375">
        <ele>43</ele>
        <time>2018-08-10T16:17:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>147</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8260915316641330718994140625" lon="2.38263417966663837432861328125">
        <ele>43</ele>
        <time>2018-08-10T16:17:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>147</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8259594328701496124267578125" lon="2.382312901318073272705078125">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:17:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>146</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82581216283142566680908203125" lon="2.38203705288469791412353515625">
        <ele>43.59999847412109375</ele>
        <time>2018-08-10T16:17:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82583194412291049957275390625" lon="2.38187335431575775146484375">
        <ele>42.59999847412109375</ele>
        <time>2018-08-10T16:17:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8258447684347629547119140625" lon="2.3816562630236148834228515625">
        <ele>43</ele>
        <time>2018-08-10T16:17:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.825759775936603546142578125" lon="2.38146507181227207183837890625">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:17:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.825696408748626708984375" lon="2.38119383342564105987548828125">
        <ele>44.200000762939453125</ele>
        <time>2018-08-10T16:17:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8256789743900299072265625" lon="2.3811295442283153533935546875">
        <ele>44.200000762939453125</ele>
        <time>2018-08-10T16:17:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>29.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82550437934696674346923828125" lon="2.38054121844470500946044921875">
        <ele>44.40000152587890625</ele>
        <time>2018-08-10T16:18:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82539340294897556304931640625" lon="2.38030560314655303955078125">
        <ele>44.200000762939453125</ele>
        <time>2018-08-10T16:18:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82535107433795928955078125" lon="2.38017710857093334197998046875">
        <ele>44</ele>
        <time>2018-08-10T16:18:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82519458420574665069580078125" lon="2.37970269285142421722412109375">
        <ele>43</ele>
        <time>2018-08-10T16:18:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82514781318604946136474609375" lon="2.3795617930591106414794921875">
        <ele>42.59999847412109375</ele>
        <time>2018-08-10T16:18:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82512484677135944366455078125" lon="2.379491217434406280517578125">
        <ele>42.40000152587890625</ele>
        <time>2018-08-10T16:18:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82488143630325794219970703125" lon="2.37874648533761501312255859375">
        <ele>42.200000762939453125</ele>
        <time>2018-08-10T16:18:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82485427893698215484619140625" lon="2.378667779266834259033203125">
        <ele>42</ele>
        <time>2018-08-10T16:18:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82482804358005523681640625" lon="2.37858940847218036651611328125">
        <ele>41.799999237060546875</ele>
        <time>2018-08-10T16:18:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.824748583137989044189453125" lon="2.37835136242210865020751953125">
        <ele>41.200000762939453125</ele>
        <time>2018-08-10T16:18:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8246692903339862823486328125" lon="2.3781134001910686492919921875">
        <ele>40.799999237060546875</ele>
        <time>2018-08-10T16:18:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82462042383849620819091796875" lon="2.37795087508857250213623046875">
        <ele>40.40000152587890625</ele>
        <time>2018-08-10T16:18:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82453844882547855377197265625" lon="2.3777083866298198699951171875">
        <ele>39.59999847412109375</ele>
        <time>2018-08-10T16:18:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8244282267987728118896484375" lon="2.37738870084285736083984375">
        <ele>39.200000762939453125</ele>
        <time>2018-08-10T16:18:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82434952072799205780029296875" lon="2.37714118324220180511474609375">
        <ele>39</ele>
        <time>2018-08-10T16:18:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8241901807487010955810546875" lon="2.376708425581455230712890625">
        <ele>38.799999237060546875</ele>
        <time>2018-08-10T16:18:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8240305893123149871826171875" lon="2.3762593232095241546630859375">
        <ele>41</ele>
        <time>2018-08-10T16:19:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82401055656373500823974609375" lon="2.3761936090886592864990234375">
        <ele>41.40000152587890625</ele>
        <time>2018-08-10T16:19:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82396797649562358856201171875" lon="2.376071400940418243408203125">
        <ele>42</ele>
        <time>2018-08-10T16:19:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82388013415038585662841796875" lon="2.3758180998265743255615234375">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:19:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82381341420114040374755859375" lon="2.37556773237884044647216796875">
        <ele>44.40000152587890625</ele>
        <time>2018-08-10T16:19:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82378592155873775482177734375" lon="2.37545591779053211212158203125">
        <ele>45</ele>
        <time>2018-08-10T16:19:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82376798428595066070556640625" lon="2.37540093250572681427001953125">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:19:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8237533159554004669189453125" lon="2.3753420077264308929443359375">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:19:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82373772561550140380859375" lon="2.3752793110907077789306640625">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:19:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82370009087026119232177734375" lon="2.375150732696056365966796875">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:19:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.823677711188793182373046875" lon="2.3750851862132549285888671875">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:19:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8236550800502300262451171875" lon="2.37502190284430980682373046875">
        <ele>47.799999237060546875</ele>
        <time>2018-08-10T16:19:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82354150526225566864013671875" lon="2.37481168471276760101318359375">
        <ele>49.200000762939453125</ele>
        <time>2018-08-10T16:19:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82345818914473056793212890625" lon="2.37454564310610294342041015625">
        <ele>50.40000152587890625</ele>
        <time>2018-08-10T16:19:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8234166987240314483642578125" lon="2.37443064339458942413330078125">
        <ele>50.799999237060546875</ele>
        <time>2018-08-10T16:19:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8233721069991588592529296875" lon="2.37432905472815036773681640625">
        <ele>51.40000152587890625</ele>
        <time>2018-08-10T16:19:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.823358528316020965576171875" lon="2.3742818646132946014404296875">
        <ele>51.40000152587890625</ele>
        <time>2018-08-10T16:19:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8232916407287120819091796875" lon="2.374086566269397735595703125">
        <ele>52</ele>
        <time>2018-08-10T16:19:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.823224417865276336669921875" lon="2.3738771863281726837158203125">
        <ele>52.799999237060546875</ele>
        <time>2018-08-10T16:19:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8232081569731235504150390625" lon="2.37382111139595508575439453125">
        <ele>53</ele>
        <time>2018-08-10T16:19:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82310749031603336334228515625" lon="2.3735057003796100616455078125">
        <ele>54</ele>
        <time>2018-08-10T16:19:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8230421952903270721435546875" lon="2.37328383140265941619873046875">
        <ele>55.40000152587890625</ele>
        <time>2018-08-10T16:19:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8229912333190441131591796875" lon="2.3731161095201969146728515625">
        <ele>55.799999237060546875</ele>
        <time>2018-08-10T16:20:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82290129549801349639892578125" lon="2.37290848977863788604736328125">
        <ele>56.40000152587890625</ele>
        <time>2018-08-10T16:20:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82288050837814807891845703125" lon="2.37285786308348178863525390625">
        <ele>56.40000152587890625</ele>
        <time>2018-08-10T16:20:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82284991443157196044921875" lon="2.3727402649819850921630859375">
        <ele>57.59999847412109375</ele>
        <time>2018-08-10T16:20:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.822762072086334228515625" lon="2.37244765274226665496826171875">
        <ele>58.40000152587890625</ele>
        <time>2018-08-10T16:20:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82274975068867206573486328125" lon="2.37239484675228595733642578125">
        <ele>58.799999237060546875</ele>
        <time>2018-08-10T16:20:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82266341708600521087646484375" lon="2.37211237661540508270263671875">
        <ele>59.40000152587890625</ele>
        <time>2018-08-10T16:20:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82260340265929698944091796875" lon="2.37190735526382923126220703125">
        <ele>59.799999237060546875</ele>
        <time>2018-08-10T16:20:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82255998440086841583251953125" lon="2.3718011565506458282470703125">
        <ele>60.40000152587890625</ele>
        <time>2018-08-10T16:20:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82251908071339130401611328125" lon="2.3716266453266143798828125">
        <ele>60.799999237060546875</ele>
        <time>2018-08-10T16:20:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.822504580020904541015625" lon="2.3715708218514919281005859375">
        <ele>61</ele>
        <time>2018-08-10T16:20:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8224853016436100006103515625" lon="2.37152245827019214630126953125">
        <ele>61</ele>
        <time>2018-08-10T16:20:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82246325723826885223388671875" lon="2.37148239277303218841552734375">
        <ele>61.200000762939453125</ele>
        <time>2018-08-10T16:20:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82243978790938854217529296875" lon="2.371443919837474822998046875">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:20:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82241548039019107818603515625" lon="2.37139622680842876434326171875">
        <ele>61.59999847412109375</ele>
        <time>2018-08-10T16:20:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>151</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82237893529236316680908203125" lon="2.37130536697804927825927734375">
        <ele>61.59999847412109375</ele>
        <time>2018-08-10T16:20:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>154</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82236334495246410369873046875" lon="2.371247112751007080078125">
        <ele>61.59999847412109375</ele>
        <time>2018-08-10T16:20:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>28.0</ns3:atemp>
            <ns3:hr>156</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82227952592074871063232421875" lon="2.3709827475249767303466796875">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:20:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>155</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82221582345664501190185546875" lon="2.3707762174308300018310546875">
        <ele>61.59999847412109375</ele>
        <time>2018-08-10T16:20:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>151</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82220408879220485687255859375" lon="2.37072483636438846588134765625">
        <ele>61.799999237060546875</ele>
        <time>2018-08-10T16:20:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>146</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8221939466893672943115234375" lon="2.37067647278308868408203125">
        <ele>61.799999237060546875</ele>
        <time>2018-08-10T16:20:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.822179697453975677490234375" lon="2.3706246726214885711669921875">
        <ele>61.799999237060546875</ele>
        <time>2018-08-10T16:20:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82212840020656585693359375" lon="2.3704706132411956787109375">
        <ele>62</ele>
        <time>2018-08-10T16:20:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82209545932710170745849609375" lon="2.37036424688994884490966796875">
        <ele>62</ele>
        <time>2018-08-10T16:20:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82199051789939403533935546875" lon="2.3700979538261890411376953125">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:21:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82195346988737583160400390625" lon="2.3699785955250263214111328125">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:21:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82191055454313755035400390625" lon="2.3699061758816242218017578125">
        <ele>61.200000762939453125</ele>
        <time>2018-08-10T16:21:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82182355038821697235107421875" lon="2.36972806043922901153564453125">
        <ele>61.799999237060546875</ele>
        <time>2018-08-10T16:21:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82173956371843814849853515625" lon="2.36950661055743694305419921875">
        <ele>62</ele>
        <time>2018-08-10T16:21:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82171894423663616180419921875" lon="2.36946118064224720001220703125">
        <ele>62</ele>
        <time>2018-08-10T16:21:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.821700923144817352294921875" lon="2.36941457726061344146728515625">
        <ele>62</ele>
        <time>2018-08-10T16:21:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82168709300458431243896484375" lon="2.36937199719250202178955078125">
        <ele>62.200000762939453125</ele>
        <time>2018-08-10T16:21:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82167116738855838775634765625" lon="2.36931131221354007720947265625">
        <ele>62.200000762939453125</ele>
        <time>2018-08-10T16:21:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>148</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82164116017520427703857421875" lon="2.36921492032706737518310546875">
        <ele>62</ele>
        <time>2018-08-10T16:21:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8215998373925685882568359375" lon="2.3691306822001934051513671875">
        <ele>61.799999237060546875</ele>
        <time>2018-08-10T16:21:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8216020166873931884765625" lon="2.36912322230637073516845703125">
        <ele>61.59999847412109375</ele>
        <time>2018-08-10T16:21:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>146</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8216020166873931884765625" lon="2.3690974898636341094970703125">
        <ele>61.59999847412109375</ele>
        <time>2018-08-10T16:21:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82159019820392131805419921875" lon="2.3690495453774929046630859375">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:21:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8215502165257930755615234375" lon="2.3689244873821735382080078125">
        <ele>61</ele>
        <time>2018-08-10T16:21:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8215290941298007965087890625" lon="2.36883572302758693695068359375">
        <ele>61</ele>
        <time>2018-08-10T16:21:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8214895315468311309814453125" lon="2.3687350563704967498779296875">
        <ele>61</ele>
        <time>2018-08-10T16:21:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8214834965765476226806640625" lon="2.36872860230505466461181640625">
        <ele>61</ele>
        <time>2018-08-10T16:21:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>146</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8214734382927417755126953125" lon="2.36870848573744297027587890625">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:21:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8214734382927417755126953125" lon="2.36870915628969669342041015625">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:22:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82147385738790035247802734375" lon="2.36871100030839443206787109375">
        <ele>61.59999847412109375</ele>
        <time>2018-08-10T16:22:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82147570140659809112548828125" lon="2.36871443688869476318359375">
        <ele>61.59999847412109375</ele>
        <time>2018-08-10T16:22:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82147788070142269134521484375" lon="2.3687374033033847808837890625">
        <ele>61.200000762939453125</ele>
        <time>2018-08-10T16:22:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82146547548472881317138671875" lon="2.36875089816749095916748046875">
        <ele>61.200000762939453125</ele>
        <time>2018-08-10T16:22:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82145038805902004241943359375" lon="2.36877076327800750732421875">
        <ele>61</ele>
        <time>2018-08-10T16:22:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82139238528907299041748046875" lon="2.3688265867531299591064453125">
        <ele>60.59999847412109375</ele>
        <time>2018-08-10T16:22:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82132683880627155303955078125" lon="2.36883438192307949066162109375">
        <ele>60.40000152587890625</ele>
        <time>2018-08-10T16:22:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8213128410279750823974609375" lon="2.3688182048499584197998046875">
        <ele>60.40000152587890625</ele>
        <time>2018-08-10T16:22:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82127285934984683990478515625" lon="2.3686629720032215118408203125">
        <ele>60.40000152587890625</ele>
        <time>2018-08-10T16:22:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82124595344066619873046875" lon="2.36852861009538173675537109375">
        <ele>60.59999847412109375</ele>
        <time>2018-08-10T16:22:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82121586240828037261962890625" lon="2.36828201450407505035400390625">
        <ele>60.40000152587890625</ele>
        <time>2018-08-10T16:22:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82110379636287689208984375" lon="2.36785856075584888458251953125">
        <ele>60.799999237060546875</ele>
        <time>2018-08-10T16:22:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8210167922079563140869140625" lon="2.36763794906437397003173828125">
        <ele>60.59999847412109375</ele>
        <time>2018-08-10T16:22:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82095065899193286895751953125" lon="2.36749470233917236328125">
        <ele>60</ele>
        <time>2018-08-10T16:22:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82092869840562343597412109375" lon="2.36742706038057804107666015625">
        <ele>60</ele>
        <time>2018-08-10T16:22:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8208887167274951934814453125" lon="2.3672916926443576812744140625">
        <ele>60</ele>
        <time>2018-08-10T16:22:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82086751051247119903564453125" lon="2.3672316782176494598388671875">
        <ele>60</ele>
        <time>2018-08-10T16:22:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82085124962031841278076171875" lon="2.3671726696193218231201171875">
        <ele>60</ele>
        <time>2018-08-10T16:22:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82083901204168796539306640625" lon="2.36710653640329837799072265625">
        <ele>59.799999237060546875</ele>
        <time>2018-08-10T16:22:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8208189792931079864501953125" lon="2.367045097053050994873046875">
        <ele>59.40000152587890625</ele>
        <time>2018-08-10T16:22:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82077296264469623565673828125" lon="2.36691257916390895843505859375">
        <ele>59.200000762939453125</ele>
        <time>2018-08-10T16:22:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8207059912383556365966796875" lon="2.36669850535690784454345703125">
        <ele>58.799999237060546875</ele>
        <time>2018-08-10T16:22:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82063985802233219146728515625" lon="2.366489209234714508056640625">
        <ele>58.799999237060546875</ele>
        <time>2018-08-10T16:22:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8206127844750881195068359375" lon="2.36640815623104572296142578125">
        <ele>58.799999237060546875</ele>
        <time>2018-08-10T16:22:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82059208117425441741943359375" lon="2.36634193919599056243896484375">
        <ele>58.799999237060546875</ele>
        <time>2018-08-10T16:22:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82047859020531177520751953125" lon="2.3660085909068584442138671875">
        <ele>58.799999237060546875</ele>
        <time>2018-08-10T16:22:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8203727267682552337646484375" lon="2.36573575995862483978271484375">
        <ele>58.799999237060546875</ele>
        <time>2018-08-10T16:23:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82029913365840911865234375" lon="2.365534007549285888671875">
        <ele>58.59999847412109375</ele>
        <time>2018-08-10T16:23:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.82015085779130458831787109375" lon="2.365201413631439208984375">
        <ele>58</ele>
        <time>2018-08-10T16:23:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81993326358497142791748046875" lon="2.36500586383044719696044921875">
        <ele>58</ele>
        <time>2018-08-10T16:23:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81988548673689365386962890625" lon="2.364954650402069091796875">
        <ele>57.799999237060546875</ele>
        <time>2018-08-10T16:23:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81984625943005084991455078125" lon="2.36485566012561321258544921875">
        <ele>57.799999237060546875</ele>
        <time>2018-08-10T16:23:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8198156654834747314453125" lon="2.3647244833409786224365234375">
        <ele>57.59999847412109375</ele>
        <time>2018-08-10T16:23:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8197980634868144989013671875" lon="2.3646582663059234619140625">
        <ele>57.59999847412109375</ele>
        <time>2018-08-10T16:23:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8197762705385684967041015625" lon="2.364589534699916839599609375">
        <ele>57.59999847412109375</ele>
        <time>2018-08-10T16:23:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81975322030484676361083984375" lon="2.364520467817783355712890625">
        <ele>57.59999847412109375</ele>
        <time>2018-08-10T16:23:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81957225501537322998046875" lon="2.3639784939587116241455078125">
        <ele>57.40000152587890625</ele>
        <time>2018-08-10T16:23:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8195248134434223175048828125" lon="2.36380021087825298309326171875">
        <ele>56.59999847412109375</ele>
        <time>2018-08-10T16:23:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.819468319416046142578125" lon="2.36366978846490383148193359375">
        <ele>56.40000152587890625</ele>
        <time>2018-08-10T16:23:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81942565552890300750732421875" lon="2.363524697721004486083984375">
        <ele>56.799999237060546875</ele>
        <time>2018-08-10T16:23:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8194111548364162445068359375" lon="2.36347482539713382720947265625">
        <ele>57</ele>
        <time>2018-08-10T16:23:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81935373879969120025634765625" lon="2.36332462169229984283447265625">
        <ele>57</ele>
        <time>2018-08-10T16:23:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81931132636964321136474609375" lon="2.363189756870269775390625">
        <ele>56.799999237060546875</ele>
        <time>2018-08-10T16:23:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.819300346076488494873046875" lon="2.36304282210767269134521484375">
        <ele>56.40000152587890625</ele>
        <time>2018-08-10T16:23:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.819355666637420654296875" lon="2.36280477605760097503662109375">
        <ele>57</ele>
        <time>2018-08-10T16:23:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8193145953118801116943359375" lon="2.36255072057247161865234375">
        <ele>56.799999237060546875</ele>
        <time>2018-08-10T16:23:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.819269500672817230224609375" lon="2.36237905919551849365234375">
        <ele>56.799999237060546875</ele>
        <time>2018-08-10T16:24:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8192324526607990264892578125" lon="2.36226514913141727447509765625">
        <ele>56.59999847412109375</ele>
        <time>2018-08-10T16:24:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8192294351756572723388671875" lon="2.3622048832476139068603515625">
        <ele>56.40000152587890625</ele>
        <time>2018-08-10T16:24:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81922331638634204864501953125" lon="2.3621237464249134063720703125">
        <ele>56.59999847412109375</ele>
        <time>2018-08-10T16:24:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81924242712557315826416015625" lon="2.36205317080020904541015625">
        <ele>57</ele>
        <time>2018-08-10T16:24:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81923320703208446502685546875" lon="2.361842282116413116455078125">
        <ele>57</ele>
        <time>2018-08-10T16:24:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81922884844243526458740234375" lon="2.36176961101591587066650390625">
        <ele>57.200000762939453125</ele>
        <time>2018-08-10T16:24:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.819140754640102386474609375" lon="2.36133961938321590423583984375">
        <ele>57.40000152587890625</ele>
        <time>2018-08-10T16:24:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>119</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.819108568131923675537109375" lon="2.3611209355294704437255859375">
        <ele>57.59999847412109375</ele>
        <time>2018-08-10T16:24:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>117</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81908618845045566558837890625" lon="2.36104524694383144378662109375">
        <ele>57.799999237060546875</ele>
        <time>2018-08-10T16:24:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8190527446568012237548828125" lon="2.3609697259962558746337890625">
        <ele>58.200000762939453125</ele>
        <time>2018-08-10T16:24:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8190145231783390045166015625" lon="2.3609165847301483154296875">
        <ele>58.799999237060546875</ele>
        <time>2018-08-10T16:24:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81891771219670772552490234375" lon="2.36084022559225559234619140625">
        <ele>59.200000762939453125</ele>
        <time>2018-08-10T16:24:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>114</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8186175562441349029541015625" lon="2.36071818508207798004150390625">
        <ele>58.59999847412109375</ele>
        <time>2018-08-10T16:24:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>114</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81852485239505767822265625" lon="2.3606644570827484130859375">
        <ele>58.40000152587890625</ele>
        <time>2018-08-10T16:24:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>115</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81841446273028850555419921875" lon="2.36057837493717670440673828125">
        <ele>58.40000152587890625</ele>
        <time>2018-08-10T16:24:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>118</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81836450658738613128662109375" lon="2.36054954119026660919189453125">
        <ele>58.59999847412109375</ele>
        <time>2018-08-10T16:24:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>117</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81817725487053394317626953125" lon="2.36043219454586505889892578125">
        <ele>58.59999847412109375</ele>
        <time>2018-08-10T16:24:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81813350133597850799560546875" lon="2.36040252260863780975341796875">
        <ele>58.59999847412109375</ele>
        <time>2018-08-10T16:24:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81805940531194210052490234375" lon="2.36034770496189594268798828125">
        <ele>58.40000152587890625</ele>
        <time>2018-08-10T16:24:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81788891740143299102783203125" lon="2.360224910080432891845703125">
        <ele>59</ele>
        <time>2018-08-10T16:24:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81783150136470794677734375" lon="2.360185682773590087890625">
        <ele>59.200000762939453125</ele>
        <time>2018-08-10T16:24:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81775899790227413177490234375" lon="2.36008970998227596282958984375">
        <ele>60</ele>
        <time>2018-08-10T16:24:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8177599199116230010986328125" lon="2.36008132807910442352294921875">
        <ele>60.200000762939453125</ele>
        <time>2018-08-10T16:25:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81770418025553226470947265625" lon="2.35996364615857601165771484375">
        <ele>60.799999237060546875</ele>
        <time>2018-08-10T16:25:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8176211155951023101806640625" lon="2.359874211251735687255859375">
        <ele>61</ele>
        <time>2018-08-10T16:25:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81756856106221675872802734375" lon="2.35984328202903270721435546875">
        <ele>61</ele>
        <time>2018-08-10T16:25:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81751592271029949188232421875" lon="2.35984194092452526092529296875">
        <ele>61</ele>
        <time>2018-08-10T16:25:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8171876035630702972412109375" lon="2.35992475412786006927490234375">
        <ele>60.40000152587890625</ele>
        <time>2018-08-10T16:25:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8171353004872798919677734375" lon="2.359939925372600555419921875">
        <ele>60.40000152587890625</ele>
        <time>2018-08-10T16:25:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8170290179550647735595703125" lon="2.3599830083549022674560546875">
        <ele>60.40000152587890625</ele>
        <time>2018-08-10T16:25:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.816973529756069183349609375" lon="2.36000337637960910797119140625">
        <ele>60.40000152587890625</ele>
        <time>2018-08-10T16:25:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8168171234428882598876953125" lon="2.360066659748554229736328125">
        <ele>60.59999847412109375</ele>
        <time>2018-08-10T16:25:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81672550924122333526611328125" lon="2.36011032946407794952392578125">
        <ele>61</ele>
        <time>2018-08-10T16:25:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8166377507150173187255859375" lon="2.36013270914554595947265625">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:25:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81652166135609149932861328125" lon="2.36012156121432781219482421875">
        <ele>61.200000762939453125</ele>
        <time>2018-08-10T16:25:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8163775764405727386474609375" lon="2.36010177992284297943115234375">
        <ele>61</ele>
        <time>2018-08-10T16:25:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81619577296078205108642578125" lon="2.36008048988878726959228515625">
        <ele>60.799999237060546875</ele>
        <time>2018-08-10T16:25:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8158996403217315673828125" lon="2.36017922870814800262451171875">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:25:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81584666669368743896484375" lon="2.3602180369198322296142578125">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:25:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81579696200788021087646484375" lon="2.3602676577866077423095703125">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:25:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81564826704561710357666015625" lon="2.36042934469878673553466796875">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:25:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81559462286531925201416015625" lon="2.36046253703534603118896484375">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:25:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81553662009537220001220703125" lon="2.3604845814406871795654296875">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:25:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8154156692326068878173828125" lon="2.3605164326727390289306640625">
        <ele>61.59999847412109375</ele>
        <time>2018-08-10T16:25:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.815359175205230712890625" lon="2.360534369945526123046875">
        <ele>61.799999237060546875</ele>
        <time>2018-08-10T16:25:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81530561484396457672119140625" lon="2.3605489544570446014404296875">
        <ele>61.799999237060546875</ele>
        <time>2018-08-10T16:25:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81525968201458454132080078125" lon="2.36054979264736175537109375">
        <ele>61.799999237060546875</ele>
        <time>2018-08-10T16:25:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81522305309772491455078125" lon="2.36052565276622772216796875">
        <ele>62</ele>
        <time>2018-08-10T16:25:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81520159542560577392578125" lon="2.36048466525971889495849609375">
        <ele>62.200000762939453125</ele>
        <time>2018-08-10T16:26:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81514568813145160675048828125" lon="2.360381819307804107666015625">
        <ele>62.200000762939453125</ele>
        <time>2018-08-10T16:26:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8150735199451446533203125" lon="2.3602907918393611907958984375">
        <ele>62</ele>
        <time>2018-08-10T16:26:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81499506533145904541015625" lon="2.360228262841701507568359375">
        <ele>62</ele>
        <time>2018-08-10T16:26:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81482541561126708984375" lon="2.36009859479963779449462890625">
        <ele>62.200000762939453125</ele>
        <time>2018-08-10T16:26:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8147374056279659271240234375" lon="2.3600341379642486572265625">
        <ele>62.40000152587890625</ele>
        <time>2018-08-10T16:26:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81454487331211566925048828125" lon="2.3598629795014858245849609375">
        <ele>62.799999237060546875</ele>
        <time>2018-08-10T16:26:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.814456276595592498779296875" lon="2.359774969518184661865234375">
        <ele>63</ele>
        <time>2018-08-10T16:26:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8144135288894176483154296875" lon="2.35974068753421306610107421875">
        <ele>63.200000762939453125</ele>
        <time>2018-08-10T16:26:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81436164490878582000732421875" lon="2.3596494086086750030517578125">
        <ele>63.200000762939453125</ele>
        <time>2018-08-10T16:26:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81435125134885311126708984375" lon="2.359590232372283935546875">
        <ele>63.40000152587890625</ele>
        <time>2018-08-10T16:26:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.814239017665386199951171875" lon="2.359249927103519439697265625">
        <ele>63.799999237060546875</ele>
        <time>2018-08-10T16:26:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8142137043178081512451171875" lon="2.35910944640636444091796875">
        <ele>63.799999237060546875</ele>
        <time>2018-08-10T16:26:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81410834379494190216064453125" lon="2.358698733150959014892578125">
        <ele>64.1999969482421875</ele>
        <time>2018-08-10T16:26:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81406844593584537506103515625" lon="2.35858884640038013458251953125">
        <ele>64.1999969482421875</ele>
        <time>2018-08-10T16:26:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81405226886272430419921875" lon="2.35852648504078388214111328125">
        <ele>64.1999969482421875</ele>
        <time>2018-08-10T16:26:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.813997618854045867919921875" lon="2.358339987695217132568359375">
        <ele>64.59999847412109375</ele>
        <time>2018-08-10T16:26:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.813899718225002288818359375" lon="2.358072437345981597900390625">
        <ele>65.40000152587890625</ele>
        <time>2018-08-10T16:26:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81387054920196533203125" lon="2.35800412483513355255126953125">
        <ele>65.40000152587890625</ele>
        <time>2018-08-10T16:26:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.813854120671749114990234375" lon="2.35794109292328357696533203125">
        <ele>65.1999969482421875</ele>
        <time>2018-08-10T16:26:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8137490116059780120849609375" lon="2.35761838965117931365966796875">
        <ele>65.40000152587890625</ele>
        <time>2018-08-10T16:26:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8136760890483856201171875" lon="2.35744589008390903472900390625">
        <ele>65.8000030517578125</ele>
        <time>2018-08-10T16:26:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81362571381032466888427734375" lon="2.3572981171309947967529296875">
        <ele>66.40000152587890625</ele>
        <time>2018-08-10T16:26:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81360459141433238983154296875" lon="2.35722594894468784332275390625">
        <ele>66.59999847412109375</ele>
        <time>2018-08-10T16:26:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81351389922201633453369140625" lon="2.35696946270763874053955078125">
        <ele>67.1999969482421875</ele>
        <time>2018-08-10T16:27:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81342370994389057159423828125" lon="2.3567744158208370208740234375">
        <ele>67.40000152587890625</ele>
        <time>2018-08-10T16:27:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81329454481601715087890625" lon="2.35652421601116657257080078125">
        <ele>67.1999969482421875</ele>
        <time>2018-08-10T16:27:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8132232986390590667724609375" lon="2.35641173087060451507568359375">
        <ele>67.1999969482421875</ele>
        <time>2018-08-10T16:27:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81301417015492916107177734375" lon="2.35596380196511745452880859375">
        <ele>66.59999847412109375</ele>
        <time>2018-08-10T16:27:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>147</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81276522763073444366455078125" lon="2.355497181415557861328125">
        <ele>66.40000152587890625</ele>
        <time>2018-08-10T16:27:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81269113160669803619384765625" lon="2.35533448867499828338623046875">
        <ele>66.59999847412109375</ele>
        <time>2018-08-10T16:27:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81261753849685192108154296875" lon="2.35514908097684383392333984375">
        <ele>65.8000030517578125</ele>
        <time>2018-08-10T16:27:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8125926442444324493408203125" lon="2.3550869710743427276611328125">
        <ele>65.40000152587890625</ele>
        <time>2018-08-10T16:27:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81253925152122974395751953125" lon="2.3549696244299411773681640625">
        <ele>65</ele>
        <time>2018-08-10T16:27:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81244420073926448822021484375" lon="2.3548735678195953369140625">
        <ele>64.8000030517578125</ele>
        <time>2018-08-10T16:27:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81235099397599697113037109375" lon="2.35474356450140476226806640625">
        <ele>64.59999847412109375</ele>
        <time>2018-08-10T16:27:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8123358227312564849853515625" lon="2.35467818565666675567626953125">
        <ele>63.799999237060546875</ele>
        <time>2018-08-10T16:27:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81230480968952178955078125" lon="2.354451119899749755859375">
        <ele>63.200000762939453125</ele>
        <time>2018-08-10T16:27:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81230640225112438201904296875" lon="2.35436487011611461639404296875">
        <ele>62.799999237060546875</ele>
        <time>2018-08-10T16:27:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81233238615095615386962890625" lon="2.35407762229442596435546875">
        <ele>61.40000152587890625</ele>
        <time>2018-08-10T16:27:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81233389489352703094482421875" lon="2.35398014076054096221923828125">
        <ele>60.59999847412109375</ele>
        <time>2018-08-10T16:27:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8123267702758312225341796875" lon="2.3538819886744022369384765625">
        <ele>60</ele>
        <time>2018-08-10T16:27:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81231880746781826019287109375" lon="2.3536144383251667022705078125">
        <ele>59.200000762939453125</ele>
        <time>2018-08-10T16:27:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.812316544353961944580078125" lon="2.3535433597862720489501953125">
        <ele>59</ele>
        <time>2018-08-10T16:27:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81234009750187397003173828125" lon="2.353352420032024383544921875">
        <ele>59</ele>
        <time>2018-08-10T16:28:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81237463094294071197509765625" lon="2.35322996042668819427490234375">
        <ele>59</ele>
        <time>2018-08-10T16:28:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.812389634549617767333984375" lon="2.35316759906709194183349609375">
        <ele>59</ele>
        <time>2018-08-10T16:28:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81244688294827938079833984375" lon="2.35279359854757785797119140625">
        <ele>57.59999847412109375</ele>
        <time>2018-08-10T16:28:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81246129982173442840576171875" lon="2.35264741815626621246337890625">
        <ele>57</ele>
        <time>2018-08-10T16:28:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81245057098567485809326171875" lon="2.35257348977029323577880859375">
        <ele>56.59999847412109375</ele>
        <time>2018-08-10T16:28:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81242156960070133209228515625" lon="2.352503836154937744140625">
        <ele>56.40000152587890625</ele>
        <time>2018-08-10T16:28:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81238368339836597442626953125" lon="2.35244323499500751495361328125">
        <ele>56.40000152587890625</ele>
        <time>2018-08-10T16:28:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81230774335563182830810546875" lon="2.35232421196997165679931640625">
        <ele>56.59999847412109375</ele>
        <time>2018-08-10T16:28:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81228527985513210296630859375" lon="2.3521908558905124664306640625">
        <ele>56.799999237060546875</ele>
        <time>2018-08-10T16:28:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81234110333025455474853515625" lon="2.3519011773169040679931640625">
        <ele>56.799999237060546875</ele>
        <time>2018-08-10T16:28:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81234504282474517822265625" lon="2.3517365567386150360107421875">
        <ele>56.40000152587890625</ele>
        <time>2018-08-10T16:28:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8123304583132266998291015625" lon="2.3515729419887065887451171875">
        <ele>55.40000152587890625</ele>
        <time>2018-08-10T16:28:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81230925209820270538330078125" lon="2.351394407451152801513671875">
        <ele>54</ele>
        <time>2018-08-10T16:28:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81228243000805377960205078125" lon="2.35121075995266437530517578125">
        <ele>52.40000152587890625</ele>
        <time>2018-08-10T16:28:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81226415745913982391357421875" lon="2.3511216603219509124755859375">
        <ele>51.799999237060546875</ele>
        <time>2018-08-10T16:28:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.812245465815067291259765625" lon="2.35102434642612934112548828125">
        <ele>51</ele>
        <time>2018-08-10T16:28:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81221093237400054931640625" lon="2.35080222599208354949951171875">
        <ele>50.200000762939453125</ele>
        <time>2018-08-10T16:28:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81219592876732349395751953125" lon="2.35068890266120433807373046875">
        <ele>50</ele>
        <time>2018-08-10T16:28:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81217195652425289154052734375" lon="2.35058027319610118865966796875">
        <ele>50</ele>
        <time>2018-08-10T16:28:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81213624961674213409423828125" lon="2.35038547776639461517333984375">
        <ele>49</ele>
        <time>2018-08-10T16:28:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81213373504579067230224609375" lon="2.350294031202793121337890625">
        <ele>48.59999847412109375</ele>
        <time>2018-08-10T16:28:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81214563734829425811767578125" lon="2.35007886774837970733642578125">
        <ele>47.799999237060546875</ele>
        <time>2018-08-10T16:28:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81219425238668918609619140625" lon="2.34985046088695526123046875">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:28:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81222912110388278961181640625" lon="2.3497366346418857574462890625">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:28:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.812281005084514617919921875" lon="2.349636219441890716552734375">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:28:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.812340013682842254638671875" lon="2.34954142011702060699462890625">
        <ele>44.799999237060546875</ele>
        <time>2018-08-10T16:28:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81245576776564121246337890625" lon="2.3493611253798007965087890625">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:28:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8125738687813282012939453125" lon="2.3491840995848178863525390625">
        <ele>42.59999847412109375</ele>
        <time>2018-08-10T16:28:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.812758438289165496826171875" lon="2.3489108495414257049560546875">
        <ele>42.59999847412109375</ele>
        <time>2018-08-10T16:28:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.812820799648761749267578125" lon="2.34883214347064495086669921875">
        <ele>42.200000762939453125</ele>
        <time>2018-08-10T16:28:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81284535862505435943603515625" lon="2.34878453426063060760498046875">
        <ele>42</ele>
        <time>2018-08-10T16:28:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.812867067754268646240234375" lon="2.34872267581522464752197265625">
        <ele>41.799999237060546875</ele>
        <time>2018-08-10T16:28:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8128373958170413970947265625" lon="2.34860348515212535858154296875">
        <ele>41.799999237060546875</ele>
        <time>2018-08-10T16:29:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81281233392655849456787109375" lon="2.34854917041957378387451171875">
        <ele>41.799999237060546875</ele>
        <time>2018-08-10T16:29:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8127586059272289276123046875" lon="2.34845445491373538970947265625">
        <ele>41.799999237060546875</ele>
        <time>2018-08-10T16:29:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.812681324779987335205078125" lon="2.348309196531772613525390625">
        <ele>42</ele>
        <time>2018-08-10T16:29:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81259205751121044158935546875" lon="2.34817273914813995361328125">
        <ele>41.799999237060546875</ele>
        <time>2018-08-10T16:29:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81247806362807750701904296875" lon="2.34797283075749874114990234375">
        <ele>41.59999847412109375</ele>
        <time>2018-08-10T16:29:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81237748079001903533935546875" lon="2.34780971892178058624267578125">
        <ele>41.40000152587890625</ele>
        <time>2018-08-10T16:29:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81234236061573028564453125" lon="2.3477477766573429107666015625">
        <ele>41.40000152587890625</ele>
        <time>2018-08-10T16:29:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8123029656708240509033203125" lon="2.34769086353480815887451171875">
        <ele>41.40000152587890625</ele>
        <time>2018-08-10T16:29:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81226365454494953155517578125" lon="2.34763202257454395294189453125">
        <ele>41.59999847412109375</ele>
        <time>2018-08-10T16:29:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81218838505446910858154296875" lon="2.34751735813915729522705078125">
        <ele>41.59999847412109375</ele>
        <time>2018-08-10T16:29:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81201672367751598358154296875" lon="2.34729456715285778045654296875">
        <ele>42.40000152587890625</ele>
        <time>2018-08-10T16:29:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81182871758937835693359375" lon="2.34701712615787982940673828125">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:29:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81179929710924625396728515625" lon="2.34695677645504474639892578125">
        <ele>43</ele>
        <time>2018-08-10T16:29:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81178789772093296051025390625" lon="2.346897013485431671142578125">
        <ele>43</ele>
        <time>2018-08-10T16:29:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81178270094096660614013671875" lon="2.3468250967562198638916015625">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:29:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81178488023579120635986328125" lon="2.34675728715956211090087890625">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:29:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.811786137521266937255859375" lon="2.34662669710814952850341796875">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:29:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81177163682878017425537109375" lon="2.34656232409179210662841796875">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:29:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81173257716000080108642578125" lon="2.34645017422735691070556640625">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:29:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>120</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81171313114464282989501953125" lon="2.34640072099864482879638671875">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:29:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>118</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.811670131981372833251953125" lon="2.3462933488190174102783203125">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:29:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>116</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8115573115646839141845703125" lon="2.34599922783672809600830078125">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:29:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>118</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81153996102511882781982421875" lon="2.3459342680871486663818359375">
        <ele>43</ele>
        <time>2018-08-10T16:29:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8115187548100948333740234375" lon="2.34587081708014011383056640625">
        <ele>43</ele>
        <time>2018-08-10T16:29:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81149285472929477691650390625" lon="2.3458095453679561614990234375">
        <ele>43</ele>
        <time>2018-08-10T16:29:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8114634342491626739501953125" lon="2.34575145877897739410400390625">
        <ele>43</ele>
        <time>2018-08-10T16:29:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81142596714198589324951171875" lon="2.34569664113223552703857421875">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:29:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81129872985184192657470703125" lon="2.34550989232957363128662109375">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:29:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.811135701835155487060546875" lon="2.34527268446981906890869140625">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:30:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8110869191586971282958984375" lon="2.345220632851123809814453125">
        <ele>42.59999847412109375</ele>
        <time>2018-08-10T16:30:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8107922114431858062744140625" lon="2.34480245970189571380615234375">
        <ele>42.40000152587890625</ele>
        <time>2018-08-10T16:30:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.810752145946025848388671875" lon="2.34474085271358489990234375">
        <ele>42.200000762939453125</ele>
        <time>2018-08-10T16:30:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8105189613997936248779296875" lon="2.3443370126187801361083984375">
        <ele>41.59999847412109375</ele>
        <time>2018-08-10T16:30:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.810333050787448883056640625" lon="2.34399930573999881744384765625">
        <ele>41.59999847412109375</ele>
        <time>2018-08-10T16:30:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81015300750732421875" lon="2.34369034878909587860107421875">
        <ele>41.200000762939453125</ele>
        <time>2018-08-10T16:30:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.81011880934238433837890625" lon="2.34363469295203685760498046875">
        <ele>41.200000762939453125</ele>
        <time>2018-08-10T16:30:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8099030591547489166259765625" lon="2.34330997802317142486572265625">
        <ele>41.40000152587890625</ele>
        <time>2018-08-10T16:30:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80975528620183467864990234375" lon="2.34308400191366672515869140625">
        <ele>41.200000762939453125</ele>
        <time>2018-08-10T16:30:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80966283380985260009765625" lon="2.34297889284789562225341796875">
        <ele>41.200000762939453125</ele>
        <time>2018-08-10T16:30:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8095198385417461395263671875" lon="2.342843525111675262451171875">
        <ele>41</ele>
        <time>2018-08-10T16:30:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80938899703323841094970703125" lon="2.3427907191216945648193359375">
        <ele>40.799999237060546875</ele>
        <time>2018-08-10T16:30:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80909747444093227386474609375" lon="2.342577315866947174072265625">
        <ele>40.59999847412109375</ele>
        <time>2018-08-10T16:30:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80904835648834705352783203125" lon="2.34254051931202411651611328125">
        <ele>40.799999237060546875</ele>
        <time>2018-08-10T16:30:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80867033265531063079833984375" lon="2.34228436835110187530517578125">
        <ele>41.40000152587890625</ele>
        <time>2018-08-10T16:30:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80822257138788700103759765625" lon="2.3419377766549587249755859375">
        <ele>41.40000152587890625</ele>
        <time>2018-08-10T16:31:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80773256532847881317138671875" lon="2.34150418080389499664306640625">
        <ele>41.59999847412109375</ele>
        <time>2018-08-10T16:31:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8072949461638927459716796875" lon="2.3410650528967380523681640625">
        <ele>41.59999847412109375</ele>
        <time>2018-08-10T16:31:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.807026557624340057373046875" lon="2.3408501408994197845458984375">
        <ele>41.40000152587890625</ele>
        <time>2018-08-10T16:31:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80673243664205074310302734375" lon="2.34054428525269031524658203125">
        <ele>41.40000152587890625</ele>
        <time>2018-08-10T16:31:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80649975501000881195068359375" lon="2.34031411819159984588623046875">
        <ele>41.59999847412109375</ele>
        <time>2018-08-10T16:31:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80645432509481906890869140625" lon="2.34026986174285411834716796875">
        <ele>41.799999237060546875</ele>
        <time>2018-08-10T16:31:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80636371672153472900390625" lon="2.34017573297023773193359375">
        <ele>42.200000762939453125</ele>
        <time>2018-08-10T16:31:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8062356412410736083984375" lon="2.3400220088660717010498046875">
        <ele>42.59999847412109375</ele>
        <time>2018-08-10T16:31:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8061576895415782928466796875" lon="2.3399100266396999359130859375">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:31:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.806121647357940673828125" lon="2.3398473300039768218994140625">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:31:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80605283193290233612060546875" lon="2.33961188234388828277587890625">
        <ele>42.40000152587890625</ele>
        <time>2018-08-10T16:32:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80600824020802974700927734375" lon="2.3394476808607578277587890625">
        <ele>42.200000762939453125</ele>
        <time>2018-08-10T16:32:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80585066042840480804443359375" lon="2.33894585631787776947021484375">
        <ele>41.799999237060546875</ele>
        <time>2018-08-10T16:32:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80579483695328235626220703125" lon="2.33878056518733501434326171875">
        <ele>41.40000152587890625</ele>
        <time>2018-08-10T16:32:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80556517280638217926025390625" lon="2.338030301034450531005859375">
        <ele>42</ele>
        <time>2018-08-10T16:32:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8054861314594745635986328125" lon="2.3378054983913898468017578125">
        <ele>41.59999847412109375</ele>
        <time>2018-08-10T16:32:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80541111342608928680419921875" lon="2.3375553824007511138916015625">
        <ele>41.200000762939453125</ele>
        <time>2018-08-10T16:32:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80522185005247592926025390625" lon="2.3370943777263164520263671875">
        <ele>42</ele>
        <time>2018-08-10T16:32:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80493770353496074676513671875" lon="2.33668173663318157196044921875">
        <ele>42</ele>
        <time>2018-08-10T16:32:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8046821393072605133056640625" lon="2.336391471326351165771484375">
        <ele>42</ele>
        <time>2018-08-10T16:32:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80436337552964687347412109375" lon="2.336032055318355560302734375">
        <ele>41.799999237060546875</ele>
        <time>2018-08-10T16:32:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80432398058474063873291015625" lon="2.33599022962152957916259765625">
        <ele>41.799999237060546875</ele>
        <time>2018-08-10T16:32:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80428542383015155792236328125" lon="2.33594848774373531341552734375">
        <ele>41.59999847412109375</ele>
        <time>2018-08-10T16:32:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80420722067356109619140625" lon="2.33585980720818042755126953125">
        <ele>41.59999847412109375</ele>
        <time>2018-08-10T16:32:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80412583239376544952392578125" lon="2.3357737250626087188720703125">
        <ele>41.799999237060546875</ele>
        <time>2018-08-10T16:32:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8040491379797458648681640625" lon="2.3356865532696247100830078125">
        <ele>42.200000762939453125</ele>
        <time>2018-08-10T16:33:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80397361703217029571533203125" lon="2.3356007225811481475830078125">
        <ele>42.59999847412109375</ele>
        <time>2018-08-10T16:33:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80393673665821552276611328125" lon="2.33555755577981472015380859375">
        <ele>42.59999847412109375</ele>
        <time>2018-08-10T16:33:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8039016164839267730712890625" lon="2.335518412292003631591796875">
        <ele>42.799999237060546875</ele>
        <time>2018-08-10T16:33:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.803744874894618988037109375" lon="2.335325963795185089111328125">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:33:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80370221100747585296630859375" lon="2.335276342928409576416015625">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:33:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.803621828556060791015625" lon="2.33518539927899837493896484375">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:33:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80336659960448741912841796875" lon="2.33488817699253559112548828125">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:33:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80313240922987461090087890625" lon="2.3346086405217647552490234375">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:33:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80294390022754669189453125" lon="2.33440185897052288055419921875">
        <ele>42.200000762939453125</ele>
        <time>2018-08-10T16:33:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8028123043477535247802734375" lon="2.33423975296318531036376953125">
        <ele>42.200000762939453125</ele>
        <time>2018-08-10T16:33:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.802642486989498138427734375" lon="2.3340001143515110015869140625">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:33:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80259764380753040313720703125" lon="2.33393951319158077239990234375">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:33:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80245506763458251953125" lon="2.33378402888774871826171875">
        <ele>43.59999847412109375</ele>
        <time>2018-08-10T16:33:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80232699215412139892578125" lon="2.33374589122831821441650390625">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:33:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80187294445931911468505859375" lon="2.33359895646572113037109375">
        <ele>44.200000762939453125</ele>
        <time>2018-08-10T16:33:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80137857981026172637939453125" lon="2.33343693427741527557373046875">
        <ele>43.799999237060546875</ele>
        <time>2018-08-10T16:33:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.80093090236186981201171875" lon="2.33329762704670429229736328125">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:34:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.800518512725830078125" lon="2.33322202228009700775146484375">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:34:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.8003473542630672454833984375" lon="2.3331686295568943023681640625">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:34:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.800186254084110260009765625" lon="2.33308791182935237884521484375">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:34:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79980764351785182952880859375" lon="2.33294961042702198028564453125">
        <ele>43.799999237060546875</ele>
        <time>2018-08-10T16:34:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79975743591785430908203125" lon="2.33292580582201480865478515625">
        <ele>43.799999237060546875</ele>
        <time>2018-08-10T16:34:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79941604100167751312255859375" lon="2.3327807150781154632568359375">
        <ele>43.799999237060546875</ele>
        <time>2018-08-10T16:34:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.799364157021045684814453125" lon="2.33276738785207271575927734375">
        <ele>44</ele>
        <time>2018-08-10T16:34:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7992050684988498687744140625" lon="2.33271474950015544891357421875">
        <ele>44</ele>
        <time>2018-08-10T16:34:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79904807545244693756103515625" lon="2.3326834850013256072998046875">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:34:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7988174892961978912353515625" lon="2.3326563276350498199462890625">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:34:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79870559088885784149169921875" lon="2.33265138231217861175537109375">
        <ele>43.59999847412109375</ele>
        <time>2018-08-10T16:34:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.798352293670177459716796875" lon="2.33257544226944446563720703125">
        <ele>43.59999847412109375</ele>
        <time>2018-08-10T16:34:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7981811352074146270751953125" lon="2.33264970593154430389404296875">
        <ele>43.799999237060546875</ele>
        <time>2018-08-10T16:34:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79812799394130706787109375" lon="2.3326962254941463470458984375">
        <ele>43.799999237060546875</ele>
        <time>2018-08-10T16:34:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79808030091226100921630859375" lon="2.33275003731250762939453125">
        <ele>43.799999237060546875</ele>
        <time>2018-08-10T16:34:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79780520685017108917236328125" lon="2.33303963206708431243896484375">
        <ele>43.59999847412109375</ele>
        <time>2018-08-10T16:35:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.797678388655185699462890625" lon="2.33318329788744449615478515625">
        <ele>43.59999847412109375</ele>
        <time>2018-08-10T16:35:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79758979193866252899169921875" lon="2.333306260406970977783203125">
        <ele>43.799999237060546875</ele>
        <time>2018-08-10T16:35:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7971629016101360321044921875" lon="2.333783693611621856689453125">
        <ele>44.200000762939453125</ele>
        <time>2018-08-10T16:35:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79677775315940380096435546875" lon="2.33418392948806285858154296875">
        <ele>43.200000762939453125</ele>
        <time>2018-08-10T16:35:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79643040709197521209716796875" lon="2.33453094027936458587646484375">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:35:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79638397134840488433837890625" lon="2.33458424918353557586669921875">
        <ele>43.40000152587890625</ele>
        <time>2018-08-10T16:35:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7959673069417476654052734375" lon="2.33501977287232875823974609375">
        <ele>43.799999237060546875</ele>
        <time>2018-08-10T16:35:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79592531360685825347900390625" lon="2.33507039956748485565185546875">
        <ele>43.799999237060546875</ele>
        <time>2018-08-10T16:35:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79572163335978984832763671875" lon="2.33530241064727306365966796875">
        <ele>44</ele>
        <time>2018-08-10T16:35:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.795633204281330108642578125" lon="2.33537231571972370147705078125">
        <ele>44.799999237060546875</ele>
        <time>2018-08-10T16:35:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7955364771187305450439453125" lon="2.33542126603424549102783203125">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:35:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7954892031848430633544921875" lon="2.3354297317564487457275390625">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:35:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7954424321651458740234375" lon="2.33543492853641510009765625">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:35:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79534310661256313323974609375" lon="2.3354273848235607147216796875">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:35:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79524059593677520751953125" lon="2.33538589440286159515380859375">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:36:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79513992927968502044677734375" lon="2.3353335075080394744873046875">
        <ele>47</ele>
        <time>2018-08-10T16:36:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79499810747802257537841796875" lon="2.33524524606764316558837890625">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:36:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7949537672102451324462890625" lon="2.33522362075746059417724609375">
        <ele>47.59999847412109375</ele>
        <time>2018-08-10T16:36:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79490372724831104278564453125" lon="2.33519931323826313018798828125">
        <ele>47.59999847412109375</ele>
        <time>2018-08-10T16:36:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.794827871024608612060546875" lon="2.33510660938918590545654296875">
        <ele>47.799999237060546875</ele>
        <time>2018-08-10T16:36:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7946231849491596221923828125" lon="2.33476965688169002532958984375">
        <ele>47.59999847412109375</ele>
        <time>2018-08-10T16:36:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7943105399608612060546875" lon="2.3343169502913951873779296875">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:36:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.794225044548511505126953125" lon="2.3342059738934040069580078125">
        <ele>46</ele>
        <time>2018-08-10T16:36:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79413611255586147308349609375" lon="2.3341043852269649505615234375">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:36:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7940565682947635650634765625" lon="2.33401947654783725738525390625">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:36:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79398691467940807342529296875" lon="2.3339414410293102264404296875">
        <ele>45</ele>
        <time>2018-08-10T16:36:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7939130701124668121337890625" lon="2.333864830434322357177734375">
        <ele>45</ele>
        <time>2018-08-10T16:36:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7938580848276615142822265625" lon="2.33381311409175395965576171875">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:36:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79384978674352169036865234375" lon="2.3338074982166290283203125">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:36:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79385054111480712890625" lon="2.33379249460995197296142578125">
        <ele>46</ele>
        <time>2018-08-10T16:36:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79384257830679416656494140625" lon="2.3337868787348270416259765625">
        <ele>46</ele>
        <time>2018-08-10T16:36:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7938336096704006195068359375" lon="2.33377723954617977142333984375">
        <ele>46</ele>
        <time>2018-08-10T16:36:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79371198825538158416748046875" lon="2.3336494155228137969970703125">
        <ele>46.799999237060546875</ele>
        <time>2018-08-10T16:36:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.793673180043697357177734375" lon="2.33361144550144672393798828125">
        <ele>46.799999237060546875</ele>
        <time>2018-08-10T16:36:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79353680647909641265869140625" lon="2.33350348658859729766845703125">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:36:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79343706183135509490966796875" lon="2.33342352323234081268310546875">
        <ele>46</ele>
        <time>2018-08-10T16:36:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79314285703003406524658203125" lon="2.33318765647709369659423828125">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:37:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79309021867811679840087890625" lon="2.33315714634954929351806640625">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:37:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79292861558496952056884765625" lon="2.3331116326153278350830078125">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:37:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79278905689716339111328125" lon="2.3330496065318584442138671875">
        <ele>45</ele>
        <time>2018-08-10T16:37:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7927362509071826934814453125" lon="2.33299956656992435455322265625">
        <ele>44.799999237060546875</ele>
        <time>2018-08-10T16:37:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79263273440301418304443359375" lon="2.33291809447109699249267578125">
        <ele>45</ele>
        <time>2018-08-10T16:37:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79244749434292316436767578125" lon="2.33278398402035236358642578125">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:37:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79240415990352630615234375" lon="2.3327533900737762451171875">
        <ele>46</ele>
        <time>2018-08-10T16:37:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.792062513530254364013671875" lon="2.33254811726510524749755859375">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:37:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79196695983409881591796875" lon="2.3324803076684474945068359375">
        <ele>44.799999237060546875</ele>
        <time>2018-08-10T16:37:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79172623157501220703125" lon="2.3322962410748004913330078125">
        <ele>44.59999847412109375</ele>
        <time>2018-08-10T16:37:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79137561656534671783447265625" lon="2.332057021558284759521484375">
        <ele>44.40000152587890625</ele>
        <time>2018-08-10T16:37:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79104696214199066162109375" lon="2.33188192360103130340576171875">
        <ele>44.799999237060546875</ele>
        <time>2018-08-10T16:37:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79098099656403064727783203125" lon="2.33182358555495738983154296875">
        <ele>44.40000152587890625</ele>
        <time>2018-08-10T16:37:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7908848561346530914306640625" lon="2.3317882977426052093505859375">
        <ele>44.59999847412109375</ele>
        <time>2018-08-10T16:37:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7908037193119525909423828125" lon="2.33175787143409252166748046875">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:37:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79075719974935054779052734375" lon="2.3317373357713222503662109375">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:37:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79042871296405792236328125" lon="2.33153709210455417633056640625">
        <ele>45</ele>
        <time>2018-08-10T16:37:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.79015646874904632568359375" lon="2.3313889838755130767822265625">
        <ele>44.59999847412109375</ele>
        <time>2018-08-10T16:38:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.790106512606143951416015625" lon="2.33136140741407871246337890625">
        <ele>44.59999847412109375</ele>
        <time>2018-08-10T16:38:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.789954297244548797607421875" lon="2.3312726430594921112060546875">
        <ele>44.59999847412109375</ele>
        <time>2018-08-10T16:38:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7897985614836215972900390625" lon="2.331194020807743072509765625">
        <ele>44.200000762939453125</ele>
        <time>2018-08-10T16:38:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78944442607462406158447265625" lon="2.33101825229823589324951171875">
        <ele>44</ele>
        <time>2018-08-10T16:38:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7889970839023590087890625" lon="2.3307962156832218170166015625">
        <ele>44.200000762939453125</ele>
        <time>2018-08-10T16:38:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7889399193227291107177734375" lon="2.33076126314699649810791015625">
        <ele>44.200000762939453125</ele>
        <time>2018-08-10T16:38:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78846206702291965484619140625" lon="2.33049589209258556365966796875">
        <ele>45</ele>
        <time>2018-08-10T16:38:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78801011480391025543212890625" lon="2.33023521490395069122314453125">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:38:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78795848228037357330322265625" lon="2.33020872808992862701416015625">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:38:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7874502874910831451416015625" lon="2.32990982942283153533935546875">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:38:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78718751482665538787841796875" lon="2.32970715500414371490478515625">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:39:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.787150718271732330322265625" lon="2.32967044226825237274169921875">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:39:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78687403164803981781005859375" lon="2.32946190051734447479248046875">
        <ele>44</ele>
        <time>2018-08-10T16:39:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.786737658083438873291015625" lon="2.32933818362653255462646484375">
        <ele>44.799999237060546875</ele>
        <time>2018-08-10T16:39:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78656960092484951019287109375" lon="2.3293062485754489898681640625">
        <ele>46</ele>
        <time>2018-08-10T16:39:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7865245901048183441162109375" lon="2.32929325662553310394287109375">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:39:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78634345717728137969970703125" lon="2.32919611036777496337890625">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:39:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78624907694756984710693359375" lon="2.32914674095809459686279296875">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:39:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7860702909529209136962890625" lon="2.329058982431888580322265625">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:39:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78598622046411037445068359375" lon="2.329010702669620513916015625">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:39:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78589938394725322723388671875" lon="2.32896317727863788604736328125">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:39:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7858557142317295074462890625" lon="2.32893920503556728363037109375">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:39:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78556913696229457855224609375" lon="2.32881012372672557830810546875">
        <ele>46</ele>
        <time>2018-08-10T16:39:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78539144061505794525146484375" lon="2.32871121726930141448974609375">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:39:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78534634597599506378173828125" lon="2.3286900110542774200439453125">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:39:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78508382476866245269775390625" lon="2.3285237140953540802001953125">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:39:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.784991204738616943359375" lon="2.32847618870437145233154296875">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:39:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78480353392660617828369140625" lon="2.328401505947113037109375">
        <ele>46.799999237060546875</ele>
        <time>2018-08-10T16:40:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7847107462584972381591796875" lon="2.32836621813476085662841796875">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:40:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78466607071459293365478515625" lon="2.32835657894611358642578125">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:40:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78434621728956699371337890625" lon="2.32824803330004215240478515625">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:40:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7840170599520206451416015625" lon="2.3281398229300975799560546875">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:40:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78383810631930828094482421875" lon="2.3280912078917026519775390625">
        <ele>46</ele>
        <time>2018-08-10T16:40:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78378982655704021453857421875" lon="2.3280798085033893585205078125">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:40:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7836965359747409820556640625" lon="2.32804954983294010162353515625">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:40:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78365035168826580047607421875" lon="2.32803261838853359222412109375">
        <ele>44.799999237060546875</ele>
        <time>2018-08-10T16:40:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7833277322351932525634765625" lon="2.327917031943798065185546875">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:40:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78305288963019847869873046875" lon="2.32781619764864444732666015625">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:40:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>121</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78300528042018413543701171875" lon="2.3278008587658405303955078125">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:40:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78271778114140033721923828125" lon="2.32772617600858211517333984375">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:40:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7825673259794712066650390625" lon="2.32768627814948558807373046875">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:40:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7822377495467662811279296875" lon="2.3275862820446491241455078125">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:40:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.782149069011211395263671875" lon="2.32756004668772220611572265625">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:40:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7819795869290828704833984375" lon="2.32753993012011051177978515625">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:41:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.781933486461639404296875" lon="2.327553592622280120849609375">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:41:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78164674155414104461669921875" lon="2.3276053927838802337646484375">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:41:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78139025531709194183349609375" lon="2.32764369808137416839599609375">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:41:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78118313848972320556640625" lon="2.32767127454280853271484375">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:41:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78064007498323917388916015625" lon="2.3277646489441394805908203125">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:41:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78034905530512332916259765625" lon="2.32781334780156612396240234375">
        <ele>45.200000762939453125</ele>
        <time>2018-08-10T16:41:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>122</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7802539207041263580322265625" lon="2.327832542359828948974609375">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:41:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.78002702258527278900146484375" lon="2.32786246575415134429931640625">
        <ele>45.40000152587890625</ele>
        <time>2018-08-10T16:41:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77973281778395175933837890625" lon="2.327913343906402587890625">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:41:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7796963565051555633544921875" lon="2.32793949544429779052734375">
        <ele>46</ele>
        <time>2018-08-10T16:41:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77965469844639301300048828125" lon="2.327963300049304962158203125">
        <ele>46</ele>
        <time>2018-08-10T16:41:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>123</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77956467680633068084716796875" lon="2.327964641153812408447265625">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:41:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77951748669147491455078125" lon="2.32795265503227710723876953125">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:41:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.779467530548572540283203125" lon="2.32794544659554958343505859375">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:41:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>124</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77930282615125179290771484375" lon="2.3279695026576519012451171875">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:41:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7792071886360645294189453125" lon="2.32797847129404544830322265625">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:41:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77912177704274654388427734375" lon="2.3279554210603237152099609375">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:42:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>125</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7788789533078670501708984375" lon="2.327965311706066131591796875">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:42:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7788315117359161376953125" lon="2.327949218451976776123046875">
        <ele>45.59999847412109375</ele>
        <time>2018-08-10T16:42:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7784544937312602996826171875" lon="2.32777495868504047393798828125">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:42:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77836992032825946807861328125" lon="2.327742017805576324462890625">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:42:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.778156600892543792724609375" lon="2.32763640582561492919921875">
        <ele>45.799999237060546875</ele>
        <time>2018-08-10T16:42:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.777891062200069427490234375" lon="2.32742744497954845428466796875">
        <ele>46</ele>
        <time>2018-08-10T16:42:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77765594981610774993896484375" lon="2.3272774927318096160888671875">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:42:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7771955318748950958251953125" lon="2.3269127123057842254638671875">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:42:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77674735151231288909912109375" lon="2.3265616782009601593017578125">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:42:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7764498777687549591064453125" lon="2.3263482749462127685546875">
        <ele>47.200000762939453125</ele>
        <time>2018-08-10T16:43:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7763311900198459625244140625" lon="2.326289601624011993408203125">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:43:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7761853449046611785888671875" lon="2.32624660246074199676513671875">
        <ele>47.799999237060546875</ele>
        <time>2018-08-10T16:43:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77604494802653789520263671875" lon="2.32625263743102550506591796875">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:43:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.775957189500331878662109375" lon="2.32628784142434597015380859375">
        <ele>47.200000762939453125</ele>
        <time>2018-08-10T16:43:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77569575794041156768798828125" lon="2.3263085447251796722412109375">
        <ele>48</ele>
        <time>2018-08-10T16:43:19.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.775641359388828277587890625" lon="2.32630929909646511077880859375">
        <ele>48.40000152587890625</ele>
        <time>2018-08-10T16:43:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77547900192439556121826171875" lon="2.32630980201065540313720703125">
        <ele>48.40000152587890625</ele>
        <time>2018-08-10T16:43:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77536919899284839630126953125" lon="2.32627107761800289154052734375">
        <ele>48.200000762939453125</ele>
        <time>2018-08-10T16:43:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77526274882256984710693359375" lon="2.32622279785573482513427734375">
        <ele>48.200000762939453125</ele>
        <time>2018-08-10T16:43:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77512075938284397125244140625" lon="2.32610285282135009765625">
        <ele>48.40000152587890625</ele>
        <time>2018-08-10T16:43:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7749753333628177642822265625" lon="2.3259809799492359161376953125">
        <ele>49.200000762939453125</ele>
        <time>2018-08-10T16:43:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77488288097083568572998046875" lon="2.3259003460407257080078125">
        <ele>49.40000152587890625</ele>
        <time>2018-08-10T16:43:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77470099367201328277587890625" lon="2.32573078013956546783447265625">
        <ele>49.40000152587890625</ele>
        <time>2018-08-10T16:43:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7744757719337940216064453125" lon="2.32549156062304973602294921875">
        <ele>49.59999847412109375</ele>
        <time>2018-08-10T16:43:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.774392791092395782470703125" lon="2.32538159005343914031982421875">
        <ele>49.799999237060546875</ele>
        <time>2018-08-10T16:43:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7741156853735446929931640625" lon="2.3249106109142303466796875">
        <ele>49</ele>
        <time>2018-08-10T16:43:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.773944862186908721923828125" lon="2.3246043361723423004150390625">
        <ele>48.799999237060546875</ele>
        <time>2018-08-10T16:43:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>146</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77383346669375896453857421875" lon="2.32441415078938007354736328125">
        <ele>48.59999847412109375</ele>
        <time>2018-08-10T16:44:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77367421053349971771240234375" lon="2.32418985106050968170166015625">
        <ele>48.200000762939453125</ele>
        <time>2018-08-10T16:44:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>146</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77362676896154880523681640625" lon="2.3241429962217807769775390625">
        <ele>48</ele>
        <time>2018-08-10T16:44:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>145</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7735802493989467620849609375" lon="2.324104607105255126953125">
        <ele>47.799999237060546875</ele>
        <time>2018-08-10T16:44:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>147</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77353590913116931915283203125" lon="2.324061356484889984130859375">
        <ele>47.799999237060546875</ele>
        <time>2018-08-10T16:44:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>146</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77349299378693103790283203125" lon="2.324010394513607025146484375">
        <ele>47.59999847412109375</ele>
        <time>2018-08-10T16:44:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77345544286072254180908203125" lon="2.323950044810771942138671875">
        <ele>47.59999847412109375</ele>
        <time>2018-08-10T16:44:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7734209932386875152587890625" lon="2.3238886892795562744140625">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:44:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77337983809411525726318359375" lon="2.32383965514600276947021484375">
        <ele>47.200000762939453125</ele>
        <time>2018-08-10T16:44:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7732691131532192230224609375" lon="2.32380746863782405853271484375">
        <ele>47.200000762939453125</ele>
        <time>2018-08-10T16:44:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77294179983437061309814453125" lon="2.3237242363393306732177734375">
        <ele>46.799999237060546875</ele>
        <time>2018-08-10T16:44:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77247626893222332000732421875" lon="2.3235301114618778228759765625">
        <ele>47</ele>
        <time>2018-08-10T16:44:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7720772065222263336181640625" lon="2.32334948144853115081787109375">
        <ele>47</ele>
        <time>2018-08-10T16:44:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77197452820837497711181640625" lon="2.323305308818817138671875">
        <ele>47.200000762939453125</ele>
        <time>2018-08-10T16:44:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>143</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77148821018636226654052734375" lon="2.32309274375438690185546875">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:44:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>147</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.771127201616764068603515625" lon="2.3228749819099903106689453125">
        <ele>46</ele>
        <time>2018-08-10T16:44:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>144</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77107925713062286376953125" lon="2.32284036464989185333251953125">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:44:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.770732916891574859619140625" lon="2.322605587542057037353515625">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:45:04.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77058573067188262939453125" lon="2.3225041665136814117431640625">
        <ele>48.40000152587890625</ele>
        <time>2018-08-10T16:45:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77053652890026569366455078125" lon="2.322473824024200439453125">
        <ele>48.40000152587890625</ele>
        <time>2018-08-10T16:45:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.77010268159210681915283203125" lon="2.32218288816511631011962890625">
        <ele>47.59999847412109375</ele>
        <time>2018-08-10T16:45:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76996454782783985137939453125" lon="2.322079874575138092041015625">
        <ele>47.200000762939453125</ele>
        <time>2018-08-10T16:45:20.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.769753240048885345458984375" lon="2.32186538167297840118408203125">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:45:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7693296186625957489013671875" lon="2.3214272595942020416259765625">
        <ele>47</ele>
        <time>2018-08-10T16:45:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76915686763823032379150390625" lon="2.32125408947467803955078125">
        <ele>47.200000762939453125</ele>
        <time>2018-08-10T16:45:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76907086931169033050537109375" lon="2.3211667500436305999755859375">
        <ele>47.200000762939453125</ele>
        <time>2018-08-10T16:45:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7690280377864837646484375" lon="2.3211214877665042877197265625">
        <ele>47.200000762939453125</ele>
        <time>2018-08-10T16:45:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76898453570902347564697265625" lon="2.321076057851314544677734375">
        <ele>47.200000762939453125</ele>
        <time>2018-08-10T16:45:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76885076053440570831298828125" lon="2.32093968428671360015869140625">
        <ele>47</ele>
        <time>2018-08-10T16:45:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>140</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76872084103524684906005859375" lon="2.3207992874085903167724609375">
        <ele>46</ele>
        <time>2018-08-10T16:45:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.768517412245273590087890625" lon="2.3205397836863994598388671875">
        <ele>46</ele>
        <time>2018-08-10T16:45:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.768476508557796478271484375" lon="2.3204876482486724853515625">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:45:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7680294178426265716552734375" lon="2.3199218697845935821533203125">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:46:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76782397739589214324951171875" lon="2.31966529972851276397705078125">
        <ele>47</ele>
        <time>2018-08-10T16:46:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.767666481435298919677734375" lon="2.31946363113820552825927734375">
        <ele>47.59999847412109375</ele>
        <time>2018-08-10T16:46:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76732877455651760101318359375" lon="2.31911142356693744659423828125">
        <ele>46.799999237060546875</ele>
        <time>2018-08-10T16:46:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76718276180326938629150390625" lon="2.31902182102203369140625">
        <ele>46</ele>
        <time>2018-08-10T16:46:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.766984529793262481689453125" lon="2.318924255669116973876953125">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:46:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76688562333583831787109375" lon="2.318879663944244384765625">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:46:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>142</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76683725975453853607177734375" lon="2.31885996647179126739501953125">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:46:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>139</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7666011415421962738037109375" lon="2.31875921599566936492919921875">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:46:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76651388593018054962158203125" lon="2.3187105171382427215576171875">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:46:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7664623372256755828857421875" lon="2.31869149021804332733154296875">
        <ele>46.799999237060546875</ele>
        <time>2018-08-10T16:46:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7660784460604190826416015625" lon="2.318529970943927764892578125">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:46:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76603561453521251678466796875" lon="2.31851010583341121673583984375">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:46:50.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>141</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76587166450917720794677734375" lon="2.3184411227703094482421875">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:46:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76577627845108509063720703125" lon="2.31839829124510288238525390625">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:46:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76532365567982196807861328125" lon="2.31820299290120601654052734375">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:47:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7648282013833522796630859375" lon="2.31802395544946193695068359375">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:47:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76467037014663219451904296875" lon="2.31803149916231632232666015625">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:47:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76445914618670940399169921875" lon="2.31805345974862575531005859375">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:47:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76404658891260623931884765625" lon="2.3180795274674892425537109375">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:47:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7639950402081012725830078125" lon="2.31808480806648731231689453125">
        <ele>46.59999847412109375</ele>
        <time>2018-08-10T16:47:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76383787952363491058349609375" lon="2.31809218414127826690673828125">
        <ele>47</ele>
        <time>2018-08-10T16:47:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7633603624999523162841796875" lon="2.31813208200037479400634765625">
        <ele>47.200000762939453125</ele>
        <time>2018-08-10T16:47:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7629479728639125823974609375" lon="2.31815873645246028900146484375">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:47:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7624423764646053314208984375" lon="2.31820081360638141632080078125">
        <ele>46.799999237060546875</ele>
        <time>2018-08-10T16:48:01.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76205974258482456207275390625" lon="2.3182260431349277496337890625">
        <ele>47.200000762939453125</ele>
        <time>2018-08-10T16:48:09.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76191808842122554779052734375" lon="2.31824255548417568206787109375">
        <ele>46.799999237060546875</ele>
        <time>2018-08-10T16:48:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7616796232759952545166015625" lon="2.31826351024210453033447265625">
        <ele>47</ele>
        <time>2018-08-10T16:48:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76131241209805011749267578125" lon="2.3182967863976955413818359375">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:48:25.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76127100549638271331787109375" lon="2.31830106116831302642822265625">
        <ele>46.200000762939453125</ele>
        <time>2018-08-10T16:48:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76119674183428287506103515625" lon="2.31831028126180171966552734375">
        <ele>46.40000152587890625</ele>
        <time>2018-08-10T16:48:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76110051758587360382080078125" lon="2.3183175735175609588623046875">
        <ele>46.799999237060546875</ele>
        <time>2018-08-10T16:48:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7609379924833774566650390625" lon="2.31832821853458881378173828125">
        <ele>48</ele>
        <time>2018-08-10T16:48:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76089298166334629058837890625" lon="2.31832184828817844390869140625">
        <ele>48.59999847412109375</ele>
        <time>2018-08-10T16:48:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.760845959186553955078125" lon="2.3183105327188968658447265625">
        <ele>49</ele>
        <time>2018-08-10T16:48:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76065828837454319000244140625" lon="2.318220175802707672119140625">
        <ele>49.200000762939453125</ele>
        <time>2018-08-10T16:48:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76055896282196044921875" lon="2.318226881325244903564453125">
        <ele>49</ele>
        <time>2018-08-10T16:48:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7604679353535175323486328125" lon="2.31827775947749614715576171875">
        <ele>48.799999237060546875</ele>
        <time>2018-08-10T16:48:45.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76040272414684295654296875" lon="2.31838655658066272735595703125">
        <ele>48.799999237060546875</ele>
        <time>2018-08-10T16:48:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76037900336086750030517578125" lon="2.31845109723508358001708984375">
        <ele>48.799999237060546875</ele>
        <time>2018-08-10T16:48:48.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76027322374284267425537109375" lon="2.3187510855495929718017578125">
        <ele>48.799999237060546875</ele>
        <time>2018-08-10T16:48:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76023751683533191680908203125" lon="2.3187809251248836517333984375">
        <ele>48.799999237060546875</ele>
        <time>2018-08-10T16:48:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76019560731947422027587890625" lon="2.31878167949616909027099609375">
        <ele>48.59999847412109375</ele>
        <time>2018-08-10T16:48:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.76010944135487079620361328125" lon="2.31872384436428546905517578125">
        <ele>48.40000152587890625</ele>
        <time>2018-08-10T16:48:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75967978499829769134521484375" lon="2.318420670926570892333984375">
        <ele>48.40000152587890625</ele>
        <time>2018-08-10T16:49:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7594466842710971832275390625" lon="2.31824708171188831329345703125">
        <ele>47.59999847412109375</ele>
        <time>2018-08-10T16:49:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.759271167218685150146484375" lon="2.31809922493994235992431640625">
        <ele>47</ele>
        <time>2018-08-10T16:49:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75899498350918292999267578125" lon="2.317903675138950347900390625">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:49:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75872910954058170318603515625" lon="2.31760821305215358734130859375">
        <ele>48.40000152587890625</ele>
        <time>2018-08-10T16:49:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75868066214025020599365234375" lon="2.317576445639133453369140625">
        <ele>48.59999847412109375</ele>
        <time>2018-08-10T16:49:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.758602626621723175048828125" lon="2.31762497685849666595458984375">
        <ele>48.59999847412109375</ele>
        <time>2018-08-10T16:49:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7585756368935108184814453125" lon="2.31768080033361911773681640625">
        <ele>48.200000762939453125</ele>
        <time>2018-08-10T16:49:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7585266865789890289306640625" lon="2.3178817145526409149169921875">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:49:34.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75846491195261478424072265625" lon="2.31813468039035797119140625">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:49:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75840246677398681640625" lon="2.3182065971195697784423828125">
        <ele>47.40000152587890625</ele>
        <time>2018-08-10T16:49:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7583657540380954742431640625" lon="2.31821112334728240966796875">
        <ele>47.59999847412109375</ele>
        <time>2018-08-10T16:49:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7582088448107242584228515625" lon="2.3181365244090557098388671875">
        <ele>48.59999847412109375</ele>
        <time>2018-08-10T16:49:46.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>127</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7581733055412769317626953125" lon="2.31811129488050937652587890625">
        <ele>48.799999237060546875</ele>
        <time>2018-08-10T16:49:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.758107088506221771240234375" lon="2.31807726435363292694091796875">
        <ele>48.799999237060546875</ele>
        <time>2018-08-10T16:49:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>126</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75804363749921321868896484375" lon="2.3180356062948703765869140625">
        <ele>49.799999237060546875</ele>
        <time>2018-08-10T16:49:51.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.757940791547298431396484375" lon="2.31798791326582431793212890625">
        <ele>49.40000152587890625</ele>
        <time>2018-08-10T16:49:54.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>128</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75769662670791149139404296875" lon="2.31785514391958713531494140625">
        <ele>50.200000762939453125</ele>
        <time>2018-08-10T16:50:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>129</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75761465169489383697509765625" lon="2.31787526048719882965087890625">
        <ele>50.200000762939453125</ele>
        <time>2018-08-10T16:50:02.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7574970535933971405029296875" lon="2.3179422318935394287109375">
        <ele>50</ele>
        <time>2018-08-10T16:50:05.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75745673663914203643798828125" lon="2.3179631866514682769775390625">
        <ele>50.200000762939453125</ele>
        <time>2018-08-10T16:50:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75729245133697986602783203125" lon="2.31805865652859210968017578125">
        <ele>51</ele>
        <time>2018-08-10T16:50:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75717024318873882293701171875" lon="2.3181365244090557098388671875">
        <ele>51.40000152587890625</ele>
        <time>2018-08-10T16:50:13.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75682331621646881103515625" lon="2.31836526654660701751708984375">
        <ele>51.200000762939453125</ele>
        <time>2018-08-10T16:50:22.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7567809037864208221435546875" lon="2.31836861930787563323974609375">
        <ele>51.200000762939453125</ele>
        <time>2018-08-10T16:50:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7567383237183094024658203125" lon="2.31835805810987949371337890625">
        <ele>50.799999237060546875</ele>
        <time>2018-08-10T16:50:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75665341503918170928955078125" lon="2.31837465427815914154052734375">
        <ele>50.59999847412109375</ele>
        <time>2018-08-10T16:50:26.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75662215054035186767578125" lon="2.31841539032757282257080078125">
        <ele>50.59999847412109375</ele>
        <time>2018-08-10T16:50:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>130</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75656574033200740814208984375" lon="2.3185122013092041015625">
        <ele>50.799999237060546875</ele>
        <time>2018-08-10T16:50:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75648963265120983123779296875" lon="2.31857188045978546142578125">
        <ele>51.200000762939453125</ele>
        <time>2018-08-10T16:50:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75621302984654903411865234375" lon="2.3187299631536006927490234375">
        <ele>51.59999847412109375</ele>
        <time>2018-08-10T16:50:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75617497600615024566650390625" lon="2.3187222518026828765869140625">
        <ele>51.799999237060546875</ele>
        <time>2018-08-10T16:50:39.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75613859854638576507568359375" lon="2.318697273731231689453125">
        <ele>52</ele>
        <time>2018-08-10T16:50:40.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.755994178354740142822265625" lon="2.31855570338666439056396484375">
        <ele>52.200000762939453125</ele>
        <time>2018-08-10T16:50:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75570885837078094482421875" lon="2.31830910779535770416259765625">
        <ele>52.59999847412109375</ele>
        <time>2018-08-10T16:50:52.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.755615986883640289306640625" lon="2.31822210364043712615966796875">
        <ele>53</ele>
        <time>2018-08-10T16:50:55.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75556477345526218414306640625" lon="2.3182022385299205780029296875">
        <ele>53.59999847412109375</ele>
        <time>2018-08-10T16:50:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7555132247507572174072265625" lon="2.31820383109152317047119140625">
        <ele>54</ele>
        <time>2018-08-10T16:50:59.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75548849813640117645263671875" lon="2.3182082735002040863037109375">
        <ele>54.40000152587890625</ele>
        <time>2018-08-10T16:51:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75543602742254734039306640625" lon="2.3182411305606365203857421875">
        <ele>54.59999847412109375</ele>
        <time>2018-08-10T16:51:03.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75535438768565654754638671875" lon="2.31831329874694347381591796875">
        <ele>55.799999237060546875</ele>
        <time>2018-08-10T16:51:07.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75533594749867916107177734375" lon="2.3183309845626354217529296875">
        <ele>56</ele>
        <time>2018-08-10T16:51:08.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.755275346338748931884765625" lon="2.31838437728583812713623046875">
        <ele>56.799999237060546875</ele>
        <time>2018-08-10T16:51:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>138</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75519613735377788543701171875" lon="2.3184399493038654327392578125">
        <ele>57.40000152587890625</ele>
        <time>2018-08-10T16:51:14.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75514718703925609588623046875" lon="2.3184627480804920196533203125">
        <ele>58</ele>
        <time>2018-08-10T16:51:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75512170605361461639404296875" lon="2.3184627480804920196533203125">
        <ele>58.200000762939453125</ele>
        <time>2018-08-10T16:51:17.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75510477460920810699462890625" lon="2.318439446389675140380859375">
        <ele>58.40000152587890625</ele>
        <time>2018-08-10T16:51:18.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75507493503391742706298828125" lon="2.31829016469419002532958984375">
        <ele>58.799999237060546875</ele>
        <time>2018-08-10T16:51:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75505230389535427093505859375" lon="2.31816150248050689697265625">
        <ele>58.799999237060546875</ele>
        <time>2018-08-10T16:51:23.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75504090450704097747802734375" lon="2.31809444725513458251953125">
        <ele>58.799999237060546875</ele>
        <time>2018-08-10T16:51:24.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75498197972774505615234375" lon="2.31781122274696826934814453125">
        <ele>57</ele>
        <time>2018-08-10T16:51:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7549647130072116851806640625" lon="2.3177362047135829925537109375">
        <ele>56.40000152587890625</ele>
        <time>2018-08-10T16:51:29.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75492691062390804290771484375" lon="2.317571081221103668212890625">
        <ele>55.59999847412109375</ele>
        <time>2018-08-10T16:51:31.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75483772717416286468505859375" lon="2.31723077595233917236328125">
        <ele>54.59999847412109375</ele>
        <time>2018-08-10T16:51:35.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75482213683426380157470703125" lon="2.317145951092243194580078125">
        <ele>54.40000152587890625</ele>
        <time>2018-08-10T16:51:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7548074685037136077880859375" lon="2.31705584563314914703369140625">
        <ele>54.200000762939453125</ele>
        <time>2018-08-10T16:51:37.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.754787854850292205810546875" lon="2.3169684223830699920654296875">
        <ele>54</ele>
        <time>2018-08-10T16:51:38.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7547179497778415679931640625" lon="2.31673448346555233001708984375">
        <ele>53.799999237060546875</ele>
        <time>2018-08-10T16:51:41.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.754685260355472564697265625" lon="2.316668517887592315673828125">
        <ele>53.59999847412109375</ele>
        <time>2018-08-10T16:51:42.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.754650391638278961181640625" lon="2.3166066594421863555908203125">
        <ele>53.59999847412109375</ele>
        <time>2018-08-10T16:51:43.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7546260841190814971923828125" lon="2.31654547154903411865234375">
        <ele>53.59999847412109375</ele>
        <time>2018-08-10T16:51:44.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75458643771708011627197265625" lon="2.3163625784218311309814453125">
        <ele>53.59999847412109375</ele>
        <time>2018-08-10T16:51:47.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>27.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7545630522072315216064453125" lon="2.3162408731877803802490234375">
        <ele>53.40000152587890625</ele>
        <time>2018-08-10T16:51:49.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7545201368629932403564453125" lon="2.31603392399847507476806640625">
        <ele>53.200000762939453125</ele>
        <time>2018-08-10T16:51:53.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75449473969638347625732421875" lon="2.31593468226492404937744140625">
        <ele>53</ele>
        <time>2018-08-10T16:51:56.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75447772443294525146484375" lon="2.31592328287661075592041015625">
        <ele>53</ele>
        <time>2018-08-10T16:51:57.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75445802696049213409423828125" lon="2.31592613272368907928466796875">
        <ele>53</ele>
        <time>2018-08-10T16:51:58.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75442466698586940765380859375" lon="2.31596720404922962188720703125">
        <ele>53.40000152587890625</ele>
        <time>2018-08-10T16:52:00.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75435761176049709320068359375" lon="2.31610718183219432830810546875">
        <ele>54.799999237060546875</ele>
        <time>2018-08-10T16:52:06.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>131</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75433950684964656829833984375" lon="2.31621396727859973907470703125">
        <ele>55.40000152587890625</ele>
        <time>2018-08-10T16:52:10.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75432215631008148193359375" lon="2.31625218875706195831298828125">
        <ele>55.59999847412109375</ele>
        <time>2018-08-10T16:52:11.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>137</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75429893843829631805419921875" lon="2.316291332244873046875">
        <ele>56</ele>
        <time>2018-08-10T16:52:12.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7542358227074146270751953125" lon="2.3164051584899425506591796875">
        <ele>57</ele>
        <time>2018-08-10T16:52:15.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>133</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7542116828262805938720703125" lon="2.31643558479845523834228515625">
        <ele>57.40000152587890625</ele>
        <time>2018-08-10T16:52:16.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>132</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75407665036618709564208984375" lon="2.31659241020679473876953125">
        <ele>58</ele>
        <time>2018-08-10T16:52:21.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75395310111343860626220703125" lon="2.3167756386101245880126953125">
        <ele>59</ele>
        <time>2018-08-10T16:52:27.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.7539320625364780426025390625" lon="2.31679952703416347503662109375">
        <ele>59.200000762939453125</ele>
        <time>2018-08-10T16:52:28.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75388453714549541473388671875" lon="2.3168339766561985015869140625">
        <ele>59.200000762939453125</ele>
        <time>2018-08-10T16:52:30.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>136</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75385033898055553436279296875" lon="2.316835485398769378662109375">
        <ele>59.200000762939453125</ele>
        <time>2018-08-10T16:52:32.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75383533537387847900390625" lon="2.31684059835970401763916015625">
        <ele>59</ele>
        <time>2018-08-10T16:52:33.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>135</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
      <trkpt lat="48.75381228514015674591064453125" lon="2.316824756562709808349609375">
        <ele>58.799999237060546875</ele>
        <time>2018-08-10T16:52:36.000Z</time>
        <extensions>
          <ns3:TrackPointExtension>
            <ns3:atemp>26.0</ns3:atemp>
            <ns3:hr>134</ns3:hr>
          </ns3:TrackPointExtension>
        </extensions>
      </trkpt>
    </trkseg>
  </trk>
</gpx>`;

export default xml;
