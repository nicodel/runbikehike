import { DateTime } from 'luxon';

import { fakeProfiles } from './usersProfiles';
import { fakeAvatarsData } from './profilesAvatars';
import { fakeSessionCoordinates } from './gpsSession/coordinates';

const fakeTimeline = [
  {
    id: '4010fa35-756c-419e-b69f-08d82d52b666',
    type: 'session',
    name: 'Amet purus gravida quis blandit',
    start_date_time: DateTime.now().toISO() as string,
    duration: 1 * 60 * 60 + 32 * 60 + 27, // 1:32:27
    distance: 20.12 * 1000, // 20.12 km
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Proin fermentum leo vel orci porta non. Sit amet volutpat consequat mauris nunc congue nisi vitae suscipit. Cursus metus aliquam eleifend mi in. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida.',
    average_speed: (20.12 * 1000) / (1 * 60 * 60 + 32 * 60 + 27), // 4:36 /km
    gps_track_coordinates: null,
    activity_name: 'running',
    location: 'Congue, Malesuada',
    nb_comments: 5,
    nb_likes: 3,
    user_id: fakeProfiles.casquette_verte.user.id,
    user_name: fakeProfiles.casquette_verte.name,
    user_avatar: fakeAvatarsData.casquette_verte.data,
  },

  {
    id: 'ebe9ac13-c3aa-49fb-9b45-c1d2ac27f0eb',
    type: 'session',
    name: 'A pellentesque sit amet porttitor eget',
    start_date_time: DateTime.now()
      .plus({ hours: Math.floor(Math.random() * 12), minutes: Math.floor(Math.random() * 59) })
      .toISO() as string,
    duration: 4 * 60 * 60 + 19 * 60 + 42, // 4:19:42
    distance: 16.8 * 1000, // 16.80 km
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Accumsan lacus vel facilisis volutpat est velit egestas dui id. Libero justo laoreet sit amet cursus sit. Velit dignissim sodales ut eu sem integer.',
    average_speed: (16.8 * 1000) / (4 * 60 * 60 + 19 * 60 + 42), // 15:28 /km
    gps_track_coordinates: null,
    activity_name: 'hiking',
    location: 'Egestas, Lacus',
    nb_comments: 0,
    nb_likes: 0,
    user_id: fakeProfiles.casquette_verte.user.id,
    user_name: fakeProfiles.casquette_verte.name,
    user_avatar: fakeAvatarsData.casquette_verte.data,
  },

  {
    id: 'f5d764cf-d1a7-4bc4-baab-9187267b38f1',
    type: 'session',
    name: 'Egestas purus viverra accumsan',
    start_date_time: DateTime.now()
      .minus({ day: 1 })
      .plus({ hours: Math.floor(Math.random() * 12), minutes: Math.floor(Math.random() * 59) })
      .toISO() as string,
    duration: 20 * 60 + 44, // 20:44
    distance: 5040,
    description: null,
    average_speed: 5040 / (20 * 60 + 44), // 4:31 /km
    gps_track_coordinates: fakeSessionCoordinates,
    activity_name: 'running',
    location: 'Tellus, Elementum',
    nb_comments: 1,
    nb_likes: 3,
    user_id: fakeProfiles.forrest_gump.user.id,
    user_name: fakeProfiles.forrest_gump.name,
    user_avatar: fakeAvatarsData.forrest_gump.data,
  },

  {
    id: 'ecf8ff2d-c57d-4885-bf49-6466330c5d95',
    type: 'session',
    name: 'Vivamus at augue',
    start_date_time: DateTime.now()
      .minus({ day: 2 })
      .plus({ hours: Math.floor(Math.random() * 12), minutes: Math.floor(Math.random() * 59) })
      .toISO() as string,
    duration: 46 * 60 + 37, // 46:37
    distance: 2500,
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eget est lorem ipsum dolor. Leo a diam sollicitudin tempor id eu nisl. Id venenatis a condimentum vitae sapien pellentesque habitant morbi. Feugiat pretium nibh ipsum consequat nisl vel pretium. Morbi non arcu risus quis varius quam quisque id diam. Sed arcu non odio euismod lacinia at quis.',
    average_speed: 2500 / (46 * 60 + 37), // 1:52 /100 mètres
    gps_track_coordinates: null,
    activity_name: 'swimming',
    location: 'Quisque, Lobortis',
    nb_comments: 2,
    nb_likes: 1,
    user_id: fakeProfiles.chloe_mccardell.user.id,
    user_name: fakeProfiles.chloe_mccardell.name,
    user_avatar: fakeAvatarsData.chloe_mccardell.data,
  },

  {
    id: '4bfe2d2d-e286-4d88-b43c-b9ee29ca88df',
    type: 'session',
    name: 'Adipiscing elit ut aliquam',
    start_date_time: DateTime.now()
      .minus({ day: 3 })
      .plus({ hours: Math.floor(Math.random() * 12), minutes: Math.floor(Math.random() * 59) })
      .toISO() as string,
    duration: 3 * 60 * 60 + 37 * 60 + 49, // 3:37:49
    distance: 17.12 * 1000,
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Interdum velit laoreet id donec ultrices tincidunt arcu non sodales. Vitae ultricies leo integer malesuada nunc. Mauris ultrices eros in cursus turpis massa tincidunt dui ut. Sit amet dictum sit amet justo donec enim diam. Nunc sed velit dignissim sodales ut eu sem integer. Integer feugiat scelerisque varius morbi enim nunc faucibus a. Ullamcorper malesuada proin libero nunc consequat interdum varius sit amet. Orci ac auctor augue mauris augue neque gravida in. In est ante in nibh mauris cursus. Id nibh tortor id aliquet lectus proin nibh nisl. Suscipit adipiscing bibendum est ultricies integer. Elementum nibh tellus molestie nunc. Aliquet sagittis id consectetur purus. In est ante in nibh mauris cursus. Dui ut ornare lectus sit amet. Elit duis tristique sollicitudin nibh sit amet commodo nulla.',
    average_speed: (17.12 * 1000) / (3 * 60 * 60 + 37 * 60 + 49), // 12:43 /km
    gps_track_coordinates: null,
    activity_name: 'walking',
    location: 'Tincidunt, Proin',
    nb_comments: 2,
    nb_likes: 1,
    user_id: fakeProfiles.forrest_gump.user.id,
    user_name: fakeProfiles.forrest_gump.name,
    user_avatar: fakeAvatarsData.forrest_gump.data,
  },

  {
    id: '26ced29d-c3ec-4466-805d-4baec4d42695',
    type: 'session',
    name: 'In eu mi bibendum',
    start_date_time: DateTime.now()
      .minus({ day: 6 })
      .plus({ hours: Math.floor(Math.random() * 12), minutes: Math.floor(Math.random() * 59) })
      .toISO() as string,
    duration: 30 * 60 + 33, // 30:33
    distance: 2000,
    description: null,
    average_speed: 2000 / (30 * 60 + 33), // 1:21 min/100m
    gps_track_coordinates: null,
    activity_name: 'swimming',
    location: null,
    nb_comments: 0,
    nb_likes: 0,
    user_id: fakeProfiles.chloe_mccardell.user.id,
    user_name: fakeProfiles.chloe_mccardell.name,
    user_avatar: fakeAvatarsData.chloe_mccardell.data,
  },

  {
    id: 'cf3bb74e-d181-4bb5-a069-97dd125a4f8e',
    type: 'session',
    name: 'Elit ullamcorper dignissim',
    start_date_time: DateTime.now()
      .minus({ day: 7 })
      .plus({ hours: Math.floor(Math.random() * 12), minutes: Math.floor(Math.random() * 59) })
      .toISO() as string,
    duration: 2 * 45 * 60,
    distance: null,
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque ornare aenean euismod elementum. Fusce id velit ut tortor pretium viverra suspendisse potenti nullam.',
    average_speed: null,
    gps_track_coordinates: null,
    activity_name: 'football',
    location: 'Neque, Convallis',
    nb_comments: 0,
    nb_likes: 0,
    user_id: fakeProfiles.casquette_verte.user.id,
    user_name: fakeProfiles.casquette_verte.name,
    user_avatar: fakeAvatarsData.casquette_verte.data,
  },
];

export { fakeTimeline };
