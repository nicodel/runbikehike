import * as chai from 'chai';
chai.should();

import request from 'supertest';
import app from './app';

describe('App', () => {
  it('should return "200 OK" to GET /', async () => {
    const result = await request(app).get('/');
    result.statusCode.should.equal(200);
  });

  it('should return "200 OK" to GET /users', async () => {
    const result = await request(app).get('/users');
    result.statusCode.should.equal(200);
  });
});
