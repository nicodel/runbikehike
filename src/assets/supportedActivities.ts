export const supportedActivities = {
  version: '1',
  activities: [
    {
      name: 'running',
      icon: 'running.png',
      use_distance: true,
    },
    {
      name: 'regular_biking',
      icon: 'regular_biking.png',
      use_distance: true,
    },
    {
      name: 'swimming',
      icon: 'swimming.png',
      use_distance: true,
    },
    {
      name: 'unknown',
      icon: 'unknown.png',
      use_distance: true,
    },
  ],
};
