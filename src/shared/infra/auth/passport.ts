/* eslint-disable @typescript-eslint/no-explicit-any */
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';

import { User } from '@/app/domains/auth/user';
import { usersRepo } from '@/app/repositories/auth';

import logger from '@/shared/utils/logger';

passport.use(
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
    },
    async (email: string, password: string, done) => {
      try {
        let err = null;
        const user = await usersRepo.getUserByEmail(email);
        if (user.isDisabled) {
          err = { message: 'User disabled', statusCode: 403 };
        }
        if (!user || !(await user.password.comparePassword(password))) {
          err = { message: 'Incorrect email or password', statusCode: 401 };
        }
        if (err) {
          logger.error(
            `[passport] LocalStrategy - Authentication Error: ${JSON.stringify(
              err
            )} for user ${email}`
          );
          return done({ ...err, name: 'AuthenticationError' }, false);
        } else {
          logger.debug(
            `[passport] LocalStrategy - Authentication successful for user ${JSON.stringify(
              user.props
            )}`
          );
          return done(null, user);
        }
      } catch (err) {
        logger.error('[passport] LocalStrategy - %s', err);
        if (err === 'User not found.') {
          return done(
            {
              name: 'AuthenticationError',
              message: 'Incorrect email or password',
              statusCode: 401,
            },
            false
          );
        }
        logger.error('[passport] LocalStrategy - Error: %s', err);
        return done({ name: 'Error', message: 'Unexpected error', statusCode: 500 }, false);
      }
    }
  )
);

passport.serializeUser((user: User, done) => {
  logger.debug('[passport] serializeUser - userId: %s', user.id.toString());
  done(null, user.id.toString());
});

passport.deserializeUser(async (id: string, done) => {
  logger.debug('[passport] deserializeUser - User for id %s', id);
  done(null, id);
});

export { passport };
