import express from 'express';

import { authRouter } from '@/app/infra/routes/auth';
import { usersRouter } from '@/app/infra/routes/users';
import { sessionsRouter } from '@/app/infra/routes/sessions';
import { timelineRouter } from '@/app/infra/routes/timeline';
import { imagesRouter } from '@/app/infra/routes/images';
import { adminRouter } from '@/app/infra/routes/admin';
import { socialRouter } from '@/app/infra/routes/social';

const v1Router = express.Router();

v1Router.use('/auth', authRouter);
v1Router.use('/users', usersRouter);
v1Router.use('/sessions', sessionsRouter);
v1Router.use('/timeline', timelineRouter);
v1Router.use('/images', imagesRouter);
v1Router.use('/admin', adminRouter);
v1Router.use('/social', socialRouter);

export { v1Router };
