import { DataSource } from 'typeorm';
import { CONFIG } from '@/config';
import logger from '@/shared/utils/logger';
import { Users } from './models/users';
import { UsersProfiles } from './models/usersProfiles';
import { ProfileAvatars } from './models/profileAvatars';
import { Activities } from './models/activities';
import { Sessions } from './models/sessions';
import { Images } from './models/images';
import { Comments } from './models/comments';

let dataSource: DataSource;
const entities = [Users, UsersProfiles, ProfileAvatars, Activities, Sessions, Images, Comments];

switch (CONFIG.DB_TYPE) {
  case 'sqlite':
    logger.debug('[database/typeorm] Data Source for %s ', CONFIG.DB_TYPE);
    dataSource = new DataSource({
      type: CONFIG.DB_TYPE,
      database: CONFIG.DB_SQLITE_PATH,
      entities,
      synchronize: !CONFIG.IS_PRODUCTION,
      logging: !CONFIG.IS_PRODUCTION,
    });
    break;

  case 'mariadb':
    logger.debug('[database/typeorm] Data Source for %s ', CONFIG.DB_TYPE);
    dataSource = new DataSource({
      type: CONFIG.DB_TYPE,
      host: CONFIG.DB_HOST,
      port: CONFIG.DB_PORT,
      username: CONFIG.DB_USERNAME,
      password: CONFIG.DB_PASSWORD,
      database: CONFIG.DB_DATABASE_NAME,
      entities,
      synchronize: !CONFIG.IS_PRODUCTION,
      logging: !CONFIG.IS_PRODUCTION,
    });
    break;

  case 'postgres':
    logger.debug('[database/typeorm] Data Source for %s ', CONFIG.DB_TYPE);
    dataSource = new DataSource({
      type: CONFIG.DB_TYPE,
      host: CONFIG.DB_HOST,
      port: CONFIG.DB_PORT,
      username: CONFIG.DB_USERNAME,
      password: CONFIG.DB_PASSWORD,
      database: CONFIG.DB_DATABASE_NAME,
      entities,
      synchronize: !CONFIG.IS_PRODUCTION,
      logging: !CONFIG.IS_PRODUCTION,
    });
    break;

  default:
    logger.error('[database/typeorm] Unknown database type: %s', CONFIG.DB_TYPE);
    logger.error('[database/typeorm] Supported database types are: sqlite, mariadb, postgres');
    process.exit(1);
    break;
}

logger.debug('[database/typeorm] Data Source Options: %s', dataSource.options);
export { dataSource };
