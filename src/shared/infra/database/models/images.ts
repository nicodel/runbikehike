import {
  Column,
  Entity,
  ManyToOne,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Sessions } from './sessions';
import { Users } from './users';

@Entity()
export class Images {
  @PrimaryColumn({ type: 'varchar', unique: true, nullable: false })
  id: string;

  @Column({ type: 'text', nullable: false })
  mimetype: string;

  @Column({ type: 'text', nullable: false })
  data: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => Sessions, (session) => session.images, { onDelete: 'CASCADE' })
  session: Sessions;

  @ManyToOne(() => Users, (user) => user.images)
  user: Users;
}
