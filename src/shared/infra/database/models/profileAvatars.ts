import {
  Column,
  Entity,
  OneToOne,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UsersProfiles } from './usersProfiles';

@Entity()
export class ProfileAvatars {
  @PrimaryColumn({ type: 'varchar', unique: true, nullable: false })
  id: string;

  @Column({ type: 'text', nullable: false })
  mimetype: string;

  @Column({ type: 'text', nullable: false })
  data: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToOne(() => UsersProfiles, (user_profile) => user_profile.avatar)
  user_profile: UsersProfiles;
}
