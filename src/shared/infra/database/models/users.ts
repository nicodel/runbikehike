import {
  Entity,
  PrimaryColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  OneToMany,
} from 'typeorm';
import { UsersProfiles } from './usersProfiles';
import { Sessions } from './sessions';
import { Images } from './images';
import { Comments } from './comments';

@Entity()
export class Users {
  @PrimaryColumn({ type: 'varchar', unique: true, nullable: false })
  id: string;

  @Column({ type: 'text', unique: true, nullable: false })
  email: string;

  @Column({ type: 'text', nullable: false })
  password: string;

  @Column({ type: 'boolean', nullable: true })
  is_deleted: boolean;

  @Column({ type: 'boolean', nullable: false })
  is_disabled: boolean;

  @Column({ type: 'boolean', nullable: false })
  is_admin: boolean;

  // @Column({ type: 'boolean', nullable: false })
  // is_email_verified: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToOne(() => UsersProfiles, (user_profile) => user_profile.user)
  user_profile: UsersProfiles;

  @OneToMany(() => Sessions, (session) => session.user)
  sessions: Sessions[];

  @OneToMany(() => Images, (image) => image.user)
  images: Images[];

  @OneToMany(() => Comments, (comment) => comment.user)
  comments: Comments[];
}
