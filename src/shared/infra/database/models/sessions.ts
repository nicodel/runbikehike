import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Users } from './users';
import { Activities } from './activities';
import { Images } from './images';
import { Comments } from './comments';
import { CONFIG } from '@/config';

@Entity()
export class Sessions {
  @PrimaryColumn({ type: 'varchar', unique: true, nullable: false })
  id: string;

  @Column({ type: 'text', nullable: false })
  name: string;

  @Column({ type: CONFIG.DB_TYPE === 'sqlite' ? 'datetime' : 'timestamp', nullable: false })
  start_date_time: string;

  @Column({ type: 'int', nullable: false })
  duration: number;

  @Column({ type: 'int', nullable: false })
  burned_calories: number;

  @Column({ type: 'int', nullable: true })
  distance: number;

  @Column({ type: 'float', nullable: true })
  average_speed: number;

  @Column({ type: 'int', nullable: true })
  average_heart_rate: number;

  @Column({ type: 'int', nullable: true })
  elevation_gain: number;

  @Column({ type: 'text', nullable: true })
  description: string;

  @Column({ type: 'text', nullable: true })
  location: string; // https://wiki.openstreetmap.org/wiki/Nominatim

  @Column({ type: 'json', nullable: true })
  gps_track: string;

  @Column({ type: 'int', nullable: true })
  nb_comments: number;

  @Column({ type: 'int', nullable: true })
  nb_likes: number;

  @Column({ type: 'boolean', nullable: false })
  is_favorite: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => Users, (user) => user.sessions)
  user: Users;

  @ManyToOne(() => Activities, (activity) => activity.sessions)
  activity: Activities;

  @OneToMany(() => Images, (image) => image.session, { cascade: true })
  images: Images[];

  @OneToMany(() => Comments, (image) => image.session, { cascade: true })
  comments: Comments[];
}
