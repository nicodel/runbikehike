import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Sessions } from './sessions';
import { Users } from './users';

@Entity()
export class Comments {
  @PrimaryColumn({ type: 'varchar', unique: true, nullable: false })
  id: string;

  @Column({ type: 'text', nullable: false })
  text: string;

  @Column({ type: 'text', nullable: false })
  date_time: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  update_at: Date;

  @ManyToOne(() => Sessions, (session) => session.comments, { onDelete: 'CASCADE' })
  session: Sessions;

  @ManyToOne(() => Users, (user) => user.comments)
  user: Users;
}
