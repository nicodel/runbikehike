import {
  Entity,
  PrimaryColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Users } from './users';
import { ProfileAvatars } from './profileAvatars';

@Entity()
export class UsersProfiles {
  @PrimaryColumn({ type: 'varchar', unique: true, nullable: false })
  id: string;

  @Column({ type: 'text', nullable: false })
  name: string;

  @Column({ type: 'text', nullable: false })
  gender: string;

  @Column({ type: 'int', nullable: false })
  birthyear: number;

  @Column({ type: 'int', nullable: false })
  height: number;

  @Column({ type: 'decimal', nullable: false })
  weight: number;

  @Column({ type: 'text', nullable: true })
  biography: string;

  @Column({ type: 'text', nullable: true })
  location: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToOne(() => Users, (user) => user.user_profile, { eager: true })
  @JoinColumn()
  user: Users;

  @OneToOne(() => ProfileAvatars, (avatar) => avatar.user_profile, { eager: true, nullable: true })
  @JoinColumn()
  avatar: ProfileAvatars;
}
