import {
  Column,
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import { Sessions } from './sessions';

@Entity()
export class Activities {
  @PrimaryColumn({ type: 'varchar', unique: true, nullable: false })
  id: string;

  @Column({ type: 'text', nullable: false })
  icon: string;

  @Column({ type: 'text', nullable: false })
  name: string;

  @Column({ type: 'boolean', nullable: false })
  use_distance: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToMany(() => Sessions, (session) => session.activity)
  sessions: Sessions[];
}
