/* eslint-disable @typescript-eslint/no-explicit-any */
import * as r from 'redis';
import { CONFIG } from '@/config';
import logger from '@/shared/utils/logger';
import { RedisErrors } from './redisErrors';

const redis: typeof r = CONFIG.ENV === 'test' ? require('redis-mock') : require('redis');

const redisClient = redis.createClient({
  // url: `redis://${CONFIG.REDIS_PASSWORD}@${CONFIG.REDIS_HOST}:${CONFIG.REDIS_PORT}`,
  socket: {
    host: CONFIG.REDIS_HOST,
    port: CONFIG.REDIS_PORT,
  },
  password: CONFIG.REDIS_PASSWORD,
});

if (CONFIG.ENV !== 'test') {
  redisClient.connect().catch(logger.error);
}

redisClient.on('connect', () => {
  logger.info(
    `[redisClient] Connection status: connected to ${CONFIG.REDIS_HOST}:${CONFIG.REDIS_PORT}`
  );
});

redisClient.on('end', (msg: any) => {
  logger.warn('[redisClient] Connection status: disconnected', msg);
  new RedisErrors.DisconnectedError(msg);
});

redisClient.on('reconnecting', () => {
  logger.info('[redisClient] Connection status: reconnecting');
});

redisClient.on('error', (err: Error) => {
  logger.error('[redisClient] Connection status: error', err);
  new RedisErrors.ConnectionError(err);
});

export { redisClient };
