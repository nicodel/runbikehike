/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-namespace */
import { UseCaseError } from '@/shared/core/UseCaseError';
import { Result } from '@/shared/core/Result';

export namespace RedisErrors {
  export class TimeOutError extends Result<UseCaseError> {
    public constructor() {
      super(false, {
        message: 'redisClient Time Out error.',
      } as UseCaseError);
    }
    public static create(): TimeOutError {
      return new TimeOutError();
    }
  }

  export class ConnectionError extends Result<UseCaseError> {
    public constructor(err: any) {
      super(false, {
        message: 'redisClient Connection error.',
        error: err,
      } as UseCaseError);
    }
    public static create(err: any): ConnectionError {
      throw new ConnectionError(err);
    }
  }

  export class DisconnectedError extends Result<UseCaseError> {
    public constructor(err: any) {
      super(false, {
        message: 'redisClient Disconnected error.',
        error: err,
      } as UseCaseError);
    }
    public static create(err: any): DisconnectedError {
      throw new DisconnectedError(err);
    }
  }

  export class SetError extends Result<UseCaseError> {
    public constructor(err: any) {
      super(false, {
        message: 'redisClient SET error.',
        error: err,
      } as UseCaseError);
    }
    public static create(err: any): SetError {
      throw new SetError(err);
    }
  }

  export class GetError extends Result<UseCaseError> {
    public constructor(err: any) {
      super(false, {
        message: 'redisClient GET error.',
        error: err,
      } as UseCaseError);
    }
    public static create(err: any): GetError {
      throw new GetError(err);
    }
  }

  export class DelError extends Result<UseCaseError> {
    public constructor(err: any) {
      super(false, {
        message: 'redisClient DEL error.',
        error: err,
      } as UseCaseError);
    }
    public static create(err: any): DelError {
      throw new DelError(err);
    }
  }
}
