/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */

import { Result } from './Result';
import { UseCaseError } from './UseCaseError';
import logger from '@/shared/utils/logger';

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace AppError {
  export class UnexpectedError extends Result<UseCaseError> {
    public constructor(err: any) {
      super(false, {
        message: 'An unexpected error occurred.',
        error: err,
      } as UseCaseError);
      logger.error('[Shared/Core/AppError] An unexpected error occurred: %s', err);
    }

    public static create(err: any): UnexpectedError {
      return new UnexpectedError(err);
    }
  }

  export class UnauthorizedError extends Result<UseCaseError> {
    public constructor(user_id: string) {
      super(false, {
        message: 'Unauthorized as unauthenticated',
      } as UseCaseError);
      logger.error(
        `[Shared/Core/AppError] Unauthorized as unauthenticated - An action was requested for an unauthenticated user ${user_id}.`
      );
    }
  }

  export class ForbiddenError extends Result<UseCaseError> {
    public constructor(action: string, user_id: string) {
      super(false, {
        message: 'You do not have permission to perform such action.',
      } as UseCaseError);
      logger.error(
        `[Shared/Core/AppError] Forbidden - User ${user_id} tried to perform ${action} without permission.`
      );
    }
  }
}
