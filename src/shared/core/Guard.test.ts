import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Guard } from './Guard';

describe('Shared / Domain / Guard', () => {
  describe('combine', () => {
    it('should return successed to true if all results have succeeded.', () => {
      const guardResult = Guard.combine([{ succeeded: true }]);
      guardResult.succeeded.should.be.true;
    });

    it('should return successed to false if one result have failed.', () => {
      const guardResult = Guard.combine([{ succeeded: true }, { succeeded: false }]);
      guardResult.succeeded.should.be.false;
    });

    it('should return message if one result have failed.', () => {
      const guardResult = Guard.combine([{ succeeded: false, message: 'xxx' }]);
      guardResult.message.should.equal('xxx');
    });
  });

  describe('greaterThan', () => {
    it('should return successed to true if actual value is greater than min value.', () => {
      const guardResult = Guard.greaterThan(2, 5);
      guardResult.succeeded.should.be.true;
    });

    it('should return successed to false if actual value is not greater than min value.', () => {
      const guardResult = Guard.greaterThan(5, 2);
      guardResult.succeeded.should.be.false;
    });

    it('should return message if actual value is not greater than min value.', () => {
      const guardResult = Guard.greaterThan(5, 2);
      guardResult.message.should.equal('Number given {2} is not greater than {5}.');
    });
  });

  describe('againstAtLeast', () => {
    it('should return successed to true if text length is at least is greater than chars number.', () => {
      const guardResult = Guard.againstAtLeast(2, 'xxx', 'name');
      guardResult.succeeded.should.be.true;
    });

    it('should return successed to false if text length is at least is not greater than chars number.', () => {
      const guardResult = Guard.againstAtLeast(5, 'xxx', 'name');
      guardResult.succeeded.should.be.false;
    });

    it('should return message if text length is not at least chars number.', () => {
      const guardResult = Guard.againstAtLeast(5, 'xxx', 'name');
      guardResult.message.should.equal('Text name is not at least 5 chars.');
    });
  });

  describe('againstAtMost', () => {
    it('should return successed to true if text length is at most is greater than chars number.', () => {
      const guardResult = Guard.againstAtMost(5, 'xxx', 'name');
      guardResult.succeeded.should.be.true;
    });

    it('should return successed to false if text length is at most is not greater than chars number.', () => {
      const guardResult = Guard.againstAtMost(2, 'xxx', 'name');
      guardResult.succeeded.should.be.false;
    });

    it('should return message if text length is not at most chars number.', () => {
      const guardResult = Guard.againstAtMost(2, 'xxx', 'name');
      guardResult.message.should.equal('Text name is greater than 2 chars.');
    });
  });

  describe('againstNullOrUndefined', () => {
    it('should return successed to true if argument is neither null nor undefined.', () => {
      const guardResult = Guard.againstNullOrUndefined(5, 'xxx');
      guardResult.succeeded.should.be.true;
    });

    it('should return successed to false if argument is null or undefined.', () => {
      const guardResult = Guard.againstNullOrUndefined(null, 'xxx');
      guardResult.succeeded.should.be.false;
    });

    it('should return message if argument is null or undefined.', () => {
      const guardResult = Guard.againstNullOrUndefined(undefined, 'xxx');
      guardResult.message.should.equal('xxx is null or undefined.');
    });
  });

  describe('againstNullOrUndefinedBulk', () => {
    it('should return successed to true if arguments are neither null nor undefined.', () => {
      const guardResult = Guard.againstNullOrUndefinedBulk([{ argument: 5, argumentName: 'xxx' }]);
      guardResult.succeeded.should.be.true;
    });

    it('should return successed to false if arguments are null or undefined.', () => {
      const guardResult = Guard.againstNullOrUndefinedBulk([
        { argument: null, argumentName: 'xxx' },
      ]);
      guardResult.succeeded.should.be.false;
    });

    it('should return message if arguments are null or undefined.', () => {
      const guardResult = Guard.againstNullOrUndefinedBulk([
        { argument: undefined, argumentName: 'xxx' },
      ]);
      guardResult.message.should.equal('xxx is null or undefined.');
    });
  });

  describe('isOneOf', () => {
    it('should return successed to true if argument is one of the provided values.', () => {
      const guardResult = Guard.isOneOf(1, [1, 2], 'xxx');
      guardResult.succeeded.should.be.true;
    });

    it('should return successed to false if argument is not one of the provided values.', () => {
      const guardResult = Guard.isOneOf(3, [1, 2], 'xxx');
      guardResult.succeeded.should.be.false;
    });

    it('should return message if argument is not one of the provided values.', () => {
      const guardResult = Guard.isOneOf(3, [1, 2], 'xxx');
      guardResult.message.should.equal('xxx isn\'t oneOf the correct types in [1,2]. Got "3".');
    });
  });

  describe('inRange', () => {
    it('should return successed to true if argument is in range of the provided values.', () => {
      const guardResult = Guard.inRange(2, 1, 5, 'xxx');
      guardResult.succeeded.should.be.true;
    });

    it('should return successed to false if argument is not in range of the provided values.', () => {
      const guardResult = Guard.inRange(10, 1, 5, 'xxx');
      guardResult.succeeded.should.be.false;
    });

    it('should return message if argument is not in range of the provided values.', () => {
      const guardResult = Guard.inRange(10, 1, 5, 'xxx');
      guardResult.message.should.equal('xxx is not within range 1 to 5.');
    });
  });

  describe('allInRange', () => {
    it('should return successed to true if arguments are all in range of the provided values.', () => {
      const guardResult = Guard.allInRange([2, 3], 1, 5, 'xxx');
      guardResult.succeeded.should.be.true;
    });

    it('should return successed to false if arguments are not all in range of the provided values.', () => {
      const guardResult = Guard.allInRange([2, 30], 1, 5, 'xxx');
      guardResult.succeeded.should.be.false;
    });

    it('should return message if arguments are not all in range of the provided values.', () => {
      const guardResult = Guard.allInRange([2, 30], 1, 5, 'xxx');
      guardResult.message.should.equal('xxx is not within the range.');
    });
  });
});
