# Records

- [GET /api/v1/records/all](#get-apiv1recordsall)
- [GET /api/v1/records/record/<record_id as string>](#get-apiv1recordsrecordrecord_id-as-string)
- [PUT /api/v1/records/record/<record_id as string>](#put-apiv1recordsrecordrecord_id-as-string)
- [DELETE /api/v1/records/record/<record_id as string>](#delete-apiv1recordsrecordrecord_id-as-string)


## GET /api/v1/records/all

Get records of the authenticated user.

## GET /api/v1/records/record/<record_id as string>

Get a specific record of the authenticated user.

## PUT /api/v1/records/record/<record_id as string>

Update a specific record of the authenticated user.

## DELETE /api/v1/records/record/<record_id as string>

Delete a specific record of the authenticated user.
