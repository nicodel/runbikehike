# Users

- [Users](#users)
  - [GET /api/v1/users/profile](#get-apiv1usersprofile)
  - [PUT /api/v1/users/profile](#put-apiv1usersprofile)
  - [GET /api/v1/users/name-avatar/`<user_id as string>`](#get-apiv1usersname-avataruser_id-as-string)

## GET /api/v1/users/profile

TODO: to be completed

Get authenticated user profile details.

```
GET /api/v1/users/profile HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  user_id: 'a9202021-d1fe-470c-b00f-ceb85084cbea',
  email: 'forrest@gump.rbh',
  is_admin: false,
  name: 'Forrest Gump',
  gender: 'male',
  birthyear: 1944,
  height: 183,
  weight: 80,
  avatar: {
    data: '/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAcFBQYFBAcGBQYIBwcIChELCgkJChUP ...',
    type: 'image/jpeg',
  },
  location: 'Greenbow, Alabama',
  biography: 'Forrest Alexander Gump is a fictional character and the protagonist of the 1986 novel by Winston Groom, Robert Zemeckis 1994 film of the same name, and Gump and Co., the written sequel to Groom novel.',
}
```

- Error:

```
HTTP/1.1 401 UNAUTHORIZED
Content-Type: application/json

{
  "message": "An unexpected error occurred."
}
```

**REQUEST JSON OBJECT:**

- **user_id** (string) - user id

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
  - `Could not find a user profile by user id <id>.` TODO: to fixed
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## PUT /api/v1/users/profile

TODO: to be completed

Update authenticated user profile details.

```
PUT /api/v1/users/profile HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  "OK"
}
```

- Error:

```
HTTP/1.1 401 UNAUTHORIZED
Content-Type: application/json

{
  "message": "An unexpected error occurred."
}
```

**REQUEST PARAMETERS**

**REQUEST HEADERS:**

TODO: to be verified, and dupllicated to others

- [Authorization](https://www.rfc-editor.org/rfc/rfc7235#section-4.2) – OAuth 2.0 Bearer Token

**REQUEST JSON OBJECT:**

TODO: to be completed

- **name** (string) - user's name
- **email** (string) - user's email
- **gender** (string) - user's gender
- **birthyear** (string) - user's birthyear
- **height** (string) - user's height
- **weigth**(string) - user's weight
- **location** (string) - user's location
- **biography** (string) - user's biography
- **avatar** (object):
  - **type** (string) - avatar image file type
  - **data** (blob) - avatar image blob

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## GET /api/v1/users/name-avatar/`<user_id as string>`

Get name and avatar from a user id.

**Example request:**

```
GET /api/v1/users/name-avatar2c9a382b-dce2-49d1-a63a-fc4e7b163730 HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  "id": "a9202021-d1fe-470c-b00f-ceb85084cbea",
  "name": "Forrest Gump",
  "avatar": {
    "mimetype": "image/jpeg"
    "data": "..."
  };
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**REQUEST JSON OBJECT:**

- **user_id** (string) - user id

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `User not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`
