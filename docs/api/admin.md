# Administration

- [Administration](#administration)
  - [GET /api/v1/admin/all_users](#get-apiv1adminall_users)
  - [PUT /api/v1/admin/disable_user/`<user_id>`](#put-apiv1admindisable_useruser_id)
  - [PUT /api/v1/admin/enable_user/`<user_id>`](#put-apiv1adminenable_useruser_id)

## GET /api/v1/admin/all_users

An Admin can retrieve the list of all users.

**Example request:**

```
GET /api/v1/admin/all_users HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success:

```
HTTP/1.1 200 SUCCESS
Content-Type: application/json

{
  "all_users": [
    {
      "id": "4ee99abe-41b0-49fd-91a3-129a334295ce",
      "email": "casquette@verte.rbh",
      "is_admin": "true",
      "is_disabled": "false",
      "name": "Alex Boucheix",
      "avatar": {
        "type": "image/jpeg",
        "data": "..."
      }
    }
  ]
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## PUT /api/v1/admin/disable_user/`<user_id>`

An Admin can disable a user.

**Example request:**

```
GET /api/v1/admin/disable_user/ed86a0f2-ddb2-4f61-b59d-b39159d8ae9e HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success:

```
HTTP/1.1 200 SUCCESS
Content-Type: application/json

{
  "OK"
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**REQUEST JSON OBJECT:**

- **user_id** (string) - user id

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `User not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## PUT /api/v1/admin/enable_user/`<user_id>`

An Admin can enable a user.

**Example request:**

```
GET /api/v1/admin/enable_user/ed86a0f2-ddb2-4f61-b59d-b39159d8ae9e HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success:

```
HTTP/1.1 200 SUCCESS
Content-Type: application/json

{
  "OK"
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**REQUEST JSON OBJECT:**

- **user_id** (string) - user id

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `User not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`
