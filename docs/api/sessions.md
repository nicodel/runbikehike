# Sessions

- [Sessions](#sessions)
  - [GET /api/v1/sessions/session/`<session_id as string>`](#get-apiv1sessionssessionsession_id-as-string)
  - [GET /api/v1/sessions/session/`<session_id as string>`/charts\_data](#get-apiv1sessionssessionsession_id-as-stringcharts_data)
  - [GET /api/v1/sessions/session/`<session_id as string>`/map\_data](#get-apiv1sessionssessionsession_id-as-stringmap_data)
  - [GET /api/v1/sessions/session/`<session_id as string>`/images](#get-apiv1sessionssessionsession_id-as-stringimages)
  - [POST /api/v1/sessions/session/comment](#post-apiv1sessionssessioncomment)
  - [GET /api/v1/sessions/session/`<session_id as string>`/comments](#get-apiv1sessionssessionsession_id-as-stringcomments)
  - [POST /api/v1/sessions/session](#post-apiv1sessionssession)
  - [POST /api/v1/sessions/gps\_file](#post-apiv1sessionsgps_file)
  - [PUT /api/v1/sessions/session/`<session_id as string>`](#put-apiv1sessionssessionsession_id-as-string)
  - [DELETE /api/v1/sessions/session/`<session_id as string>`](#delete-apiv1sessionssessionsession_id-as-string)
  - [DELETE /api/v1/sessions/session/comment/`<comment_id as string>`/](#delete-apiv1sessionssessioncommentcomment_id-as-string)
  - [GET /api/v1/sessions/activities](#get-apiv1sessionsactivities)

## GET /api/v1/sessions/session/`<session_id as string>`

Get session's metadata for an authentified user

**Example request:**

```
GET /api/v1/sessions/session/2c9a382b-dce2-49d1-a63a-fc4e7b163730 HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  "session": {
    "activity_name": "running",
    "average_heart_rate": null,
    "average_speed": 0.658116418161907,
    "burned_calories": 5318208,
    "description": "Heartbroken, Forrest, \"for no particular reason\", starts running and embarks on a cross-country marathon, becoming famous for another feat. Forrest starts to garner many followers, some of whom are struggling businessmen, whom he unwittingly gives inspiration. After a total of about three years and two-and-a half months running, Forrest decides to end the run, and returns to Greenbow, much to the surprise of his followers.",
    "distance": 25000000,
    "duration": 37987200,
    "elevation_gain": null,
    "gps_charts": true,
    "gps_track": "[
      [
        {
          "date_time":"2023-07-30T12:04:12.000Z",
          "latitude":"7.78693682514131069183349609375",
          "longitude":"-80.2727569453418254852294921875",
          "altitude":38,
          "heart_rate":92
          },
        {
          "date_time":"2023-07-30T12:04:13.000Z",
          "latitude":"7.78692341409623622894287109375",
          "longitude":"-80.27275417931377887725830078125",
          "altitude":"38.200000762939453125",
          "speed":1.5220574644292633,
          "heart_rate":93,
          "distance":1.5220574644292633
          }, ...
      ]
    ]",
    "id": "e09ade35-f953-4f98-aa1d-035be98eec23",
    "images": false,
    "is_favorite": false,
    "location": "Greenbow, Alabama, USA",
    "name": "Five times across the US",
    "nb_likes": 0,
    "nb_comments": 0,
    "start_date_time": "1976-07-06T10:00:00.000Z",
    "user_id": "a9202021-d1fe-470c-b00f-ceb85084cbea"
  }
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**REQUEST JSON OBJECT:**

- **session_id** (string) - session id

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `Session not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## GET /api/v1/sessions/session/`<session_id as string>`/charts_data

Get data a session for an authentified user, for displaying charts.

**Example request:**

```
GET /api/v1/sessions/session/2c9a382b-dce2-49d1-a63a-fc4e7b163730/charts_data HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  "charts": {
    "date_times": ["2023-07-30T12:04:12.000Z", "2023-07-30T12:04:13.000Z", ...];
    "altitudes": [38, 38.200000762939453125", ...];
    "speeds": [null, "1.5220574644292633", ...];
    "heart_rates": [92, 93, ...];
  }
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**REQUEST JSON OBJECT:**

- **session_id** (string) - session id

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `Session not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## GET /api/v1/sessions/session/`<session_id as string>`/map_data

Get data a session for an authentified user, for displaying map.

**Example request:**

```
GET /api/v1/sessions/session/2c9a382b-dce2-49d1-a63a-fc4e7b163730/map_data HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  "map_data": [
      {
        "date_time":"2023-07-30T12:04:12.000Z",
        "latitude":"7.78693682514131069183349609375",
        "longitude":"-80.2727569453418254852294921875",
        },
      {
        "date_time":"2023-07-30T12:04:13.000Z",
        "latitude":"7.78692341409623622894287109375",
        "longitude":"-80.27275417931377887725830078125",
        }, ...
    ]
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**REQUEST JSON OBJECT:**

- **session_id** (string) - session id

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `Session not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## GET /api/v1/sessions/session/`<session_id as string>`/images

Get teh images for a session for an authentified user.

**Example request:**

```
GET /api/v1/sessions/session/2c9a382b-dce2-49d1-a63a-fc4e7b163730/images HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  "session_images": [
        {
            "data": "/9j/4SLARXhpZgAATU0AKgAAAAg...",
            "id": "5c512f83-72dd-430c-8a7e-98af597e78a8",
            "mimetype": "image/jpeg"
        },
        ...
    ]
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**REQUEST JSON OBJECT:**

- **session_id** (string) - session id

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `Session not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## POST /api/v1/sessions/session/comment

Add a new comment to a session.

**Example request:**

```
GET POST /api/v1/sessions/session/comment HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  "id": "dd560edd-f216-4578-8654-8949bf38507d"
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**REQUEST JSON OBJECT:**

- **text** (string) - Comment
- **date_time** (string) - Date and time, in ISO format
- **session_id** (string) - session id
- **user_id** (string) - user id from comment's author

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `Session not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## GET /api/v1/sessions/session/`<session_id as string>`/comments

## POST /api/v1/sessions/session

Add a session for an authenticated user.

**Example request:**

```
POST /api/v1/sessions/session HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  "id": "d9b3a440-a3d7-44b4-9a1e-190f394b08e6"
}
```

- Error:

```
HTTP/1.1 401 UNAUTHORIZED
Content-Type: application/json

{
  "message": "Unauthorized"
}
```

**REQUEST JSON OBJECT:**

- **name** (string) – session name
- **start_date_time** (string) – start date and time, in ISO format
- **duration** (string) – duration in seconds
- **burned_calories** (string) – session burned calroies in kCal (not mandatory)
- **distance** (string) – session total distance in meters (not mandatory)
- **average_speed** (string) – average speed in meters/second (not mandatory)
- **description** (string) – session description (not mandatory)
- **gps_track** (json[][]) – session gps track (not mandatory)
- **activity_name** (string) – session activity name

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [400 Bad Request](https://www.rfc-editor.org/rfc/rfc9110.html#name-400-bad-request)
  - `Wrong value "<value>" for <label>.`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

---

## POST /api/v1/sessions/gps_file

Import a GPS file for an authenticated user.

**Example request:**

```
POST /api/v1/sessions/gps_file HTTP/1.1
Content-Type: multipart/form-data
```

**Example response:**

```
{
  name: 'Five times across the US',
  start_date_time: '1976-07-06T10:00:00.000Z',
  duration: 37987200,
  burned_calories: 5318208,
  distance: 25000000,
  average_speed: 0.658116418161907,
  description: 'Heartbroken, Forrest, "for no particular reason", starts running and embarks on a cross-country marathon, becoming famous for another feat. Forrest starts to garner many followers, some of whom are struggling businessmen, whom he unwittingly gives inspiration. After a total of about three years and two-and-a half months running, Forrest decides to end the run, and returns to Greenbow, much to the surprise of his followers.',
  gps_track: "[
    [
      {
        "date_time":"2023-07-30T12:04:12.000Z",
        "latitude":"7.78693682514131069183349609375",
        "longitude":"-80.2727569453418254852294921875",
        "altitude":38,
        "heart_rate":92
      },
      {
        "date_time":"2023-07-30T12:04:13.000Z",
        "latitude":"7.78692341409623622894287109375",
        "longitude":"-80.27275417931377887725830078125",
        "altitude":"38.200000762939453125",
        "speed":1.5220574644292633,
        "heart_rate":93,
        "distance":1.5220574644292633
      }, ...
    ]
  ]",
  user_id: 'a9202021-d1fe-470c-b00f-ceb85084cbea',
  activity_name: 'running',
}
```

**FORM PARAMETERS**

- **extension** (string) – gps file extension (allowed extensions: .gpx)
- **data** (string) – parsed gps file in json format

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [400 Bad Request](https://www.rfc-editor.org/rfc/rfc9110.html#name-400-bad-request)
  - `Format "<extension>" not supported.`
  - `Error parsing XML: <error details>`
  - `Unknonw error, while trying to import a GPS file: <error details>`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## PUT /api/v1/sessions/session/`<session_id as string>`

Update a specific session of the authenticated user.

**Example request:**

```
POST /api/v1/auth/signin HTTP/1.1
Content-Type: application/json

{
  "session": {
    "activity_name": "running",
    "average_heart_rate": null,
    "average_speed": 0.658116418161907,
    "burned_calories": 5318208,
    "description": "Heartbroken, Forrest, \"for no particular reason\", starts running and embarks on a cross-country marathon, becoming famous for another feat. Forrest starts to garner many followers, some of whom are struggling businessmen, whom he unwittingly gives inspiration. After a total of about three years and two-and-a half months running, Forrest decides to end the run, and returns to Greenbow, much to the surprise of his followers.",
    "distance": 25000000,
    "duration": 37987200,
    "elevation_gain": null,
    "gps_track": "[
      [
        {
          "date_time":"2023-07-30T12:04:12.000Z",
          "latitude":"7.78693682514131069183349609375",
          "longitude":"-80.2727569453418254852294921875",
          "altitude":38,
          "heart_rate":92
        },
        {
          "date_time":"2023-07-30T12:04:13.000Z",
          "latitude":"7.78692341409623622894287109375",
          "longitude":"-80.27275417931377887725830078125",
          "altitude":"38.200000762939453125",
          "speed":1.5220574644292633,
          "heart_rate":93,
          "distance":1.5220574644292633
        }, ...
      ]
    ]",
    "id": "e09ade35-f953-4f98-aa1d-035be98eec23",
    "images_ids": [],
    "is_favorite": false,
    "location": "Greenbow, Alabama, USA",
    "name": "Five times across the US",
    "nb_likes": 10000000000,
    "nb_comments": 0,
    "start_date_time": "1976-07-06T10:00:00.000Z",
    "user_id": "a9202021-d1fe-470c-b00f-ceb85084cbea"
  }
}

```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  "id": "b56bcc8b-ad07-443a-b378-30346c548fb5"
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission."
}
```

**REQUEST JSON OBJECT:**

- **activity_name** (string) – session activity name
- **average_speed** (number) – average speed in meters/second (not mandatory)
- **average_heart_rate** (number) - average heart rate in bpm (not mandatory)
- **burned_calories** (number) – session burned calroies in kCal (not mandatory)
- **description** (string) – session description (not mandatory)
- **distance** (number) – session total distance in meters (not mandatory)
- **duration** (number) – duration in seconds
- **elevation_gain** (number) - total elevation gain in meters (not mandatory)
- **id** (string) - session unique identifier
- **images_ids** (string[]) - list of the sesion's images unique identifiers (not mandatory)
- **is_favorite** (boolean) - is the sesiosn in user favorites list
- **gps_track** (json[][]) – session gps track (not mandatory)
- **location** (string) - sesison location (not mandatory)
- **name** (string) – session name
- **nb_comments** (number) - number of comments
- **nb_likes** (number) - number of likes
- **start_date_time** (string) – start date and time, in ISO format

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [400 Bad Request](https://www.rfc-editor.org/rfc/rfc9110.html#name-400-bad-request)
  - `Missing mandatory value "${value}".`
  - `Wrong value ${value}.`
  - `Unknown activity name "${activity_name}".`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `Session not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## DELETE /api/v1/sessions/session/`<session_id as string>`

Delete a specific session of the authenticated user.

**Example request:**

```
DELETE /api/v1/sessions/sesssion/2c9a382b-dce2-49d1-a63a-fc4e7b163730 HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

OK

```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**REQUEST JSON OBJECT:**

- **session_id** (string) - session id

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `Session not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## DELETE /api/v1/sessions/session/comment/`<comment_id as string>`/

Delete a comment written by the authenticated user.

**Example request:**

```
DELETE /api/v1/sessions/comment/85abc1b4-3300-43e1-bec3-c63c2385da94 HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

OK

```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**REQUEST JSON OBJECT:**

- **comment_id** (string) - comment id

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `Comment not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## GET /api/v1/sessions/activities

Get the list of supported activities, along with their icons.
