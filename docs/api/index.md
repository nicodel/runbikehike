# API documentation

1. [Authentication](./auth.md)
1. [Profiles](./profiles.md)
1. [Sessions](./sessions.md)
1. [Images](./images.md)
1. [Records](./records.md)
1. [Timeline](./timeline.md)
1. [Admin](./admin.md)

```
GET /api/auth/profile : Get authenticated user info (profile, account, preferences).
POST /api/auth/profile/edit : Edit authenticated user profile.
POST /api/auth/profile/edit/preferences : Edit authenticated user preferences.
POST /api/auth/profile/edit/sports : Edit authenticated user sport preferences.
DELETE /api/auth/profile/reset/sports/(sport_id) : Reset authenticated user preferences for a given sport.
PATCH /api/auth/profile/edit/account : Update authenticated user email and password.

POST /api/auth/picture : Update authenticated user picture.
DELETE /api/auth/picture : Delete authenticated user picture.


POST /api/auth/email/update : Update user email after confirmation.
POST /api/auth/logout : User logout. If a valid token is provided, it will be blacklisted.
POST /api/auth/account/privacy-policy : The authenticated user accepts the privacy policy.
GET /api/auth/account/export : Get a data export info for authenticated user if a request exists.
POST /api/auth/account/export/request : Request a data export for authenticated user.
GET /api/auth/account/export/(string: file_name) : Download a data export archive
```

# Template

## POST /api/v1/\*\*\*\*

**Example request:**

```
POST /api/v1/auth/signin HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  "auth_token": "JSON Web Token",
  TO BE COMPLETED
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission."
}
```

**REQUEST JSON OBJECT:**

- **data** (string) - data

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `Session not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`
