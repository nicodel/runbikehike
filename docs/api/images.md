# Images

- GET /api/v1/images/image

## GET /api/v1/images/image

Retreive an image based on the provided ID.

**Example request:**

```
GET /api/v1/images/image HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  "image": {
        "data": "/9j/4SLARXhpZgAATU0AKgAAAAgABgESAAMAAAABA ... IuX3joc9QRXkfjP/hA7vxFb6xBozwlSCXik8vLDnOBwK6LwhF4J1DVTqEWn3ckkQLzNJMWVc9OKsD//2Q==",
        "id": "205b1894-7978-4663-90d1-35007f8b07a9",
        "mimetype": "image/jpeg"
    }
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**REQUEST JSON OBJECT:**

- **image_id** (string) - the ID of the image to be retreived.

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `Image not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`
