# Timeline

- GET /api/v1/timeline/today
- GET /api/v1/timeline/7days

## GET /api/v1/timeline

Gather the authenticated user timeline items (sessions)

**Example request:**

```
GET /api/v1/timeline HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
    TO BE COMPLETED
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**REQUEST JSON OBJECT:**

- **period** (string) - the period for which timeline items should be returned. Acceptable periods are : "last 7 days", "last 14 days".

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
-
-
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`
