# Authentication

```
POST /api/auth/account/confirm : Activate user account after registration.
POST /api/auth/account/resend-confirmation : Resend email with instructions to confirm account.
```

- [Authentication](#authentication)
  - [POST /api/v1/auth/register](#post-apiv1authregister)
  - [POST /api/v1/auth/signin](#post-apiv1authsignin)
  - [POST /api/v1/auth/signout](#post-apiv1authsignout)

## POST /api/v1/auth/register

Register a new user.

**Example request:**

```
POST /api/v1/auth/register HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success:

```
HTTP/1.1 200 SUCCESS
Content-Type: application/json

{
  "OK"
}
```

- Error on registration:

```
HTTP/1.1 400 BAD REQUEST
Content-Type: application/json

{
  "message": "The email <email> is already used by another account.",
}
```

**REQUEST JSON OBJECT:**

- **email** (string) - user email
- **password** (string) - password (8 characters required

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [400 Bad Request](https://www.rfc-editor.org/rfc/rfc9110.html#name-400-bad-request)
  - `Email address not valid.`
  - `Text password is not at least 8 chars.`
  - `The email <email> is already used by another account.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

## POST /api/v1/auth/signin

User sign in. Only user with an active account can sign in.

**Example request:**

```
POST /api/v1/auth/signin HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  "message": "Authentication successful"
}
```

- Error on sign in:

```
HTTP/1.1 400 BAD REQUEST
Content-Type: application/json

{
  "message": "Incorrect email or password.",
}
```

**REQUEST JSON OBJECT:**

- **email** (string) - user email
- **password** (string) - password (8 characters required)

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-400-bad-request)
  - `Incorrect email or password`
  - `User disabled`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`

---

## POST /api/v1/auth/signout

User sign out.. Only user with an active account can sign out.

**Example request:**

```
POST /api/v1/auth/signout HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success

```
HTTP/1.1 200 OK
Content-Type: application/json

{
  "OK"
}
```

- Error on sign out:

```
HTTP/1.1 500 Internal Server Error
Content-Type: application/json

```

**REQUEST JSON OBJECT:**

- **email** (string) - user email
- **password** (string) - password (8 characters required)

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`
