# Social

## GET /api/v1/social/users

An authenticated user can search another users by providing at least 3 letters of her/his name.

**Example request:**

```
GET /api/v1/social/users?search=someletters HTTP/1.1
Content-Type: application/json
```

**Example responses:**

- Success:

```
HTTP/1.1 200 SUCCESS
Content-Type: application/json
{
  "users": [
    "id": "a9202021-d1fe-470c-b00f-ceb85084cbea",
    "name": "Forrest Gump",
    "avatar": {
      "mimetype": "image/jpeg"
      "data": "..."
    };
  ]
}
```

- Error:

```
HTTP/1.1 403 FORBIDDEN
Content-Type: application/json

{
  "message": "You do not have permission to perform such action."
}
```

**ANSWER STATUS CODES**

- [200 OK](https://www.rfc-editor.org/rfc/rfc9110.html#name-200-ok)
  - `OK`
- [401 Unauthorized](https://www.rfc-editor.org/rfc/rfc9110.html#name-401-unauthorized)
  - `Unauthorized`
- [403 Forbidden](https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden)
  - `You do not have permission to perform such action.`
- [404 Not Found](https://www.rfc-editor.org/rfc/rfc9110.html#name-404-not-found)
  - `User not found.`
- [500 Internal Server Error](https://www.rfc-editor.org/rfc/rfc9110.html#name-500-internal-server-error)
  - `An unexpected error occurred.`
